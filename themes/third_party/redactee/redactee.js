var redactee_toolbars = {
	setup: function($textarea, cid) {
		var opts = {};
		if (this.hasOwnProperty(cid)) {
			// must clone since redactor may play alter our data internally
			opts = $.extend(true, opts, this[cid]);
		}
		$textarea.redactor(opts);
	}
};
$(function(){
	$('.redactee-editor').each(function(){
		var $textarea = $(this);
		redactee_toolbars.setup($textarea, $textarea.attr('data-config'));
	});
	if (window.hasOwnProperty('Matrix')) {
		Matrix.bind('redactee', 'display', function(cell){
			$textarea = $('.redactee-editor', cell.dom.$td);
			redactee_toolbars.setup($textarea, $textarea.attr('data-config'));
			cell.dom.$td.addClass('redactee-matrix');
		});
	}
});
