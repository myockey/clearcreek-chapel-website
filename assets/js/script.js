// Main Menu reveal on phones
$(function () {
	$('.menu-link').click(function (e) {
		e.preventDefault();
		
		$('.main-nav').slideToggle('fast');
		return false;
	});
});


// audio.js
audiojs.events.ready(function() {
  audiojs.createAll();
});



// Google Maps
$(function() {
  var myLatlng = new google.maps.LatLng(39.583166,-84.240361);
  
  var mapOptions = {
    zoom: 13,
    center: myLatlng,
    disableDefaultUI: true,
    draggable: false,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  }
  var map = new google.maps.Map(document.getElementById("cc_map"), mapOptions);
  
  var marker = new google.maps.Marker({
      position: myLatlng,
      map: map,
      title:"Clearcreek Chapel"
  });
});