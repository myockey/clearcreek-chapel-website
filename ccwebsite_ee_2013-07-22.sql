# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 205.178.146.110 (MySQL 5.0.91)
# Database: ccwebsite_ee
# Generation Time: 2013-07-22 13:54:25 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table exp_accessories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_accessories`;

CREATE TABLE `exp_accessories` (
  `accessory_id` int(10) unsigned NOT NULL auto_increment,
  `class` varchar(75) NOT NULL default '',
  `member_groups` varchar(50) NOT NULL default 'all',
  `controllers` text,
  `accessory_version` varchar(12) NOT NULL,
  PRIMARY KEY  (`accessory_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_accessories` WRITE;
/*!40000 ALTER TABLE `exp_accessories` DISABLE KEYS */;

INSERT INTO `exp_accessories` (`accessory_id`, `class`, `member_groups`, `controllers`, `accessory_version`)
VALUES
	(1,'Expressionengine_info_acc','1|5','addons_extensions|members|design|tools_utilities|tools_communicate|addons_accessories|addons_modules|addons_fieldtypes|addons|admin_system|admin_content|tools_data|tools_logs|myaccount|content_publish|homepage|tools|addons_plugins|content_edit|content|content_files_modal|content_files','1.0');

/*!40000 ALTER TABLE `exp_accessories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_actions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_actions`;

CREATE TABLE `exp_actions` (
  `action_id` int(4) unsigned NOT NULL auto_increment,
  `class` varchar(50) NOT NULL,
  `method` varchar(50) NOT NULL,
  PRIMARY KEY  (`action_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_actions` WRITE;
/*!40000 ALTER TABLE `exp_actions` DISABLE KEYS */;

INSERT INTO `exp_actions` (`action_id`, `class`, `method`)
VALUES
	(5,'Safecracker','submit_entry'),
	(6,'Safecracker','combo_loader'),
	(7,'Search','do_search'),
	(8,'Channel','insert_new_entry'),
	(9,'Channel','filemanager_endpoint'),
	(10,'Channel','smiley_pop'),
	(11,'Rte','get_js'),
	(13,'Backup_proish','cron'),
	(14,'Redactee','imageGetJson'),
	(15,'Redactee','imageUpload'),
	(16,'Redactee','fileUpload'),
	(17,'Redactee','pagesGetJson');

/*!40000 ALTER TABLE `exp_actions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_backup_proish_settings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_backup_proish_settings`;

CREATE TABLE `exp_backup_proish_settings` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `setting_key` varchar(30) NOT NULL default '',
  `setting_value` text NOT NULL,
  `serialized` int(1) default '0',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_captcha
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_captcha`;

CREATE TABLE `exp_captcha` (
  `captcha_id` bigint(13) unsigned NOT NULL auto_increment,
  `date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL default '0',
  `word` varchar(20) NOT NULL,
  PRIMARY KEY  (`captcha_id`),
  KEY `word` (`word`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_categories`;

CREATE TABLE `exp_categories` (
  `cat_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_id` int(6) unsigned NOT NULL,
  `parent_id` int(4) unsigned NOT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_url_title` varchar(75) NOT NULL,
  `cat_description` text,
  `cat_image` varchar(120) default NULL,
  `cat_order` int(4) unsigned NOT NULL,
  PRIMARY KEY  (`cat_id`),
  KEY `group_id` (`group_id`),
  KEY `cat_name` (`cat_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_categories` WRITE;
/*!40000 ALTER TABLE `exp_categories` DISABLE KEYS */;

INSERT INTO `exp_categories` (`cat_id`, `site_id`, `group_id`, `parent_id`, `cat_name`, `cat_url_title`, `cat_description`, `cat_image`, `cat_order`)
VALUES
	(1,1,1,0,'Elder','elder','','0',2),
	(2,1,1,0,'Office Staff','office-staff','','0',3),
	(3,1,1,0,'Custodial Staff','custodial-staff','','0',1);

/*!40000 ALTER TABLE `exp_categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_category_field_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_category_field_data`;

CREATE TABLE `exp_category_field_data` (
  `cat_id` int(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_id` int(4) unsigned NOT NULL,
  PRIMARY KEY  (`cat_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_category_field_data` WRITE;
/*!40000 ALTER TABLE `exp_category_field_data` DISABLE KEYS */;

INSERT INTO `exp_category_field_data` (`cat_id`, `site_id`, `group_id`)
VALUES
	(1,1,1),
	(2,1,1),
	(3,1,1);

/*!40000 ALTER TABLE `exp_category_field_data` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_category_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_category_fields`;

CREATE TABLE `exp_category_fields` (
  `field_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL default '',
  `field_label` varchar(50) NOT NULL default '',
  `field_type` varchar(12) NOT NULL default 'text',
  `field_list_items` text NOT NULL,
  `field_maxl` smallint(3) NOT NULL default '128',
  `field_ta_rows` tinyint(2) NOT NULL default '8',
  `field_default_fmt` varchar(40) NOT NULL default 'none',
  `field_show_fmt` char(1) NOT NULL default 'y',
  `field_text_direction` char(3) NOT NULL default 'ltr',
  `field_required` char(1) NOT NULL default 'n',
  `field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY  (`field_id`),
  KEY `site_id` (`site_id`),
  KEY `group_id` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_category_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_category_groups`;

CREATE TABLE `exp_category_groups` (
  `group_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_name` varchar(50) NOT NULL,
  `sort_order` char(1) NOT NULL default 'a',
  `exclude_group` tinyint(1) unsigned NOT NULL default '0',
  `field_html_formatting` char(4) NOT NULL default 'all',
  `can_edit_categories` text,
  `can_delete_categories` text,
  PRIMARY KEY  (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_category_groups` WRITE;
/*!40000 ALTER TABLE `exp_category_groups` DISABLE KEYS */;

INSERT INTO `exp_category_groups` (`group_id`, `site_id`, `group_name`, `sort_order`, `exclude_group`, `field_html_formatting`, `can_edit_categories`, `can_delete_categories`)
VALUES
	(1,1,'Leaders','a',0,'none','','');

/*!40000 ALTER TABLE `exp_category_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_category_posts
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_category_posts`;

CREATE TABLE `exp_category_posts` (
  `entry_id` int(10) unsigned NOT NULL,
  `cat_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`entry_id`,`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_category_posts` WRITE;
/*!40000 ALTER TABLE `exp_category_posts` DISABLE KEYS */;

INSERT INTO `exp_category_posts` (`entry_id`, `cat_id`)
VALUES
	(33,1),
	(34,1),
	(35,1),
	(36,1),
	(37,1),
	(38,1),
	(39,1),
	(41,2),
	(42,2),
	(43,2),
	(44,1),
	(45,3),
	(46,3),
	(47,2),
	(48,1),
	(49,1),
	(50,2),
	(56,1);

/*!40000 ALTER TABLE `exp_category_posts` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_channel_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_channel_data`;

CREATE TABLE `exp_channel_data` (
  `entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL default '1',
  `channel_id` int(4) unsigned NOT NULL,
  `field_id_3` text,
  `field_ft_3` tinytext,
  `field_id_4` text,
  `field_ft_4` tinytext,
  `field_id_5` text,
  `field_ft_5` tinytext,
  `field_id_6` text,
  `field_ft_6` tinytext,
  `field_id_7` text,
  `field_ft_7` tinytext,
  `field_id_8` text,
  `field_ft_8` tinytext,
  `field_id_9` text,
  `field_ft_9` tinytext,
  `field_id_10` text,
  `field_ft_10` tinytext,
  `field_id_11` text,
  `field_ft_11` tinytext,
  `field_id_12` text,
  `field_ft_12` tinytext,
  `field_id_13` text,
  `field_ft_13` tinytext,
  `field_id_14` text,
  `field_ft_14` tinytext,
  `field_id_15` text,
  `field_ft_15` tinytext,
  `field_id_16` text,
  `field_ft_16` tinytext,
  `field_id_17` text,
  `field_ft_17` tinytext,
  `field_id_18` text,
  `field_ft_18` tinytext,
  `field_id_19` text,
  `field_ft_19` tinytext,
  `field_id_20` mediumtext,
  `field_ft_20` tinytext,
  `field_id_21` text,
  `field_ft_21` tinytext,
  `field_id_22` text,
  `field_ft_22` tinytext,
  `field_id_23` text,
  `field_ft_23` tinytext,
  `field_id_24` text,
  `field_ft_24` tinytext,
  `field_id_25` text,
  `field_ft_25` tinytext,
  `field_id_26` text,
  `field_ft_26` tinytext,
  `field_id_27` text,
  `field_ft_27` tinytext,
  `field_id_28` text,
  `field_ft_28` tinytext,
  `field_id_29` text,
  `field_ft_29` tinytext,
  `field_id_30` text,
  `field_ft_30` tinytext,
  `field_id_31` text,
  `field_ft_31` tinytext,
  `field_id_32` text,
  `field_ft_32` tinytext,
  `field_id_33` text,
  `field_ft_33` tinytext,
  `field_id_34` text,
  `field_ft_34` tinytext,
  `field_id_35` text,
  `field_ft_35` tinytext,
  `field_id_36` text,
  `field_ft_36` tinytext,
  PRIMARY KEY  (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_channel_data` WRITE;
/*!40000 ALTER TABLE `exp_channel_data` DISABLE KEYS */;

INSERT INTO `exp_channel_data` (`entry_id`, `site_id`, `channel_id`, `field_id_3`, `field_ft_3`, `field_id_4`, `field_ft_4`, `field_id_5`, `field_ft_5`, `field_id_6`, `field_ft_6`, `field_id_7`, `field_ft_7`, `field_id_8`, `field_ft_8`, `field_id_9`, `field_ft_9`, `field_id_10`, `field_ft_10`, `field_id_11`, `field_ft_11`, `field_id_12`, `field_ft_12`, `field_id_13`, `field_ft_13`, `field_id_14`, `field_ft_14`, `field_id_15`, `field_ft_15`, `field_id_16`, `field_ft_16`, `field_id_17`, `field_ft_17`, `field_id_18`, `field_ft_18`, `field_id_19`, `field_ft_19`, `field_id_20`, `field_ft_20`, `field_id_21`, `field_ft_21`, `field_id_22`, `field_ft_22`, `field_id_23`, `field_ft_23`, `field_id_24`, `field_ft_24`, `field_id_25`, `field_ft_25`, `field_id_26`, `field_ft_26`, `field_id_27`, `field_ft_27`, `field_id_28`, `field_ft_28`, `field_id_29`, `field_ft_29`, `field_id_30`, `field_ft_30`, `field_id_31`, `field_ft_31`, `field_id_32`, `field_ft_32`, `field_id_33`, `field_ft_33`, `field_id_34`, `field_ft_34`, `field_id_35`, `field_ft_35`, `field_id_36`, `field_ft_36`)
VALUES
	(1,1,1,'Flocks','none','Flocks are the basic, one-anothering unit of ministry at the Chapel.','none','Learn More »','none','/flocks','none','Sermons','none','Sundays sermons are published every Tuesday.','none','Download Sermons »','none','/sermons','none','News & Events','none','The Clearcreek Chapel calendar includes all official Chapel events.','none','See What\'s Going On »','none','/news-and-events','none','','none','','none','','none','','none','','xhtml','','xhtml','','xhtml','','none','welcome','none','<p>Clearcreek Chapel is a commissional community of believers and followers of the Lord Jesus Christ, gathered to make disciples in our neighborhoods and in the nations. By God\'s grace and through His Word, it is our vision to cultivate in the hearts of God\'s people a passion for the supremacy of God magnified in love for Him, manifested in love for one another, and multiplied by love for the lost.</p>','xhtml','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(2,1,7,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}banner-about-2x.jpg','none','about us','none','<p>Clearcreek Chapel is a commissional community of believers and followers of the Lord Jesus Christ. We are located in Springboro, Ohio. We are gathered to make disciples in our neighborhoods and in the nations. We strive to magnify the worth of God in every thing that we do. We value going hard after God as an all-satisfying goal in Himself. We seek to be shaped by the Scripture as the fully sufficient and trustworthy revelation of God.</p><p></p><p><a href=\"/ministries/expansion\">News and updates about Chapel Expansion</a>\n</p>','xhtml','','','','','','','','none','','xhtml','','none','','none','Teaching about the Lord Jesus Christ with all boldness','none','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(3,1,9,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}banner-home-savoring-2x.jpg','none','Savoring The Supremacy of God','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(4,1,9,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}banner-home-shaping-2x.jpg','none','Shaping the People of God','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(5,1,9,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}banner-home-spreading-2x.jpg','none','Spreading the Gospel of God','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(6,1,9,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}banner-home-studying-2x.jpg','none','Studying the Word of God','none','','none','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(8,1,7,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}banner-sermons.jpg','none','sermons','none','<h2>The Chapel Pulpit...</h2><p>will systematically declare the whole counsel of God through the Bible saturated, Christ centered, Spirit enabled, Scripturally expositional preaching by godly men aimed at bringing people to maturity in Christ.</p>','xhtml','','','','','','','','','','','','','','','Him we will proclaim warning everyone and teaching everyone with all wisdom','none','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(9,1,7,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}banner-flocks-2x.jpg','none','flocks','none','<p>Flocks are the basic, one-anothering unit of ministry at the Chapel. Flocks are defined by neighborhoods and are overseen by one or more elders. Generally, fellowship and friendships are cultivated at flock meetings .There is time in the weekly meeting for fellowship, often over a meal, prayer and interaction over the Sunday sermons. Flocks meet on Wednesday Evenings at 6:00 PM.</p>','xhtml','','','','','','','','','','','','','','','Therefore take heed to yourselves and to all the flock','none','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(10,1,7,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}banner-ministries-2x.jpg','none','ministries','none','<p>Ministries of Clearcreek Chapel are those works of service done by believers, aimed at building up the body of Christ. In love we strive to attain a unity in doctrine, a personal knowledge of Jesus Christ and a maturity measuring up to the standard of the fullness of Christ. Believers are equipped for these works through the teaching of the Word by a team of godly Elders who shepherd (pastor) the flock. </p>','xhtml','','','','','','','','','','','','','','','Equip the saints for the work of ministry','none','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(11,1,2,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<h1>Church</h1>\n<ul>\n<li><a href=\"http://www.9marks.org/\" target=\"_blank\">9Marks</a></li>\n<li><a href=\"http://firefellowship.org/\" target=\"_blank\">Fellowship of Independent Reformed Evangelicals</a></li>\n<li><a href=\"http://www.founders.org/\" target=\"_blank\">Founders Movement</a></li>\n<li>Grace Partners (No website)</li></ul>\n<h1>Ministries</h1>\n<ul>\n<li><a href=\"http://www.nanc.org/\" target=\"_blank\">National Association of Nouthetic Counselors</a></li>\n<li><a href=\"http://www.ccef.org/\" target=\"_blank\">Christian Counseling Education Foundation</a></li>\n<li><a href=\"http://www.desiringgod.org/\" target=\"_blank\">Desiring God Ministries</a></li></ul>\n<h1>Resources</h1>\n<ul>\n<li><a href=\"http://www.sermonaudio.com/theChapel\" target=\"_blank\">Sermon Audio</a></li>\n<li><a href=\"http://www.wtsbooks.com/\" target=\"_blank\">Westminster Bookstore</a></li></ul>\n<p></p>\n','xhtml','<p><img src=\"{filedir_1}about-network-association.jpg\" style=\"\"></p><blockquote>because of your partnership in the gospel from the first day until now.</blockquote><blockquote>— Philippians 1:5</blockquote>\n','xhtml','{filedir_1}subnav-about-network-2x.png','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(12,1,2,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<p>Clearcreek Chapel is led by a team of Biblically qualified men who serve together as the Elders. We also are served administratively by a team of diaconally qualified people who are on staff. Working together with ministry leaders, the Chapel seeks to glorify God through loving, Spirit enabled, sacrificial ministry.</p>\n','xhtml','Pay careful attention to yourselves and to all the flock, in which the Holy Spirit has made you overseers, to care for the church of God, which he obtained with his own blood. (Acts 20:28, ESV)','xhtml','{filedir_1}subnav-about-leaders-icon-2x.jpg','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(33,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}elder-russ-k.jpg','none','Pastor for Preaching and Counseling','none','<P>Pastor Russ has been preaching and teaching since 1975. He has served churches in Ohio, Illinois and West Germany. He has served on staff&nbsp;at Clearcreek Chapel as Pastor for Preaching since 1998. He has a teaching and training ministry in Eastern Europe and the Far East. </P>','none','<p>Russ was born and reared in the Belgian Congo (Zaire). He received his B.A. from Tennessee Temple University in Chattanooga, Tennessee and his MA in Theology from International Seminary. He is a certified Biblical Counselor through the National Association of Nouthetic Counselors. </p>\n\n<p>He has ministered in Christian schools and churches in Ohio and Illinois. He founded and served as teaching pastor in an English speaking church in West Germany. He is the Executive Director of Chapel Ministries International and owner of the\nExcalibix Group, an international software, quality assurance and business consulting firm.</p>\nRuss is married to\nEsther Kennedy and they have three grown children, Cheri, Nathan and Hilary.\n\n','none','Oversees the Pulpit, Counseling, Facility, Media and IT ministries','none','Centerville','none','Kennedy','none','','none','','none'),
	(13,1,2,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<h1>Prologue</h1>\n<p>To display the supreme worth of His glory, God has chosen to save a people and call them out of the world so that they might assemble themselves together in local congregations or churches. Throughout history, these local, gathered communities of the Lord have drawn up confessions of faith summarizing what they believed the Bible to be teaching. They have also drawn up By-laws as a means to function corporately.</p>\n\n<p>Therefore, this gathered church has drawn up this Book of Faith and Order as our Confession, Covenant, and Constitution, organizing ourselves under the name, <i>Clearcreek Chapel</i>. We also will refer to ourselves as <i>The Chapel</i>.</p>\n\n<p>1. The Truths We Treasure</p>\n\n<p>The Elders have developed <i>The Truths We Treasure </i>as the basic statement of doctrine that each member of the church must believe and affirm. It is worded simply so as to be accessible to all, including new Christians, and can be affirmed by all with a clear conscience.</p>\n\n<p>2. The Truths We Teach</p>\n\n<p>The Elders have developed <i>The Truths We Teach</i> as a more detailed, confessional statement of doctrine. Affirming and submitting to these truths is required of all teachers, elders, and deacons/deaconesses of the church. This document is not a part of the <i>Book of Faith and Order</i>, but is under regular review and development by the Elders who update it as needed.</p>\n\n<p>3. The Constitution that Governs Us</p>\n\n<p>The Elders have developed<i> The Principles that Govern Us</i> as our governing principles. These are necessary as we work out how we are to function as a church and community. We have chosen, rather than writing extensive, detailed legislation, to articulate the principles on which our order is based, to write what few applications are necessary to establish order and guide governance, and to maintain what policies are necessary for day-to-day governance. In order to achieve this, the Elders will maintain a <i>Chapel Ministry Handbook</i> that is available to the congregation.</p>\n\n<h1>Section 1: Faith: The Truths We Treasure</h1>\n\n<p>We believe that the Scriptures of the Old and New Testament are verbally inspired of God, inerrant and infallible in the original writings (2Timothy 3:16; 1Peter 1:20-21; 1Corinthians 2:1-16), and are our supreme and final authority in all matters of life and faith (2 Peter 1:1-11). In <i>The Truths We Treasure </i>and <i>The</i> <i>Truths We Teach</i> we summarize what we believe the Bible teaches. However, the Scriptures themselves are the final rule of faith and order for this congregation and the ultimate authority to which we will appeal (2 Timothy 3:15-17; 2 Peter 1:19-21; Romans 15:4; 1 Thessalonians 2:13; 1 Corinthians 2:13).</p>\n\n<p>The following statements of truth must be affirmed by anyone desiring to become a member of the church.</p>\n\n<p>1.God.</p>\n\n<p>We believe in the one true and living God, eternally existing in three Persons: the Father, the Son and the Holy Spirit. Our God exists to glorify and enjoy Himself forever. He is infinite in being and perfection, a spirit invisible, personal, omnipresent, eternal, dependent on none, unchanging, truthful, trustworthy, almighty, sovereign, omniscient, righteous, holy, good, loving, merciful, long-suffering, and gracious. (Exodus 3:13-14; 34:4-7; Deuteronomy 6:1-9; 1 Kings 8:27; Nehemiah 9:32-33; Job 22:2-3; Psalm 5:4-8; 90:1-2; 95:1-7; 115:1-7; 119:65-68; 145:17-20; Proverbs 16:4; Isaiah 6:1-3; 40:10-31; 46:9-13; Jeremiah 10:10; 23:23-24; Daniel 4:34-35; Malachi 3:6; Matthew 28:16-20; John 1:1-18; 4:21-24; 14:1-11; 15:26-27; Acts 7:2-50; 20:28; Romans 11:33-36; 1 Corinthians 8:5-6; 2Corinthians 13:14; Galatians 4:4-7; Hebrews 4:12-13; 11:6; Revelation 4:8; 5:11-14)</p>\n\n<p>2.Holy Scriptures</p>\n\n<p>We believe that Almighty God has revealed all that is necessary for life and faith in the sixty-six books of the Holy Scripture, which are the Word of God. All Scripture was given by inspiration by God; that is, it is God-breathed as holy men of old wrote, being borne along by the Holy Spirit. The Scriptures are infallible and inerrant in the original writings and are the final arbiter in all disputes. Its authority is derived from its Author and not from the opinions of men. (Matthew 5:18; 24:35; John 10:35;16:12 13; 17:17; 1 Corinthians 2:7-14; 1 Thessalonians 2:13; 2 Timothy 3: 15-17; Hebrews 4:12; 2 Peter 1:20-21; Revelation 1:1-3; 22:6,18-19)</p>\n\n<p>3.Creation and Federal Headship</p>\n\n<p>We believe that God created the universe from nothing and all that is in it in six literal days, declaring it all very good. We further believe that God made Adam and Eve in His image  perfect, holy, and upright  to exercise dominion over all creation. Adam was appointed representative and head of the human race so that his obedience or disobedience to Gods commands were imputed and transmitted to all his offspring. (Genesis 1:1-2:2; Psalm 19:1-6; 90:1-2; Isaiah 40:25-26; John 1:1-3; Romans 1:18-20; 2:14-15; Colossians 1:15-16; Hebrews 1:1-2)</p>\n\n<p>4.Fall and Creation</p>\n\n<p>We believe that Adam fell from his original righteousness into sin and brought upon himself and all his offspring physical and spiritual death, depravity, condemnation, and the state of being sinners. (Genesis 2:15-17; 3:1-24; 6:5-7; Psalm 14:1-3; 51:1-5; Jeremiah 17:9-10; Romans 3:9-20; 5:12-21; 6:15-23; 1 Corinthians 15:20-49; Titus 1:15-16; Ephesians 2:1-3; Colossians 1:21-23; Hebrews 2:14-18)</p>\n\n<p>5.Depravity and Sinfulness</p>\n\n<p>We believe that it is utterly beyond the power of fallen man to love God, to keep His laws, to understand the Gospel, to repent of sin, or to trust in Christ. (Jeremiah 17:9; John 3:3-5; 8:43 Romans 3:9-19; Colossians 1:21-23; Ephesians 2:1-3; Titus 1:15-16)</p>\n\n<p>6.Election</p>\n\n<p>We believe that God, before the foundation of the world, decreed all that comes to pass, but in no way being the author of sin and for His own glory, chose a host of men and women to eternal life as an act of free and sovereign grace. This choice was in no way dependent upon Gods foresight of human faith, decision, works, or merit. (Numbers 23:19; Isaiah 46:9-10; Matthew 25:34; Luke 10:17-20; John 6:35-40; 10:22-30; 17:1-26; 19:8-11; Acts 4:27-28; 27:1-44; Romans 9:14-29; 11:1-6; 33-36; Ephesians 1:1-6; 1:3-14; 1 Thessalonians 1:4-6; 2 Thessalonians 2:13-15; 1Timothy 5:21; Hebrews 6:16-17; James 1:13-15; 1 Peter 1:1-9; 2 Peter 1:10-11; Jude 3-4)</p>\n\n<p>7.Incarnation</p>\n\n<p>We believe that God sent His Son, Jesus Christ into the world, conceived of the virgin Mary by the Holy Spirit, unchangeably sinless, both God and man, born under the Law, to live a perfect life of righteousness on behalf of His people. (Psalm 2:1-12; Isaiah 7:14; 9:6-8; 42:1-4; 52:13-53:12; Matthew 1:18-25; Luke 1:26-33; 1:26-56; John 1:1-18; 17:1-12; Acts 3:22-23; 17:29-31; Romans 3:21-26; 8:1-4, 28-30; Galatians 4:4; Ephesians 1:15-23; 1Timothy 2:1-6; Hebrews 1:1-4; 2:14-18; 9:11-14; 1 Peter 1:17-20)</p>\n\n<p>8.Redemption</p>\n\n<p>We believe that Gods Son died on the cross to effect propitiation, reconciliation, redemption, and atonement for His people. God bore testimony to His acceptance of His Sons completed redemptive work by raising Him from the dead. (Isaiah 53:4-5,10-12; Matthew 26:36-38; 27:45-46; John 10:14-18; 20:24-31; Romans 1:4)</p>\n\n<p>9. Ascension and Heavenly Ministry</p>\n\n<p>We believe that Gods Son ascended to the right hand of His Father and is enthroned in glory, where He intercedes on behalf of His people and rules over all things as sovereign Lord for the praise of His glory. (Acts 1:1-11; 10:39-43; Romans 8:31-39; 1 Corinthians 15:1-8; 2 Corinthians 5:21; Philippians 2:5-11; Hebrews 9:23-28; 10:5-10; 1 Peter 3:18; 2 Peter 3:1-18)</p>\n\n<p>10.Regeneration</p>\n\n<p>We believe that God the Father has poured out the Holy Spirit to work alongside the preached Word. The Spirit of God regenerates the elect sinners, drawing them irresistibly to Himself by giving the gifts of faith in Christ the Savior and repentance towards God. (John 1:12-13; 3:5-8; 10:25-30; Ephesians 2:4-10; James 1:18; 2 Timothy 2:9)</p>\n\n<p>11.Justification</p>\n\n<p>We believe that God calls to Himself the elect in all ages. He justifies them solely on account of the imputed righteousness of Jesus Christ, which they receive through faith alone. This faith must always be accompanied by works, though these works do not merit salvation in any way. (Romans 3:24; 8:30; 4:5-8; 5:17-19 10:9-13; 1 Corinthians 1:30-31; 2 Corinthians 5:21; Ephesians 1:7; 2:8-10; Philippians 3:8-9)</p>\n\n<p>12.Salvation, Sanctification and Preservation</p>\n\n<p>We believe that those people whom God calls, regenerates, and justifies will believe the gospel, repent of their sins, and acknowledge Jesus Christ as Sovereign Lord. His grace causes them to persevere in holiness by growing in sanctification and the putting off of sin by the Holy Spirit and never finally falling away. (John 10:28-29; Romans 6:14; 7:18,23; 8:13, 28-30; 2 Corinthians 3:18; 7:1; Galatians 5:17; Philippians 1:6; 2 Timothy 2:19; Hebrews 6:17-18; 1 Peter 1:3-9; 22-23; 2 Peter 3:18)</p>\n\n<p>13.Adoption</p>\n\n<p>We believe that all those who have been redeemed receive the adoption as sons and thus all the privileges of a parent and child relationship with God, including being delivered from the slavery of the Old Covenant, having the inner witness of the Spirit who enables them to recognize God as their Father, receiving the glorified body, and being designated to jointly inherit all that has been promised to Gods Son. (Psalm 103:13; Proverbs 14:26; Isaiah 54:8-9; John 1:10-13; Romans 8:14-25; 2 Corinthians 6:18; Galatians 4:1-7; Ephesians 1:5; 4:30; Hebrews 12:4-6; Revelation 3:11-13)</p>\n\n<p>14.Ordinances</p>\n\n<p>We believe that baptism by immersion and the Lords Supper are gospel ordinances belonging only to regenerated believers. The ordinances have no saving merit for they are emblems and types of spiritual realities, not the realities themselves. Baptism is a responsibility of each believer to declare ones allegiance to Christ. (Matthew 3:16-17; 28:18-20; Acts 2:14-41; 8:12-40; 16:25-40; 18:7-8; Romans 6:1-4; 1Corinthians 10:14-17; 11:26) The Lords Table is an expression of our communion with Christ and with one another. (Matthew 26:17-30; 1 Corinthians 10:14-21; 11:17-34; Hebrews 9:11-28)</p>\n\n<p>15.Church</p>\n\n<p>We believe that the local church is under the authority of Christ alone who rules through His Word and is governed by a plurality of men called elders who shepherd the church. The communion of saints, however, requires recognition of and fellowship with other churches. (Matthew 20:24-28; Acts 20: 17,28; Ephesians 4:12-14; 1 Peter 5:1-4)</p>\n\n<p>16.Eschatology</p>\n\n<p>We believe that the Lord Jesus Christ shall come again to raise the dead, both the righteous and the unrighteous. The righteous shall enjoy everlasting life with God, and the wicked shall endure everlasting, conscious punishment away from the presence of Gods glory. (Genesis 3:17-19; Job 19:25-27; Ecclesiastes 12:1-8; Isaiah 26:19; Daniel 12:1-4; Matthew 25:31-46; John 5:28-29; Luke 16:19-31; 23:32-43; Acts 24:14-16; Romans 9:19-26; 1 Corinthians 15:35-56; 2 Corinthians 5:1-8; Philippians 1:21-26; 1 Thessalonians 4:13-18; 2Thessalonians 1:5-10; Hebrews 12:22-24; Jude 3-7; Revelation 22:12-21)</p>\n\n<h1>Section 2: Order: The Principles that Govern Us</h1>\n\n<p>The members of Clearcreek Chapel constitute, establish, and willingly submit themselves to the following articles. This Ohio nonprofit religious organization has its principal offices in the proximity of Springboro, Ohio.</p>\n\n<p>Article 1.  Our Shared Mission</p>\n\n<p>By Gods grace and through His Word, we will cultivate in the hearts of Gods people a passion for the supremacy of God <i>magnified </i>in love for Him and <i>manifested </i>in love for one another.</p>\n\n<p>Article 2.  Our Threefold Purposes</p>\n\n<p>The church exists to glorify God (Ephesians 3:21). The universal church is the redeemed people of God placed into the Body of Christ. The local church is the people of God gathered together in localities: to carry out the Great Commission (Matthew 28:18-20) as the Kingdom of God in this world (Colossians 1:13-14), to meet regularly together in an outward worship that expresses inward and spiritual truths (John 4:23-24; Hebrews 10:19-25; 12:18-29), and to encourage, edify, and build up one another. Therefore, we exist as a church for the following primary purposes:</p>\n\n<p>1.  Exaltation: Our Doxological Purpose</p>\n\n<p>Exaltation is all that we do in terms of pursuing God in worship. This is the overarching purpose of the church. The church exists to glorify God and to enjoy Him forever.</p>\n\n<p>2.  Edification: Our Discipleship Purpose</p>\n\n<p>Edification is all that we do to know God better, to please Him in every facet of life, to learn, to believe, to understand and to obey the Scriptures fully, and to love God and one another in each sphere of life as we grow in Christ-likeness.</p>\n\n<p>3.  Evangelism: Our Declarative Purpose</p>\n\n<p>Evangelism is all that we do to proclaim the gospel to every people group in order to see all of Gods elect brought into His Kingdom.</p>\n\n<p>Article 3.  Our Shaping Vision</p>\n\n<p>1.  Savoring the Supremacy of God</p>\n\n<p>Our vision is to magnify God as the all-satisfying object of the believers life in worship and service, here and forever.</p>\n\n<p>2.  Studying the Word of God</p>\n\n<p>Our vision is to work out both the large picture and the immediate texts in Holy Spirit enabled exposition and application in the public and private ministry of the Word of God.</p>\n\n<p>3.  Shaping the People of God</p>\n\n<p>Our vision is to see minds, affections, and wills transformed by the Word of God so that God is magnified by an obeying faith in every sphere of life.</p>\n\n<p>4.  Spreading the Gospel of God</p>\n\n<p>Our vision is to proclaim the gospel to all the people groups so that God is glorified in every tribe, language, and people group.</p>\n\n<p>Article 4.  Our Church Government</p>\n\n<p>1.  The Lord Jesus Christ -- The Head and Chief Shepherd of the Church</p>\n\n<p>We joyfully submit to Christ alone as the Head of His body, the church. We acknowledge Him as our Chief Shepherd. We recognize that He governs His church through the Holy Scriptures as the full and final authority in all matters of faith, church order, and discipline. While we may also seek the assistance and counsel of other churches when special concerns arise; we are neither accountable to, nor under the jurisdiction of, nor under the direct supervision of, any other ecclesiastical body. This congregation may never encourage, support, fellowship, or cooperate with any church or group which permits the presence of apostasy or undermines fundamental tenets of the Gospel by what it teaches or denies (Acts 15:1-35; Ephesians1:22-23; Colossians 1:18; 2Timothy 3:12-17; 2Peter 1:19-21).</p>\n\n<p>2.  The Elders -- The Servant-Leaders in the Church</p>\n\n<p>A plurality of elders shall lead this local church according to the Scriptures. These men shall at all times and in all activities stand under the authority of Christ and His Word. The Elders shall collectively and individually oversee, provide for, and encourage the spiritual life, welfare and total ministry of the congregation by equipping the saints for service so that they build up the body of Christ. This will result in believers who are mature, Christ-like, stable in doctrine, and not susceptible to false doctrine. The Elders, as a body, shall exercise authoritative and decisive leadership, but they must do so as servant-leaders and faithful stewards in the community, not as lords and dictators (Matthew 20:24-28; Acts 20: 17,28; Ephesians 4:12-14; 1 Peter 5:1-4).</p>\n\n<p>3.  The Deacons/Deaconesses  The Servant-Ministers in the Church</p>\n\n<p>The church itself is the ministering community (Ephesians 4:12-16). The church identifies the men and women who will be in charge of the day-to-day ministry and work of the church. They are appointed by the Elders to lead or serve in ministry, serving as Deacons/Deaconesses (Acts 6:1-6) so as to free the Elders for their ministry of the Word.</p>\n\n<p>Article 5.  Articles of Faith</p>\n\n<p>1.  Scripture Alone as the Final Authority</p>\n\n<p>The Scripture alone is the final and ultimate authority in all matters related to the Christian life. Since Gods Word remains the final authority for this community, statements of doctrine contained herein do not bind this congregation in a strict and absolute sense but are confessed together to assist its leaders and members in the event that controversy should arise.</p>\n\n<p>2.  Truths Confessed by the Membership</p>\n\n<p>All members of the Chapel shall confess their submission to the central truths of Biblical Christianity as summarized in Section 2 <i>Faith: The Truths We Treasure</i>. All members shall demonstrate both a willingness to abide by this <i>Book of Faith and Order</i> and a sincere desire to grow in their understanding of the Scriptures and the truths we teach.</p>\n\n<p>3.  Truths Confessed by the Ministry Leaders</p>\n\n<p>All teachers, all members of our deaconate, and all missionaries commissioned by and sent out from the Clearcreek Chapel will confess their essential agreement with <i>The Truths We Teach</i> and teach in full harmony with it.</p>\n\n<p>4.  Truths Taught by the Elders</p>\n\n<p>All elders of the Chapel will wholeheartedly confess their essential agreement with and teach in full harmony with <i>The</i> <i>Truths We Teach.</i> This statement has been adopted by the Elders as a brief systematic theology, helping them to develop and communicate a Biblical worldview that grounds the members of the church in the faith so that they will mature spiritually and become better equipped to glorify and serve God (1 Peter 3:15).</p>\n\n<p>Article 6.  Membership</p>\n\n<p>1.  Definition and Function</p>\n\n<p>All those who have been born of the Holy Spirit and have put their faith in Jesus Christ are immediately placed into the universal church, the body of Christ, of which He is the head (1Corinthians 12:12-20; Ephesians 1:22). All believers in Christ should join with a local church, thus becoming members of a clearly defined local community of believers (Acts 2:41-47; 14:21-23; Hebrews 10:25).</p>\n\n<p>2.  Chapel Covenant</p>\n\n<p>Recognizing our responsibility to obey all the Scriptures and the need to distinguish ourselves from the world as a community of believers, all members shall affirm their commitment to please God in all areas of life by entering into this covenant:</p>\n\n<p>Humbly depending on the Holy Spirit\'s enabling and aiding us, and affirming <i>The Truths We Treasure</i>, we Covenant to Glorify God by striving:</p>\n\n<ul><li>To walk in obedience to the Scriptures by loving the Lord God with all our heart, all our souls, and all our minds;</li><li>To walk in harmony with our fellow Christians by loving them as we love ourselves;</li><li>To be faithful in our witnessing, to uphold our testimony, to defend the doctrines of the Word of God, and to expand the Kingdom of God;</li><li>To be faithful in edifying, exhorting, rebuking, discipling, encouraging, praying for, and meeting the needs of the Body of Christ;</li><li>To exercise our spiritual gifts to build up and to serve one another;</li><li>To be submissive to one another in Christian love;</li><li>To regularly attend the services of the church and not forsake the assembling of ourselves together;</li><li>To be submissive to the God-ordained elders as to those who give an account for our souls;</li><li>To give heed to the ministry of the Word;</li><li>To attend the ordinances of the church faithfully, approaching them in a serious, spiritual, and holy attitude;</li><li>To honor the Lord in our finances in all things including regular, proportionate giving to the church;</li><li>To be consistent in our own study of the Word;</li><li>To love our wives as Christ loved the church or to submit to our husbands and to teach and train our children in the nurture and admonition of the Lord;</li><li>To extend the Lordship of Christ into all areas of our lives;</li><li>To abstain from practices harmful to our physical bodies and injurious to our testimony;</li><li>To purpose that if we relocate we will, as soon as possible, unite with another church of like faith, where we can carry out the spirit of this Covenant and the principles of God\'s Word.(Proverbs 13:24; 23:13; 29:15; Malachi 3:8-11; Acts 2:42, 47; Romans 8:3-4; 1 Corinthians 15; 16:2; 2 Corinthians 5:11-21; 12:13; Ephesians 4:11-14; 5:23-24; 6:1-4; Philippians 1:3-6; Colossians 4:2-4; 2 Timothy 3:16-4:4; Hebrews 10:24, 25; 13:17; James 2:12; 5:13-14; 1 Peter 2:5,9; 3:7; 1 John 2:19).</li></ul>\n\n<p> \n\n</p><p>3.  Requirements for Membership</p>\n\n<p>Any person who desires to unite in membership with the Chapel must profess repentance toward God and faith in Jesus Christ as Savior, submit to Him as Lord and Sovereign, be Biblically baptized following this profession, and must not be under Biblically administered church discipline. Having met these requirements, this person shall joyfully enter into this Covenant with this people, expressing willingness to follow the beliefs and practices of this community, and evidencing willingness to submit to its Elders.</p>\n\n<p>4.  Admission to Membership</p>\n\n<p>The Elders shall be responsible to receive applicants into membership. This shall include reviewing the application, conducting an interview, and evaluating their standing when coming from another church. Upon determining that the applicants meet the requirements, the Elders shall present them to the church as members.</p>\n\n<p>5.  Categories of Membership</p>\n\n<p>Resident Membership is for those active, participating members who comprise the majority of the Chapel. Associate Membership, having all the privileges and responsibilities of membership except that of voting, may be extended by the Elders to those who will be absent for an extended period of time, or who are at the Chapel for a short period of time and wish to minister while maintaining membership in their home churches.</p>\n\n<p>6.  Congregational Voting Privilege</p>\n\n<p>To be eligible to vote at congregational meetings, one must be a resident member on the day of the vote, at least sixteen (16) years of age, in attendance at the meeting and not have forfeited their voting privilege by being placed on the inactive list or being subject to discipline. The Chapel may permit absentee ballots in exceptional circumstances as requested of and granted by the Elders on a case-by-case basis.</p>\n\n<p>7.  Removal from Membership</p>\n\n<p>Membership will end by physical death, transfer of membership to churches holding to Biblical doctrine, or by the process of corrective discipline ending in excommunication. Members, who, for three months are absent willingly from the meetings or the ministries of the Chapel, shall be placed on an inactive list and shall be subject to removal.</p>\n\n<p>8.  Corrective Discipline</p>\n\n<p>The Scriptures require us to implement the Biblical steps in discipline. The primary aim of all steps of discipline is the repentance and restoration of the erring member. Failing that, the aim is to protect the purity of the church and the reputation of Christ.</p>\n\n<p>8.A.  One-on-One Attempt to Restore</p>\n\n<p>Any member of the Chapel having <i>factual knowledge </i>of an erring members heresy, sin, or refusal to be reconciled must approach that person in private, loving confrontation, and seek his or her restoration (Matthew 18:15; Galatians 6:1-2).</p>\n\n<p>8.B.  One or Two Witnesses Attempt to Restore</p>\n\n<p>If, after repeated attempts, the erring member refuses to heed such warnings, then the warning member should return, privately, with one or two witnesses. These witnesses, after self-examination and prayer, should adjudicate the matter and seek to reconcile the parties and/or to restore the erring member (Matthew 18:16, 19-20).</p>\n\n<p>8.C.  Members Attempt to Restore</p>\n\n<p>8.C.1.  Stage 1  Report to The Elders</p>\n\n<p>If the witnesses determine that the erring member refuses to heed such warnings, they should report the matter to the Elders, who are responsible to keep watch over the flock (Hebrews 13:17). At this stage, the witnesses along with the Elders, after self-examination and prayer, should adjudicate the matter and seek to reconcile the parties and/or to restore the erring member (Matthew 18:16, 19-20).</p>\n\n<p>8.C.2.  Stage 2  Report to the Membership</p>\n\n<p>If the erring member continues to refuse to heed such warnings, the Elders will report the matter to the members of the Chapel, requiring them to associate with the erring member <i>only to warn him or her of their error</i>. During this sobering time, the erring member will be excluded from participation in the Lords Table and the privileges of membership, according to the guidelines of Scripture (Matthew 18:17; 1 Corinthians 5:9-11; 2Thessalonians 3:6, 14-15). Further, according to 1 Corinthians 5:1-13, if the sin is public, known to the congregation, and not repented of, the Elders may go immediately to this step when they have found it impossible to implement the informal and private steps first.</p>\n\n<p>8.D.  Excommunication</p>\n\n<p>8.D.1.  Upon Unrepentance</p>\n\n<p>If, after members of the Chapel have attempted to restore the erring member, he or she still refuses to repent, he or she shall be publicly dismissed from the Chapel, and the congregation will be instructed to treat him or her as an unbeliever on the grounds of his or her unrepentance (Matthew 18:17-18; 1Corinthians 5:4-5, 13; 1Timothy 1:18-20). Any church that receives a member under discipline into their number shall be informed of that persons status.</p>\n\n<p>8.D.2.  Upon Resignation</p>\n\n<p>If, during any of the steps of corrective discipline, the erring member seeks to resign from the membership of the Chapel, the Elders shall accept the resignation. The Elders shall in such instances report the reasons for the resignation to the members, publicly disclosing any unresolved issues, and instructing the congregation to treat the resigning member as an unbeliever on the grounds of his or her unrepentance (Matthew 18:17 and 1Corinthians 5:9-11). Any church that receives a member under discipline into their number shall be informed of that persons status.</p>\n\n<p>8.E.  Restoration of the Erring Member</p>\n\n<p>8.E.1.  At the Informal and Private Level of Discipline</p>\n\n<p>If the erring member repents at the informal and private level of discipline (Section 3:Article 6.8.A and Section 3:Article 6.8.B), or the sin is not known publicly, then repentance, confession, and restoration shall be private (Matthew 18:15-16; Luke 17:3-4; Ephesians 4:32; Colossians 3:13).</p>\n\n<p>8.E.2.  Formal and Public Level of Discipline</p>\n\n<p>If the discipline proceeds to the formal level of public disclosure before the congregation, or if the sin of the erring person is public, then the repentance, confession, and restoration must also be formal and before the congregation (2 Corinthians 2:5-11).</p>\n\n<p>8.E.3.  Responsibility of Membership</p>\n\n<p>At whatever level restoration occurs, members must forgive the offender upon his or her confession and repentance (Luke 17:3-4; 2Corinthians 2:5-11; Ephesians 4:32).</p>\n\n<p>Article 7.  Ordinances</p>\n\n<p>The Scriptures have commanded only two ordinances in the church, Baptism and the Lord\'s Table. No ordinance has saving efficacy, and both are commanded of believers as deeply meaningful outward symbols of spiritual realities.</p>\n\n<p>1.  The Ordinance of Baptism</p>\n\n<p>Baptism is the ordinance of the church whereby believers publicly confess their allegiance to Christ. We practice baptism only by immersion. Any person who wants to publicly profess faith in Christ or who has not been Biblically baptized may ask for baptism. The Elders, upon ascertaining a credible profession, shall schedule the person to be baptized.</p>\n\n<p>2.  The Ordinance of the Lord\'s Table</p>\n\n<p>The Lord\'s Table is the ordinance of the church whereby believers, in Christian unity and harmony, look back to remember the Lord\'s death, look inward in self-examination, look outward in proclaiming the gospel and look forward until the Lord returns as those united in the New Covenant. The Lord\'s Table shall be scheduled as often as the Elders wish. All believers and only believers present at its serving, shall be invited to participate, being reminded of its Biblical characteristics and waiting on one another for all to be served (Mark 14:22-23; 1 Corinthians 11:23-33).</p>\n\n<p>Article 8.  Offices</p>\n\n<p>The Bible establishes two permanent offices in the local church, Elders and Deacons/Deaconesses (Philippians 1:1; 1Timothy 3:1-13). The governance and oversight of the church is the responsibility of the Elders who are to shepherd or pastor the church. The Deacons/Deaconesses are responsible to serve God in the church and, under the Elders, to lead the ministry teams doing the daily work of the ministry.</p>\n\n<p>1.  The Elders</p>\n\n<p>Jesus Christ alone is Lord of the church and her Chief Shepherd (1Peter5:4). As the risen Lord, He rules the church from heaven by the Spirit, mediating that rule through the Word of God, the Scriptures (Hebrews 13:20; 1Peter 5:4).</p>\n\n<p>Jesus Christ has ordained that the church shall be overseen by elders who shall shepherd the church according to His will (Acts 20:28; 1 Peter 5:2). The Elders shall govern the church (1 Timothy 5:17) and the church shall lovingly and prayerfully submit to their authority (Hebrews 13:17).</p>\n\n<p>1.A.  Body of Elders</p>\n\n<p>1.A.1.  The Plurality of the Elders</p>\n\n<p>The church shall at all times, in Gods providence, seek to have a plurality of elders. There shall be no maximum number of elders, the number of which shall be determined by men available who are qualified and who desire to serve (Acts 20:17; Titus 1:5,7).</p>\n\n<p>1.A.2.  The Equality and Unanimity of the Elders</p>\n\n<p>The Elders of the Chapel shall constitute a body in which all members shall be equal and shall have one vote. In the spirit of unity in the body, all decisions shall be by unanimous vote of the Elders (Acts 15). Exceptions to the unanimous vote rule include: when an issue needs to be voted on when an elder is incapacitated or the issue would (or could) be construed as a conflict of interest.</p>\n\n<p>From time to time, an item of business may require immediate attention. Matters needing immediate attention may be resolved by not less than 2 elders and shall be reviewed by the entire body at the first opportunity. Electronic means for meeting and voting may be implemented according to guidelines established by the Elders. An elder being removed or disciplined shall be ineligible to vote on decisions regarding himself.</p>\n\n<p>1.B.  The Shepherding by the Elders</p>\n\n<p>The Elders are charged by God to shepherd the church (Acts 20:28; 1 Peter 5:1-4) by:</p>\n\n<p>1. Preaching and teaching the Word of God (Colossians 1:28; 1 Timothy 5:17; 2Timothy 4:1);</p>\n\n<p>2. Establishing the theological and doctrinal purity of the church and guarding the flock of God against error and heresy (Acts 20:28-31);</p>\n\n<p>3. Discipling, counseling, and admonishing believers, confronting sin, and teaching individuals and families as men who will give an account to God (Ezekiel 34; Acts 20:20; Colossians 1:28; 1 Thessalonians 5:12; Hebrews 13:17);</p>\n\n<p>4. Praying for the congregation and the work of God (Acts 6:3-4);</p>\n\n<p>5. Equipping the membership for ministry (Ephesians 4:11-16; 2 Timothy 2:2).</p>\n\n<p>1.C.  The Overseeing by the Elders</p>\n\n<p>The Elders are charged by God to oversee the church (Acts 20:28; 1 Timothy 3:1), governing it as a father manages his family (1 Thessalonians 5:12-13; 1 Timothy 3:4-5; 5:17). Therefore, the Elders shall govern the Chapel by:</p>\n\n<p>1.  Providing for the public ministry of the Word;</p>\n\n<p>2.  Overseeing all the ministries of the Chapel by being ex-officio members of all church ministry teams; designating all ministry teams and their leaders; appointing those identified by the congregation to serve as Deacons and assigning them their responsibilities.</p>\n\n<p>3.  Leading in the correction or disciplining of erring members; confronting any member or attendee who may create dissension that impairs the doctrinal or spiritual unity of the church.</p>\n\n<p>4. Reviewing and approving any missionaries or organizations the Chapel will support, financially or otherwise, and providing opportunities for congregational involvement in support, prayer, communication, and fellowship with them.</p>\n\n<p>5. Ensuring that the financial offerings of the Chapel are used with integrity and accountability, preparing a budget, overseeing the disbursement of funds, receiving regular reports from those delegated with this responsibility and making report to the congregation.</p>\n\n<p>1.D.  The Qualifications for Elders</p>\n\n<p>Elders shall be men who meet the qualifications of 1 Timothy 3:1-7 and Titus 1:5-9. They must be able to teach, have a desire for the privilege and responsibility of the office, shall not seek the office for the sake of money or power (1 Peter 5:2-3), and are in essential agreement with <i>The Truths We Teach</i> statement.</p>\n\n<p>1.E.  The Terms of Office for Elders</p>\n\n<p>Elders shall be appointed to an indefinite term of office. The term may be ended by disqualification or resignation. The Elders shall arrange for sabbaticals to be taken as needed. An elder may resign from office for reasons sufficient unto himself after giving proper notice.</p>\n\n<p>1.F.  The Appointment of Elders</p>\n\n<p>The Elders shall establish a process to identify, examine and qualify men for the Eldership that culminates in their being publicly appointed to the office. The Elders are responsible to identify those men whom God has given to the church to be an elder. The Elders shall examine the prospective elder to determine Biblical qualifications, fitness for the office, and affirmation of the churchs doctrine, distinctives, and Mission and Vision Statement. They shall give the prospective elder opportunity to teach as a way to begin to present the prospective elder to the church (1Timothy 3:2; 5:22).</p>\n\n<p>The Elders shall present the prospective elder to the church in a Special Congregational Meeting. After the presentation, at least two weeks shall be designated for members of the congregation to have the opportunity to resolve any personal issues with the prospective elder regarding his qualifications by meeting with him privately. If the issues cannot be resolved privately, a meeting shall be arranged with the Elders. The prospective elder shall be present at all such meetings and shall have the opportunity to respond to issues that are presented.</p>\n\n<p>During these same two weeks, the congregation shall be called to a season of prayer and fasting. At the end of the prescribed time the Elders shall meet again with the prospective elder to review all the issues brought forth. Upon agreeing to appoint the prospective elder to the office, the Elders shall set a date on which the prospective elder will be publicly installed into office. They shall give notice during regular worship services at least two weeks before the installation date.</p>\n\n<p>1.G.  The Removal of Elders</p>\n\n<p>1.G.1.  Reasons for the Removal of an Elder</p>\n\n<p>An elder may be removed from office for the following Biblical reasons:</p>\n\n<p>1.  Disqualification under 1 Timothy 3:1-7 and Titus 1:5-9;</p>\n\n<p>2.  Not holding to the doctrines clearly taught in the Word (Acts 20:30; 1John 2:19);</p>\n\n<p>3.  Other unrepentant sin. No charge of sin may be received against an elder except by two or more witnesses having factual knowledge of the sin (1Timothy 5:19-21);</p>\n\n<p>4.  Inability to discharge the duties of their office.</p>\n\n<p>1.G.2.  Process of Removing of an Elder</p>\n\n<p>The Elders shall receive and consider an accusation that is brought by no less than two members having factual knowledge of the sin or the false teaching. They shall investigate and verify the charges. If the charges are verified and the elder does not repent, then the Elders shall bring the unrepentant elder before the congregation, shall rebuke him publicly, and dismiss him from the Eldership. If necessary, discipline may be administered as outlined in Section 3:Article 6.8 Corrective Discipline on page 9.</p>\n\n<p>1.G.3.  Restoration of a Removed Elder</p>\n\n<p>An elder who has previously been dismissed from the Eldership, after an appropriate time, shall be eligible to be identified, examined and re-qualified by the Elders. Care should be taken to preserve the reputation of Christ and the purity of the church in such a restoration to the Eldership.</p>\n\n<p>1.H.  The Meetings of the Elders</p>\n\n<p>The Elders shall meet regularly and as often as is necessary to fulfill their responsibilities. Minutes of the meeting shall be kept and filed.</p>\n\n<p>1.I.  The Roles among the Elders</p>\n\n<p>Recognizing the different gifts, abilities, and experience each elder brings to the Eldership, the Elders recognize and will fill the following roles on the Eldership.</p>\n\n<p>1.I.1.  Chairman of the Elders</p>\n\n<p>This elder is responsible to moderate all meetings of the Elders and special congregational meetings of the Chapel. He shall be responsible to prepare and carry out the agenda for the meetings and to enforce Biblical order and decorum in discussions, debate, and decisions of issues as well as all other responsibilities the Elders deem appropriate.</p>\n\n<p>1.I.2.  Vice-Chairman of the Elders</p>\n\n<p>This elder is responsible to assist the Chairman in his duties, to moderate in the absence of the Chairman or when the Chairman is excused due to a conflict of interest as well as all other responsibilities the Elders deem appropriate.</p>\n\n<p>1.I.3.  Secretary of the Elders</p>\n\n<p>This elder is responsible to take accurate minutes of the meetings of the Elders. He shall record the minutes, submit them to the Elders for approval, and ensure that they are filed in the permanent records of the church. He shall also ensure that all policies and procedures arising from the decisions by the Elders are compiled in <i>The Chapel Ministry Handbook</i>.</p>\n\n<p>1.I.4.  Teaching Elders</p>\n\n<p>On the principle of first among equals, the Elders shall have among their number at least one who is gifted and designated as a Teaching Elder. They will be primarily responsible for the doctrine and the public preaching of the Word of God. These elders should be financially supported whenever possible.</p>\n\n<p>1.I.5.  Other Roles and Responsibilities</p>\n\n<p>The Elders shall assign ministry oversight responsibilities among themselves according to their gifts, abilities, and experience. The Elders may create other formal roles, assigning each role a specific set of responsibilities and appointing elders to fill those roles, including non-elder staff positions.</p>\n\n<p>1.I.6.  Legal Trustees</p>\n\n<p>To meet the legal requirements, the Trustees of Clearcreek Chapel shall be the Chairman, Vice-Chairman, and Secretary of the Elders with their respective titles.</p>\n\n<p>1.J.  Financial Support of Vocational Elders</p>\n\n<p>The Chapel family is responsible to provide generous financial support to those men who devote all or large portions of their time and energy to their work as vocational elders (1 Timothy 5:17-18; 1 Corinthians 9:9-11). When considering a vocational elder, the Elders should look among themselves first. Then they may invite men from outside the local congregation to come into its midst and serve in this capacity. The Elders shall establish procedures for (1) the request by an elder for financial support, (2) the consideration of a member or an elder for financial support, and (3) the consideration of a man from outside the church for a vocational elder position. Any man called from outside the congregation to be a vocational elder must be able to conscientiously affirm his essential agreement with the <i>Book of Faith and Order</i>. Should he at any time move from this position, he is under obligation to make this fact known to the Elders.</p>\n\n<p>2.  Deacons/Deaconesses</p>\n\n<p>The Bible has charged the deacons/deaconesses with the ministry of assisting the Elders and the congregation in the work of the ministry.</p>\n\n<p>2.A.  The Functioning of the Deacons/Deaconesses</p>\n\n<p>Deacons/Deaconesses assist the Eldership by assuming responsibility to lead or to serve in those ministries assigned to them by the Elders so that such work will not interfere with the Elders ministry of the Word and prayer (Acts 6:1-7). They will be accountable to the elder overseeing the ministry area in which they are ministering.</p>\n\n<p>2.B.  The Qualifications for Deacons/Deaconesses</p>\n\n<p>Deacons/Deaconesses shall be men or women who meet the qualifications as set forth in Scripture, particularly in 1 Timothy 3:8-13, demonstrate a willingness and ability to serve, and are in essential agreement with <i>The</i> <i>Truths We Teach</i> statement.</p>\n\n<p>2.C.  The Terms of Office for Deacons/Deaconesses</p>\n\n<p>Deacons/Deaconesses shall be appointed to an indefinite term of office. The term may be ended by disqualification or by resignation. The Elders shall arrange for sabbaticals to be taken as needed. Deacons/Deaconesses may resign from office for reasons sufficient unto themselves after giving proper notice.</p>\n\n<p>2.D.  The Selection of Deacons/Deaconesses</p>\n\n<p>The Elders must carefully consider the needs of the church and seek to add qualified people to serve as deacons. The members of the congregation shall identify those who are qualified and willing to serve, recommending them to the Elders who shall examine them regarding their qualifications. Those who are appointed to serve as deacons shall be publicly installed into office.</p>\n\n<p>2.E.  The Removal of Deacons/Deaconesses</p>\n\n<p>2.E.1.  Reasons for the Removal</p>\n\n<p>A deacon/deaconess may be removed from office for the following Biblical reasons:</p>\n\n<p>1.  Disqualification under 1 Timothy 3:8-13;</p>\n\n<p>2.  Not holding to the doctrines clearly taught in the Word (Acts 20:30; 1John 2:19);</p>\n\n<p>3.  Other unrepentant sin;</p>\n\n<p>4.  Inability to discharge the duties of their office.</p>\n\n<p>2.E.2.  Process of Removal</p>\n\n<p>The Elders shall receive and consider an accusation that is brought by no less than two members having factual knowledge of the sin. They shall investigate and verify the charges. If the charges are verified and the deacon does not repent, then the Elders shall remove the unrepentant deacon from office. If necessary, discipline may be administered as outlined in Section 3:Article 6.8 Corrective Discipline on page 9.</p>\n\n<p>2.E.3.  Restoration of One Removed</p>\n\n<p>A deacon/deaconess who has previously been removed from office, after an appropriate time, shall be eligible to be identified, examined and re-qualified by the Elders. Care should be taken to preserve the reputation of Christ and the purity of the church in such a restoration to the office.</p>\n\n<p>Article 9.  Stewardship and Finances</p>\n\n<p>1.  Financial Oversight</p>\n\n<p>The Elders are responsible for the financial oversight of the church. The Elders shall oversee the development and implementation of an annual budget as a financial guideline. The budget shall be presented to the church for affirmation at the Annual Congregational Meeting. At the Elder\'s discretion, unbudgeted expenditures may be made.</p>\n\n<p>2.  Financial Support</p>\n\n<p>The Chapel shall be supported by free-will offerings and sacrificial giving. Special project fund-raising shall require Eldership approval. The disbursement of all moneys including designated moneys shall be at the discretion of the Elders.</p>\n\n<p>3.  Financial Non-indebtedness</p>\n\n<p>It is the intention of the Chapel to finance its growth as God provides and not to incur indebtedness beyond accounts payable.</p>\n\n<p>Article 10.  Meetings</p>\n\n<p>1.  Regular Worship Meetings</p>\n\n<p>The Elders shall set the frequency and times of the regular meetings of the Chapel, including worship on the Lord\'s Day and other services as is desirable to best fulfill the Mission, Vision, and Purposes of the Chapel.</p>\n\n<p>2.  Congregational Meetings</p>\n\n<p>Major decisions such as those relating to finances, budget, and major capital improvements, as well as other issues affecting the Chapel family as a whole, may be presented to the congregation for affirmation. The Elders, in the spirit of not lording over the church (Acts 6:5; 15:22; 1 Peter 5:1-4), shall openly communicate with the congregation, teaching relevant truths, receiving input, and carefully considering the responses and counsel of the Body. To foster cooperation and unity for the sake of the Kingdom, the Elders will be ever mindful of being servant leaders who set an example (1 Peter 5:1-4) and the congregation of being wisely and lovingly submissive to its leaders (Hebrews 13:7,17).</p>\n\n<p>2.A.  Annual Congregational Meeting</p>\n\n<p>The Annual Congregational Meeting of the Chapel shall be held each ministry year at a time set by the Elders for the presentation and adoption of a budget, giving of ministry reports, and any other business scheduled by the Elders.</p>\n\n<p>2.B.  Special Congregational Meetings</p>\n\n<p>The Elders may call special congregational meetings to bring before the Chapel family matters needing the support or input of the congregation.</p>\n\n<p>Special Congregational Meetings may also be called by written request to the Elders that states the reasons for the meeting. At least twenty-five percent (25%) of the active resident members of the Chapel in good standing must sign such request. The Elders shall take the concerns expressed under careful and prayerful consideration.</p>\n\n<p>2.C.  Congregational Affirmation/Vote</p>\n\n<p>At their discretion, the Elders may ask for a Congregational Affirmation/Vote at any Special Congregational Meeting on an issue before them. Twenty-five percent (25%) of the active resident members in good standing shall constitute a quorum. All members who are in good standing as described in Section 3:Article 6.6 Congregational Voting Privilege are eligible to participate. Affirmation/Vote shall be conducted by a show of hands or by ballot. Ballots are valid only when a member has written his or her name on it. Except when otherwise stated, a two-thirds (2/3) majority of valid votes received are required to pass an issue.</p>\n\n<p>Article 11.  Amendment</p>\n\n<p><i>The Chapel Book of Faith and Order</i> may be amended by a three-fourths (3/4) majority affirmation of the eligible members voting at a duly called Special Congregational Meeting. The proposed amendments must be presented to and approved by the Elders. The proposed changes will be presented in written form at least two Sundays prior to the congregational meeting.</p>\n\n<p>Article 12.  Dissolution</p>\n\n<p>1.  The Process of Dissolution</p>\n\n<p>Only the Trustees, acting upon the recommendation of the Elders, may recommend the dissolution of this corporation to the congregation. A three-fourths (3/4) majority affirmation of the eligible members shall be required in order to dissolve this corporation.</p>\n\n<p>2.  The Responsibilities at Dissolution</p>\n\n<p>In the event of the dissolution of this corporation, all debts are to be satisfied. The official trustees shall ensure that there is no division of assets, and all assets herein are to be distributed to another corporation or corporations with purposes similar to those identified in Section 1: Prologue and in Section 2: A three-fourths (3/4) majority affirmation of the eligible members shall be required in order to decide on the distribution of assets.</p>\n','xhtml','<a href=\"../uploads/Clearcreek-Chapel-Book-of-Faith-and-Order.pdf\" title=\"Clearcreek Chapel Book of Faith and Order\" target=\"_blank\">Download the Clearcreek Chapel - Book of Faith and Order in PDF format</a>.<br><br><br><br>\n','xhtml','{filedir_1}subnav-about-faith-and-order-2x.png','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(35,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}elder-dan-t.jpg','none','Pastor for Outreach','none','<p>Pastor Dan\'s vision and practice in outreach is largely focused on equipping the Chapel in local outreach ministries and commissional lifestyle.  Dan serves as the Body of Elders Vice Chair. He has served in pastoral ministry since the early 1990\'s.\n\n</p><p></p>\n','none','<p>\n\n<p>Dan was born in\nCreston, Iowa. He came to faith while serving in the military. Dan is a\ngraduate of Multnomah Biblical Seminary where he received a BA in Theology and\na Masters in Divinity.&nbsp; After&nbsp; pastorates in churches in Montana and Ohio,\nDan joined the Chapel staff in 2001. His teaching and counseling connect\nScripture and people assisting them to daily dwell in gospel goodness.</p>\n<p>Dan is married to\nLisa and they are raising four children together. He appreciates the gym and\nmeeting people, the coffee shop with a book in hand and the family room romping\nwith kids and dogs. He takes the greatest pleasure in Christ as his great Savior.&nbsp; </p>\n\n</p>','none','Oversees Outreach preparation and participation','none','Kettering','none','Turner','none','','none','','none'),
	(37,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}elder-tim-n.jpg','none','Pastor for Adult Bible Education and Leadership Development','none','<p>\nPastor Tim has been preaching and teaching since the mid 1980\'s. He has served in pastoral, church planting, discipleship, outreach and teaching ministries.<br></p><p></p>\n','none','<p>Tim was born in\nDayton in 1957 and has spent his entire life living in the Miami Valley.&nbsp; He received his formal ministry training at\nLiberty University. Tim began his ministry at a church in north Dayton working\nwith teenagers. He served as associate pastor at that church for 19 years\nduring which he was licensed and later ordained. The Lord lead him to a church\nplanting ministry in 2005, a work that began in the Miamisburg/Springboro area.\nTim served there for 4 years until the church merged with another in\nCenterville.&nbsp; In May 2011, Tim came to\nClearcreek Chapel; he was ordained as an elder in the summer of 2012.&nbsp; </p>\n<p>Tim currently works\nin the information technology management field.&nbsp;\nHe is married to Jayne; they have two grown children, Emily and Amanda.</p>\n\n<p></p>','none','Oversees the Adult Bible Education and Leadership Development ministries','none','West Carrollton','none','Nixon','none','','none','','none'),
	(14,1,2,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<h1>Our Vision </h1><p>\n\n</p>\n\n<h3>Savoring the Supremacy of God</h3>\n\n<p>Our vision is to\nmagnify God as the all-satisfying object of the believers life in worship and\nservice, here and forever.</p>\n\n<h3>Studying the Word of God</h3>\n\n<p>Our vision is to work\nout both the large picture and the immediate texts in Holy Spirit enabled\nexposition and application in the public and private ministry of the Word of\nGod.</p>\n\n<h3>Shaping the People of God</h3>\n\n<p>Our vision is to see\nminds, affections, and wills transformed by the Word of God so that God is\nmagnified by an obeying faith in every sphere of life.</p>\n\n<h3>Spreading the Gospel of God</h3>\n\n<p>Our vision is to\nproclaim the gospel to all the people groups so that God is glorified in every\ntribe, language, and people group.</p>\n\n\n\n<p><br></p><h1>Our Purposes<br></h1><h3>Exaltation: Our Doxological Purpose</h3>\n\n<p>Exaltation is all that\nwe do in terms of pursuing God in worship. This is the overarching purpose of\nthe church. The church exists to glorify God and to enjoy Him forever.</p>\n\n<h3>Edification: Our Discipleship Purpose</h3>\n\n<p>Edification is all that\nwe do to know God better, to please Him in every facet of life, to learn, to\nbelieve, to understand and to obey the Scriptures fully, and to love God and\none another in each sphere of life as we grow in Christ-likeness.</p>\n\n<h3>Evangelism: Our Declarative Purpose</h3>\n\n<p>Evangelism is all that\nwe do to proclaim the gospel to every people group in order to see all of God\'s\nelect brought into His Kingdom.</p><p></p>\n','xhtml','<p><img src=\"{filedir_1}about-our-mission.jpg\" style=\"\"></p>\n<h1>Our Mission </h1>\n\n<p>By God\'s\ngrace<span><br></span><span>and through\nHis Word,<span> we will\ncultivate<span></span></span></span><br><span><span><span></span></span></span><span><span><span>in the\nhearts of God\'s people<span><br>a passion for the supremacy <br>of the Lord Jesus Christ<br>\nmagnified in love for\nHim<span><br>manifested\nin love\nfor one another<br><span>and multiplied by love for the lost.</span></span></span></span></span></span></p><p></p>\n','xhtml','{filedir_1}subnav-about-mission-2x.png','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(15,1,2,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<h1>Sunday</h1><h3>Morning Worship...................9:30am</h3><p>We gather to speak to God in prayer and praise<br>and to hear from God in preaching God\'s Word.<br></p><h3>Bible Education....................11:00am</h3><p>We gather to study God\'s Word in classroom settings:<br></p><ul><li>To become one in the Faith</li><li>To become mature in Christ</li><li>To become equipped for ministry</li><li>To grow in becoming more like Christ.</li></ul><p></p><h3>Evening Worship...................6:00pm</h3><p>We gather as God\'s family to share in fellowship, ministry, worship and the Word.<br>The Lord\'s Table and Baptism are most often shared in our evening gathering.<br></p><p></p><p>\n</p><h1>Wednesday</h1><h3>Flock Meetings......................6:00pm</h3><p>Flocks meet in homes throughout the Dayton region. <br>Go <a href=\"/flocks\">here</a>&nbsp;for more information about Flocks.<br></p>\n','xhtml','<div><p><img src=\"{filedir_1}about-gatherings.jpg\" style=\"\"></p></div><p></p><blockquote>And they devoted themselves to the apostles teaching and the fellowship, to the breaking of bread and the prayers.&nbsp;<br>— Acts 2:42, ESV</blockquote><div><p></p>\n</div>','xhtml','{filedir_1}subnav-about-gatherings-icon-2x.jpg','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(16,1,2,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<p><h1>Directions</h1><p>We are located in Springboro, Ohio.</p><p>Traveling South: Take I-75 to exit 41, Austin Blvd. Go east onto Austin Blvd. Turn right at the second traffic light; head south on St. Rte. 741. At the first traffic light, turn right onto Pennyroyal Road. Drive about 1 mile until you see our sign on the right. Turn in at the drive immediately after the sign; continue down the long lane until you reach the church property.</p><p>Traveling North:&nbsp; Take I-75 to exit 38, St. Rte. 73/W. Central Ave.&nbsp; Go east toward Springboro about .75 miles.&nbsp; Turn left onto Clearcreek-Franklin Rd.&nbsp; Take the last right before the interstate overpass onto Pennyroyal Rd.&nbsp; Drive .30 miles until you see the Clearcreek Chapel sign on the left.&nbsp; Turn in at the drive immediately before the sign; continue down the long lane until you reach the church property.</p></p>','xhtml','<h1>Contact Information</h1><p></p><h4>Mailing</h4><p>P.O. Box 327<span><br></span>Springboro, OH 45066<br></p><h4>Location</h4><p><a href=\"https://maps.google.com/maps?q=2738+Pennyroyal+Rd+Springboro,+Ohio+45066&amp;hl=en&amp;client=safari&amp;hnear=2738+Pennyroyal+Rd,+Springboro,+Warren,+Ohio+45066&amp;gl=us&amp;t=m&amp;z=14\" target=\"_blank\">2738 Pennyroyal Road<span><br></span>Miamisburg, Ohio 45342</a></p><h4>Phone</h4><p>(937) 885-2143</p><p></p><h4>Email</h4><p><a href=\"mailto:contact@clearcreekchapel.org\">contact@clearcreekchapel.org</a></p>\n','xhtml','{filedir_1}subnav-about-location-icon-2x.jpg','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(17,1,5,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<table class=\"styled cols4\">\n<tbody>\n <tr class=\"even\">\n  <th>Neighborhood</th>\n  <th>Shepherd</th>\n  <th>Host Location</th>\n  <th>Contact</th>\n </tr>\n <tr class=\"odd\">\n  <td>Bellbrook</td>\n  <td>Dale Evans</td>\n  <td>\n    Host Varies<br>\n    Call Contact\n  </td>\n  <td>\n  Julie Resch\n  </td>\n </tr>\n <tr class=\"even\">\n  <td>\n    Belmont\n  </td>\n  <td>\n    Michael Engle\n  </td>\n  <td>\n    Host Varies\n  </td>\n  <td>\n    Amy Frederick\n  </td>\n </tr>\n <tr class=\"odd\">\n  <td>Centerville</td>\n  <td>\n    Russ Kennedy\n  </td>\n  <td>\n    Russ Kennedy<br>\n    211 Marsha Jeanne Way<br>\n    Centerville, OH\n  </td>\n  <td>\n    Sheelah Beaver\n  </td>\n </tr>\n <tr class=\"even\">\n  <td>Franklin/Monroe</td>\n  <td>\n    Devon Berry\n  </td>\n  <td>\n    Host Varies\n  </td>\n  <td>\n    Becky Thompson\n  </td>\n </tr>\n <tr class=\"odd\">\n  <td>Kettering\n  </td>\n  <td>\n    Dan Turner<br>Chad Bresson\n  </td>\n  <td>\n    Host Varies\n  </td>\n  <td>\n    Beth Black\n  </td>\n </tr>\n <tr class=\"even\">\n  <td>Miamisburg</td>\n  <td>\n    Mark Schindler\n  </td>\n  <td>\n    Host Varies\n  </td>\n  <td>\n    Amy Mitchell\n  </td>\n </tr>\n <tr class=\"odd\">\n  <td>Springboro East</td>\n  <td>\n    Greg Simmons  </td>\n  <td>\n    Host Varies\n  </td>\n  <td>\n    Debbie Simmons\n  </td>\n </tr>\n <tr class=\"even\">\n  <td>Springboro West</td>\n  <td>\n    Tim Rech\n  </td>\n  <td>\n    Host Varies\n  </td>\n  <td>\n    Lisa Watkins\n  </td>\n </tr>\n <tr class=\"odd\">\n  <td>Waynesville/Lebanon</td>\n  <td>\n    Steve Vaughan\n  </td>\n  <td>\n  Host Varies\n  </td>\n  <td>\n    Lois Mascotti\n  </td>\n </tr>\n <tr class=\"even\">\n  <td>West Carrollton</td>\n  <td>\n    Tim Nixon\n  </td>\n  <td>\n    Tim Nixon<br>\n    5997 Munger Rd<br>\n    Dayton, OH 45459\n  </td>\n  <td>\n    Cheryl Watkins\n  </td>\n </tr>\n</tbody>\n</table>For more information please contact the <a href=\"mailto:contact@clearcreekchapel.org\"></a><a href=\"mailto:contact@clearcreekchapel.org\">church office</a>.\n','xhtml','<p>Our Flock Groups are organized by neighborhood. Some Flocks are hosted in different homes through the month. Locate your community on the chart. Please call the church office for the contact information on where your flock is meeting this coming week.</p><p><a href=\"/news-and-events/details/2013-summer-flocks\">View the 2013 Summer Joint Flock Schedule</a>.</p>\n','xhtml','{filedir_1}subnav-flock-groups-icon-2x.jpg','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(18,1,5,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<h2>Emphasis\n</h2><p>The Flock meetings shall emphasize relationships, fellowship and mutual care. They may include a short Bible study.  Much time is to be spent in sharing, prayer requests, prayer together and then a lesson from the Word.</p><h2>Environment\n</h2><p>Flock leadership shall strive to create a warm and welcoming environment in order to draw people to attend and become enthusiastic about it. Elders are to be hospitable and thus ought to be able to develop this important aspect.\n</p><h2>Meeting\n</h2><p>Flocks shall meet each week on a weekday evening in the home of a member of the Flock. Initially, all Flocks will be meeting on Wednesday evenings.\nOn each fifth Wednesday of a month, the Elders and their families will meet as a Flock.</p>Fellowship and community are best developed around meals shared together. Major effort should be made to have a regular meal together and if at all possible, every week. Meals are times to eat, talk and build relationships. The responsibly to provide the meal builds team work, commitment and an appreciation for one anothers labors for the whole. \n<h2>Shepherding\n</h2><p>Elders shall use the Flock setting to teach, challenge and confront believers in regard to their life and ministry together. Regular Christian disciplines and duty should be explained, exhorted and examined.\n</p><h2>Discipleship\n</h2><p>Personal and small group discipleship should be modeled and encouraged among the Flock. This is the place where relationships may be cultivated for Titus 2 ministry between our more mature and our younger believers.\n</p><h2>Outreach\n</h2><p>Flock meetings shall be a time and place where the lost are invited and welcomed. It is imperative that Flocks be evangelistic and outward reaching. In this day of isolation and loneliness, the richness of Christian fellowship is a powerful evangelistic opportunity.\n</p><h2>Mutual Care</h2><p>The Flock shall be the first line of diaconal care. Hospital visits, care for new mothers and those suffering illness, meeting practical needs, visiting and encouraging the widows and shut-ins shall begin with and primarily be the responsibility of the Flock.\nA Flock may identify men and women who are willing to lead and organize this care.</p>\n','xhtml','<p><img src=\"{filedir_1}flocks-strategies.jpg\" alt=\"flocks-strategies.jpg\" style=\"\"><br></p>\n<blockquote><p>For this reason I bow my knees before the Father, from whom every family in heaven and on earth is named, that according to the riches of his glory he may grant you to be strengthened with power through his Spirit in your inner being, so that Christ may dwell in your hearts through faith  that you, being rooted and grounded in love, may have strength to comprehend\nwith all the saints what is the breadth and length and height and depth, and to\nknow the love of Christ that surpasses knowledge, that you may be filled with\nall the fullness of God. <br>— Ephesians 3:14-19, ESV</p>\n</blockquote>\n','xhtml','{filedir_1}subnav-flocks-strategies-2x.png','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(19,1,5,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<h3>Shepherding</h3>\n\n<p>We value the importance of elders/pastors shepherding the church at the personal, face-to-face level.</p>\n\n<h3>Community</h3>\n\n<p>We value being together as believers in terms of community and our shared life in Christ with one another.</p>\n\n<h3>Fellowship</h3>\n\n<p>We value the sweetness of our fellowship as believers with God and one another around shared meals, talk and recreation. </p>\n\n<h3>Praying Together</h3>\n\n<p>We value the greatness of our privilege to pray to God together over our life together, its needs, problems, heartaches, pressures and praises.</p>\n\n<h3>Practical Care</h3>\n\n<p>We value the unity, opportunity and challenges of helping one another in practical ways and expressing our love for God through sacrificial deeds of love for one another. </p>\n','xhtml','<p><img src=\"{filedir_1}flocks-values.jpg\" style=\"\"></p><blockquote>Let us hold fast the confession of our hope without wavering, for he who promised is faithful. And let us consider how to stir up one another to love and good works, not neglecting to meet together, as is the habit of some, but encouraging one another, and all the more as you see the Day drawing near.<br> — Hebrews 10:23-25, ESV</blockquote>\n\n<br>\n','xhtml','{filedir_1}subnav-flocks-values-2x.png','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(20,1,5,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<h3>We aim...<br></h3><p>\n\n</p><ul><li>To establish, as the primary Chapel\nfamily ministry unit, community based and geographically defined flocks.</li><li>To place over each Flock an elder who\nshall shepherd that Flock according to the Word of God.</li><li>To meet together regularly for study,\nprayer, fellowship, one-anothering ministry and outreach.</li><li>To cultivate loving and caring\nrelationships for discipleship, training and mutual care.</li><li>To establish\nevangelistic beachheads in each community in which the Chapel families live.</li></ul>\n','xhtml','<h1><p><img src=\"{filedir_1}flocks-vision.jpg\" style=\"\"></p></h1><h1>Mission</h1><blockquote>To cultivate and sustain Biblical shepherding, life, fellowship, and ministry at the neighborhood level.\n</blockquote>\n','xhtml','{filedir_1}subnav-flocks-vision-2x.png','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(21,1,10,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<p>Below is a list of our recent pulpit messages. You can find more at <a href=\"http://www.sermonaudio.com/source_detail.asp?sourceid=thechapel\" target=\"_blank\">SermonAudio.com</a>.<br></p>\n','xhtml','<p><img src=\"{filedir_1}SermonAudio_Button.jpg\" alt=\"SermonAudio_Button.jpg\"><br></p>Our full Chapel Pulpit Archive will be available soon. It will give access to most of the sermons preached at the Chapel from 2003 through 2012. Please visit these pages again soon<br><p><br></p><p>&nbsp;</p>','xhtml','{filedir_1}subnav-sermons-repository-2x.png','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(39,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}elder-chad-b.jpg','none','Pastor for Adult Bible Education','none','<p>Pastor\nChad is a long-time resident of Xenia, Ohio. He is journalist by profession,\nformerly working as a news director for a local radio station. He has been an\nelder since 2005 and a part-time staff pastor since 2008.<br></p>','none','<p>* awaiting text *<br></p>\n','none','Oversees Adult Bible Education and Biblical Theology Study Center','none','Kettering','none','Bresson','none','','none','','none'),
	(22,1,10,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<p>These series represent important work in the Word that has defined our doctrine, distinctives and direction. Each link will open a PDF of the series that you may save and read. Please be aware that editing on the manuscripts has been minimal.&nbsp;<br></p><p><a href=\"{filedir_1}Hebrews.pdf\"></a><a href=\"{filedir_1}Hebrews.pdf\" target=\"_blank\">Hebrews - Magnifying Christ, Maturing Believers</a><br><a href=\"{filedir_1}Galatians3.pdf\"></a><a href=\"{filedir_1}Galatians3.pdf\" target=\"_blank\">Galatians - The Gospel Transforming</a><br><a href=\"{filedir_1}Genesis1.pdf\"></a><a href=\"{filedir_1}Genesis1.pdf\" target=\"_blank\">Genesis - Beginnings</a><br><a href=\"{filedir_1}Understanding_the_Church.pdf\"></a><a href=\"{filedir_1}Understanding_the_Church.pdf\" target=\"_blank\">Understanding the Church</a><br><a href=\"{filedir_1}Becoming_a_Commissional_Church2.pdf\"></a><a href=\"{filedir_1}Becoming_a_Commissional_Church2.pdf\" target=\"_blank\">Becoming a Commissional Church</a><br><br>And more to come...</p>\n<p><br>&nbsp;</p>\n<p><br></p>\n','xhtml','<p><p><img src=\"{filedir_1}sermons-key-text.jpg\" style=\"\"></p><br></p>','xhtml','{filedir_1}subnav-sermons-key-series-2x.png','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(23,1,10,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<h1>Sunday July 21<br></h1>\n<h3>Morning Gathering</h3><p><i><b>The Pilgrim\'s Righteousness</b></i><b><i><br></i></b>Psalm 7<br><br>Pastor Russ Kennedy</p><p><br>\n</p><h3>Evening Gathering</h3><p><i><b>The Gospel:&nbsp; Our Responses in Matters of Conscience - Part 2</b></i><b><i><br></i></b>Romans14:1-15:7<br><br>Pastor Russ Kennedy<br></p>\n','xhtml','<p><img style=\"\" src=\"{filedir_1}festival-favorites.jpg\"></p>\n<h3>August 25\n</h3><h4><i>Festival of Favorites</i></h4><p>An all music Sunday evening featuring songs selected by the\ncongregation. The Chapel family will have the opportunity to request their\nfavorite worship songs and special music soloists or groups that have\nministered throughout the year. Request forms will be available at the\nInformation Table; return completed forms to Linda Frye.</p><h3></h3><h3>Current Series</h3>\n\n<p>AM - <i>Songs of the Pilgrim</i><br>\n&nbsp;&nbsp;&nbsp;&nbsp; Pastor Russ Kennedy<br></p>\n\n<p>PM - <i>Romans - The Gospel of God</i><br>\n&nbsp;&nbsp;&nbsp;&nbsp; Pastor Russ Kennedy<br></p>\n','xhtml','{filedir_1}subnav-sermons-next-week-2x.png','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(56,1,12,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'{filedir_1}elder-greg-s.jpg','none','Pastor for Springboro East Flock','none','<p>Pastor Greg was ordained to the eldership in 2013. He oversees the Usher\'s ministry and serves on the Adult Bible Education ministry team.<br></p>','none','<p>*awaiting text*<br></p>','none','Oversees Springboro East Flock','none','Springboro East','none','Simmons','none','',NULL,'',NULL),
	(24,1,10,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<h1>July 14<br></h1><h3>Morning Gathering</h3><b><i>The Pilgrim\'s Languishing<br></i></b>Psalm 6<br><p></p><p>Pastor Russ Kennedy<br></p><br><p></p><p>\n\n</p><h3>Evening Gathering</h3>\n\n<p><i><b>The Gospel:&nbsp; Our Responses in Matters of Conscience - Part 1<br></b></i>Romans14:1-15:7<br></p><p>Pastor Russ Kennedy<br></p>\n','xhtml','<h3><p><img src=\"{filedir_1}sermons-this-weeks.jpg\" style=\"\"></p></h3><h4>Our prayer for the pulpit ministry...</h4><p>\n\n</p><blockquote>And it is my prayer that your love may abound more and more, with knowledge and all discernment, so that you may approve what is excellent, and so be pure and blameless for the day of Christ, filled with the fruit of righteousness that comes through Jesus Christ, to the glory and praise of God.&nbsp;</blockquote><blockquote>— Philippians 1:1, ESV</blockquote>\n','xhtml','{filedir_1}subnav-sermons-this-week-icon-2x.jpg','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(42,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','none','CMI Administrator','none','<p>\n\n<p>Anita joined the\noffice staff in 2003 to serve alongside Pastor Kennedy as administrative\nassistant and bookkeeper for Chapel Ministries International.&nbsp; It has been her privilege to work with the\nvarious missionaries and ministries that have been served by CMI.</p>\n\n</p>','none','<p>\n\n<p>Anita was born and\nraised in western PA.&nbsp; She graduated with\na B.A. in Behavioral Science from Cedarville College.&nbsp; There she served in a ministry that\neventually would lead to a first visit to the Chapel.</p>\n<p>Anita served as a\nvolunteer policy writer for a proposed CMI ministry before accepting a staff\nposition. Her office duties now include CMI finance and administration,\nmembership, flock, and facilities management, assisting Pastor Russ, and\nserving as music librarian.</p>\n<p>Anita is married to\nTadd.&nbsp; They have been Chapel members\nsince 1989.</p>\n\n</p>','none','','none','','none','Dieringer','none','','none','','none'),
	(25,1,7,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}banner-news-2x.jpg','none','news & events','none','<p>Learn what is happening in and around Clearcreek Chapel. Check the most up-to-date time, location, and information on our web calendar.</p>\n<p> </p>\n\n<p><a href=\"/ministries/expansion\"><img src=\"http://www.clearcreekchapel.org/uploads/Chapel_Expansion_Icon.png\" alt=\"News and updates on the Chapel Expansion\" width=\"50\" align=\"left\" style=\"margin-right: 1em;\"></a><a href=\"/ministries/expansion\">News and updates on the Chapel Expansion</a></p>','xhtml','','','','','','','','','','','','','','','And all who believed were together and had all things in common','none','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(26,1,6,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<h1>Our Programs</h1>\n<h2>Personal Counseling</h2>\n<p>We provide personal counseling to people who are struggling with issues of life. This includes marital, relational, parent-child, eating disorders, depression and substance abuse as well as many\nother areas.</p>\n<p>Our personal counseling is generally scheduled on Mondays from 10:00a.m.-8:00p.m. You\nwill be assigned a counselor based on availability and your needs. Our personal\ncounseling is available on a donation basis and there is no fee.&nbsp; To schedule an\nappointment or for more information, please call the church office at (937)\n885-2143 or email <a href=\"mailto:counseling@clearcreekchapel.org\">counseling@clearcreekchapel.org</a></p>\n<h2><a href=\"http://www.chapelevents.org/CTC/CTC_Main.htm\" target=\"_blank\">Nouthetic Counseling Training Classes</a></h2>\n\n<h3>NANC Basic Training Module</h3>\n<p>This course is the basic course in Biblical counseling. It will introduce you to the philosophy and practice of Nouthetic Counseling and you will receive the Level One Certificate upon\ncompletion.</p>\n\n<h3>Counseling Workshops</h3>\n<p>These workshops are designed to equip people to help themselves and others who are overcome with particularly challenging issues of life and sin.</p>\n<h3>Training Schedule</h3>\n<p>February-May: NANC Basic Training is scheduled the 3rd weekend of each of these months. The Workshops are held the 3rd Saturdays of some of these months. Email or call for current dates and topics or to register for any training.</p>\nEmail: <a href=\"mailto:counseling@clearcreekchapel.org\">counseling@clearcreekchapel.org</a>\n\n<blockquote><p>For inquiries on having any of this training at your location, please call and ask to speak to the counseling administrator.</p></blockquote>\n\n<h1>Our Practice</h1>\n\n<h2>Our Counselors</h2>\n\n<h3>Qualifications</h3>\n\n<p>All our counselors are trained and\nexperienced in helping people deal with the issues of life and change. They\nhave gone through counseling training classes and on-going development through\nclasses, conferences, oversight and critique.</p>\n\n<h3>Credentials</h3>\n\n<p>While we do not seek professional credentials for ministry, we counsel according to the Biblical standards established by the National Association of Nouthetic Counselors (NANC).</p>\n\n<h3>Accountability</h3>\n\n<p>Our counselors are accountable to the Elders of Clearcreek Chapel, particularly to the Pastor for Counseling.</p>\n\n<h3>Training</h3>\n\n<p>We are recognized and sought out to teach and train other Biblical counselors. Clearcreek Chapel is a NANC approved Training Center.</p>\n\n\n<p><i>Our counselors counsel out of love for God and love for His people. We want to express Gods\nlove for people through our attention and empathy with people in their need.</i></p>\n','xhtml','<h1>Our Philosophy</h1>\n\n<h2>Our Foundation</h2>\n\n<p>Believing that we live in the world, created and interpreted by God and now fallen, we counsel so as to bring people to love God and love neighbor through believing obedience.</p>\n\n<p>Believing that the Bible is God\'s revelation and that it is a sufficient guide for faith and life,\nwe counsel based on and with the Scriptures. The Bible gives us God\'s interpretation of our lives and how we can be pleasing to Him.</p>\n\n<h2>Our Framework</h2>\n\n<p>Believing that the church is the central place of God\'s work, we counsel as a ministry of a local church. Our counseling is under the direction of and is a ministry of Clearcreek Chapel.</p>\n\n<p>Believing that people need help, we counsel people from other churches or who are not churched\nwho are willing to commit to certain expectations.</p>\n\n<h2>Our Focus</h2>\n\n<p>Believing that people live out how they think and what they want, we focus our counsel on both the outward and the inward, on the behavior and the heart.</p>\n\n<p>Believing that true life change comes through repentance in the heart, we seek to bring about heart\nchange through the Word and Spirit of God in counsel. We also work to implement practical steps of action that make putting off sin and putting on righteousness a reality.</p>\n','xhtml','{filedir_1}subnav-ministries-counseling-icon-2x.jpg','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(27,1,6,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<p>Please come visit again. This page will be prepared to describe our commitment to Community and to Discipleship....</p><p>\n</p><h3>Titus 2:115 (ESV)</h3>\n	\n<p>But as for you, teach what accords with sound doctrine. Older men are to be sober-minded, dignified, self-controlled, sound in faith, in love, and in steadfastness. Older women likewise are to be reverent in behavior, not slanderers or slaves to much wine. They are to teach what is good, and so train the young women to love their husbands and children, to be self-controlled, pure, working at home, kind, and submissive to their own husbands, that the word of God may not be reviled. Likewise, urge the younger men to be self-controlled. Show yourself in all respects to be a model of good works, and in your teaching show integrity, dignity, and sound speech that cannot be condemned, so that an opponent may be put to shame, having nothing evil to say about us. Bondservants are to be submissive to their own masters in everything; they are to be well-pleasing, not argumentative, not pilfering, but showing all good faith, so that in everything they may adorn the doctrine of God our Savior.</p>\n<p>For the grace of God has appeared, bringing salvation for all people, training us to renounce ungodliness and worldly passions, and to live self-controlled, upright, and godly lives in the present age, waiting for our blessed hope, the appearing of the glory of our great God and Savior Jesus Christ, who gave himself for us to redeem us from all lawlessness and to purify for himself a people for his own possession who are zealous for good works. </p>\n<p>Declare these things; exhort and rebuke with all authority. Let no one disregard you.</p><br><p></p>\n','xhtml','<p>And Jesus came and said to them, All authority in heaven and on earth has been given to me. Go therefore and make disciples of all nations, baptizing them in the name of the Father and of the Son and of the Holy Spirit, teaching them to observe all that I have commanded you. And behold, I am with you always, to the end of the age. (Matthew 28:1820, ESV)<br></p><p></p>\n','xhtml','{filedir_1}subnav-ministries-discipleship-2x.png','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(28,1,6,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<h1>Children\'s Ministries</h1><p>\n\n</p><p>By God\'s grace and\nthrough His Word, we intend to cultivate in the hearts of the children of\nClearcreek Chapel a passion for the supremacy of God that will become a love\nfor Him and be seen in love for one another and love for the lost.</p>\n<p>Our vision is to see\nthe thoughts, emotions, and behaviors of the children of Clearcreek Chapel\ntransformed by the Word of God so that God is magnified by their faith in every\naspect of their lives.</p>\n<p>Our curriculum is\nintentionally God-centered, Christ-focused, and Bible-based. The Bible\'s\nstoryline is presented at every age, showing the Scriptures to be God\'s\nWord.&nbsp; </p>\n<p>Classroom instruction\ncurrently is engaged in four age groups:</p>\n<p></p><ul><li>Preschool children\nfrom age 3 are introduced to God and His Word by learning about several\nfoundational events and concepts from the Bible.<br></li><li>The Beginner\ndepartment, ages 4 through kindergarten, in a two-year sequence, navigate the\nOld and New Testaments seeing the wonder of God as He has revealed Himself\nthroughout history.<br></li><li>The Primary\ndepartment, those in the first through third grades, survey the Bible, seeing\nthe Gospel-Christ centered orientation of the Scriptures and being exposed to\nthemes of God\'s promises and Jesus life and work.<br></li><li>The Junior department,\nchildren in the fourth through sixth grades, also survey the Bible from its\nGospel-Christ centered focus and explore themes of God\'s character and His\nprovidence. <br></li></ul><p><br></p><h1>\n\nClearcreek Chapel Youth Ministries (CCYM)<br></h1><p>\n\n</p><p>The vision of the\nClearcreek Chapel Youth Ministry is:</p><p></p><ol><li>to see unbelieving youth embrace Christ\nas Lord and Savior through the&nbsp; clear and\nfrequent proclamation of the gospel; and</li><li>to see believing youth progress\nfrom folly to wisdom, from basic to advanced Bible teachings, from private to\npublic confessions through baptism, and from spiritual immaturity to maturity\nas we teach the Word and engage together in body life.</li></ol><p>\n\n</p><p><br></p><h1>Adult Bible Education (ABE)<br></h1>\n\n<h2>Mission</h2>\n\n<p>The mission of ABE\nis to provide Christ-centered teaching and learning in order to equip and mature the community\nin Christlikeness so that the community is grounded in sound doctrine.</p>\n\n<h2>Vision </h2>\n\n<p>ABE\'s vision is\ndiscipleship through Christ-centered, expositional teaching and learning in\nboth the Old Testament and\nNew Testament in the context of church community.</p>\n\n<h2>Purpose</h2>\n\n<p>ABE\'s purpose is to\nglorify God by equipping the saints through Christ-centered theological education for the\nministry of building up the body of Christ, and of expanding the gospel beyond our community.</p>\n\n<p></p><p></p>\n','xhtml','<p><br></p>','xhtml','{filedir_1}subnav-ministries-education-icon-2x.jpg','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(29,1,6,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<p><i>Spreading the Gospel of God Ministries</i> is\nabout two things. It is first about getting the gospel right. In a culture of\nmultiple versions of the gospel, we only want to know what the Bible says regarding\nthe authentic gospel. The gospel is about God and His Son Jesus. It is about\nwhat God has done through the life, death and resurrection of Jesus. It is\nabout how God\'s justice to uphold His righteousness and His love to justify\nsinners come together at the cross. This is the heart of the gospel story. </p>\nSecondly, the ministry is about valuing the gospel to the extent that we\ncannot keep it to ourselves. We see it as our joy to spread it wherever we are.\nWe intentionally teach and train our congregation for creative, courageous,\ncompassionate, Christ-reliant evangelism and gospel expansion. Our vision is to see our entire church\nactively bringing our biblical faith and Christian love into the lives of people\nwho do not yet understand and believe the gospel.<span>&nbsp; &nbsp; </span><br><p></p>\n','xhtml','<p>To help carry out our mission listed below are a few of our\nstrategies.</p>\n\n<p></p><ul><li><span><span></span></span>We preach expositional\nredemptive and relevant messages</li><li>We teach\nan ongoing 13-week training course called <i>Becoming<span> Commissional as a Church</span></i></li><li><span><span></span></span>We offer field\ntraining in evangelism </li><li><span><span></span></span>We provide all-church\noutreach events and ministries</li><li>We\npursue opportunities to spread the gospel through the establishing of churches\nwhere no gospel witness exists </li></ul><p></p>\n','xhtml','{filedir_1}subnav-ministries-outreach-2x.png','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(30,1,6,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<p>Clearcreek Chapels Worship Ministry\nexists to:</p>\n\n<ol><li>Exalt the name of the Lord\n     through praise and worship</li><li>Lead the Chapel family in\n     meaningful, Christ-centered, God-glorifying worship</li><li>Provide music and lyrics for\n     the Chapel family that magnifies God\'s character and lifts up the truth of\n     God\'s word</li></ol>\n\n<p>The character of our worship will reflect\nthe glory and beauty of God in all that we do. We will use music with songs\nthat release and focus our affections on Christ, led by musicians characterized\nby humility, commitment, and service to others.</p>\n\n<p>We will encourage the people of God to\nworship through Christ-centered, God-exalting lyrics that reflect the truths of\nScripture and the marvelous story of redemption.</p>\n\n<p></p>\n','xhtml','<p>Our\nworship team currently consists of a worship leader, back-up vocalists, piano,\nguitars, keyboards, drums, bass, and violin. <br></p><p>We rehearse Sunday mornings at\n8:30 with the Morning Gathering beginning at 9:30. We also rehearse at 4:30\nSunday afternoons with the Evening Gathering beginning at 6:00 p.m. <br></p><p>We strive\nfor excellence in performance within the talents and abilities God has given\nour team.<br></p>\n','xhtml','{filedir_1}subnav-ministries-worship-icon-2-2x.jpg','none','','','','','','','','','','','','none','','none','','none','','none','','none','','none','','none','','none','','none'),
	(32,1,10,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<h1>Vision\n\n</h1><p>Our vision\nis to work out both the large picture and the immediate texts in Holy Spirit\nenabled exposition and application in the public and private ministry of the\nWord of God.</p>\n<h1>Purpose</h1>\n\n<h4>The Chapel\nPulpit exists and aims:</h4><ul><li>To exalt Christ in\n     the preaching of the Scriptures</li><li>To exposit the\n     Scriptures regularly and systematically</li><li>To edify and\n     encourage believers by the teaching the Scriptures</li><li>To evangelize the\n     lost by preaching the gospel through the Scriptures</li><li>To enable ministry\n     through modeling the use of the Scriptures</li></ul><p></p><h1>Values</h1>\n\n<h4>The Chapel\nPulpit is guided by the following values and treasures:</h4>\n\n<ul><li>We value preaching\n     that is expositional of the text.</li><li>We value preaching\n     that is Christ centered.</li><li>We value preaching\n     through the whole Bible.</li><li>We value preaching\n     that is passionate.</li><li>We value preaching\n     that transforms lives. </li></ul><p>\n</p><h1>Objectives</h1>\n\n<h4>We aim to\nachieve the following goals as God allows and enables:</h4>\n\n<ul><li>To follow the\n     guidelines for exposition in the Supplement.</li><li>To preach the whole\n     Bible in the way taught by Christ and modeled by the apostles.</li><li>To preach with\n     authority, clarity, humility and Spirit enabled passion and power.</li><li>To publish the\n     pulpit ministry as much as possible.</li><li>To be men of the\n     Word as well as men who preach the Word.</li></ul>\n<p></p><h1>Organization</h1>\n\n<h4>We will\nestablish the following core administrative procedures to organize the\nministry:</h4>\n\n<ul><li>The pastor for\n     preaching will be responsible for all pulpit scheduling</li><li>The pastor for\n     preaching will solicit series from the Elders and others</li><li>The elders will\n     submit series proposals as requested</li><li>No less than the\n     present and the next quarter will be fully scheduled.</li><li>The office will\n     maintain the current pulpit schedule.</li><li>Elders unable to\n     meet their obligation will reschedule through the pastor for preaching,\n     and in his absence, the chairman of the elders.</li></ul>\n<p></p><h1>Guidelines for the Exposition of the Word of God</h1>\n\n<h2>Content - Getting it Right</h2>\n\n<h4>Truth  Holding the Line</h4>\n\n<p>We will stay in the line of Scripture, not adding to nor\ntaking away. We will strive to be true to what the text says and intends.</p>\n\n<h4>Themes  Listening to the Symphony</h4>\n\n<p>We will speak in harmony with the large themes of the\nScripture recognizing that God gave us a book in the shape and form that it is\nin. Our emphasis will be on the developing of a Canonical Theology.</p>\n\n<h4>Texts  Following Melodic Lines</h4>\n\n<p>We will sing the song the text does. We will take larger\nsections so that we can follow the thoughts of the author. We will avoid\nmicromanaging the text. We do paragraph by paragraph exposition.</p>\n\n<h4><i>Telos </i> Submitting to Intent</h4>\n\n<p>We use the text the way the author intended. We will strive\nto bow to the texts agenda and not impose our own or use the text to serve our\nagenda.</p>\n\n<h4>Trajectory  Pointing to Christ</h4>\n\n<p>We will see Christ through the text as the author and text\nintend. We will guard against Jesus under every bush while savoring His glory\neverywhere it is.</p>\n\n<h4>Type  Observing the Literature</h4>\n\n<p>We will preach the literature of the Bible as it is. Our\npreaching will be different when we are in different types of Scripture:\nnarrative, poetry, prophesy, epistles, doctrinal. We affirm that the form of\nthe message as well as the content of the message is inspired. </p>\n\n<h4>Terrain  Mapping the Contours</h4>\n\n<p>In our preaching we will follow the texts own structures. In\nstudying and preaching, we will have our terrain following systems tuned high. </p>\n\n<h4>Texture  Feeling the Affections</h4>\n\n<p>We will aim to resonate with the text. We will not preach in\na dry, distanced academic tone. We will say what we are saying with the\naffections and emotions in the text itself.</p>\n\n<h4>Tone  Speaking in its Voice</h4>\n\n<p>We will talk like the text does. We will be story-like in\nnarratives. We will be authoritative where there are imperatives. We will talk\nin the voice the Scripture is.</p>\n\n<h4>Travel  Getting from Then to Now</h4>\n\n<p>We will aim to get our travel instructions from the text\nitself. We aim to help bring the text to the listener and the listener to the\ntext.</p>\n\n<h2>Communication  Getting it Across</h2>\n\n<h4>Authority  Speaking for God</h4>\n\n<p>We aim to speak with the authority of God. We are not having\na conversation, making suggestions, giving optional counsel; preaching is the\none way declaration of Word and Spirit.</p>\n\n<h4>Humility  Depending on the Spirit</h4>\n\n<p>We will humble ourselves under our Redeemer and Ruler so that\nour authority is not arrogance. We will bow with joy to God and His Word so\nthat we are utterly dependent on the Spirit for the power in our preaching and\nfor our listeners.</p>\n\n<h4>Charity  Loving the Listener</h4>\n\n<p>We will preach as Shepherds to the sheep, loving you with all\nwe are. We will then bring the Scriptures with life and love so that even the\nhard medicines are balms in Gilead.</p>\n\n<h4>Clarity  Making it Clear</h4>\n\n<p>We will preach in a way and with words that make the text\nclear. Our eloquence is the beauty of Christ, the truth of the Word, the\ngrandness of the gospel described. We intend for what is in the Bible to become\nclear in the listeners hearts. </p>\n\n<h4>Accessibility  Keeping it Simple</h4>\n\n<p>We will preach in a way that makes the text simpler, not more\ncomplicated. We aim to take the difficult and obscure because of culture and\ndistance and open the door so that the listener will be able to understand it.</p>\n\n<h4>Connectability  Rising from the Text</h4>\n\n<p>We will preach so that what we say is obvious from the text\nwe are preaching. We will pull no exegetical rabbits out of the original\nlanguage hat. We will preach in such a way that the listener will be able to\nsee how we got it and learn how to get it for themselves.</p>\n\n<h4>Visibility  Seeing through Illustration</h4>\n\n<p>We will use illustrations to illustrate. We will use them to\nhelp incarnate texts, to make them living and alive. We will do what the\nincarnation has done: the Word, the Lord Jesus, has become flesh,</p>\n\n<h4>Practicality  Applying its Sense</h4>\n\n<p>We will aim to apply the text. We will not shift the meaning\nor the purpose of the text through application. The text is its own\napplication. It was meant in its original giving to teach and transform the\nhearer and reader. Our application will be right from the text itself.</p>\n\n<h2>Connection  Getting it Through</h2>\n\n<p>We aim to pierce the heart though our preaching to bring\ntransformation in our beliefs and desires, our actions and affections. In doing\nso, these series of couplets express our desire to bring the message of the\nScriptures in the midst of the mess.</p>\n\n<h4>Instruction and Incarnation</h4>\n\n<p>Teaching that has flesh and blood in it.</p>\n\n<h4>Truth and Doing</h4>\n\n<p>Doctrine that has action from it.</p>\n\n<h4>Principle and Practice</h4>\n\n<p>Principles that have practical aim.</p>\n\n<h4>Faith and Love</h4>\n\n<p>All that matters is love working by faith.</p>\n\n<h4>Wisdom and Walk</h4>\n\n<p>Marrying the Word to life that has feet to it.</p>\n\n<p></p>\n','none','<p><p><img src=\"{filedir_1}sermons-philosophy.jpg\" style=\"\"></p></p><blockquote>All Scripture is\nbreathed out by God and profitable for teaching, for reproof, for correction,\nand for training in righteousness, that the man of God may be complete,\nequipped for every good work.<br>I charge you in the\npresence of God and of Christ Jesus, who is to judge the living and the dead,\nand by his appearing and his kingdom: preach the word; be ready in season and\nout of season; reprove, rebuke, and exhort, with complete patience and teaching.&nbsp; For the time is coming when people will not\nendure sound teaching, but having itching ears they will accumulate for\nthemselves teachers to suit their own passions,&nbsp;\nand will turn away from listening to the truth and wander off into\nmyths.&nbsp; As for you, always be\nsober-minded, endure suffering, do the work of an evangelist, fulfill your\nministry.<br>— 2 Timothy 3:16-4:5 (ESV)\n</blockquote>\n\n','none','{filedir_1}subnav-sermons-philosophy-2x.jpg','none','','','','','','','','','','','','','','','','','','','','none','','none','','none','','none','','none'),
	(34,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}elder-mark-s.jpg','none','Pastor for Community and Worship','none','<p>Mark was ordained to\nthe eldership in 2006 and currently serves as the Body of Elders\nSecretary.&nbsp; He is a Chapel worship\nleader.</p><p>\n</p><p></p>\n','none','<p>Mark was born in Wisconsin on June 24, 1962. God graciously allowed him to grow up in a\nChristian home and attend solid Bible-teaching churches. While outwardly\nliving a good life, God continued to draw him and he entrusted his life to\nChrist at the age of 20. Mark attended the University of Wisconsin-Stout\nand graduated in 1985 with a degree in Applied Mathematics and a minor in\nComputer Science. He met and married Sheila in his final year and they\nmoved to Dayton where Mark went to work for NCR. Mark has worked in a\nvariety of companies in the Information Technology (IT) and Software\nfields. He is currently a Senior Consultant for Cincom Systems.</p>\n\n<p>Mark and Sheila have four children and have attended Clearcreek since 1988. Their children are\nLaura (b. 1987), Paul (b. 1990), and twin boys Kyle and Kevin (b. 1994). Mark has been involved in a variety of leadership, teaching, and music ministries over his time at Clearcreek. He currently oversees worship and community\nministries.</p>\n\n','none','Oversees Worship, Flock, Men\'s and Women\'s ministries','none','Miamisburg','none','Schindler','none','','none','','none'),
	(36,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}elder-steve-v.jpg','none','Pastor for Stewardship','none','<p>Pastor Steve has been an elder since May 2008. He enjoys playing drums for the worship team.<br>\n</p>\n','none','<p>Steve was born to\nArvin and Patricia Vaughan in Huntington, W. Va. on February 20, 1962.&nbsp;\nSpending most of his life in Southwest Ohio, Steve attended Nyack College in\nNew York, Cedarville University, and eventually graduated from Oral Roberts\nUniversity in Tulsa Oklahoma in 1987, where Steve graduated with a B.A. in\nTheological and Historical Studies.&nbsp; It was in this degree program where\nSteve discovered the richness of the Reformers and historical Reformation\ntheology. </p>\n<p>Steve has been a\nyouth pastor, a Christian high school Bible teacher and has coached youth and\nhigh school soccer since 1988. He currently owns and operates a financial\nplanning and investment company which was started by his father in the 1976.</p>\n<p>Steve is married to\nHeidi Henkle Vaughan; together they have four children, Petr, Parker, Morgan,\nand Jameson.<br></p>\n\n<p></p>\n','none','Oversees the Finance and Stewardship ministry','none','Waynesville/Lebanon','none','Vaughan','none','','none','','none'),
	(38,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}elder-devon-b.jpg','none','Pastor for Youth','none','<p>Pastor Devon, Chair for the Elders, has been at the Chapel since 1999 and credits his spiritual formation to the work the Lord has done through this local body. He rejoices in the many opportunities to rely fully on the Lords strength that pastoring provides.\n</p><p></p><p></p>\n','none','<p>Devon was born in Toledo, Ohio in 1973 to Christian parents. He is the fifth of eight children. Most of his formative years were spent in southern Michigan where his family lived on a small farm. During this time he was saved through the ministry of a backyard Bible club hosted in his home. In 1999 Devon was married to Beth Irving. They have two young children, Reuben (2009) and Theo (2011), and are strong advocates for Christian families adopting. Trained as a nurse researcher, Devon currently serves as faculty at Wright State University.\n</p><p></p>\n','none','Oversees the Youth Ministry','none','Franklin-Monroe','none','Berry','none','','none','','none'),
	(41,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','none','Office Administrator, Bookkeeper','none','<p>Barb\njoined the office staff in August of 2005. She serves as office\nadministrator,&nbsp; bookkeeper, and as an\nassistant to the Elders. She enjoys learning and developing her skills as she\nassists with various IT tasks. <br></p>','none','<p>\n\n</p><p>Barb was born and\nraised in Dayton, Ohio. After high school she studied to be a Respiratory\nTherapist and worked in homecare and in the hospital setting for over 18 years.\nBarb and her family moved to Waynesville&nbsp;\nin 1998 and in June 1999 she was introduced to the Chapel.&nbsp; </p>\n<p>Barb joined the\noffice staff&nbsp; in August of 2005. Her\nduties include office management, bookkeeping, and assisting the Body of\nElders. Barb is the content publisher for the web development team and assists\nwith general IT tasks.</p>\n<p>Barb is married to\nLinus and they have two grown children, Eric and Kaitlin. They have been\nmembers at Clearcreek Chapel since 2000.</p>\n\n<p></p>\n','none','','none','','none','Berger','none','','none','','none'),
	(48,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}elder-michael-e.jpg','none','Pastor for Children\'s Ministries','none','<p>\n\n<p>Pastor Michael has\nbeen an elder since March of 2011. He serves the body in our counseling and IT\nministries.</p>\n\n</p>','none','<p>\n\n<p>Michael was born and\nraised in West Carrollton, Ohio. He started attending Clearcreek as an\nunbeliever in 1991 with his parents when they moved to Springboro after his\nhigh school graduation.&nbsp; The Clearcreek\nChapel family&nbsp; showed him the love of\nChrist through the loss of his sister, Traci, in a car wreck. Pastor John\nStreet and others encouraged him to attend Cedarville College in January 1993.\nGod saved him a few days later while listening to Dr. Joe Stowell preach from\nPhilippians 2. </p>\n<p>Michael works in IT\nas a Systems Administrator. He is married to Amy and they have four children.</p>\n\n</p>','none','Oversees the Children\'s Ministries','none','Belmont','none','Engle','none','','none','','none'),
	(43,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','none','Counseling Administrator','none','<p>Jayne joined the office staff in 2012 to serve\nalongside the Counseling Staff as the Counseling Administrator.&nbsp; She enjoys working in this ministry and\nassisting those in need of counsel as well as coordinating all aspects of\nClearcreek Chapel\'s counseling ministry.<br></p>\n','none','<p>\n\n<p>Jayne was born and\nraised in the Dayton, OH area.&nbsp; She has\nserved alongside her husband in Youth and Church Planting ministries.&nbsp; She also loves being involved with Women\'s\nMinistries and hosting their small group in their home each week.</p>\n<p>Jayne is married to\nTim, has 2 grown married children and 3 grandchildren.&nbsp; They have been at the Chapel since 2011.</p>\n\n</p>','none','','none','','none','Nixon','none','','none','','none'),
	(44,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}elder-tim-r.jpg','none','Pastor for Missions','none','<p>\n\n<p>Pastor Tim has\nserved in churches in Wisconsin and Ohio. He has served at Clearcreek Chapel\nsince 2010. Pastor Tim is responsible for the overseas missions work of the\nChapel.</p>\n\n</p>','none','<p>\n\n</p><p>Tim is originally\nfrom Wisconsin and moved to southwest Ohio in 2008. He holds a B.S degree in\nengineering from St. Cloud State University and has held numerous engineering\nand supply chain management positions throughout the years. Tim currently works\nfor New Page Corporation in Miamisburg.</p>\n<p>Tim has personally\nbeen involved in missions and church planting work in Kenya, India, and\nRomania. He leads the Chapel missions ministry active in eastern Europe,\nFar East Asia, the Middle East, and Brazil.</p>\n<p>Tim is married to\nStacy who serves as a biblical counselor at the Chapel. They have five grown\nchildren; Marcus, Ben, Alex, Ana, and Nancy.</p>\n\n<p></p>\n','none','Oversees the Missions Ministry','none','Springboro West','none','Rech','none','','none','','none'),
	(45,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','none','Assistant Custodian','none','<p>\n\n<p>Kent began serving as the assistant Custodian in\nJuly of 2012. His duties also include helping with general facility\nmaintenance. Kent is married to Marie and enjoys working on automobiles with\nhis adult sons.</p>\n\n</p>','none','<p>* none *<br></p>\n','none','','none','','none','Voegele','none','','none','','none'),
	(46,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','none','Custodian','none','<p>Marie serves as the\nCustodian for Clearcreek Chapel. She joined the custodial staff in 2008 as an\nassistant and became the custodian in 2012. Marie has been a Chapel member\nsince 1993 and has been active in the Chapel\'s music ministry.</p>\n\n<p></p>\n','none','<p>\n\n</p><p>* none *</p>\n\n<p></p>\n','none','','none','','none','Voegele','none','','none','','none'),
	(47,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','none','Chief Musician','none','<p>\n\n<p>Linda has served the\nChapel as the Chief Musician since 1994. It is her joy to lead the Chapel\nfamily in meaningful, Christ-centered worship each week.</p>\n\n</p>','none','<p>\n\n</p>\n\n<p>Linda was raised in\nMI.&nbsp; She graduated from Cedarville\nCollege with a bachelor\'s degree in Music Education.</p>\n<p>As Chief Musician,\nLinda plans the music for worship gatherings and special events. She loves to\nfind music that is gospel-saturated and that magnifies the great name of our\nSavior, Jesus Christ. Other responsibilities include playing the piano for\ngatherings, leading worship team rehearsals, and scheduling musicians, special\nmusic and offertories.</p>\n<p>Linda and her\nhusband, Greg, have two young adult children.&nbsp;\nThey have been Chapel members since 1992.</p>\n\n\n\n<p></p>\n','none','','none','','none','Frye','none','','none','','none'),
	(49,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','{filedir_1}elder-dale-e.jpg','none','Pastor for Spiritual Development','none','<p>\n\n<p>Pastor Dale was\nordained as an elder in 2001. He has been instrumental in the development of\nministry curriculum.</p>\n\n</p>','none','<p>\n\n<p>Pastor Dale is a\nDayton native and was raised in a home that served the Lord. &nbsp;He received\nB.S., M.S., and M.D. degrees from Wright State University. &nbsp;He is board\ncertified in Pediatrics and Pediatric Emergency Medicine and works at the Soin\nPediatric Trauma and Emergency Center, The Children\'s Medical Center, Dayton,\nOH. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\n&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;\n&nbsp;</p>\n<p>Dale is married to\nSandy and has two sons, Geoffrey and Gregory. &nbsp;He and his family have been\nat Clearcreek Chapel since 1999; he was ordained to Eldership in 2001.<br></p>\n\n</p>','none','Oversees Ministry Coordination and Communication','none','Bellbrook','none','Evans','none','','none','','none'),
	(50,1,12,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','none','Reception Secretary','none','\n\n<p>Catherine joined the\noffice staff in 2012. She is the reception secretary and assistant to Pastor\nDan. Her favorite part of serving the\nChapel family is praying when she receives prayer requests. </p>\n\n\n\n<p></p>\n','none','<p>\n\n</p><p>\n\n<p>Catherine was born\nand raised in Washington state.&nbsp; She\nstudied American Sign Language at California State University, Northridge and\ncoordinates the Chapel\'s ministry to the deaf.</p>\n<p>Catherine met her\nhusband, John, while attending The Master\'s College in CA. They moved to OH in\n2004 after the birth of their daughter.&nbsp;\nThey been&nbsp; Chapel members since\n2005.</p>\n\n</p>\n\n<p></p>\n','none','','none','','none','Ambro','none','','none','','none'),
	(51,1,11,'','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','','<p>Here is a list of upcoming events at Clearcreek Chapel.</p>','none','<p>Thanks to SermonAudio.com for hosting our events calendar.</p>','none','{filedir_1}subnav-location.jpg','none','','','','','','','','','','','','','','','','','','','','','','','','','','none','','none'),
	(52,1,6,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'<p>\n\n<p><b>May 12, 2013</b></p>\n\n<p><b>Announcement: Chapel’s\nPlans for Responding to Numeric Growth</b></p>\n\n<p>In the last three years\nthe Lord has chosen to bless the Chapel with growth in attendance. During this\ntime our&nbsp;Sunday&nbsp;morning gatherings have grown from approximately\n225-250 people to 325-350 people. With this growth has come new realities, one\nof them being the issue of space. Recently, the elders participated in a\nretreat to focus specifically on next steps for the Chapel with regard to\ncurrent and future numeric growth.&nbsp;</p>\n\n<p>Orienting our approach\nto these discussions was another decision made at a retreat about five years\nago - in brief, we committed to becoming commissional as a church. A succinct\nphrase we used to sum up all that this meant was \"maximum truth to maximum\npeople.\" Embedded in this commitment was our determined intention to\nmaintain the Chapel\'s distinctives -&nbsp;expositional preaching,\nChrist-focused exaltation, and intentional evangelism to neighborhoods and\nnations.</p>\n\n<p>As we grow in numbers,\nthere is the real potential that a tension will be introduced between\n\"maximum truth\" and \"maximum people\"... perhaps causing us\nto feel pressured to minimize one for the sake of maximizing the other. It is\nnot a matter of one or the other, however. It is a matter of seeking wisdom\nfrom God to do all that he has called us to do and to do it in a manner that brings\nHim glory and His people good. If we allow a kind of “either – or” thinking to\nshape our actions, we will put ourselves at risk of unbiblical thinking and\nacting.</p>\n\n<p>With these things in\nmind, the Elders have made the following provisional plan to maintain maximum\ntruth to maximum people as we grow. This plan was introduced in flock meetings\non Wednesday,&nbsp;<span>May&nbsp;1st, and we\nare repeating it now in the AM and PM services to make sure that all Chapel\nmembers and attenders are made aware of these potentially historic plans. By\ndoing this, we hope to solicit your, wisdom, input, and feedback as we move\nforward. We also hope to begin preparing the Chapel for the possibility of\nsignificant change.</span></p>\n\n<p>So first, let me tell\nyou what we are not going to do. Many of you know that we have been considering\na \"two-service\" model over the last year or so. After much\ndiscussion, we have decided to not go this direction as we felt it was not\noptimal at this time for maintaining our commitment to maximum truth for maximum\npeople.</p>\n\n<p>If not two services,\nthen what? We are channeling our efforts in two directions&nbsp;as near-term\noptions for addressing our space issues.&nbsp;With regards to this campus and\nour present space... we are planning on the following changes - I say planning\nbecause these plans are subject to change as we receive insight from ministry\nleaders, deacons, and others who serve in the ministries effected.</p>\n\n<p><span>1.<span>&nbsp;\n</span></span>Parking:\nWe are making the necessary inquiries to determine what it would take to expand\nour current parking</p>\n\n<p><span>2.<span>&nbsp;\n</span></span>Nurseries:\nWe are considering a larger room for at least one of the nurseries</p>\n\n<p><span>3.<span>&nbsp;\n</span></span>Auditorium:\nWe will likely be clearing out the mailboxes and tables from along the back\nwalls and adding additional chairs. The mailboxes will be relocated to an area&nbsp;outside\nof the auditorium.</p>\n\n<p><span>4.<span>&nbsp;\n</span></span>Overflow\nroom: We will use the overflow room as a true “overflow” room to ensure that as\nmany seats as possible in the auditorium are utilized.&nbsp;We are also\nreconsidering how best to maximize the effectiveness of our current flock-based\napproach.</p>\n\n<p>All of these changes are focused on optimizing the\nuse of our current space on this campus. We hope that this will bring some\nsmall degree of relief to the congestion while allowing us to be sure that the\nproclamation of truth can be heard by all who desire to hear.</p>\n\n<p>With regards to\nexpanding off-campus we are exploring two options simultaneously.</p>\n\n<p><span>1.<span>&nbsp;\n</span></span>Relocation\nto a larger building. We are exploring the possibility of identifying a\nbuilding in the current locale (within 3-5 miles of our current property) that\nis larger and could accommodate the needs of the Chapel. Concurrently, we are\nexploring the outlook for selling our current property.</p>\n\n<p><span>2.<span>&nbsp;\n</span></span>Strategically\ndevelop a second campus. We are also exploring locations where the present\nministry of the Chapel could be replicated. By the word “strategic,” we mean\nstrategic for gospel witness. Therefore, we will focus on identifying venues\nthat are easily accessible to a large number of Chapel families <i>but </i>where\nno like-minded gospel witness currently exists.</p>\n\n<p>We have tasked Pastors\nRuss and Tim Nixon with gathering the necessary data to make informed decisions\non both of these off-campus options.&nbsp;</p>\n\n<p>With this said, we need\nto make one clarification as different messages went out to different\nflocks.&nbsp;While the off-campus options just mentioned are our primary focus\npresently, they do not necessarily preclude the exploration of other options\nsuch as expanding our current campus by adding more space to this\nbuilding.&nbsp;</p>\n\n<p>This is a lot of\ninformation to take in and undoubtedly will lead to many questions. We want to\nstrongly encourage you to approach your flock elder with your questions. He\nwill be happy to do his best to answer them. Just be prepared - the <i>best</i>\nanswer may often be, \"I don\'t know!\" In any case, we invite your\nfeedback and observations. The more informed the eldership is in this process,\nthe more likely it is we will have the necessary data to make wise decisions.\nSo please work on this with us.</p>\n\n<p>As of 11:00 AM today,\nthis announcement will be posted on the website so that you can return to it\nand think and pray over these potential directions we are considering. We will\nwork hard to communicate regularly, clearly, and consistently&nbsp;and correct\nour communication (as we did today) when necessary.&nbsp;The website will be an\nexcellent resource for keeping all of us on the same page. We ask that if we\ncause confusion, you would seek out your flock elder for explanation. It is our\nstrong desire to serve you well as we move through this phase of the Chapel\'s\nlife together..&nbsp;</p>\n\n<p>Finallly, pray and pray\nmuch. Pray that we as a body would be unified in our commitment to be all that\nwe have been called to in the great&nbsp;commission&nbsp;- that we would\nselflessly sacrifice our own desires where necessary to see His people grow and\nhis kingdom expand.</p>\n\nPastor Devon Berry<br>For the Elders<br></p>','none','<p>...And the Lord added to their number&nbsp;<a href=\"http://biblia.com/books/esv/Ac2.35#\"></a>day by day those who were being saved. (Acts 2:47b [ESV])<br></p>\n','none','{filedir_1}subnav-ministries-expansion-2x.png','none','',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','','none'),
	(54,1,13,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','<p>\n		\n	\n	\n		</p><p>\n			</p><p>\n				</p><p>\n					</p><p>\n						</p><h4>Five classes starting June 2, ending August 25</h4>\n					<p></p>\n				<p></p>\n			<p></p>\n		<p></p>\n		\n	\n	\n		<p>\n			</p><p>\n				</p><p>\n					</p><p>\n						</p><p>The mission of Adult Bible Education is to provide Christ-centered\nteaching and learning in order to equip and mature the community in\nChristlikeness so that the community is grounded in sound doctrine.\nHere is a brief summary of what will be offered in the upcoming weeks,\nbeginning June 2, 2013:&nbsp;</p><p><br></p>\n					<p></p>\n				<p></p>\n			<p></p>\n		<p></p>\n		\n	\n	\n		<p>\n			</p><p>\n				</p><p>\n					</p><h2><p><img src=\"{filedir_1}becoming-commissional.jpg\" style=\"float: left; margin: 0px 20px 10px 0px; \" alt=\"\"></p>Becoming\nCommissional&nbsp;</h2>\n				<p></p>\n			<p></p>\n		<p></p>\n		\n	\n	\n		<p>\n			</p><p>\n				</p><p>\n					</p><p>Becoming Commissional is\ntargeted teaching and training&nbsp;to help our members reach our\nneighborhoods with the gospel\nof Jesus Christ. The class will\ndiscuss the power of the gospel,\nthe patterns of its expansion,\nand some practical methods as families and Flocks. This\nclass will rotate by Flocks. This\nquarter, only Springboro East\nand West Flocks will attend&nbsp;this class.&nbsp;</p><p><br></p><p>\n		\n	\n	\n		</p><p>\n			</p><p>\n				</p><div>\n					<h2><p><img src=\"{filedir_1}gospel-centered-worship.jpg\" style=\"float: left; margin: 0px 20px 10px 0px; \" alt=\"\"></p>Gospel Centered\nWorship&nbsp;</h2>\n				<p></p>\n			<p></p>\n		<p></p>\n		\n	\n	\n		<p>\n			</p><p>\n				</p><p>\n					</p><p>The purpose of this class is&nbsp;to teach a Biblical theology&nbsp;of worship and work through\npersonal and corporate\napplications. The class will&nbsp;take the student through a\nprogression of Old and New\nTestament passages to build&nbsp;a foundational understanding&nbsp;of worship in the context of the\nredemptive storyline of scripture.\nOut of this foundational view,\nthe class will explore various\nquestions around practice and\nparticipation in the church’s\ngathered worship.&nbsp;</p><p><br></p>\n				<p></p>\n			<p></p>\n		<p></p>\n		\n	\n	\n		<p>\n			</p><p>\n				</p><p>\n					</p><h2><p><img src=\"{filedir_1}gospel-love.jpg\" style=\"float: left; margin: 0px 20px 10px 0px; \" alt=\"\"></p>Gospel Love&nbsp;</h2>\n				<p></p>\n			<p></p>\n		<p></p>\n		\n	\n	\n		<p>\n			</p><p>\n				</p><p>\n					</p><p>Gospel Love focuses on how the\ngospel of Jesus Christ frees us\nto love others. Topics covered\ninclude barriers to loving others,\nhow love is an expression&nbsp;of faith, the role of the Spirit&nbsp;in relationships, honesty vs.\njudging, and how to live with\nforgiveness and compassion.\nWorld Harvest Mission adapted\nGospel Love from Neil Williams’\nGospel Transformation\nworkbook.&nbsp;</p><p><i>This class is limited\nto the first 18 people that\nsignup.&nbsp;</i></p><p><i><br></i></p>\n				<p></p>\n			<p></p>\n		<p></p><p></p><p></p></div><p></p><p></p><h2><p><img src=\"{filedir_1}christianity-and-its-competitors.jpg\" style=\"float: left; margin: 0px 20px 10px 0px; \" alt=\"\"></p>Christianity and\nits Competitors&nbsp;</h2>\n				<p></p>\n			<p></p>\n		<p></p>\n		\n	\n	\n		<p>\n			</p><p>\n				</p><p>\n					</p><p>Many current day heresies root\nthemselves in ancient thoughts\nand beliefs. This course seeks\nto not only identify the historical\nroots of these heretical beliefs\nbut also gives a vigorous Biblical\ndefense of the Christian faith.&nbsp;</p><p><i>This class is limited to the first 30\npeople that signup.&nbsp;</i></p><p><br></p><p>\n		\n	\n	\n		</p><p style=\"font-style: italic; \">\n			</p><p>\n				</p><div>\n					<h2><p><img src=\"{filedir_1}chapel-starters.jpg\" style=\"float: left; margin: 0px 20px 10px 0px; \" alt=\"\"></p>Chapel Starters&nbsp;</h2>\n				<p style=\"font-style: italic; \"></p>\n			<p></p>\n		<p></p>\n		\n	\n	\n		<p>\n			</p><p>\n				</p><p>\n					</p><p>This is a five-week introduction to Clearcreek Chapel.\nThis class provides a brief overview of the Chapel’s\nhistory, beliefs, flocks, ministries, and vision. This\nshort course is for those who are new to the Chapel\nexperience and/or have been considering making\nthe Chapel a church home.&nbsp;</p><p></p><p></p><p></p>\n				<p></p>\n			<p></p>\n		</div><br><p></p>\n','none'),
	(53,1,13,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'','none','<p>This is the schedule for summer joint flock meetings for fellowship and prayer. The host flock is responsible for the location and the planning of the meal.</p>\n\n<p>Location information is listed below the schedule.</p>\n\n<table class=\"styled\" cellpadding=\"0\" cellspacing=\"0\">\n  <tbody><tr class=\"even\">\n    <th colspan=\"3\"><h4>June 12</h4></th>\n  </tr>\n  <tr class=\"odd\">\n    <th>Host Flock</th>\n    <th>Guest Flock</th>\n    <th>Location</th>\n  </tr>\n  <tr class=\"even\">\n    <td>Kettering</td>\n    <td>Belmont</td>\n    <td>Delco Park – Shelter #2</td>\n  </tr>\n  <tr class=\"odd\">\n    <td>Centerville</td>\n    <td>Bellbrook</td>\n    <td>Oak Grove Park</td>\n  </tr>\n  <tr class=\"even\">\n    <td>Franklin/Monroe</td>\n    <td>Waynesville/Lebanon</td>\n    <td>Franklin Community Park</td>\n  </tr>\n  <tr class=\"odd\">\n    <td>Miamisburg</td>\n    <td>Springboro West</td>\n    <td>Mark &amp; Sheila Schindler Home</td>\n  </tr>\n  <tr class=\"even\">\n    <td>Springboro East</td>\n    <td>West Carrollton</td>\n    <td>Scott &amp; Stephanie Myers Home</td>\n  </tr>\n</tbody></table>\n\n<table class=\"styled\" cellpadding=\"0\" cellspacing=\"0\">\n  <tbody><tr class=\"odd\">\n    <th colspan=\"3\"><h4>July 10</h4></th>\n  </tr>\n  <tr class=\"even\">\n    <th>Host Flock</th>\n    <th>Guest Flock</th>\n    <th>Location</th>\n  </tr>\n  <tr class=\"odd\">\n    <td>Waynesville/Lebanon</td>\n    <td>Springboro West</td>\n    <td>Armco Park</td>\n  </tr>\n  <tr class=\"even\">\n    <td>Belmont</td>\n    <td>Franklin/Monroe</td>\n    <td>Chapel</td>\n  </tr>\n  <tr class=\"odd\">\n    <td>Springboro East</td>\n    <td>Centerville</td>\n    <td>Stubbs Memorial Park</td>\n  </tr>\n  <tr class=\"even\">\n    <td>Bellbrook</td>\n    <td>Miamisburg</td>\n    <td>6:15pm Magee Park</td>\n  </tr>\n  <tr class=\"odd\">\n    <td>West Carrollton</td>\n    <td>Kettering</td>\n    <td>Wilson Park</td>\n  </tr>\n</tbody></table>\n\n<table class=\"styled\" cellpadding=\"0\" cellspacing=\"0\">\n  <tbody><tr class=\"even\">\n    <th colspan=\"3\"><h4>August 14</h4></th>\n  </tr>\n  <tr class=\"odd\">\n    <th>Host Flock</th>\n    <th>Guest Flock</th>\n    <th>Location</th>\n  </tr>\n  <tr class=\"even\">\n    <td>Springboro West</td>\n    <td>West Carrollton</td>\n    <td>Springboro North Park</td>\n  </tr>\n  <tr class=\"odd\">\n    <td>Kettering</td>\n    <td>Centerville</td>\n    <td>Indian Riffle Park – Shelter #2</td>\n  </tr>\n  <tr class=\"even\">\n    <td>Waynesville/Lebanon</td>\n    <td>Springboro East</td>\n    <td>Armco Park</td>\n  </tr>\n  <tr class=\"odd\">\n    <td>Franklin/Monroe</td>\n    <td>Bellbrook</td>\n    <td>Franklin Community Park</td>\n  </tr>\n  <tr class=\"even\">\n    <td>Belmont</td>\n    <td>Miamisburg</td>\n    <td>Delco Park</td>\n  </tr>\n</tbody></table>\n\n<table class=\"styled\" cellpadding=\"0\" cellspacing=\"0\">\n  <tbody><tr class=\"odd\">\n    <th colspan=\"3\"><h4>September 4</h4></th>\n  </tr>\n  <tr class=\"even\">\n    <th>Host Flock</th>\n    <th>Guest Flock</th>\n    <th>Location</th>\n  </tr>\n  <tr class=\"odd\">\n    <td>West Carrollton</td>\n    <td>Franklin/Monroe</td>\n    <td>Tim &amp; Jayne Nixon Home</td>\n  </tr>\n  <tr class=\"even\">\n    <td>Centerville</td>\n    <td>Belmont</td>\n    <td>Delco Park</td>\n  </tr>\n  <tr class=\"odd\">\n    <td>Miamisburg</td>\n    <td>Kettering</td>\n    <td>Chapel</td>\n  </tr>\n  <tr class=\"even\">\n    <td>Springboro West</td>\n    <td>Springboro East</td>\n    <td>Patricia Allyn Park</td>\n  </tr>\n  <tr class=\"odd\">\n    <td>Bellbrook</td>\n    <td>Waynesville/Lebanon</td>\n    <td>6:15pm Magee Park</td>\n  </tr>\n</tbody></table>\n\n\n<h1>Location Information</h1>\n\n<h4>Delco Park</h4>\n<p><a href=\"http://maps.google.com/maps?q=1700+Woodman+Center+Dr.+Kettering&amp;hl=en&amp;sll=39.622854,-84.205413&amp;sspn=0.22478,0.507088&amp;t=h&amp;gl=us&amp;hnear=1700+Woodman+Center+Dr,+Dayton,+Ohio+45420&amp;z=17\" target=\"_blank\">1700 Woodman Center Dr. Kettering</a><br><b>Directions:</b> Intersection of Dorothy Lane and Woodman Center Drive by Krispy Kreme. Take Woodman Center Drive north toward Kroger and the park entrance is on the left.</p>\n\n<h4>Franklin Community Park</h4>\n<p><a href=\"http://maps.google.com/maps?q=Franklin+Lions+Club+Municipal+Park,+Franklin,+OH+45005&amp;client=safari&amp;oe=UTF-8&amp;safe=active&amp;fb=1&amp;gl=us&amp;hq=Franklin+Lions+Club+Municipal+Park,+Franklin,+OH+45005&amp;hnear=Franklin+Lions+Club+Municipal+Park,+Franklin,+OH+45005&amp;cid=0,0,9532204003978030438&amp;t=h&amp;z=16&amp;iwloc=A\" target=\"_blank\">Franklin Lions Club Municipal Park, Franklin, OH 45005</a><br><b>Directions:</b> I-75 north or south to west on St Rt 123 (exit 36). Park is on right past golf course.</p>\n\n<h4>Indian Riffle Park</h4>\n<p><a href=\"http://maps.google.com/maps?q=2801+E+Stroop+Rd,+Kettering,+OH&amp;hl=en&amp;sll=39.655167,-84.253592&amp;sspn=0.007021,0.015846&amp;t=h&amp;gl=us&amp;hnear=2801+E+Stroop+Rd,+Kettering,+Ohio+45420&amp;z=17\" target=\"_blank\">2801 E Stroop Rd, Kettering, OH</a><br><b>Directions:</b> I-675 north or south to exit 10. Left on Indian Ripple then left on E. Stroop.</p>\n\n<h4>Magee Park</h4>\n<p><a href=\"http://maps.google.com/maps?q=N+39+38.458+W+084+04.736&amp;hl=en&amp;ll=39.640916,-84.079096&amp;spn=0.002483,0.003962&amp;sll=39.666966,-84.085952&amp;sspn=0.002482,0.003962&amp;t=h&amp;gl=us&amp;z=19\" target=\"_blank\">1820 Little Sugarcreek Rd Bellbrook</a><br><b>Directions:</b> 1820 Little Sugarcreek Rd. between St. Rte. 725 &amp; Possum Run Rd. I-675 to Exit 7, north on Wilmington Pike, rt. on Feedwire Rd., rt. on Little Sugarcreek.</p>\n\n<h4>Myers Home</h4>\n<p>Please contact the church office for this address, or find their address listed in the Church Family Directory.</p>\n\n<h4>Nixon Home</h4>\n<p>Please contact the church office for this address, or find their address listed in the Church Family Directory.</p>\n\n<h4>Oak Grove Park</h4>\n<p><a href=\"http://maps.google.com/maps?q=Oak+Grove+Park,+Dayton,+OH+45458&amp;hl=en&amp;ll=39.584631,-84.130265&amp;spn=0.007028,0.015846&amp;sll=39.584987,-84.129009&amp;sspn=0.007028,0.015846&amp;t=h&amp;gl=us&amp;hq=Oak+Grove+Park,+Dayton,+OH+45458&amp;z=17\" target=\"_blank\">1790 E. Social Row Rd. Centerville</a><br><b>Directions from OH-725:</b> south on S. Main/Waynesville Rd. to rt. on Ferry Rd. Right on E. Centerville Rd. briefly then right on Ferry. Left on Wilmington Dayton Rd. then right onto Social Rd. <b>Directions from Yankee St:</b> east on Social Rd. across Rte. 48.</p>\n\n<h4>Patricia Allyn Park</h4>\n<p><a href=\"http://maps.google.com/maps?q=7256+OH+48,+Springboro,+OH+45066&amp;hl=en&amp;sll=39.702933,-84.127856&amp;sspn=0.007016,0.015846&amp;t=h&amp;gl=us&amp;hnear=7256+Ohio+48,+Springboro,+Ohio+45066&amp;z=17\" target=\"_blank\">7256 OH 48, Springboro, OH 45066</a><br><b>Directions:</b> Travel north or south on St. Rte. 48 to 7266 N. St. Rte. 48. The park is on the east side of the road approximately 1⁄2 mile south of St. Rte. 73.</p>\n\n<h4>Schindler Home</h4>\n<p>Please contact the church office for this address,&nbsp;or find their address listed in the latest &nbsp;2013 Church Family Directory.</p>\n\n<h4>Wilson Park</h4>\n<p><a href=\"http://maps.google.com/maps?q=Robert+Wilson+Park,+West+Carrollton,+OH+45449&amp;hl=en&amp;ll=39.655167,-84.253592&amp;spn=0.007021,0.015846&amp;sll=39.653633,-84.248847&amp;sspn=0.007021,0.015846&amp;t=h&amp;gl=us&amp;hq=Robert+Wilson+Park,+West+Carrollton,+OH+45449&amp;z=17\" target=\"_blank\">1226 S. Elm Street, West Carrolton, OH</a><br><b>Directions:</b> Take I-75 to exit 44, west on rt. 725 to north on S. Alex Rd, left on S. Elm Street to 1181 or north on rt. 741 to west on rt. 725.</p>\n','none'),
	(55,1,13,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'',NULL,'June 23, 2013','none','<p>Clearcreek Chapel\'s annual church picnic is Sunday, June 23rd at Camp Chautauqua. Activities for the day include swimming (weather-permitting), miniature golf, bowling, soccer, tennis, basketball and volleyball. Sign up after services this Sunday. For more information please call the church office at 937-885-2143.<br></p>\n','none');

/*!40000 ALTER TABLE `exp_channel_data` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_channel_entries_autosave
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_channel_entries_autosave`;

CREATE TABLE `exp_channel_entries_autosave` (
  `entry_id` int(10) unsigned NOT NULL auto_increment,
  `original_entry_id` int(10) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL default '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL default '0',
  `pentry_id` int(10) NOT NULL default '0',
  `forum_topic_id` int(10) unsigned default NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL default 'n',
  `view_count_one` int(10) unsigned NOT NULL default '0',
  `view_count_two` int(10) unsigned NOT NULL default '0',
  `view_count_three` int(10) unsigned NOT NULL default '0',
  `view_count_four` int(10) unsigned NOT NULL default '0',
  `allow_comments` varchar(1) NOT NULL default 'y',
  `sticky` varchar(1) NOT NULL default 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL default 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL default '0',
  `comment_expiration_date` int(10) NOT NULL default '0',
  `edit_date` bigint(14) default NULL,
  `recent_comment_date` int(10) default NULL,
  `comment_total` int(4) unsigned NOT NULL default '0',
  `entry_data` text,
  PRIMARY KEY  (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_channel_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_channel_fields`;

CREATE TABLE `exp_channel_fields` (
  `field_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_id` int(4) unsigned NOT NULL,
  `field_name` varchar(32) NOT NULL,
  `field_label` varchar(50) NOT NULL,
  `field_instructions` text,
  `field_type` varchar(50) NOT NULL default 'text',
  `field_list_items` text NOT NULL,
  `field_pre_populate` char(1) NOT NULL default 'n',
  `field_pre_channel_id` int(6) unsigned default NULL,
  `field_pre_field_id` int(6) unsigned default NULL,
  `field_related_to` varchar(12) NOT NULL default 'channel',
  `field_related_id` int(6) unsigned NOT NULL default '0',
  `field_related_orderby` varchar(12) NOT NULL default 'date',
  `field_related_sort` varchar(4) NOT NULL default 'desc',
  `field_related_max` smallint(4) NOT NULL default '0',
  `field_ta_rows` tinyint(2) default '8',
  `field_maxl` smallint(3) default NULL,
  `field_required` char(1) NOT NULL default 'n',
  `field_text_direction` char(3) NOT NULL default 'ltr',
  `field_search` char(1) NOT NULL default 'n',
  `field_is_hidden` char(1) NOT NULL default 'n',
  `field_fmt` varchar(40) NOT NULL default 'xhtml',
  `field_show_fmt` char(1) NOT NULL default 'y',
  `field_order` int(3) unsigned NOT NULL,
  `field_content_type` varchar(20) NOT NULL default 'any',
  `field_settings` text,
  PRIMARY KEY  (`field_id`),
  KEY `group_id` (`group_id`),
  KEY `field_type` (`field_type`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_channel_fields` WRITE;
/*!40000 ALTER TABLE `exp_channel_fields` DISABLE KEYS */;

INSERT INTO `exp_channel_fields` (`field_id`, `site_id`, `group_id`, `field_name`, `field_label`, `field_instructions`, `field_type`, `field_list_items`, `field_pre_populate`, `field_pre_channel_id`, `field_pre_field_id`, `field_related_to`, `field_related_id`, `field_related_orderby`, `field_related_sort`, `field_related_max`, `field_ta_rows`, `field_maxl`, `field_required`, `field_text_direction`, `field_search`, `field_is_hidden`, `field_fmt`, `field_show_fmt`, `field_order`, `field_content_type`, `field_settings`)
VALUES
	(27,1,7,'section_banner_text','Section Banner Text','This is the text that is in the image. It will be displayed if the image cannot be loaded.','text','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',2,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(3,1,1,'block_one_headline','Block One Headline','This is the large headline for the first block.','text','','0',0,0,'channel',1,'title','desc',0,6,128,'y','ltr','n','n','none','n',5,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(4,1,1,'block_one_paragraph','Block One Paragraph','This is the short one sentence that describes where this block links to.','text','','0',0,0,'channel',1,'title','desc',0,6,128,'y','ltr','n','n','none','n',6,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(5,1,1,'block_one_link_text','Block One Link Text','This is the short text for the link at the bottom of the block. (i.e. Learn More »)','text','','0',0,0,'channel',1,'title','desc',0,6,128,'y','ltr','n','n','none','n',7,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(6,1,1,'block_one_link','Block One Link','Please paste the relative link that the block will direct to. (i.e. /about)','text','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',8,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(7,1,1,'block_two_headline','Block Two Headline','This is the large headline for the second block.','text','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',9,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(8,1,1,'block_two_paragraph','Block Two Paragraph','This is the short one sentence that describes where this block links to.','text','','0',0,0,'channel',1,'title','desc',0,6,128,'y','ltr','n','n','none','n',10,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(9,1,1,'block_two_link_text','Block Two Link Text','This is the short text for the link at the bottom of the block. (i.e. Learn More »)','text','','0',0,0,'channel',1,'title','desc',0,6,128,'y','ltr','n','n','none','n',11,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(10,1,1,'block_two_link','Block Two Link','Please paste the relative link that the block will direct to. (i.e. /about)','text','','0',0,0,'channel',1,'title','desc',0,6,128,'y','ltr','n','n','none','n',12,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(11,1,1,'block_three_headline','Block Three Headline','This is the large headline for the first block.','text','','0',0,0,'channel',1,'title','desc',0,6,128,'y','ltr','n','n','none','n',13,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(12,1,1,'block_three_paragraph','Block Three Paragraph','This is the short one sentence that describes where this block links to.','text','','0',0,0,'channel',1,'title','desc',0,6,128,'y','ltr','n','n','none','n',14,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(13,1,1,'block_three_link_text','Block Three Link Text','This is the short text for the link at the bottom of the block. (i.e. Learn More »)','text','','0',0,0,'channel',1,'title','desc',0,6,128,'y','ltr','n','n','none','n',15,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(14,1,1,'block_three_link','Block Three Link','Please paste the relative link that the block will direct to. (i.e. /about)','text','','0',0,0,'channel',1,'title','desc',0,6,128,'y','ltr','n','n','none','n',16,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(15,1,8,'message_active','Message Active','Should the emergency message currently be displayed?','radio','Yes\nNo','n',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',1,'any','YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(16,1,8,'message_body','Message Body','Please type the message here. (i.e. No church tonight due to weather.)','textarea','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',2,'any','YTo2OntzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(17,1,7,'section_banner_image','Section Banner Image','This is the main image for the section. Image dimensions: 775px x 324px','file','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',1,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiMSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
	(18,1,7,'section_overview_title','Section Overview Title','One or two word header that will appear in the section overview area.','text','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',3,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(19,1,7,'section_overview_paragraph','Section Overview Paragraph','This is the main paragraph that will display in the overview section of the landing page. (Please keep the length to around 72 words or 400 characters.)','rte','','0',0,0,'channel',2,'title','desc',0,10,128,'y','ltr','n','n','xhtml','n',4,'any','YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
	(20,1,9,'main_body_area','Main Body Area','This is the main body area. ','redactee','','0',0,0,'channel',2,'title','desc',0,10,128,'y','ltr','n','n','none','n',2,'any','YToxNTp7czoxMDoidG9vbGJhcl9pZCI7czoxOiIyIjtzOjE1OiJpbWFnZV91cGxvYWRfaWQiO3M6MToiMSI7czoxNDoiZmlsZV91cGxvYWRfaWQiO3M6MToiMSI7czoxMjoiaW1hZ2VfYnJvd3NlIjtzOjE6IjEiO3M6MjA6ImltYWdlX2Jyb3dzZV9zdWJkaXJzIjtzOjA6IiI7czo5OiJtaW5IZWlnaHQiO3M6MDoiIjtzOjEwOiJhdXRvcmVzaXplIjtzOjE6IlkiO3M6MTA6InNpdGVfcGFnZXMiO3M6NDoibm9uZSI7czoxMzoic2l0ZV9saXN0aW5ncyI7czowOiIiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
	(21,1,9,'sidebar_area','Sidebar Area','This is the sidebar area.','redactee','','0',0,0,'channel',2,'title','desc',0,10,128,'y','ltr','n','n','none','n',3,'any','YToxNTp7czoxMDoidG9vbGJhcl9pZCI7czoxOiIyIjtzOjE1OiJpbWFnZV91cGxvYWRfaWQiO3M6MToiMSI7czoxNDoiZmlsZV91cGxvYWRfaWQiO3M6MToiMSI7czoxMjoiaW1hZ2VfYnJvd3NlIjtzOjE6IjEiO3M6MjA6ImltYWdlX2Jyb3dzZV9zdWJkaXJzIjtzOjA6IiI7czo5OiJtaW5IZWlnaHQiO3M6MDoiIjtzOjEwOiJhdXRvcmVzaXplIjtzOjE6IlkiO3M6MTA6InNpdGVfcGFnZXMiO3M6NDoibm9uZSI7czoxMzoic2l0ZV9saXN0aW5ncyI7czowOiIiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
	(22,1,9,'preview_image','Preview Image','This is the preview image for the page. Image dimensions: 186px x 158px','file','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',1,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiMSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
	(23,1,1,'home_overview_headline','Home Overview Headline','This is the intro headline on the homepage.','text','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',3,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(24,1,1,'home_overview_paragraph','Home Overview Paragraph','This is the paragraph that displays under the homepage overview headline. (Please keep the length to around 72 words or 400 characters.)','rte','','0',0,0,'channel',2,'title','desc',0,10,128,'y','ltr','n','n','xhtml','n',4,'any','YTo4OntzOjI0OiJydGVfZmllbGRfdGV4dF9kaXJlY3Rpb24iO3M6MzoibHRyIjtzOjExOiJydGVfdGFfcm93cyI7czoyOiIxMCI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
	(25,1,10,'home_banner_image','Homepage Banner Image','This is the banner image that will appear on the homepage.','file','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',1,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiMSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
	(26,1,10,'home_banner_text','Homepage Banner Text','This is the text that is in the image. It will be displayed if the image cannot be loaded.','text','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',2,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(28,1,11,'leader_image','Leader Image','This is the image for the person\'s bio. It will also be used as the preview image.','file','','0',0,0,'channel',2,'title','desc',0,6,128,'n','ltr','n','n','none','n',5,'any','YTo4OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6NToiaW1hZ2UiO3M6MTk6ImFsbG93ZWRfZGlyZWN0b3JpZXMiO3M6MToiMSI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9'),
	(29,1,11,'position_title','Position Title','This is the title of the persons position.','text','','0',0,0,'channel',2,'title','desc',0,6,128,'n','ltr','n','n','none','n',2,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(30,1,11,'bio_preview_overview','Bio Preview Overview','This is the short one-to-two sentence preview that will display on the listing page.','redactee','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',6,'any','YToxNTp7czoxMDoidG9vbGJhcl9pZCI7czoxOiIyIjtzOjE1OiJpbWFnZV91cGxvYWRfaWQiO3M6NDoibm9uZSI7czoxNDoiZmlsZV91cGxvYWRfaWQiO3M6NDoibm9uZSI7czoxMjoiaW1hZ2VfYnJvd3NlIjtzOjQ6Im5vbmUiO3M6MjA6ImltYWdlX2Jyb3dzZV9zdWJkaXJzIjtzOjA6IiI7czo5OiJtaW5IZWlnaHQiO3M6MDoiIjtzOjEwOiJhdXRvcmVzaXplIjtzOjE6IlkiO3M6MTA6InNpdGVfcGFnZXMiO3M6NDoibm9uZSI7czoxMzoic2l0ZV9saXN0aW5ncyI7czowOiIiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
	(31,1,11,'full_bio','Full Bio','This is the full bio that will be displayed on the profile detail page.','redactee','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',7,'any','YToxNTp7czoxMDoidG9vbGJhcl9pZCI7czoxOiIyIjtzOjE1OiJpbWFnZV91cGxvYWRfaWQiO3M6NDoibm9uZSI7czoxNDoiZmlsZV91cGxvYWRfaWQiO3M6NDoibm9uZSI7czoxMjoiaW1hZ2VfYnJvd3NlIjtzOjQ6Im5vbmUiO3M6MjA6ImltYWdlX2Jyb3dzZV9zdWJkaXJzIjtzOjA6IiI7czo5OiJtaW5IZWlnaHQiO3M6MDoiIjtzOjEwOiJhdXRvcmVzaXplIjtzOjE6IlkiO3M6MTA6InNpdGVfcGFnZXMiO3M6NDoibm9uZSI7czoxMzoic2l0ZV9saXN0aW5ncyI7czowOiIiO3M6MTg6ImZpZWxkX3Nob3dfc21pbGV5cyI7czoxOiJuIjtzOjE5OiJmaWVsZF9zaG93X2dsb3NzYXJ5IjtzOjE6Im4iO3M6MjE6ImZpZWxkX3Nob3dfc3BlbGxjaGVjayI7czoxOiJuIjtzOjI2OiJmaWVsZF9zaG93X2Zvcm1hdHRpbmdfYnRucyI7czoxOiJuIjtzOjI0OiJmaWVsZF9zaG93X2ZpbGVfc2VsZWN0b3IiO3M6MToibiI7czoyMDoiZmllbGRfc2hvd193cml0ZW1vZGUiO3M6MToibiI7fQ=='),
	(32,1,11,'oversite_area','Oversite Area','This is the area of oversite for the Elder. This can be blank for non Elders.','text','','0',0,0,'channel',2,'title','desc',0,6,128,'n','ltr','n','n','none','n',4,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(33,1,11,'elders_flock','Flock ','This is the flock that the Elder is the leader over. This can be blank for non Elders.','text','','0',0,0,'channel',2,'title','desc',0,6,128,'n','ltr','n','n','none','n',3,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(34,1,11,'last_name','Last name','This is the persons last name.','text','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',1,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(35,1,6,'event_date','Event Date','This is an optional field if this is an event with a particular date. For example: June 9, 2013','text','','0',0,0,'channel',2,'title','desc',0,6,128,'n','ltr','n','n','none','n',1,'any','YTo3OntzOjE4OiJmaWVsZF9jb250ZW50X3R5cGUiO3M6MzoiYWxsIjtzOjE4OiJmaWVsZF9zaG93X3NtaWxleXMiO3M6MToibiI7czoxOToiZmllbGRfc2hvd19nbG9zc2FyeSI7czoxOiJuIjtzOjIxOiJmaWVsZF9zaG93X3NwZWxsY2hlY2siO3M6MToibiI7czoyNjoiZmllbGRfc2hvd19mb3JtYXR0aW5nX2J0bnMiO3M6MToibiI7czoyNDoiZmllbGRfc2hvd19maWxlX3NlbGVjdG9yIjtzOjE6Im4iO3M6MjA6ImZpZWxkX3Nob3dfd3JpdGVtb2RlIjtzOjE6Im4iO30='),
	(36,1,6,'news_body','Body','This is the description for the news item or event.','redactee','','0',0,0,'channel',2,'title','desc',0,6,128,'y','ltr','n','n','none','n',2,'any','YToxNTp7czoxMDoidG9vbGJhcl9pZCI7czoxOiIyIjtzOjE1OiJpbWFnZV91cGxvYWRfaWQiO3M6MToiMSI7czoxNDoiZmlsZV91cGxvYWRfaWQiO3M6MToiMSI7czoxMjoiaW1hZ2VfYnJvd3NlIjtzOjM6ImFsbCI7czoyMDoiaW1hZ2VfYnJvd3NlX3N1YmRpcnMiO3M6MDoiIjtzOjk6Im1pbkhlaWdodCI7czowOiIiO3M6MTA6ImF1dG9yZXNpemUiO3M6MToiWSI7czoxMDoic2l0ZV9wYWdlcyI7czo0OiJub25lIjtzOjEzOiJzaXRlX2xpc3RpbmdzIjtzOjA6IiI7czoxODoiZmllbGRfc2hvd19zbWlsZXlzIjtzOjE6Im4iO3M6MTk6ImZpZWxkX3Nob3dfZ2xvc3NhcnkiO3M6MToibiI7czoyMToiZmllbGRfc2hvd19zcGVsbGNoZWNrIjtzOjE6Im4iO3M6MjY6ImZpZWxkX3Nob3dfZm9ybWF0dGluZ19idG5zIjtzOjE6Im4iO3M6MjQ6ImZpZWxkX3Nob3dfZmlsZV9zZWxlY3RvciI7czoxOiJuIjtzOjIwOiJmaWVsZF9zaG93X3dyaXRlbW9kZSI7czoxOiJuIjt9');

/*!40000 ALTER TABLE `exp_channel_fields` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_channel_member_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_channel_member_groups`;

CREATE TABLE `exp_channel_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `channel_id` int(6) unsigned NOT NULL,
  PRIMARY KEY  (`group_id`,`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_channel_member_groups` WRITE;
/*!40000 ALTER TABLE `exp_channel_member_groups` DISABLE KEYS */;

INSERT INTO `exp_channel_member_groups` (`group_id`, `channel_id`)
VALUES
	(6,1),
	(6,2),
	(6,5),
	(6,6),
	(6,7),
	(6,8),
	(6,9),
	(6,10),
	(6,11),
	(6,12),
	(6,13),
	(7,1),
	(7,2),
	(7,5),
	(7,6),
	(7,7),
	(7,8),
	(7,9),
	(7,10),
	(7,11),
	(7,12),
	(7,13),
	(8,1),
	(8,2),
	(8,5),
	(8,6),
	(8,7),
	(8,8),
	(8,9),
	(8,10),
	(8,11),
	(8,12),
	(8,13),
	(9,1),
	(9,2),
	(9,5),
	(9,6),
	(9,7),
	(9,8),
	(9,9),
	(9,10),
	(9,11),
	(9,12),
	(9,13);

/*!40000 ALTER TABLE `exp_channel_member_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_channel_titles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_channel_titles`;

CREATE TABLE `exp_channel_titles` (
  `entry_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL default '0',
  `pentry_id` int(10) NOT NULL default '0',
  `forum_topic_id` int(10) unsigned default NULL,
  `ip_address` varchar(45) NOT NULL,
  `title` varchar(100) NOT NULL,
  `url_title` varchar(75) NOT NULL,
  `status` varchar(50) NOT NULL,
  `versioning_enabled` char(1) NOT NULL default 'n',
  `view_count_one` int(10) unsigned NOT NULL default '0',
  `view_count_two` int(10) unsigned NOT NULL default '0',
  `view_count_three` int(10) unsigned NOT NULL default '0',
  `view_count_four` int(10) unsigned NOT NULL default '0',
  `allow_comments` varchar(1) NOT NULL default 'y',
  `sticky` varchar(1) NOT NULL default 'n',
  `entry_date` int(10) NOT NULL,
  `dst_enabled` varchar(1) NOT NULL default 'n',
  `year` char(4) NOT NULL,
  `month` char(2) NOT NULL,
  `day` char(3) NOT NULL,
  `expiration_date` int(10) NOT NULL default '0',
  `comment_expiration_date` int(10) NOT NULL default '0',
  `edit_date` bigint(14) default NULL,
  `recent_comment_date` int(10) default NULL,
  `comment_total` int(4) unsigned NOT NULL default '0',
  PRIMARY KEY  (`entry_id`),
  KEY `channel_id` (`channel_id`),
  KEY `author_id` (`author_id`),
  KEY `url_title` (`url_title`),
  KEY `status` (`status`),
  KEY `entry_date` (`entry_date`),
  KEY `expiration_date` (`expiration_date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_channel_titles` WRITE;
/*!40000 ALTER TABLE `exp_channel_titles` DISABLE KEYS */;

INSERT INTO `exp_channel_titles` (`entry_id`, `site_id`, `channel_id`, `author_id`, `pentry_id`, `forum_topic_id`, `ip_address`, `title`, `url_title`, `status`, `versioning_enabled`, `view_count_one`, `view_count_two`, `view_count_three`, `view_count_four`, `allow_comments`, `sticky`, `entry_date`, `dst_enabled`, `year`, `month`, `day`, `expiration_date`, `comment_expiration_date`, `edit_date`, `recent_comment_date`, `comment_total`)
VALUES
	(1,1,1,1,0,0,'108.254.190.58','Home','home','open','y',0,0,0,0,'n','n',1361929469,'n','2013','02','27',0,0,20130409144130,0,0),
	(2,1,7,1,0,0,'70.63.34.233','About Us','about-us','open','y',0,0,0,0,'n','n',1362711646,'n','2013','03','08',0,0,20130510090947,0,0),
	(3,1,9,1,0,0,'107.9.12.83','Savoring The Supremacy of God','savoring-the-supremacy-of-god','open','y',0,0,0,0,'y','n',1362876083,'n','2013','03','10',0,0,20130319003424,0,0),
	(4,1,9,1,0,0,'107.9.12.83','Shaping the People of God','shaping-the-people-of-god','open','y',0,0,0,0,'y','n',1362876126,'n','2013','03','10',0,0,20130319003407,0,0),
	(5,1,9,1,0,0,'107.9.12.83','Spreading the Gospel of God','spreading-the-gospel-of-god','open','y',0,0,0,0,'y','n',1362876213,'n','2013','03','10',0,0,20130319003334,0,0),
	(6,1,9,1,0,0,'107.9.12.83','Studying the Word of God','studying-the-word-of-god','open','y',0,0,0,0,'y','n',1362876229,'n','2013','03','10',0,0,20130319003350,0,0),
	(8,1,7,1,0,0,'108.254.190.58','Sermons','sermons','open','y',0,0,0,0,'n','n',1362876577,'n','2013','03','10',0,0,20130514174138,0,0),
	(9,1,7,1,0,0,'108.254.190.58','Flocks','flocks','open','y',0,0,0,0,'y','n',1362876625,'n','2013','03','10',0,0,20130325122426,0,0),
	(10,1,7,1,0,0,'107.9.25.183','Ministries','ministries','open','y',0,0,0,0,'n','n',1362876599,'n','2013','03','10',0,0,20130406000200,0,0),
	(11,1,2,1,0,0,'108.254.190.58','Network Associations','network-associations','open','y',0,0,0,0,'n','n',1362876905,'n','2013','03','10',0,0,20130406221406,0,0),
	(12,1,2,1,0,0,'108.254.190.58','Leaders','leaders','open','y',0,0,0,0,'n','n',1363046712,'n','2013','03','12',0,0,20130406182213,0,0),
	(13,1,2,1,0,0,'108.254.190.58','Book of Faith & Order','book-of-faith-and-order','open','y',0,0,0,0,'n','n',1363046585,'n','2013','03','12',0,0,20130406000606,0,0),
	(14,1,2,1,0,0,'70.60.44.186','Mission & Vision','mission-vision','open','y',0,0,0,0,'n','n',1363046664,'n','2013','03','12',0,0,20130409175725,0,0),
	(15,1,2,1,0,0,'108.254.190.58','Gatherings','gatherings','open','y',0,0,0,0,'n','n',1363046804,'n','2013','03','12',0,0,20130707194045,0,0),
	(16,1,2,1,0,0,'70.60.44.186','Location','location','open','y',0,0,0,0,'n','n',1363046809,'n','2013','03','12',0,0,20130408180350,0,0),
	(17,1,5,1,0,0,'70.60.44.186','Flock Groups','flock-groups','open','y',0,0,0,0,'n','n',1363650966,'n','2013','03','18',0,0,20130528154807,0,0),
	(18,1,5,1,0,0,'108.254.190.58','Strategies','strategies','open','y',0,0,0,0,'n','n',1363651190,'n','2013','03','18',0,0,20130406220851,0,0),
	(19,1,5,1,0,0,'108.254.190.58','Values','values','open','y',0,0,0,0,'n','n',1363651256,'n','2013','03','19',0,0,20130406220557,0,0),
	(20,1,5,1,0,0,'108.254.190.58','Vision','flocks-vision','open','y',0,0,0,0,'n','n',1363651283,'n','2013','03','19',0,0,20130406220624,0,0),
	(21,1,10,1,0,0,'108.254.190.58','Sermon Repository','sermon-repository','open','y',0,0,0,0,'n','n',1363697034,'n','2013','03','19',0,0,20130514174055,0,0),
	(22,1,10,1,0,0,'108.254.190.58','Key Series','key-series','open','y',0,0,0,0,'n','n',1363697131,'n','2013','03','19',0,0,20130406214532,0,0),
	(23,1,10,1,0,0,'70.60.44.186','Next Sunday\'s Sermons','next-weeks-sermons','open','y',0,0,0,0,'n','n',1363697342,'n','2013','03','19',0,0,20130718152403,0,0),
	(24,1,10,1,0,0,'70.60.44.186','Last Sunday\'s Sermons','this-weeks-sermons','open','y',0,0,0,0,'n','n',1363697352,'n','2013','03','19',0,0,20130718152613,0,0),
	(25,1,7,1,0,0,'108.254.190.58','News & Events','news-events','open','y',0,0,0,0,'n','n',1364150464,'n','2013','03','24',0,0,20130514173505,0,0),
	(26,1,6,1,0,0,'70.60.44.186','Counseling','counseling','open','y',0,0,0,0,'n','n',1364151750,'n','2013','03','24',0,0,20130523154531,0,0),
	(27,1,6,1,0,0,'70.60.44.186','Discipleship','discipleship','open','y',0,0,0,0,'n','n',1364151993,'n','2013','03','24',0,0,20130410164634,0,0),
	(28,1,6,1,0,0,'71.64.159.80','Education','education','open','y',0,0,0,0,'n','n',1364152064,'n','2013','03','24',0,0,20130523231345,0,0),
	(29,1,6,1,0,0,'71.64.159.80','Outreach','outreach','open','y',0,0,0,0,'n','n',1364152068,'n','2013','03','24',0,0,20130406232049,0,0),
	(30,1,6,1,0,0,'70.60.44.186','Worship','worship','open','y',0,0,0,0,'n','n',1364152082,'n','2013','03','24',0,0,20130523150503,0,0),
	(42,1,12,5,0,0,'70.60.44.186','Anita','anita','open','y',0,0,0,0,'y','n',1365089258,'n','2013','04','04',0,0,20130404152738,0,0),
	(32,1,10,1,0,0,'70.60.44.186','Philosophy','philosophy','open','y',0,0,0,0,'n','n',1363572314,'n','2013','03','18',0,0,20130410163415,0,0),
	(33,1,12,2,0,0,'108.254.190.58','Russ','russ-kennedy','open','y',0,0,0,0,'n','n',1357027222,'n','2013','01','01',0,0,20130405234123,0,0),
	(34,1,12,2,0,0,'108.254.190.58','Mark','mark-schindler','open','y',0,0,0,0,'n','n',1357016410,'n','2013','01','01',0,0,20130405234011,0,0),
	(41,1,12,5,0,0,'71.64.159.80','Barb','barb','open','y',0,0,0,0,'n','n',1365089112,'n','2013','04','04',0,0,20130405183713,0,0),
	(35,1,12,5,0,0,'70.60.44.186','Daniel','daniel-turner','open','y',0,0,0,0,'n','n',1357012825,'n','2013','01','01',0,0,20130528154326,0,0),
	(36,1,12,5,0,0,'108.254.190.58','Stevan','stevan-vaughan','open','y',0,0,0,0,'n','n',1357009241,'n','2013','01','01',0,0,20130405233942,0,0),
	(37,1,12,5,0,0,'70.60.44.186','Tim','tim-nixon','open','y',0,0,0,0,'n','n',1357023616,'n','2013','01','01',0,0,20130626152217,0,0),
	(38,1,12,5,0,0,'108.254.190.58','Devon','devon-berry','open','y',0,0,0,0,'n','n',1357041651,'n','2013','01','01',0,0,20130405234252,0,0),
	(39,1,12,5,0,0,'70.60.44.186','Chad','chad-bresson','closed','y',0,0,0,0,'n','n',1357038029,'n','2013','01','01',1371548429,0,20130618135930,0,0),
	(43,1,12,5,0,0,'71.64.159.80','Jayne','jayne','open','y',0,0,0,0,'n','n',1365089290,'n','2013','04','04',0,0,20130405203911,0,0),
	(44,1,12,5,0,0,'108.254.190.58','Tim','tim','open','y',0,0,0,0,'n','n',1357020042,'n','2013','01','01',0,0,20130405234043,0,0),
	(45,1,12,5,0,0,'71.64.159.80','Kent','kent','open','y',0,0,0,0,'n','n',1365100415,'n','2013','04','04',0,0,20130405184236,0,0),
	(46,1,12,5,0,0,'71.64.159.80','Marie','marie','open','y',0,0,0,0,'n','n',1365100592,'n','2013','04','04',0,0,20130405184133,0,0),
	(47,1,12,5,0,0,'71.64.159.80','Linda','linda','open','y',0,0,0,0,'n','n',1365101142,'n','2013','04','04',0,0,20130405183843,0,0),
	(48,1,12,5,0,0,'70.60.44.186','Michael','michael','open','y',0,0,0,0,'n','n',1357034434,'n','2013','01','01',0,0,20130626152335,0,0),
	(49,1,12,5,0,0,'70.60.44.186','Dale G.','dale-g','open','y',0,0,0,0,'n','n',1357030807,'n','2013','01','01',0,0,20130626152608,0,0),
	(50,1,12,5,0,0,'70.60.44.186','Catherine','catherine','open','y',0,0,0,0,'n','n',1365102057,'n','2013','04','04',0,0,20130408180858,0,0),
	(51,1,11,1,0,0,'108.254.190.58','Upcoming Events','upcoming-events','open','y',0,0,0,0,'n','n',1365192797,'n','2013','04','05',0,0,20130405201317,0,0),
	(52,1,6,1,0,NULL,'188.25.230.158','Expansion','expansion','open','y',0,0,0,0,'n','n',1364151614,'n','2013','03','24',0,0,20130512194015,0,0),
	(55,1,13,1,0,NULL,'70.60.44.186','Church Picnic','church-picnic','open','y',0,0,0,0,'n','n',1369320580,'n','2013','05','23',1372070680,0,20130611141541,0,0),
	(53,1,13,1,0,NULL,'70.60.44.186','2013 Summer Flocks','2013-summer-flocks','open','y',0,0,0,0,'n','n',1368234800,'n','2013','05','11',0,0,20130522134621,0,0),
	(54,1,13,1,0,NULL,'108.254.190.58','Adult Bible Education Classes - Summer Quarter 2013','adult-bible-education-classes-summer-quarter-2013','open','y',0,0,0,0,'n','n',1368556535,'n','2013','05','14',0,0,20130514185636,0,0),
	(56,1,12,5,0,NULL,'70.60.44.186','Greg','greg-simmons','open','y',0,0,0,0,'n','n',1369754847,'n','2013','05','28',0,0,20130529153128,0,0);

/*!40000 ALTER TABLE `exp_channel_titles` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_channels
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_channels`;

CREATE TABLE `exp_channels` (
  `channel_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `channel_name` varchar(40) NOT NULL,
  `channel_title` varchar(100) NOT NULL,
  `channel_url` varchar(100) NOT NULL,
  `channel_description` varchar(225) default NULL,
  `channel_lang` varchar(12) NOT NULL,
  `total_entries` mediumint(8) NOT NULL default '0',
  `total_comments` mediumint(8) NOT NULL default '0',
  `last_entry_date` int(10) unsigned NOT NULL default '0',
  `last_comment_date` int(10) unsigned NOT NULL default '0',
  `cat_group` varchar(255) default NULL,
  `status_group` int(4) unsigned default NULL,
  `deft_status` varchar(50) NOT NULL default 'open',
  `field_group` int(4) unsigned default NULL,
  `search_excerpt` int(4) unsigned default NULL,
  `deft_category` varchar(60) default NULL,
  `deft_comments` char(1) NOT NULL default 'y',
  `channel_require_membership` char(1) NOT NULL default 'y',
  `channel_max_chars` int(5) unsigned default NULL,
  `channel_html_formatting` char(4) NOT NULL default 'all',
  `channel_allow_img_urls` char(1) NOT NULL default 'y',
  `channel_auto_link_urls` char(1) NOT NULL default 'n',
  `channel_notify` char(1) NOT NULL default 'n',
  `channel_notify_emails` varchar(255) default NULL,
  `comment_url` varchar(80) default NULL,
  `comment_system_enabled` char(1) NOT NULL default 'y',
  `comment_require_membership` char(1) NOT NULL default 'n',
  `comment_use_captcha` char(1) NOT NULL default 'n',
  `comment_moderate` char(1) NOT NULL default 'n',
  `comment_max_chars` int(5) unsigned default '5000',
  `comment_timelock` int(5) unsigned NOT NULL default '0',
  `comment_require_email` char(1) NOT NULL default 'y',
  `comment_text_formatting` char(5) NOT NULL default 'xhtml',
  `comment_html_formatting` char(4) NOT NULL default 'safe',
  `comment_allow_img_urls` char(1) NOT NULL default 'n',
  `comment_auto_link_urls` char(1) NOT NULL default 'y',
  `comment_notify` char(1) NOT NULL default 'n',
  `comment_notify_authors` char(1) NOT NULL default 'n',
  `comment_notify_emails` varchar(255) default NULL,
  `comment_expiration` int(4) unsigned NOT NULL default '0',
  `search_results_url` varchar(80) default NULL,
  `ping_return_url` varchar(80) default NULL,
  `show_button_cluster` char(1) NOT NULL default 'y',
  `rss_url` varchar(80) default NULL,
  `enable_versioning` char(1) NOT NULL default 'n',
  `max_revisions` smallint(4) unsigned NOT NULL default '10',
  `default_entry_title` varchar(100) default NULL,
  `url_title_prefix` varchar(80) default NULL,
  `live_look_template` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`channel_id`),
  KEY `cat_group` (`cat_group`),
  KEY `status_group` (`status_group`),
  KEY `field_group` (`field_group`),
  KEY `channel_name` (`channel_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_channels` WRITE;
/*!40000 ALTER TABLE `exp_channels` DISABLE KEYS */;

INSERT INTO `exp_channels` (`channel_id`, `site_id`, `channel_name`, `channel_title`, `channel_url`, `channel_description`, `channel_lang`, `total_entries`, `total_comments`, `last_entry_date`, `last_comment_date`, `cat_group`, `status_group`, `deft_status`, `field_group`, `search_excerpt`, `deft_category`, `deft_comments`, `channel_require_membership`, `channel_max_chars`, `channel_html_formatting`, `channel_allow_img_urls`, `channel_auto_link_urls`, `channel_notify`, `channel_notify_emails`, `comment_url`, `comment_system_enabled`, `comment_require_membership`, `comment_use_captcha`, `comment_moderate`, `comment_max_chars`, `comment_timelock`, `comment_require_email`, `comment_text_formatting`, `comment_html_formatting`, `comment_allow_img_urls`, `comment_auto_link_urls`, `comment_notify`, `comment_notify_authors`, `comment_notify_emails`, `comment_expiration`, `search_results_url`, `ping_return_url`, `show_button_cluster`, `rss_url`, `enable_versioning`, `max_revisions`, `default_entry_title`, `url_title_prefix`, `live_look_template`)
VALUES
	(1,1,'home','Home','http://www.clearcreekchapel.org/index.php','','en',1,0,1361929469,0,'',1,'open',1,0,'','y','y',0,'all','y','n','n','','','y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','n',10,'','',0),
	(2,1,'about','About','http://www.clearcreekchapel.org/index.php','','en',6,0,1363046809,0,'',1,'open',9,0,'','y','y',0,'all','y','n','n','','','y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','n',10,'','',0),
	(9,1,'homepage_hero','Homepage Hero Image','http://www.clearcreekchapel.org/index.php','','en',4,0,1362876229,0,'',1,'open',10,0,'','y','y',0,'all','y','n','n','','','y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','n',10,'','',0),
	(5,1,'flocks','Flocks','http://www.clearcreekchapel.org/index.php','','en',4,0,1363651283,0,'',1,'open',9,0,'','y','y',0,'all','y','n','n','','','y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','n',10,'','',0),
	(6,1,'ministries','Ministries','http://www.clearcreekchapel.org/index.php','','en',6,0,1364152082,0,'',1,'open',9,0,'','y','y',0,'all','y','n','n','','','y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','n',10,'','',0),
	(7,1,'landing_pages','Landing Pages','http://www.clearcreekchapel.org/index.php','','en',5,0,1364150464,0,'',1,'open',7,0,'','y','y',0,'all','y','n','n','','','y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','n',10,'','',0),
	(8,1,'emergency_message','Emergency Message','http://www.clearcreekchapel.org/index.php','','en',0,0,0,0,'',1,'open',8,0,'','y','y',0,'all','y','n','n','','','y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','n',10,'','',0),
	(10,1,'sermons','Sermons','http://www.clearcreekchapel.org/index.php','','en',5,0,1363697352,0,'',1,'open',9,0,'','y','y',0,'all','y','n','n','','','y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','n',10,'','',0),
	(11,1,'news','News','http://www.clearcreekchapel.org/index.php','','en',1,0,1365192797,0,'',1,'open',9,0,'','y','y',0,'all','y','n','n','','','y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','n',10,'','',0),
	(12,1,'leaders','Leaders','http://www.clearcreekchapel.org/','','en',17,0,1369754847,0,'1',1,'open',11,0,'','y','y',0,'all','y','n','n','','','y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n','',0,'','','y','','n',10,'','',0),
	(13,1,'news_listing','News Listing','http://www.clearcreekchapel.org/',NULL,'en',3,0,1369320580,0,'',1,'open',6,NULL,NULL,'y','y',NULL,'all','y','n','n',NULL,NULL,'y','n','n','n',5000,0,'y','xhtml','safe','n','y','n','n',NULL,0,NULL,NULL,'y',NULL,'n',10,'','',0);

/*!40000 ALTER TABLE `exp_channels` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_cp_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_cp_log`;

CREATE TABLE `exp_cp_log` (
  `id` int(10) NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `member_id` int(10) unsigned NOT NULL,
  `username` varchar(32) NOT NULL,
  `ip_address` varchar(45) NOT NULL default '0',
  `act_date` int(10) NOT NULL,
  `action` varchar(200) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_cp_log` WRITE;
/*!40000 ALTER TABLE `exp_cp_log` DISABLE KEYS */;

INSERT INTO `exp_cp_log` (`id`, `site_id`, `member_id`, `username`, `ip_address`, `act_date`, `action`)
VALUES
	(1,1,1,'jondaiello','107.9.12.83',1361660508,'Logged in'),
	(2,1,1,'jondaiello','107.9.12.83',1361660518,'Field Group Created:&nbsp;Home'),
	(3,1,1,'jondaiello','67.39.17.22',1361881997,'Logged in'),
	(4,1,1,'jondaiello','67.39.17.22',1361887421,'Logged in'),
	(5,1,1,'jondaiello','67.39.17.22',1361887577,'Channel Created:&nbsp;&nbsp;Home'),
	(6,1,1,'jondaiello','107.9.12.83',1361889754,'Logged in'),
	(7,1,1,'jondaiello','107.9.12.83',1361889811,'Member profile created:&nbsp;&nbsp;russ.kennedy'),
	(8,1,2,'russ.kennedy','70.60.44.186',1361916420,'Logged in'),
	(9,1,1,'jondaiello','107.9.12.83',1361927311,'Logged in'),
	(10,1,1,'jondaiello','107.9.12.83',1361929214,'Field Group Created:&nbsp;About Us'),
	(11,1,1,'jondaiello','107.9.12.83',1361929284,'Field Group Created:&nbsp;Sermons'),
	(12,1,1,'jondaiello','107.9.12.83',1361929294,'Field Group Created:&nbsp;Flocks'),
	(13,1,1,'jondaiello','107.9.12.83',1361929303,'Field Group Created:&nbsp;Ministries'),
	(14,1,1,'jondaiello','107.9.12.83',1361929350,'Field Group Created:&nbsp;News and Events'),
	(15,1,1,'jondaiello','107.9.12.83',1361929360,'Field Group Created:&nbsp;Landing Pages'),
	(16,1,1,'jondaiello','107.9.12.83',1361930313,'Channel Created:&nbsp;&nbsp;About'),
	(17,1,1,'jondaiello','107.9.12.83',1361930321,'Channel Created:&nbsp;&nbsp;Sermons'),
	(18,1,1,'jondaiello','107.9.12.83',1361930338,'Channel Created:&nbsp;&nbsp;News & Events'),
	(19,1,1,'jondaiello','107.9.12.83',1361930359,'Channel Created:&nbsp;&nbsp;Flocks'),
	(20,1,1,'jondaiello','107.9.12.83',1361930367,'Channel Created:&nbsp;&nbsp;Ministries'),
	(21,1,1,'jondaiello','107.9.12.83',1361930377,'Channel Created:&nbsp;&nbsp;Landing Pages'),
	(22,1,1,'jondaiello','107.9.12.83',1361998539,'Logged in'),
	(23,1,1,'jondaiello','107.9.12.83',1361998648,'Channel Created:&nbsp;&nbsp;Emergency Message'),
	(24,1,1,'jondaiello','70.60.44.186',1362330549,'Logged in'),
	(25,1,1,'jondaiello','70.60.44.186',1362330625,'Field Group Created:&nbsp;Emergency Message'),
	(26,1,2,'russ.kennedy','70.60.44.186',1362502033,'Logged in'),
	(27,1,1,'jondaiello','107.9.12.83',1362590432,'Logged in'),
	(28,1,1,'jondaiello','107.9.12.83',1362710621,'Logged in'),
	(29,1,1,'jondaiello','107.9.12.83',1362711126,'Field Group Created:&nbsp;Basic Page'),
	(30,1,1,'jondaiello','107.9.12.83',1362711137,'Field group Deleted:&nbsp;&nbsp;About Us'),
	(31,1,1,'jondaiello','107.9.12.83',1362711143,'Field group Deleted:&nbsp;&nbsp;Flocks'),
	(32,1,1,'jondaiello','107.9.12.83',1362711152,'Field group Deleted:&nbsp;&nbsp;Ministries'),
	(33,1,1,'jondaiello','107.9.12.83',1362711156,'Field group Deleted:&nbsp;&nbsp;Sermons'),
	(34,1,1,'jondaiello','107.9.12.83',1362711338,'Channel Deleted:&nbsp;&nbsp;News & Events'),
	(35,1,1,'jondaiello','107.9.12.83',1362711342,'Channel Deleted:&nbsp;&nbsp;Sermons'),
	(36,1,1,'jondaiello','107.9.12.83',1362874558,'Logged in'),
	(37,1,1,'jondaiello','107.9.12.83',1362875725,'Channel Created:&nbsp;&nbsp;Homepage Hero Image'),
	(38,1,1,'jondaiello','107.9.12.83',1362875746,'Field Group Created:&nbsp;Homepage Hero Image'),
	(39,1,1,'jondaiello','107.9.12.83',1362876091,'Custom Field Deleted:&nbsp;Banner Image'),
	(40,1,1,'jondaiello','107.9.12.83',1362876096,'Custom Field Deleted:&nbsp;Banner Text'),
	(41,1,1,'jondaiello','107.9.12.83',1363045674,'Logged in'),
	(42,1,1,'jondaiello','107.9.12.83',1363046001,'Channel Created:&nbsp;&nbsp;Sermons'),
	(43,1,1,'jondaiello','107.9.12.83',1363046010,'Channel Created:&nbsp;&nbsp;News'),
	(44,1,2,'russ.kennedy','70.63.34.233',1363066756,'Logged in'),
	(45,1,2,'russ.kennedy','70.63.34.233',1363308483,'Logged in'),
	(46,1,2,'russ.kennedy','70.63.34.233',1363322870,'Logged in'),
	(47,1,2,'russ.kennedy','70.63.34.233',1363323552,'Logged out'),
	(48,1,2,'russ.kennedy','113.31.38.53',1363334904,'Logged in'),
	(49,1,2,'russ.kennedy','113.31.38.53',1363334923,'Logged out'),
	(50,1,1,'jondaiello','107.9.12.83',1363352876,'Logged in'),
	(51,1,1,'jondaiello','107.9.12.83',1363650689,'Logged in'),
	(52,1,1,'jondaiello','107.9.12.83',1363696499,'Logged in'),
	(53,1,1,'jondaiello','107.9.12.83',1363696656,'Member profile created:&nbsp;&nbsp;john.ambro'),
	(54,1,1,'jondaiello','107.9.12.83',1363696774,'Member Group Created:&nbsp;&nbsp;Publisher'),
	(55,1,1,'jondaiello','108.254.190.58',1364150515,'Logged in'),
	(56,1,1,'jondaiello','108.254.190.58',1364214151,'Logged in'),
	(57,1,1,'jondaiello','108.254.190.58',1364299362,'Logged in'),
	(58,1,1,'jondaiello','108.254.190.58',1364383832,'Logged in'),
	(59,1,2,'russ.kennedy','70.60.44.186',1364397987,'Logged in'),
	(60,1,2,'russ.kennedy','70.60.44.186',1364398470,'Logged out'),
	(61,1,2,'russ.kennedy','70.60.44.186',1364400808,'Logged in'),
	(62,1,2,'russ.kennedy','70.60.44.186',1364400919,'Member profile created:&nbsp;&nbsp;Chapel.Staff'),
	(63,1,1,'jondaiello','108.254.190.58',1364403459,'Logged in'),
	(64,1,4,'Chapel.Staff','70.60.44.186',1364404241,'Logged in'),
	(65,1,4,'Chapel.Staff','70.60.44.186',1364404322,'Logged out'),
	(66,1,4,'Chapel.Staff','70.60.44.186',1364410166,'Logged in'),
	(67,1,4,'Chapel.Staff','70.60.44.186',1364410434,'Logged in'),
	(68,1,2,'russ.kennedy','70.60.44.186',1364410808,'Logged in'),
	(69,1,2,'russ.kennedy','70.60.44.186',1364417807,'Logged in'),
	(70,1,2,'russ.kennedy','107.9.25.183',1364469735,'Logged in'),
	(71,1,2,'russ.kennedy','107.9.25.183',1364470658,'Logged out'),
	(72,1,1,'jondaiello','108.254.190.58',1364473808,'Logged in'),
	(73,1,2,'russ.kennedy','70.60.44.186',1364475960,'Logged in'),
	(74,1,2,'russ.kennedy','70.60.44.186',1364499749,'Logged in'),
	(75,1,2,'russ.kennedy','70.60.44.186',1364549894,'Logged in'),
	(76,1,2,'russ.kennedy','70.60.44.186',1364560352,'Logged in'),
	(77,1,1,'jondaiello','108.254.190.58',1364564370,'Logged in'),
	(78,1,1,'jondaiello','108.254.190.58',1364564405,'Channel Created:&nbsp;&nbsp;Leaders'),
	(79,1,1,'jondaiello','108.254.190.58',1364564424,'Field Group Created:&nbsp;Leaders'),
	(80,1,1,'jondaiello','108.254.190.58',1364564645,'Category Group Created:&nbsp;&nbsp;Leaders'),
	(81,1,2,'russ.kennedy','70.60.44.186',1364565655,'Logged in'),
	(82,1,1,'jondaiello','108.254.190.58',1364571834,'Logged in'),
	(83,1,2,'russ.kennedy','70.60.44.186',1364578608,'Logged in'),
	(84,1,2,'russ.kennedy','70.60.44.186',1364586990,'Logged in'),
	(85,1,2,'russ.kennedy','70.60.44.186',1364591841,'Logged in'),
	(86,1,2,'russ.kennedy','107.9.25.183',1364636794,'Logged in'),
	(87,1,2,'russ.kennedy','107.9.25.183',1364636874,'Logged in'),
	(88,1,2,'russ.kennedy','107.9.25.183',1364641907,'Logged in'),
	(89,1,1,'jondaiello','108.254.190.58',1364645448,'Logged in'),
	(90,1,2,'russ.kennedy','107.9.25.183',1364648007,'Logged out'),
	(91,1,2,'russ.kennedy','107.9.25.183',1364681623,'Logged in'),
	(92,1,1,'jondaiello','108.254.190.58',1364756828,'Logged in'),
	(93,1,2,'russ.kennedy','107.9.25.183',1364809380,'Logged in'),
	(94,1,1,'jondaiello','108.254.190.58',1364823617,'Logged in'),
	(95,1,4,'Chapel.Staff','70.60.44.186',1364828291,'Logged in'),
	(96,1,1,'jondaiello','108.254.190.58',1364828624,'Logged in'),
	(97,1,2,'russ.kennedy','70.60.44.186',1364911486,'Logged in'),
	(98,1,1,'jondaiello','108.254.190.58',1364913433,'Logged in'),
	(99,1,2,'russ.kennedy','70.60.44.186',1364926295,'Logged in'),
	(100,1,2,'russ.kennedy','70.60.44.186',1364928802,'Logged in'),
	(101,1,2,'russ.kennedy','70.60.44.186',1364943072,'Logged in'),
	(102,1,1,'jondaiello','108.254.190.58',1364954585,'Logged in'),
	(103,1,2,'russ.kennedy','70.60.44.186',1365000220,'Logged in'),
	(104,1,4,'Chapel.Staff','70.60.44.186',1365003023,'Logged in'),
	(105,1,1,'jondaiello','108.254.190.58',1365004330,'Logged in'),
	(106,1,1,'jondaiello','108.254.190.58',1365005113,'Member Group Updated:&nbsp;&nbsp;Publisher'),
	(107,1,1,'jondaiello','108.254.190.58',1365005253,'Member profile created:&nbsp;&nbsp;Content.Publisher'),
	(108,1,1,'jondaiello','108.254.190.58',1365005310,'Member Group Created:&nbsp;&nbsp;Editor'),
	(109,1,5,'Content.Publisher','70.60.44.186',1365005590,'Logged in'),
	(110,1,2,'russ.kennedy','70.60.44.186',1365007577,'Member Group Updated:&nbsp;&nbsp;Publisher'),
	(111,1,1,'jondaiello','108.254.190.58',1365013773,'Logged in'),
	(112,1,5,'Content.Publisher','70.60.44.186',1365014333,'Logged in'),
	(113,1,5,'Content.Publisher','70.60.44.186',1365016539,'Logged in'),
	(114,1,5,'Content.Publisher','70.60.44.186',1365021328,'Logged out'),
	(115,1,1,'jondaiello','108.254.190.58',1365035292,'Logged in'),
	(116,1,5,'Content.Publisher','71.64.159.80',1365036168,'Logged in'),
	(117,1,5,'Content.Publisher','71.64.159.80',1365036359,'Logged out'),
	(118,1,1,'jondaiello','108.254.190.58',1365076510,'Logged in'),
	(119,1,5,'Content.Publisher','70.60.44.186',1365081817,'Logged in'),
	(120,1,2,'russ.kennedy','70.60.44.186',1365086368,'Logged in'),
	(121,1,2,'russ.kennedy','70.60.44.186',1365086390,'Logged in'),
	(122,1,1,'jondaiello','108.254.190.58',1365087691,'Logged in'),
	(123,1,1,'jondaiello','108.254.190.58',1365093316,'Logged in'),
	(124,1,5,'Content.Publisher','70.60.44.186',1365100498,'Logged in'),
	(125,1,2,'russ.kennedy','70.60.44.186',1365105286,'Logged in'),
	(126,1,5,'Content.Publisher','70.60.44.186',1365109009,'Logged in'),
	(127,1,5,'Content.Publisher','70.60.44.186',1365109289,'Logged out'),
	(128,1,2,'russ.kennedy','70.60.44.186',1365110440,'Logged out'),
	(129,1,2,'russ.kennedy','70.60.44.186',1365110440,'Logged out'),
	(130,1,2,'russ.kennedy','107.9.25.183',1365121861,'Logged in'),
	(131,1,2,'russ.kennedy','107.9.25.183',1365121920,'Logged in'),
	(132,1,1,'jondaiello','108.254.190.58',1365160787,'Logged in'),
	(133,1,2,'russ.kennedy','70.60.44.186',1365163810,'Logged in'),
	(134,1,2,'russ.kennedy','70.60.44.186',1365174822,'Logged in'),
	(135,1,5,'Content.Publisher','71.64.159.80',1365186627,'Logged in'),
	(136,1,5,'Content.Publisher','71.64.159.80',1365187538,'Logged out'),
	(137,1,1,'jondaiello','108.254.190.58',1365191441,'Logged in'),
	(138,1,5,'Content.Publisher','71.64.159.80',1365194042,'Logged in'),
	(139,1,2,'russ.kennedy','70.60.44.186',1365194569,'Logged in'),
	(140,1,5,'Content.Publisher','71.64.159.80',1365195696,'Logged out'),
	(141,1,2,'russ.kennedy','107.9.25.183',1365204836,'Logged in'),
	(142,1,2,'russ.kennedy','107.9.25.183',1365204849,'Logged in'),
	(143,1,1,'jondaiello','108.254.190.58',1365205085,'Logged in'),
	(144,1,1,'jondaiello','108.254.190.58',1365213957,'Logged in'),
	(145,1,1,'jondaiello','108.254.190.58',1365216326,'Logged in'),
	(146,1,2,'russ.kennedy','107.9.25.183',1365246588,'Logged in'),
	(147,1,2,'russ.kennedy','107.9.25.183',1365256996,'Logged in'),
	(148,1,2,'russ.kennedy','107.9.25.183',1365257147,'Member profile created:&nbsp;&nbsp;Michael.Yockey'),
	(149,1,6,'Michael.Yockey','107.9.25.183',1365266394,'Logged in'),
	(150,1,6,'Michael.Yockey','107.9.34.246',1365266490,'Logged in'),
	(151,1,2,'russ.kennedy','107.9.25.183',1365266495,'Logged in'),
	(152,1,2,'russ.kennedy','107.9.25.183',1365266748,'Logged out'),
	(153,1,6,'Michael.Yockey','107.9.34.246',1365267030,'Username was changed to:&nbsp;&nbsp;myockey'),
	(154,1,6,'myockey','107.9.34.246',1365267042,'Logged out'),
	(155,1,6,'myockey','107.9.34.246',1365267085,'Logged in'),
	(156,1,1,'jondaiello','108.254.190.58',1365268975,'Logged in'),
	(157,1,2,'russ.kennedy','107.9.25.183',1365279271,'Logged in'),
	(158,1,1,'jondaiello','108.254.190.58',1365282989,'Logged in'),
	(159,1,1,'jondaiello','108.254.190.58',1365286655,'Logged in'),
	(160,1,2,'russ.kennedy','107.9.25.183',1365287515,'Logged out'),
	(161,1,5,'Content.Publisher','71.64.159.80',1365290373,'Logged in'),
	(162,1,5,'Content.Publisher','71.64.159.80',1365290618,'Logged out'),
	(163,1,1,'jondaiello','108.254.190.58',1365292457,'Logged in'),
	(164,1,1,'jondaiello','108.254.190.58',1365293771,'Logged in'),
	(165,1,1,'jondaiello','108.254.190.58',1365296856,'Logged in'),
	(166,1,2,'russ.kennedy','107.9.25.183',1365298171,'Logged in'),
	(167,1,2,'russ.kennedy','107.9.25.183',1365301418,'Logged in'),
	(168,1,5,'Content.Publisher','71.64.159.80',1365380620,'Logged in'),
	(169,1,5,'Content.Publisher','71.64.159.80',1365380757,'Logged out'),
	(170,1,2,'russ.kennedy','107.9.25.183',1365384787,'Logged in'),
	(171,1,2,'russ.kennedy','107.9.25.183',1365385370,'Logged in'),
	(172,1,1,'jondaiello','108.254.190.58',1365390053,'Logged in'),
	(173,1,2,'russ.kennedy','107.9.25.183',1365423650,'Logged in'),
	(174,1,2,'russ.kennedy','107.9.25.183',1365424376,'Logged out'),
	(175,1,1,'jondaiello','108.254.190.58',1365427394,'Logged in'),
	(176,1,5,'Content.Publisher','70.60.44.186',1365428677,'Logged in'),
	(177,1,5,'Content.Publisher','70.60.44.186',1365431583,'Logged out'),
	(178,1,5,'Content.Publisher','70.60.44.186',1365442997,'Logged in'),
	(179,1,5,'Content.Publisher','70.60.44.186',1365445028,'Logged out'),
	(180,1,2,'russ.kennedy','107.9.25.183',1365502802,'Logged in'),
	(181,1,1,'jondaiello','108.254.190.58',1365516102,'Logged in'),
	(182,1,1,'jondaiello','108.254.190.58',1365516644,'Member Group Created:&nbsp;&nbsp;SEO Editor'),
	(183,1,1,'jondaiello','108.254.190.58',1365516669,'Member Group Updated:&nbsp;&nbsp;SEO Editor'),
	(184,1,1,'jondaiello','108.254.190.58',1365517618,'Member profile created:&nbsp;&nbsp;Mark.Schindler'),
	(185,1,7,'Mark.Schindler','108.254.190.58',1365517760,'Logged in'),
	(186,1,1,'jondaiello','108.254.190.58',1365517792,'Member Group Updated:&nbsp;&nbsp;SEO Editor'),
	(187,1,1,'jondaiello','108.254.190.58',1365517850,'Member Group Updated:&nbsp;&nbsp;SEO Editor'),
	(188,1,5,'Content.Publisher','70.60.44.186',1365530156,'Logged in'),
	(189,1,5,'Content.Publisher','70.60.44.186',1365530848,'Logged out'),
	(190,1,5,'Content.Publisher','70.60.44.186',1365534867,'Logged in'),
	(191,1,5,'Content.Publisher','70.60.44.186',1365535056,'Logged out'),
	(192,1,1,'jondaiello','108.254.190.58',1365607302,'Logged in'),
	(193,1,5,'Content.Publisher','70.60.44.186',1365611453,'Logged in'),
	(194,1,5,'Content.Publisher','70.60.44.186',1365612467,'Logged out'),
	(195,1,1,'jondaiello','108.254.190.58',1365698478,'Logged in'),
	(196,1,5,'Content.Publisher','70.60.44.186',1366033436,'Logged in'),
	(197,1,5,'Content.Publisher','70.60.44.186',1366033858,'Logged out'),
	(198,1,5,'Content.Publisher','70.60.44.186',1366133086,'Logged in'),
	(199,1,5,'Content.Publisher','70.60.44.186',1366133230,'Logged out'),
	(200,1,5,'Content.Publisher','70.60.44.186',1366639329,'Logged in'),
	(201,1,5,'Content.Publisher','70.60.44.186',1366640232,'Logged out'),
	(202,1,5,'Content.Publisher','71.64.159.80',1366666982,'Logged in'),
	(203,1,5,'Content.Publisher','71.64.159.80',1366667084,'Logged out'),
	(204,1,5,'Content.Publisher','70.60.44.186',1367258967,'Logged in'),
	(205,1,5,'Content.Publisher','70.60.44.186',1367259247,'Logged out'),
	(206,1,5,'Content.Publisher','70.60.44.186',1367332889,'Logged in'),
	(207,1,5,'Content.Publisher','70.60.44.186',1367333011,'Logged out'),
	(208,1,1,'jondaiello','108.254.190.58',1367856243,'Logged in'),
	(209,1,5,'Content.Publisher','70.60.44.186',1367866007,'Logged in'),
	(210,1,5,'Content.Publisher','70.60.44.186',1367866288,'Logged out'),
	(211,1,5,'Content.Publisher','70.60.44.186',1368026638,'Logged in'),
	(212,1,5,'Content.Publisher','70.60.44.186',1368026695,'Logged out'),
	(213,1,5,'Content.Publisher','70.60.44.186',1368026729,'Logged in'),
	(214,1,5,'Content.Publisher','70.60.44.186',1368026806,'Logged out'),
	(215,1,2,'russ.kennedy','70.63.34.233',1368174469,'Logged in'),
	(216,1,2,'russ.kennedy','70.63.34.233',1368181396,'Logged out'),
	(217,1,1,'jondaiello','108.254.190.58',1368186145,'Logged in'),
	(218,1,1,'jondaiello','108.254.190.58',1368230882,'Logged in'),
	(219,1,1,'jondaiello','108.254.190.58',1368234364,'Logged in'),
	(220,1,1,'jondaiello','108.254.190.58',1368234628,'Channel Created:&nbsp;&nbsp;News Listing'),
	(221,1,1,'jondaiello','108.254.190.58',1368280909,'Logged in'),
	(222,1,1,'jondaiello','70.194.200.96',1368365401,'Logged in'),
	(223,1,2,'russ.kennedy','188.25.230.158',1368387270,'Logged in'),
	(224,1,1,'jondaiello','108.254.190.58',1368445871,'Logged in'),
	(225,1,5,'Content.Publisher','70.60.44.186',1368452362,'Logged in'),
	(226,1,5,'Content.Publisher','70.60.44.186',1368452984,'Logged out'),
	(227,1,5,'Content.Publisher','70.60.44.186',1368538904,'Logged in'),
	(228,1,5,'Content.Publisher','70.60.44.186',1368538953,'Logged out'),
	(229,1,5,'Content.Publisher','70.60.44.186',1368543709,'Logged in'),
	(230,1,5,'Content.Publisher','70.60.44.186',1368543767,'Logged out'),
	(231,1,1,'jondaiello','108.254.190.58',1368552213,'Logged in'),
	(232,1,5,'Content.Publisher','70.60.44.186',1368557091,'Logged in'),
	(233,1,5,'Content.Publisher','70.60.44.186',1368557830,'Logged out'),
	(234,1,5,'Content.Publisher','70.60.44.186',1368558281,'Logged in'),
	(235,1,5,'Content.Publisher','70.60.44.186',1368558511,'Logged out'),
	(236,1,1,'jondaiello','108.254.190.58',1368558729,'Member Group Updated:&nbsp;&nbsp;Publisher'),
	(237,1,1,'jondaiello','108.254.190.58',1368558738,'Member Group Updated:&nbsp;&nbsp;Editor'),
	(238,1,1,'jondaiello','108.254.190.58',1368558749,'Member Group Updated:&nbsp;&nbsp;SEO Editor'),
	(239,1,1,'jondaiello','108.254.190.58',1368642482,'Logged in'),
	(240,1,5,'Content.Publisher','70.60.44.186',1368922976,'Logged in'),
	(241,1,5,'Content.Publisher','70.60.44.186',1368923035,'Logged out'),
	(242,1,5,'Content.Publisher','70.60.44.186',1369059135,'Logged in'),
	(243,1,5,'Content.Publisher','70.60.44.186',1369059825,'Logged out'),
	(244,1,5,'Content.Publisher','70.60.44.186',1369077414,'Logged in'),
	(245,1,5,'Content.Publisher','70.60.44.186',1369077477,'Logged out'),
	(246,1,5,'Content.Publisher','70.60.44.186',1369230215,'Logged in'),
	(247,1,5,'Content.Publisher','70.60.44.186',1369230399,'Logged out'),
	(248,1,1,'jondaiello','70.60.44.186',1369318120,'Logged in'),
	(249,1,1,'jondaiello','70.60.44.186',1369319740,'Logged in'),
	(250,1,1,'jondaiello','70.60.44.186',1369321890,'Custom Field Deleted:&nbsp;Summary'),
	(251,1,1,'jondaiello','70.60.44.186',1369322737,'Member Group Created:&nbsp;&nbsp;Content Manager'),
	(252,1,1,'jondaiello','70.60.44.186',1369322804,'Member Group Updated:&nbsp;&nbsp;Content Manager'),
	(253,1,1,'jondaiello','70.60.44.186',1369322999,'Screen name was changed to:&nbsp;&nbsp;Content.Manager\nUsername was changed to:&nbsp;&nbsp;Content.Manager'),
	(254,1,1,'jondaiello','70.60.44.186',1369326660,'Logged out'),
	(255,1,5,'Content.Manager','71.64.159.80',1369350512,'Logged in'),
	(256,1,5,'Content.Manager','71.64.159.80',1369350847,'Logged out'),
	(257,1,5,'Content.Manager','70.60.44.186',1369745699,'Logged in'),
	(258,1,5,'Content.Manager','70.60.44.186',1369746524,'Logged out'),
	(259,1,5,'Content.Manager','70.60.44.186',1369754249,'Logged in'),
	(260,1,5,'Content.Manager','70.60.44.186',1369756249,'Logged out'),
	(261,1,5,'Content.Manager','70.60.44.186',1369760868,'Logged in'),
	(262,1,5,'Content.Manager','70.60.44.186',1369761414,'Logged out'),
	(263,1,5,'Content.Manager','70.60.44.186',1369762682,'Logged in'),
	(264,1,5,'Content.Manager','70.60.44.186',1369762750,'Logged out'),
	(265,1,5,'Content.Manager','70.60.44.186',1369769311,'Logged in'),
	(266,1,5,'Content.Manager','70.60.44.186',1369769369,'Logged out'),
	(267,1,1,'jondaiello','108.254.190.58',1369769965,'Logged in'),
	(268,1,5,'Content.Manager','70.60.44.186',1369841305,'Logged in'),
	(269,1,5,'Content.Manager','70.60.44.186',1369842131,'Logged out'),
	(270,1,5,'Content.Manager','70.60.44.186',1370268174,'Logged in'),
	(271,1,5,'Content.Manager','70.60.44.186',1370268510,'Logged out'),
	(272,1,5,'Content.Manager','70.60.44.186',1370276801,'Logged in'),
	(273,1,5,'Content.Manager','70.60.44.186',1370276997,'Logged out'),
	(274,1,5,'Content.Manager','70.60.44.186',1370873752,'Logged in'),
	(275,1,5,'Content.Manager','70.60.44.186',1370878329,'Logged in'),
	(276,1,1,'jondaiello','108.254.190.58',1370881386,'Logged in'),
	(277,1,5,'Content.Manager','70.60.44.186',1370881996,'Logged in'),
	(278,1,1,'jondaiello','108.254.190.58',1370882307,'Logged in'),
	(279,1,1,'jondaiello','108.254.190.58',1370891493,'Logged in'),
	(280,1,5,'Content.Manager','70.60.44.186',1370959915,'Logged in'),
	(281,1,5,'Content.Manager','70.60.44.186',1370960262,'Logged out'),
	(282,1,5,'Content.Manager','70.60.44.186',1371477315,'Logged in'),
	(283,1,5,'Content.Manager','70.60.44.186',1371479296,'Logged out'),
	(284,1,5,'Content.Manager','70.60.44.186',1371487211,'Logged in'),
	(285,1,5,'Content.Manager','70.60.44.186',1371487459,'Logged out'),
	(286,1,5,'Content.Manager','70.60.44.186',1371491544,'Logged in'),
	(287,1,5,'Content.Manager','70.60.44.186',1371491847,'Logged out'),
	(288,1,5,'Content.Manager','70.60.44.186',1371562745,'Logged in'),
	(289,1,5,'Content.Manager','70.60.44.186',1371562950,'Logged out'),
	(290,1,1,'jondaiello','108.254.190.58',1371563054,'Logged in'),
	(291,1,5,'Content.Manager','70.60.44.186',1371563943,'Logged in'),
	(292,1,5,'Content.Manager','70.60.44.186',1371563977,'Logged out'),
	(293,1,5,'Content.Manager','70.60.44.186',1371582717,'Logged in'),
	(294,1,5,'Content.Manager','70.60.44.186',1371582820,'Logged out'),
	(295,1,5,'Content.Manager','70.60.44.186',1372081970,'Logged in'),
	(296,1,5,'Content.Manager','70.60.44.186',1372084683,'Logged out'),
	(297,1,5,'Content.Manager','70.60.44.186',1372259958,'Logged in'),
	(298,1,5,'Content.Manager','70.60.44.186',1372260470,'Logged out'),
	(299,1,1,'jondaiello','108.254.190.58',1372427489,'Logged in'),
	(300,1,5,'Content.Manager','70.60.44.186',1372686151,'Logged in'),
	(301,1,5,'Content.Manager','70.60.44.186',1372687067,'Logged out'),
	(302,1,1,'jondaiello','108.254.190.58',1373225999,'Logged in'),
	(303,1,5,'Content.Manager','70.60.44.186',1373293144,'Logged in'),
	(304,1,5,'Content.Manager','70.60.44.186',1373293353,'Logged out'),
	(305,1,5,'Content.Manager','70.60.44.186',1373391923,'Logged in'),
	(306,1,5,'Content.Manager','70.60.44.186',1373391957,'Logged out'),
	(307,1,5,'Content.Manager','70.60.44.186',1373906040,'Logged in'),
	(308,1,5,'Content.Manager','70.60.44.186',1373906450,'Logged in'),
	(309,1,5,'Content.Manager','70.60.44.186',1373906687,'Logged out'),
	(310,1,5,'Content.Manager','70.60.44.186',1374160963,'Logged in'),
	(311,1,5,'Content.Manager','70.60.44.186',1374161052,'Logged out'),
	(312,1,5,'Content.Manager','70.60.44.186',1374161080,'Logged in'),
	(313,1,5,'Content.Manager','70.60.44.186',1374161202,'Logged out');

/*!40000 ALTER TABLE `exp_cp_log` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_cp_search_index
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_cp_search_index`;

CREATE TABLE `exp_cp_search_index` (
  `search_id` int(10) unsigned NOT NULL auto_increment,
  `controller` varchar(20) default NULL,
  `method` varchar(50) default NULL,
  `language` varchar(20) default NULL,
  `access` varchar(50) default NULL,
  `keywords` text,
  PRIMARY KEY  (`search_id`),
  FULLTEXT KEY `keywords` (`keywords`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_developer_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_developer_log`;

CREATE TABLE `exp_developer_log` (
  `log_id` int(10) unsigned NOT NULL auto_increment,
  `timestamp` int(10) unsigned NOT NULL,
  `viewed` char(1) NOT NULL default 'n',
  `description` text,
  `function` varchar(100) default NULL,
  `line` int(10) unsigned default NULL,
  `file` varchar(255) default NULL,
  `deprecated_since` varchar(10) default NULL,
  `use_instead` varchar(100) default NULL,
  PRIMARY KEY  (`log_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_email_cache
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_email_cache`;

CREATE TABLE `exp_email_cache` (
  `cache_id` int(6) unsigned NOT NULL auto_increment,
  `cache_date` int(10) unsigned NOT NULL default '0',
  `total_sent` int(6) unsigned NOT NULL,
  `from_name` varchar(70) NOT NULL,
  `from_email` varchar(70) NOT NULL,
  `recipient` text NOT NULL,
  `cc` text NOT NULL,
  `bcc` text NOT NULL,
  `recipient_array` mediumtext NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  `plaintext_alt` mediumtext NOT NULL,
  `mailinglist` char(1) NOT NULL default 'n',
  `mailtype` varchar(6) NOT NULL,
  `text_fmt` varchar(40) NOT NULL,
  `wordwrap` char(1) NOT NULL default 'y',
  `priority` char(1) NOT NULL default '3',
  PRIMARY KEY  (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_email_cache_mg
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_email_cache_mg`;

CREATE TABLE `exp_email_cache_mg` (
  `cache_id` int(6) unsigned NOT NULL,
  `group_id` smallint(4) NOT NULL,
  PRIMARY KEY  (`cache_id`,`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_email_cache_ml
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_email_cache_ml`;

CREATE TABLE `exp_email_cache_ml` (
  `cache_id` int(6) unsigned NOT NULL,
  `list_id` smallint(4) NOT NULL,
  PRIMARY KEY  (`cache_id`,`list_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_email_console_cache
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_email_console_cache`;

CREATE TABLE `exp_email_console_cache` (
  `cache_id` int(6) unsigned NOT NULL auto_increment,
  `cache_date` int(10) unsigned NOT NULL default '0',
  `member_id` int(10) unsigned NOT NULL,
  `member_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL default '0',
  `recipient` varchar(70) NOT NULL,
  `recipient_name` varchar(50) NOT NULL,
  `subject` varchar(120) NOT NULL,
  `message` mediumtext NOT NULL,
  PRIMARY KEY  (`cache_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_entry_ping_status
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_entry_ping_status`;

CREATE TABLE `exp_entry_ping_status` (
  `entry_id` int(10) unsigned NOT NULL,
  `ping_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`entry_id`,`ping_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_entry_versioning
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_entry_versioning`;

CREATE TABLE `exp_entry_versioning` (
  `version_id` int(10) unsigned NOT NULL auto_increment,
  `entry_id` int(10) unsigned NOT NULL,
  `channel_id` int(4) unsigned NOT NULL,
  `author_id` int(10) unsigned NOT NULL,
  `version_date` int(10) NOT NULL,
  `version_data` mediumtext NOT NULL,
  PRIMARY KEY  (`version_id`),
  KEY `entry_id` (`entry_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_extensions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_extensions`;

CREATE TABLE `exp_extensions` (
  `extension_id` int(10) unsigned NOT NULL auto_increment,
  `class` varchar(50) NOT NULL default '',
  `method` varchar(50) NOT NULL default '',
  `hook` varchar(50) NOT NULL default '',
  `settings` text NOT NULL,
  `priority` int(2) NOT NULL default '10',
  `version` varchar(10) NOT NULL default '',
  `enabled` char(1) NOT NULL default 'y',
  PRIMARY KEY  (`extension_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_extensions` WRITE;
/*!40000 ALTER TABLE `exp_extensions` DISABLE KEYS */;

INSERT INTO `exp_extensions` (`extension_id`, `class`, `method`, `hook`, `settings`, `priority`, `version`, `enabled`)
VALUES
	(1,'Safecracker_ext','form_declaration_modify_data','form_declaration_modify_data','',10,'2.1','y'),
	(2,'Rte_ext','myaccount_nav_setup','myaccount_nav_setup','',10,'1.0','y'),
	(3,'Rte_ext','cp_menu_array','cp_menu_array','',10,'1.0','y'),
	(4,'Rte_ext','publish_form_entry_data','publish_form_entry_data','',10,'1.0','y');

/*!40000 ALTER TABLE `exp_extensions` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_field_formatting
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_field_formatting`;

CREATE TABLE `exp_field_formatting` (
  `formatting_id` int(10) unsigned NOT NULL auto_increment,
  `field_id` int(10) unsigned NOT NULL,
  `field_fmt` varchar(40) NOT NULL,
  PRIMARY KEY  (`formatting_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_field_formatting` WRITE;
/*!40000 ALTER TABLE `exp_field_formatting` DISABLE KEYS */;

INSERT INTO `exp_field_formatting` (`formatting_id`, `field_id`, `field_fmt`)
VALUES
	(81,27,'xhtml'),
	(80,27,'br'),
	(79,27,'none'),
	(7,3,'none'),
	(8,3,'br'),
	(9,3,'xhtml'),
	(10,4,'none'),
	(11,4,'br'),
	(12,4,'xhtml'),
	(13,5,'none'),
	(14,5,'br'),
	(15,5,'xhtml'),
	(16,6,'none'),
	(17,6,'br'),
	(18,6,'xhtml'),
	(19,7,'none'),
	(20,7,'br'),
	(21,7,'xhtml'),
	(22,8,'none'),
	(23,8,'br'),
	(24,8,'xhtml'),
	(25,9,'none'),
	(26,9,'br'),
	(27,9,'xhtml'),
	(28,10,'none'),
	(29,10,'br'),
	(30,10,'xhtml'),
	(31,11,'none'),
	(32,11,'br'),
	(33,11,'xhtml'),
	(34,12,'none'),
	(35,12,'br'),
	(36,12,'xhtml'),
	(37,13,'none'),
	(38,13,'br'),
	(39,13,'xhtml'),
	(40,14,'none'),
	(41,14,'br'),
	(42,14,'xhtml'),
	(43,15,'none'),
	(44,15,'br'),
	(45,15,'xhtml'),
	(46,16,'none'),
	(47,16,'br'),
	(48,16,'xhtml'),
	(49,17,'none'),
	(50,17,'br'),
	(51,17,'xhtml'),
	(52,18,'none'),
	(53,18,'br'),
	(54,18,'xhtml'),
	(55,19,'none'),
	(56,19,'br'),
	(57,19,'xhtml'),
	(58,20,'none'),
	(59,20,'br'),
	(60,20,'xhtml'),
	(61,21,'none'),
	(62,21,'br'),
	(63,21,'xhtml'),
	(64,22,'none'),
	(65,22,'br'),
	(66,22,'xhtml'),
	(67,23,'none'),
	(68,23,'br'),
	(69,23,'xhtml'),
	(70,24,'none'),
	(71,24,'br'),
	(72,24,'xhtml'),
	(73,25,'none'),
	(74,25,'br'),
	(75,25,'xhtml'),
	(76,26,'none'),
	(77,26,'br'),
	(78,26,'xhtml'),
	(82,28,'none'),
	(83,28,'br'),
	(84,28,'xhtml'),
	(85,29,'none'),
	(86,29,'br'),
	(87,29,'xhtml'),
	(88,30,'none'),
	(89,30,'br'),
	(90,30,'xhtml'),
	(91,31,'none'),
	(92,31,'br'),
	(93,31,'xhtml'),
	(94,32,'none'),
	(95,32,'br'),
	(96,32,'xhtml'),
	(97,33,'none'),
	(98,33,'br'),
	(99,33,'xhtml'),
	(100,34,'none'),
	(101,34,'br'),
	(102,34,'xhtml'),
	(103,35,'none'),
	(104,35,'br'),
	(105,35,'xhtml'),
	(106,36,'none'),
	(107,36,'br'),
	(108,36,'xhtml');

/*!40000 ALTER TABLE `exp_field_formatting` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_field_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_field_groups`;

CREATE TABLE `exp_field_groups` (
  `group_id` int(4) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY  (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_field_groups` WRITE;
/*!40000 ALTER TABLE `exp_field_groups` DISABLE KEYS */;

INSERT INTO `exp_field_groups` (`group_id`, `site_id`, `group_name`)
VALUES
	(1,1,'Home'),
	(10,1,'Homepage Hero Image'),
	(6,1,'News Listing'),
	(7,1,'Landing Pages'),
	(8,1,'Emergency Message'),
	(9,1,'Basic Page'),
	(11,1,'Leaders');

/*!40000 ALTER TABLE `exp_field_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_fieldtypes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_fieldtypes`;

CREATE TABLE `exp_fieldtypes` (
  `fieldtype_id` int(4) unsigned NOT NULL auto_increment,
  `name` varchar(50) NOT NULL,
  `version` varchar(12) NOT NULL,
  `settings` text,
  `has_global_settings` char(1) default 'n',
  PRIMARY KEY  (`fieldtype_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_fieldtypes` WRITE;
/*!40000 ALTER TABLE `exp_fieldtypes` DISABLE KEYS */;

INSERT INTO `exp_fieldtypes` (`fieldtype_id`, `name`, `version`, `settings`, `has_global_settings`)
VALUES
	(1,'select','1.0','YTowOnt9','n'),
	(2,'text','1.0','YTowOnt9','n'),
	(3,'textarea','1.0','YTowOnt9','n'),
	(4,'date','1.0','YTowOnt9','n'),
	(5,'file','1.0','YTowOnt9','n'),
	(6,'multi_select','1.0','YTowOnt9','n'),
	(7,'checkboxes','1.0','YTowOnt9','n'),
	(8,'radio','1.0','YTowOnt9','n'),
	(9,'rel','1.0','YTowOnt9','n'),
	(10,'rte','1.0','YTowOnt9','n'),
	(11,'redactee','2.2','YTo5OntzOjEwOiJ0b29sYmFyX2lkIjtpOjE7czoxNToiaW1hZ2VfdXBsb2FkX2lkIjtzOjQ6Im5vbmUiO3M6MTQ6ImZpbGVfdXBsb2FkX2lkIjtzOjQ6Im5vbmUiO3M6MTI6ImltYWdlX2Jyb3dzZSI7czo0OiJub25lIjtzOjIwOiJpbWFnZV9icm93c2Vfc3ViZGlycyI7czowOiIiO3M6OToibWluSGVpZ2h0IjtzOjA6IiI7czoxMDoiYXV0b3Jlc2l6ZSI7czoxOiJZIjtzOjEwOiJzaXRlX3BhZ2VzIjtzOjQ6Im5vbmUiO3M6MTM6InNpdGVfbGlzdGluZ3MiO3M6MDoiIjt9','y');

/*!40000 ALTER TABLE `exp_fieldtypes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_file_categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_file_categories`;

CREATE TABLE `exp_file_categories` (
  `file_id` int(10) unsigned default NULL,
  `cat_id` int(10) unsigned default NULL,
  `sort` int(10) unsigned default '0',
  `is_cover` char(1) default 'n',
  KEY `file_id` (`file_id`),
  KEY `cat_id` (`cat_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_file_dimensions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_file_dimensions`;

CREATE TABLE `exp_file_dimensions` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned default '1',
  `upload_location_id` int(4) unsigned default NULL,
  `title` varchar(255) default '',
  `short_name` varchar(255) default '',
  `resize_type` varchar(50) default '',
  `width` int(10) default '0',
  `height` int(10) default '0',
  `watermark_id` int(4) unsigned default NULL,
  PRIMARY KEY  (`id`),
  KEY `upload_location_id` (`upload_location_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_file_watermarks
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_file_watermarks`;

CREATE TABLE `exp_file_watermarks` (
  `wm_id` int(4) unsigned NOT NULL auto_increment,
  `wm_name` varchar(80) default NULL,
  `wm_type` varchar(10) default 'text',
  `wm_image_path` varchar(100) default NULL,
  `wm_test_image_path` varchar(100) default NULL,
  `wm_use_font` char(1) default 'y',
  `wm_font` varchar(30) default NULL,
  `wm_font_size` int(3) unsigned default NULL,
  `wm_text` varchar(100) default NULL,
  `wm_vrt_alignment` varchar(10) default 'top',
  `wm_hor_alignment` varchar(10) default 'left',
  `wm_padding` int(3) unsigned default NULL,
  `wm_opacity` int(3) unsigned default NULL,
  `wm_hor_offset` int(4) unsigned default NULL,
  `wm_vrt_offset` int(4) unsigned default NULL,
  `wm_x_transp` int(4) default NULL,
  `wm_y_transp` int(4) default NULL,
  `wm_font_color` varchar(7) default NULL,
  `wm_use_drop_shadow` char(1) default 'y',
  `wm_shadow_distance` int(3) unsigned default NULL,
  `wm_shadow_color` varchar(7) default NULL,
  PRIMARY KEY  (`wm_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_files
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_files`;

CREATE TABLE `exp_files` (
  `file_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned default '1',
  `title` varchar(255) default NULL,
  `upload_location_id` int(4) unsigned default '0',
  `rel_path` varchar(255) default NULL,
  `mime_type` varchar(255) default NULL,
  `file_name` varchar(255) default NULL,
  `file_size` int(10) default '0',
  `description` text,
  `credit` varchar(255) default NULL,
  `location` varchar(255) default NULL,
  `uploaded_by_member_id` int(10) unsigned default '0',
  `upload_date` int(10) default NULL,
  `modified_by_member_id` int(10) unsigned default '0',
  `modified_date` int(10) default NULL,
  `file_hw_original` varchar(20) NOT NULL default '',
  PRIMARY KEY  (`file_id`),
  KEY `upload_location_id` (`upload_location_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_files` WRITE;
/*!40000 ALTER TABLE `exp_files` DISABLE KEYS */;

INSERT INTO `exp_files` (`file_id`, `site_id`, `title`, `upload_location_id`, `rel_path`, `mime_type`, `file_name`, `file_size`, `description`, `credit`, `location`, `uploaded_by_member_id`, `upload_date`, `modified_by_member_id`, `modified_date`, `file_hw_original`)
VALUES
	(1,1,'banner-home.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-home.jpg','image/jpeg','banner-home.jpg',119514,'','','',1,1361929555,1,1364403474,'474 1700'),
	(2,1,'banner-about.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-about.jpg','image/jpeg','banner-about.jpg',61761,'','','',1,1362711656,1,1364403473,'324 775'),
	(3,1,'banner-home-savoring.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-home-savoring.jpg','image/jpeg','banner-home-savoring.jpg',49747,'','','',1,1362876143,1,1364403473,'324 775'),
	(4,1,'banner-home-shaping.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-home-shaping.jpg','image/jpeg','banner-home-shaping.jpg',45693,'','','',1,1362876177,1,1364403473,'324 775'),
	(5,1,'banner-home-spreading.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-home-spreading.jpg','image/jpeg','banner-home-spreading.jpg',47612,'','','',1,1362876204,1,1364403474,'324 775'),
	(6,1,'banner-home-studying.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-home-studying.jpg','image/jpeg','banner-home-studying.jpg',46106,'','','',1,1362876239,1,1364403474,'324 775'),
	(8,1,'banner-sermons.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-sermons.jpg','image/jpeg','banner-sermons.jpg',53144,'','','',1,1362876612,1,1364403474,'324 775'),
	(9,1,'banner-flocks.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-flocks.jpg','image/jpeg','banner-flocks.jpg',57314,'','','',1,1362876642,1,1364403473,'324 775'),
	(10,1,'banner-ministries.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-ministries.jpg','image/jpeg','banner-ministries.jpg',71804,'','','',1,1362876670,1,1364403474,'324 775'),
	(11,1,'subnav-network.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-network.png','image/png','subnav-network.png',2118,'','','',1,1362876973,1,1364403476,'158 186'),
	(12,1,'subnav-leaders.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-leaders.jpg','image/jpeg','subnav-leaders.jpg',10607,'','','',1,1363046555,1,1364403476,'158 186'),
	(13,1,'subnav-book-of-faith.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-book-of-faith.png','image/png','subnav-book-of-faith.png',1634,'','','',1,1363046676,1,1364403475,'158 186'),
	(14,1,'subnav-mission.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-mission.png','image/png','subnav-mission.png',2330,'','','',1,1363046740,1,1364403476,'158 186'),
	(15,1,'subnav-gatherings.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-gatherings.jpg','image/jpeg','subnav-gatherings.jpg',13670,'','','',1,1363046775,1,1364403475,'158 186'),
	(16,1,'subnav-location.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-location.jpg','image/jpeg','subnav-location.jpg',10284,'','','',1,1363046810,1,1364403476,'158 186'),
	(17,1,'banner-about-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-about-2x.jpg','image/jpeg','banner-about-2x.jpg',156660,'','','',1,1363650861,1,1364403473,'652 1552'),
	(18,1,'banner-flocks-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-flocks-2x.jpg','image/jpeg','banner-flocks-2x.jpg',101426,'','','',1,1363650920,1,1364403473,'652 1552'),
	(19,1,'subnav-flock-groups-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-flock-groups-2x.jpg','image/jpeg','subnav-flock-groups-2x.jpg',24415,'','','',1,1363651106,1,1364403475,'316 372'),
	(20,1,'subnav-flocks-strategies-2x.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-flocks-strategies-2x.png','image/png','subnav-flocks-strategies-2x.png',3053,'','','',1,1363651214,1,1364403475,'316 372'),
	(21,1,'subnav-flocks-values-2x.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-flocks-values-2x.png','image/png','subnav-flocks-values-2x.png',2846,'','','',1,1363651327,1,1364403475,'316 372'),
	(22,1,'subnav-flocks-vision-2x.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-flocks-vision-2x.png','image/png','subnav-flocks-vision-2x.png',3817,'','','',1,1363651376,1,1364403475,'316 372'),
	(23,1,'subnav-about-network-2x.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-about-network-2x.png','image/png','subnav-about-network-2x.png',4398,'','','',1,1363651979,1,1364403475,'316 372'),
	(24,1,'subnav-about-leaders-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-about-leaders-2x.jpg','image/jpeg','subnav-about-leaders-2x.jpg',18260,'','','',1,1363652039,1,1364403475,'316 372'),
	(25,1,'subnav-about-faith-and-order-2x.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-about-faith-and-order-2x.png','image/png','subnav-about-faith-and-order-2x.png',2973,'','','',1,1363652166,1,1364403474,'316 372'),
	(26,1,'Clearcreek-Chapel-Book-of-Faith-and-Order.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Clearcreek-Chapel-Book-of-Faith-and-Order.pdf','application/pdf','Clearcreek-Chapel-Book-of-Faith-and-Order.pdf',106066,'','','',1,1363652480,1,1363652480,' '),
	(27,1,'banner-home-spreading-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-home-spreading-2x.jpg','image/jpeg','banner-home-spreading-2x.jpg',95689,'','','',1,1363653211,1,1364403473,'652 1552'),
	(28,1,'banner-home-studying-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-home-studying-2x.jpg','image/jpeg','banner-home-studying-2x.jpg',92955,'','','',1,1363653227,1,1364403474,'652 1552'),
	(29,1,'banner-home-shaping-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-home-shaping-2x.jpg','image/jpeg','banner-home-shaping-2x.jpg',92306,'','','',1,1363653245,1,1364403473,'652 1552'),
	(30,1,'banner-home-savoring-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-home-savoring-2x.jpg','image/jpeg','banner-home-savoring-2x.jpg',98902,'','','',1,1363653261,1,1364403473,'652 1552'),
	(31,1,'banner-ministries-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-ministries-2x.jpg','image/jpeg','banner-ministries-2x.jpg',120114,'','','',1,1364150555,1,1364403474,'652 1552'),
	(32,1,'banner-news-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/banner-news-2x.jpg','image/jpeg','banner-news-2x.jpg',114324,'','','',1,1364150630,1,1364403474,'652 1552'),
	(33,1,'subnav-sermons-repository-2x.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-sermons-repository-2x.png','image/png','subnav-sermons-repository-2x.png',1740,'','','',1,1364151667,1,1364403477,'316 372'),
	(34,1,'subnav-sermons-next-week-2x.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-sermons-next-week-2x.png','image/png','subnav-sermons-next-week-2x.png',1806,'','','',1,1364151690,1,1364403477,'316 372'),
	(35,1,'subnav-sermons-key-series-2x.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-sermons-key-series-2x.png','image/png','subnav-sermons-key-series-2x.png',1697,'','','',1,1364151714,1,1364403477,'316 372'),
	(36,1,'subnav-sermons-this-week-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-sermons-this-week-2x.jpg','image/jpeg','subnav-sermons-this-week-2x.jpg',16878,'','','',1,1364151738,1,1364403477,'316 372'),
	(37,1,'subnav-ministries-counseling-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-counseling-2x.jpg','image/jpeg','subnav-ministries-counseling-2x.jpg',19268,'','','',1,1364151966,1,1364403476,'316 372'),
	(38,1,'subnav-ministries-discipleship-2x.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-discipleship-2x.png','image/png','subnav-ministries-discipleship-2x.png',2930,'','','',1,1364152021,1,1364403476,'316 372'),
	(39,1,'subnav-ministries-education-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-education-2x.jpg','image/jpeg','subnav-ministries-education-2x.jpg',22775,'','','',1,1364152066,1,1364403476,'316 372'),
	(40,1,'subnav-ministries-outreach-2x.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-outreach-2x.png','image/png','subnav-ministries-outreach-2x.png',4194,'','','',1,1364152100,1,1364403476,'316 372'),
	(41,1,'subnav-ministries-worship-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-worship-2x.jpg','image/jpeg','subnav-ministries-worship-2x.jpg',20965,'','','',1,1364152159,1,1364403476,'316 372'),
	(42,1,'subnav-ministries-worship-icon-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-worship-icon-2x.jpg','image/jpeg','subnav-ministries-worship-icon-2x.jpg',3545,'','','',1,1364299387,1,1364403476,'316 372'),
	(43,1,'subnav-ministries-education-icon-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-education-icon-2x.jpg','image/jpeg','subnav-ministries-education-icon-2x.jpg',2028,'','','',1,1364300181,1,1364403476,'316 372'),
	(44,1,'subnav-ministries-counseling-icon-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-counseling-icon-2x.jpg','image/jpeg','subnav-ministries-counseling-icon-2x.jpg',3308,'','','',1,1364300214,1,1364403476,'316 372'),
	(45,1,'subnav-flock-groups-icon-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-flock-groups-icon-2x.jpg','image/jpeg','subnav-flock-groups-icon-2x.jpg',3196,'','','',1,1364384121,1,1364403475,'316 372'),
	(46,1,'subnav-sermons-this-week-icon-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-sermons-this-week-icon-2x.jpg','image/jpeg','subnav-sermons-this-week-icon-2x.jpg',2657,'','','',1,1364384195,1,1364403477,'316 372'),
	(47,1,'subnav-about-location-icon-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-about-location-icon-2x.jpg','image/jpeg','subnav-about-location-icon-2x.jpg',3294,'','','',1,1364384359,1,1364403475,'316 372'),
	(48,1,'subnav-about-gatherings-icon-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-about-gatherings-icon-2x.jpg','image/jpeg','subnav-about-gatherings-icon-2x.jpg',3210,'','','',1,1364384393,1,1364403475,'316 372'),
	(49,1,'subnav-about-leaders-icon-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-about-leaders-icon-2x.jpg','image/jpeg','subnav-about-leaders-icon-2x.jpg',2718,'','','',1,1364384425,1,1364403475,'316 372'),
	(50,1,'subnav-about-mission-2x.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-about-mission-2x.png','image/png','subnav-about-mission-2x.png',5461,'','','',1,1364384445,1,1364403475,'316 372'),
	(51,1,'subnav-about-gatherings-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-about-gatherings-2x.jpg','image/jpeg','subnav-about-gatherings-2x.jpg',29389,'','','',1,1364403436,1,1364403436,'316 372'),
	(52,1,'subnav-about-location-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-about-location-2x.jpg','image/jpeg','subnav-about-location-2x.jpg',20567,'','','',1,1364403437,1,1364403437,'316 372'),
	(53,1,'subnav-flocks-filler-2.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-flocks-filler-2.jpg','image/jpeg','subnav-flocks-filler-2.jpg',29427,'','','',1,1364403438,1,1364403438,'158 382'),
	(54,1,'subnav-flocks-filler.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-flocks-filler.jpg','image/jpeg','subnav-flocks-filler.jpg',12279,'','','',1,1364403438,1,1364403438,'158 186'),
	(55,1,'subnav-flocks-groups.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-flocks-groups.jpg','image/jpeg','subnav-flocks-groups.jpg',7414,'','','',1,1364403438,1,1364403438,'158 186'),
	(56,1,'subnav-flocks-mission.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-flocks-mission.png','image/png','subnav-flocks-mission.png',4073,'','','',1,1364403438,1,1364403438,'158 186'),
	(57,1,'subnav-flocks-purpose.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-flocks-purpose.png','image/png','subnav-flocks-purpose.png',1607,'','','',1,1364403438,1,1364403438,'158 186'),
	(58,1,'subnav-key-series.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-key-series.png','image/png','subnav-key-series.png',1419,'','','',1,1364403439,1,1364403439,'158 186'),
	(59,1,'subnav-ministries-children-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-children-2x.jpg','image/jpeg','subnav-ministries-children-2x.jpg',13088,'','','',1,1364403439,1,1364403439,'316 372'),
	(60,1,'subnav-ministries-counseling.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-counseling.jpg','image/jpeg','subnav-ministries-counseling.jpg',8906,'','','',1,1364403439,1,1364403439,'158 186'),
	(61,1,'subnav-ministries-discipleship.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-discipleship.png','image/png','subnav-ministries-discipleship.png',1636,'','','',1,1364403440,1,1364403440,'158 186'),
	(62,1,'subnav-ministries-education.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-education.jpg','image/jpeg','subnav-ministries-education.jpg',12260,'','','',1,1364403440,1,1364403440,'158 186'),
	(63,1,'subnav-ministries-filler.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-filler.jpg','image/jpeg','subnav-ministries-filler.jpg',9875,'','','',1,1364403440,1,1364403440,'158 186'),
	(64,1,'subnav-ministries-outreach.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-outreach.png','image/png','subnav-ministries-outreach.png',2122,'','','',1,1364403440,1,1364403440,'158 186'),
	(65,1,'subnav-ministries-worship.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-ministries-worship.jpg','image/jpeg','subnav-ministries-worship.jpg',11974,'','','',1,1364403440,1,1364403440,'158 186'),
	(66,1,'subnav-next-week.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-next-week.png','image/png','subnav-next-week.png',1430,'','','',1,1364403440,1,1364403440,'158 186'),
	(67,1,'subnav-sermon-repository.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-sermon-repository.png','image/png','subnav-sermon-repository.png',1369,'','','',1,1364403441,1,1364403441,'158 186'),
	(68,1,'subnav-sermons-filler-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-sermons-filler-2x.jpg','image/jpeg','subnav-sermons-filler-2x.jpg',39416,'','','',1,1364403441,1,1364403441,'316 764'),
	(69,1,'subnav-sermons-filler.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-sermons-filler.jpg','image/jpeg','subnav-sermons-filler.jpg',20959,'','','',1,1364403441,1,1364403441,'158 382'),
	(70,1,'subnav-this-weeks-sermon.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-this-weeks-sermon.jpg','image/jpeg','subnav-this-weeks-sermon.jpg',9438,'','','',1,1364403442,1,1364403442,'158 186'),
	(71,1,'Chapel_Map.PNG',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Chapel_Map.PNG','image/png','Chapel_Map.PNG',640553,'','','',2,1364419785,2,1364419785,'538 663'),
	(72,1,'Sermon_Manuscript.AM.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Sermon_Manuscript.AM.pdf','application/pdf','Sermon_Manuscript.AM.pdf',296837,'','','',2,1364642450,2,1364642450,' '),
	(73,1,'Sermon_Manuscript.AM1.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Sermon_Manuscript.AM1.pdf','application/pdf','Sermon_Manuscript.AM1.pdf',296837,'','','',2,1364642640,2,1364642640,' '),
	(74,1,'Hebrews.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Hebrews.pdf','application/pdf','Hebrews.pdf',2051021,'','','',2,1364918439,2,1364918439,' '),
	(75,1,'Galatians.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Galatians.pdf','application/pdf','Galatians.pdf',1238077,'','','',2,1364918551,2,1364918551,' '),
	(76,1,'Understanding_the_Church.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Understanding_the_Church.pdf','application/pdf','Understanding_the_Church.pdf',1580063,'','','',2,1364918788,2,1364918788,' '),
	(77,1,'Galatians1.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Galatians1.pdf','application/pdf','Galatians1.pdf',1238077,'','','',2,1364918948,2,1364918948,' '),
	(78,1,'Galatians2.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Galatians2.pdf','application/pdf','Galatians2.pdf',1238077,'','','',2,1364919070,2,1364919070,' '),
	(79,1,'Galatians3.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Galatians3.pdf','application/pdf','Galatians3.pdf',1040773,'','','',2,1364919185,2,1364919185,' '),
	(80,1,'Becoming_a_Commissional_Church.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Becoming_a_Commissional_Church.pdf','application/pdf','Becoming_a_Commissional_Church.pdf',1338245,'','','',2,1364919421,2,1364919421,' '),
	(81,1,'Becoming_a_Commissional_Church1.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Becoming_a_Commissional_Church1.pdf','application/pdf','Becoming_a_Commissional_Church1.pdf',1333729,'','','',2,1364919540,2,1364919540,' '),
	(82,1,'Becoming_a_Commissional_Church2.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Becoming_a_Commissional_Church2.pdf','application/pdf','Becoming_a_Commissional_Church2.pdf',1333729,'','','',2,1364919579,2,1364919579,' '),
	(83,1,'Genesis.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Genesis.pdf','application/pdf','Genesis.pdf',3128115,'','','',2,1364919775,2,1364919775,' '),
	(84,1,'Genesis1.pdf',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Genesis1.pdf','application/pdf','Genesis1.pdf',3128115,'','','',2,1364919809,2,1364919809,' '),
	(85,1,'sermonaudio.PNG',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/sermonaudio.PNG','image/png','sermonaudio.PNG',4863,'','','',2,1364923240,2,1364924333,'50 220'),
	(86,1,'SermonAudio_Button.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/SermonAudio_Button.jpg','image/jpeg','SermonAudio_Button.jpg',3292,'','','',2,1364924351,2,1364924383,'72 72'),
	(87,1,'SermonAudio_Banner.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/SermonAudio_Banner.jpg','image/jpeg','SermonAudio_Banner.jpg',2087,'','','',2,1364924422,2,1364924590,'24 126'),
	(88,1,'subnav-sermons-philosophy-2x.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/subnav-sermons-philosophy-2x.jpg','image/jpeg','subnav-sermons-philosophy-2x.jpg',3123,'','','',1,1364954926,1,1364954926,'316 372'),
	(90,1,'Staff.Kennedy_2008.JPG',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Staff.Kennedy_2008.JPG','image/jpeg','Staff.Kennedy_2008.JPG',2786,'','','',2,1365001145,2,1365001468,'120 80'),
	(92,1,'Chapel_Ministry_Visual.png',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/Chapel_Ministry_Visual.png','image/png','Chapel_Ministry_Visual.png',55401,'','','',2,1365176037,2,1365176349,'297 350'),
	(93,1,'elder-steve-v.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/elder-steve-v.jpg','image/jpeg','elder-steve-v.jpg',25590,'','','',1,1365205171,1,1365205171,'372 372'),
	(94,1,'elder-dan-t.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/elder-dan-t.jpg','image/jpeg','elder-dan-t.jpg',26071,'','','',1,1365205195,1,1365205195,'372 372'),
	(95,1,'elder-mark-s.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/elder-mark-s.jpg','image/jpeg','elder-mark-s.jpg',29071,'','','',1,1365205208,1,1365205208,'372 372'),
	(96,1,'elder-tim-r.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/elder-tim-r.jpg','image/jpeg','elder-tim-r.jpg',24228,'','','',1,1365205238,1,1365205238,'372 372'),
	(97,1,'elder-tim-n.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/elder-tim-n.jpg','image/jpeg','elder-tim-n.jpg',21862,'','','',1,1365205257,1,1365205257,'372 372'),
	(98,1,'elder-russ-k.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/elder-russ-k.jpg','image/jpeg','elder-russ-k.jpg',25979,'','','',1,1365205280,1,1365205280,'372 372'),
	(99,1,'elder-dale-e.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/elder-dale-e.jpg','image/jpeg','elder-dale-e.jpg',25651,'','','',1,1365205302,1,1365205302,'372 372'),
	(100,1,'elder-michael-e.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/elder-michael-e.jpg','image/jpeg','elder-michael-e.jpg',28652,'','','',1,1365205330,1,1365205330,'372 372'),
	(101,1,'elder-chad-b.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/elder-chad-b.jpg','image/jpeg','elder-chad-b.jpg',26706,'','','',1,1365205344,1,1365205344,'372 372'),
	(102,1,'elder-devon-b.jpg',1,'/home/jrocket/public_html/johnnyrocketcreative/clients/clearcreek/uploads/elder-devon-b.jpg','image/jpeg','elder-devon-b.jpg',24689,'','','',1,1365205370,1,1365205370,'372 372'),
	(103,1,'subnav-ministries-worship-icon-1-2x.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/subnav-ministries-worship-icon-1-2x.jpg','image/jpeg','subnav-ministries-worship-icon-1-2x.jpg',2284,NULL,NULL,NULL,1,1365217866,1,1365217874,'316 372'),
	(104,1,'subnav-ministries-worship-icon-2-2x.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/subnav-ministries-worship-icon-2-2x.jpg','image/jpeg','subnav-ministries-worship-icon-2-2x.jpg',3062,NULL,NULL,NULL,1,1365218020,1,1365218024,'316 372'),
	(105,1,'sermons-philosophy.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/sermons-philosophy.jpg','image/jpeg','sermons-philosophy.jpg',36188,NULL,NULL,NULL,1,1365284487,1,1365284487,'432 764'),
	(106,1,'sermons-key-text.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/sermons-key-text.jpg','image/jpeg','sermons-key-text.jpg',45773,NULL,NULL,NULL,1,1365284728,1,1365284728,'432 764'),
	(107,1,'sermons-this-weeks.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/sermons-this-weeks.jpg','image/jpeg','sermons-this-weeks.jpg',68792,NULL,NULL,NULL,1,1365284800,1,1365284800,'432 764'),
	(108,1,'flocks-values.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/flocks-values.jpg','image/jpeg','flocks-values.jpg',46694,NULL,NULL,NULL,1,1365285267,1,1365285267,'432 764'),
	(109,1,'flocks-vision.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/flocks-vision.jpg','image/jpeg','flocks-vision.jpg',43633,NULL,NULL,NULL,1,1365285797,1,1365285797,'432 764'),
	(110,1,'flocks-strategies.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/flocks-strategies.jpg','image/jpeg','flocks-strategies.jpg',87030,NULL,NULL,NULL,1,1365286043,1,1365286043,'432 764'),
	(111,1,'about-gatherings.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/about-gatherings.jpg','image/jpeg','about-gatherings.jpg',67727,NULL,NULL,NULL,1,1365286281,1,1365286281,'432 764'),
	(112,1,'about-our-mission.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/about-our-mission.jpg','image/jpeg','about-our-mission.jpg',49859,NULL,NULL,NULL,1,1365286343,1,1365286343,'432 764'),
	(113,1,'about-network-association.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/about-network-association.jpg','image/jpeg','about-network-association.jpg',57559,NULL,NULL,NULL,1,1365286413,1,1365286413,'432 764'),
	(114,1,'about-book-of-faith-order.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/about-book-of-faith-order.jpg','image/jpeg','about-book-of-faith-order.jpg',55224,NULL,NULL,NULL,1,1365286485,1,1365286485,'432 764'),
	(115,1,'about-book-of-faith-order1.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/about-book-of-faith-order1.jpg','image/jpeg','about-book-of-faith-order1.jpg',55224,NULL,NULL,NULL,1,1365286557,1,1365286557,'432 764'),
	(117,1,'subnav-ministries-expansion-2x.png',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/subnav-ministries-expansion-2x.png','image/png','subnav-ministries-expansion-2x.png',5622,NULL,NULL,NULL,1,1367856606,1,1367856606,'316 372'),
	(118,1,'Chapel_Expansion_Icon.png',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/Chapel_Expansion_Icon.png','image/png','Chapel_Expansion_Icon.png',4058,NULL,NULL,NULL,2,1368176552,2,1368176788,'68 75'),
	(119,1,'becoming-commissional.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/becoming-commissional.jpg','image/jpeg','becoming-commissional.jpg',8970,NULL,NULL,NULL,1,1368557608,1,1368557608,'171 132'),
	(120,1,'gospel-centered-worship.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/gospel-centered-worship.jpg','image/jpeg','gospel-centered-worship.jpg',10404,NULL,NULL,NULL,1,1368557631,1,1368557631,'171 128'),
	(121,1,'gospel-love.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/gospel-love.jpg','image/jpeg','gospel-love.jpg',6646,NULL,NULL,NULL,1,1368557646,1,1368557646,'171 128'),
	(122,1,'christianity-and-its-competitors.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/christianity-and-its-competitors.jpg','image/jpeg','christianity-and-its-competitors.jpg',7301,NULL,NULL,NULL,1,1368557663,1,1368557663,'171 127'),
	(123,1,'chapel-starters.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/chapel-starters.jpg','image/jpeg','chapel-starters.jpg',3215,NULL,NULL,NULL,1,1368557685,1,1368557685,'171 127'),
	(124,1,'elder-greg-s.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/elder-greg-s.jpg','image/jpeg','elder-greg-s.jpg',13609,NULL,NULL,NULL,5,1369841472,5,1369841472,'404 405'),
	(125,1,'festival-favorites.jpg',1,'/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/festival-favorites.jpg','image/jpeg','festival-favorites.jpg',127334,NULL,NULL,NULL,5,1371491595,5,1371491595,'432 764');

/*!40000 ALTER TABLE `exp_files` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_global_variables
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_global_variables`;

CREATE TABLE `exp_global_variables` (
  `variable_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `variable_name` varchar(50) NOT NULL,
  `variable_data` text NOT NULL,
  PRIMARY KEY  (`variable_id`),
  KEY `variable_name` (`variable_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_html_buttons
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_html_buttons`;

CREATE TABLE `exp_html_buttons` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `member_id` int(10) NOT NULL default '0',
  `tag_name` varchar(32) NOT NULL,
  `tag_open` varchar(120) NOT NULL,
  `tag_close` varchar(120) NOT NULL,
  `accesskey` varchar(32) NOT NULL,
  `tag_order` int(3) unsigned NOT NULL,
  `tag_row` char(1) NOT NULL default '1',
  `classname` varchar(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_html_buttons` WRITE;
/*!40000 ALTER TABLE `exp_html_buttons` DISABLE KEYS */;

INSERT INTO `exp_html_buttons` (`id`, `site_id`, `member_id`, `tag_name`, `tag_open`, `tag_close`, `accesskey`, `tag_order`, `tag_row`, `classname`)
VALUES
	(1,1,0,'b','<strong>','</strong>','b',1,'1','btn_b'),
	(2,1,0,'i','<em>','</em>','i',2,'1','btn_i'),
	(3,1,0,'blockquote','<blockquote>','</blockquote>','q',3,'1','btn_blockquote'),
	(4,1,0,'a','<a href=\"[![Link:!:http://]!]\"(!( title=\"[![Title]!]\")!)>','</a>','a',4,'1','btn_a'),
	(5,1,0,'img','<img src=\"[![Link:!:http://]!]\" alt=\"[![Alternative text]!]\" />','','',5,'1','btn_img');

/*!40000 ALTER TABLE `exp_html_buttons` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_layout_publish
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_layout_publish`;

CREATE TABLE `exp_layout_publish` (
  `layout_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `member_group` int(4) unsigned NOT NULL default '0',
  `channel_id` int(4) unsigned NOT NULL default '0',
  `field_layout` text,
  PRIMARY KEY  (`layout_id`),
  KEY `site_id` (`site_id`),
  KEY `member_group` (`member_group`),
  KEY `channel_id` (`channel_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_layout_publish` WRITE;
/*!40000 ALTER TABLE `exp_layout_publish` DISABLE KEYS */;

INSERT INTO `exp_layout_publish` (`layout_id`, `site_id`, `member_group`, `channel_id`, `field_layout`)
VALUES
	(1,1,1,12,'a:5:{s:7:\"publish\";a:10:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:28;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:29;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:30;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:31;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:32;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:33;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:34;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:3:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:8:\"seo_lite\";a:4:{s:10:\"_tab_label\";s:8:\"SEO Lite\";s:24:\"seo_lite__seo_lite_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:27:\"seo_lite__seo_lite_keywords\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:30:\"seo_lite__seo_lite_description\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}'),
	(2,1,6,12,'a:5:{s:7:\"publish\";a:10:{s:10:\"_tab_label\";s:7:\"Publish\";s:5:\"title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:9:\"url_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:1;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}i:28;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:29;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:30;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:31;a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:32;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:33;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}i:34;a:4:{s:7:\"visible\";s:4:\"true\";s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:0;s:5:\"width\";s:4:\"100%\";}}s:4:\"date\";a:3:{s:10:\"_tab_label\";s:4:\"Date\";s:10:\"entry_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:15:\"expiration_date\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:10:\"categories\";a:2:{s:10:\"_tab_label\";s:10:\"Categories\";s:8:\"category\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:7:\"options\";a:6:{s:10:\"_tab_label\";s:7:\"Options\";s:11:\"new_channel\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"status\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:6:\"author\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:7:\"options\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:4:\"ping\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}s:8:\"seo_lite\";a:4:{s:10:\"_tab_label\";s:8:\"SEO Lite\";s:24:\"seo_lite__seo_lite_title\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:27:\"seo_lite__seo_lite_keywords\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}s:30:\"seo_lite__seo_lite_description\";a:4:{s:7:\"visible\";b:1;s:8:\"collapse\";b:0;s:11:\"htmlbuttons\";b:1;s:5:\"width\";s:4:\"100%\";}}}');

/*!40000 ALTER TABLE `exp_layout_publish` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_member_bulletin_board
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_member_bulletin_board`;

CREATE TABLE `exp_member_bulletin_board` (
  `bulletin_id` int(10) unsigned NOT NULL auto_increment,
  `sender_id` int(10) unsigned NOT NULL,
  `bulletin_group` int(8) unsigned NOT NULL,
  `bulletin_date` int(10) unsigned NOT NULL,
  `hash` varchar(10) NOT NULL default '',
  `bulletin_expires` int(10) unsigned NOT NULL default '0',
  `bulletin_message` text NOT NULL,
  PRIMARY KEY  (`bulletin_id`),
  KEY `sender_id` (`sender_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_member_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_member_data`;

CREATE TABLE `exp_member_data` (
  `member_id` int(10) unsigned NOT NULL,
  PRIMARY KEY  (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_member_data` WRITE;
/*!40000 ALTER TABLE `exp_member_data` DISABLE KEYS */;

INSERT INTO `exp_member_data` (`member_id`)
VALUES
	(1),
	(2),
	(3),
	(4),
	(5),
	(6),
	(7);

/*!40000 ALTER TABLE `exp_member_data` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_member_fields
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_member_fields`;

CREATE TABLE `exp_member_fields` (
  `m_field_id` int(4) unsigned NOT NULL auto_increment,
  `m_field_name` varchar(32) NOT NULL,
  `m_field_label` varchar(50) NOT NULL,
  `m_field_description` text NOT NULL,
  `m_field_type` varchar(12) NOT NULL default 'text',
  `m_field_list_items` text NOT NULL,
  `m_field_ta_rows` tinyint(2) default '8',
  `m_field_maxl` smallint(3) NOT NULL,
  `m_field_width` varchar(6) NOT NULL,
  `m_field_search` char(1) NOT NULL default 'y',
  `m_field_required` char(1) NOT NULL default 'n',
  `m_field_public` char(1) NOT NULL default 'y',
  `m_field_reg` char(1) NOT NULL default 'n',
  `m_field_cp_reg` char(1) NOT NULL default 'n',
  `m_field_fmt` char(5) NOT NULL default 'none',
  `m_field_order` int(3) unsigned NOT NULL,
  PRIMARY KEY  (`m_field_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_member_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_member_groups`;

CREATE TABLE `exp_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_title` varchar(100) NOT NULL,
  `group_description` text NOT NULL,
  `is_locked` char(1) NOT NULL default 'y',
  `can_view_offline_system` char(1) NOT NULL default 'n',
  `can_view_online_system` char(1) NOT NULL default 'y',
  `can_access_cp` char(1) NOT NULL default 'y',
  `can_access_content` char(1) NOT NULL default 'n',
  `can_access_publish` char(1) NOT NULL default 'n',
  `can_access_edit` char(1) NOT NULL default 'n',
  `can_access_files` char(1) NOT NULL default 'n',
  `can_access_fieldtypes` char(1) NOT NULL default 'n',
  `can_access_design` char(1) NOT NULL default 'n',
  `can_access_addons` char(1) NOT NULL default 'n',
  `can_access_modules` char(1) NOT NULL default 'n',
  `can_access_extensions` char(1) NOT NULL default 'n',
  `can_access_accessories` char(1) NOT NULL default 'n',
  `can_access_plugins` char(1) NOT NULL default 'n',
  `can_access_members` char(1) NOT NULL default 'n',
  `can_access_admin` char(1) NOT NULL default 'n',
  `can_access_sys_prefs` char(1) NOT NULL default 'n',
  `can_access_content_prefs` char(1) NOT NULL default 'n',
  `can_access_tools` char(1) NOT NULL default 'n',
  `can_access_comm` char(1) NOT NULL default 'n',
  `can_access_utilities` char(1) NOT NULL default 'n',
  `can_access_data` char(1) NOT NULL default 'n',
  `can_access_logs` char(1) NOT NULL default 'n',
  `can_admin_channels` char(1) NOT NULL default 'n',
  `can_admin_upload_prefs` char(1) NOT NULL default 'n',
  `can_admin_design` char(1) NOT NULL default 'n',
  `can_admin_members` char(1) NOT NULL default 'n',
  `can_delete_members` char(1) NOT NULL default 'n',
  `can_admin_mbr_groups` char(1) NOT NULL default 'n',
  `can_admin_mbr_templates` char(1) NOT NULL default 'n',
  `can_ban_users` char(1) NOT NULL default 'n',
  `can_admin_modules` char(1) NOT NULL default 'n',
  `can_admin_templates` char(1) NOT NULL default 'n',
  `can_admin_accessories` char(1) NOT NULL default 'n',
  `can_edit_categories` char(1) NOT NULL default 'n',
  `can_delete_categories` char(1) NOT NULL default 'n',
  `can_view_other_entries` char(1) NOT NULL default 'n',
  `can_edit_other_entries` char(1) NOT NULL default 'n',
  `can_assign_post_authors` char(1) NOT NULL default 'n',
  `can_delete_self_entries` char(1) NOT NULL default 'n',
  `can_delete_all_entries` char(1) NOT NULL default 'n',
  `can_view_other_comments` char(1) NOT NULL default 'n',
  `can_edit_own_comments` char(1) NOT NULL default 'n',
  `can_delete_own_comments` char(1) NOT NULL default 'n',
  `can_edit_all_comments` char(1) NOT NULL default 'n',
  `can_delete_all_comments` char(1) NOT NULL default 'n',
  `can_moderate_comments` char(1) NOT NULL default 'n',
  `can_send_email` char(1) NOT NULL default 'n',
  `can_send_cached_email` char(1) NOT NULL default 'n',
  `can_email_member_groups` char(1) NOT NULL default 'n',
  `can_email_mailinglist` char(1) NOT NULL default 'n',
  `can_email_from_profile` char(1) NOT NULL default 'n',
  `can_view_profiles` char(1) NOT NULL default 'n',
  `can_edit_html_buttons` char(1) NOT NULL default 'n',
  `can_delete_self` char(1) NOT NULL default 'n',
  `mbr_delete_notify_emails` varchar(255) default NULL,
  `can_post_comments` char(1) NOT NULL default 'n',
  `exclude_from_moderation` char(1) NOT NULL default 'n',
  `can_search` char(1) NOT NULL default 'n',
  `search_flood_control` mediumint(5) unsigned NOT NULL,
  `can_send_private_messages` char(1) NOT NULL default 'n',
  `prv_msg_send_limit` smallint(5) unsigned NOT NULL default '20',
  `prv_msg_storage_limit` smallint(5) unsigned NOT NULL default '60',
  `can_attach_in_private_messages` char(1) NOT NULL default 'n',
  `can_send_bulletins` char(1) NOT NULL default 'n',
  `include_in_authorlist` char(1) NOT NULL default 'n',
  `include_in_memberlist` char(1) NOT NULL default 'y',
  `include_in_mailinglists` char(1) NOT NULL default 'y',
  PRIMARY KEY  (`group_id`,`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_member_groups` WRITE;
/*!40000 ALTER TABLE `exp_member_groups` DISABLE KEYS */;

INSERT INTO `exp_member_groups` (`group_id`, `site_id`, `group_title`, `group_description`, `is_locked`, `can_view_offline_system`, `can_view_online_system`, `can_access_cp`, `can_access_content`, `can_access_publish`, `can_access_edit`, `can_access_files`, `can_access_fieldtypes`, `can_access_design`, `can_access_addons`, `can_access_modules`, `can_access_extensions`, `can_access_accessories`, `can_access_plugins`, `can_access_members`, `can_access_admin`, `can_access_sys_prefs`, `can_access_content_prefs`, `can_access_tools`, `can_access_comm`, `can_access_utilities`, `can_access_data`, `can_access_logs`, `can_admin_channels`, `can_admin_upload_prefs`, `can_admin_design`, `can_admin_members`, `can_delete_members`, `can_admin_mbr_groups`, `can_admin_mbr_templates`, `can_ban_users`, `can_admin_modules`, `can_admin_templates`, `can_admin_accessories`, `can_edit_categories`, `can_delete_categories`, `can_view_other_entries`, `can_edit_other_entries`, `can_assign_post_authors`, `can_delete_self_entries`, `can_delete_all_entries`, `can_view_other_comments`, `can_edit_own_comments`, `can_delete_own_comments`, `can_edit_all_comments`, `can_delete_all_comments`, `can_moderate_comments`, `can_send_email`, `can_send_cached_email`, `can_email_member_groups`, `can_email_mailinglist`, `can_email_from_profile`, `can_view_profiles`, `can_edit_html_buttons`, `can_delete_self`, `mbr_delete_notify_emails`, `can_post_comments`, `exclude_from_moderation`, `can_search`, `search_flood_control`, `can_send_private_messages`, `prv_msg_send_limit`, `prv_msg_storage_limit`, `can_attach_in_private_messages`, `can_send_bulletins`, `include_in_authorlist`, `include_in_memberlist`, `include_in_mailinglists`)
VALUES
	(1,1,'Super Admins','','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','y','','y','y','y',0,'y',20,60,'y','y','y','y','y'),
	(2,1,'Banned','','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','','n','n','n',60,'n',20,60,'n','n','n','n','n'),
	(3,1,'Guests','','y','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','n','n','n','n','','y','n','y',15,'n',20,60,'n','n','n','n','n'),
	(4,1,'Pending','','y','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','n','n','n','n','','y','n','y',15,'n',20,60,'n','n','n','n','n'),
	(5,1,'Members','','y','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','y','y','n','','y','n','y',10,'y',20,60,'y','n','n','y','y'),
	(6,1,'Publisher','','y','n','y','y','y','y','y','y','n','n','n','n','n','n','n','n','n','n','n','n','n','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','y','y','n','n','y','y','n','y','n','n','n','n','n','n','n','y','n','n','','y','n','y',15,'n',20,60,'n','n','y','y','n'),
	(7,1,'Editor','','y','n','y','y','y','n','y','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','','y','n','y',15,'n',20,60,'n','n','n','n','n'),
	(8,1,'SEO Editor','','y','y','y','y','y','n','y','n','n','n','y','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','n','y','y','y','n','n','n','n','n','n','n','n','n','n','n','y','n','n','n','n','','n','n','n',15,'n',20,60,'n','n','n','n','n'),
	(9,1,'Content Manager','','y','y','y','y','y','y','y','y','n','n','y','y','n','n','n','n','y','n','y','n','n','n','n','n','y','n','n','n','n','n','n','n','n','n','n','y','y','y','y','y','y','y','n','n','n','n','n','n','n','n','n','n','n','n','n','n','','n','n','y',15,'n',20,60,'n','n','n','n','n');

/*!40000 ALTER TABLE `exp_member_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_member_homepage
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_member_homepage`;

CREATE TABLE `exp_member_homepage` (
  `member_id` int(10) unsigned NOT NULL,
  `recent_entries` char(1) NOT NULL default 'l',
  `recent_entries_order` int(3) unsigned NOT NULL default '0',
  `recent_comments` char(1) NOT NULL default 'l',
  `recent_comments_order` int(3) unsigned NOT NULL default '0',
  `recent_members` char(1) NOT NULL default 'n',
  `recent_members_order` int(3) unsigned NOT NULL default '0',
  `site_statistics` char(1) NOT NULL default 'r',
  `site_statistics_order` int(3) unsigned NOT NULL default '0',
  `member_search_form` char(1) NOT NULL default 'n',
  `member_search_form_order` int(3) unsigned NOT NULL default '0',
  `notepad` char(1) NOT NULL default 'r',
  `notepad_order` int(3) unsigned NOT NULL default '0',
  `bulletin_board` char(1) NOT NULL default 'r',
  `bulletin_board_order` int(3) unsigned NOT NULL default '0',
  `pmachine_news_feed` char(1) NOT NULL default 'n',
  `pmachine_news_feed_order` int(3) unsigned NOT NULL default '0',
  PRIMARY KEY  (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_member_homepage` WRITE;
/*!40000 ALTER TABLE `exp_member_homepage` DISABLE KEYS */;

INSERT INTO `exp_member_homepage` (`member_id`, `recent_entries`, `recent_entries_order`, `recent_comments`, `recent_comments_order`, `recent_members`, `recent_members_order`, `site_statistics`, `site_statistics_order`, `member_search_form`, `member_search_form_order`, `notepad`, `notepad_order`, `bulletin_board`, `bulletin_board_order`, `pmachine_news_feed`, `pmachine_news_feed_order`)
VALUES
	(1,'l',1,'l',2,'n',0,'r',1,'n',0,'r',2,'r',0,'l',0),
	(2,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0),
	(3,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0),
	(4,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0),
	(5,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0),
	(6,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0),
	(7,'l',0,'l',0,'n',0,'r',0,'n',0,'r',0,'r',0,'n',0);

/*!40000 ALTER TABLE `exp_member_homepage` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_member_search
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_member_search`;

CREATE TABLE `exp_member_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) unsigned NOT NULL default '1',
  `search_date` int(10) unsigned NOT NULL,
  `keywords` varchar(200) NOT NULL,
  `fields` varchar(200) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(8) unsigned NOT NULL,
  `query` text NOT NULL,
  PRIMARY KEY  (`search_id`),
  KEY `member_id` (`member_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_members
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_members`;

CREATE TABLE `exp_members` (
  `member_id` int(10) unsigned NOT NULL auto_increment,
  `group_id` smallint(4) NOT NULL default '0',
  `username` varchar(50) NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `password` varchar(128) NOT NULL,
  `salt` varchar(128) NOT NULL default '',
  `unique_id` varchar(40) NOT NULL,
  `crypt_key` varchar(40) default NULL,
  `authcode` varchar(10) default NULL,
  `email` varchar(72) NOT NULL,
  `url` varchar(150) default NULL,
  `location` varchar(50) default NULL,
  `occupation` varchar(80) default NULL,
  `interests` varchar(120) default NULL,
  `bday_d` int(2) default NULL,
  `bday_m` int(2) default NULL,
  `bday_y` int(4) default NULL,
  `aol_im` varchar(50) default NULL,
  `yahoo_im` varchar(50) default NULL,
  `msn_im` varchar(50) default NULL,
  `icq` varchar(50) default NULL,
  `bio` text,
  `signature` text,
  `avatar_filename` varchar(120) default NULL,
  `avatar_width` int(4) unsigned default NULL,
  `avatar_height` int(4) unsigned default NULL,
  `photo_filename` varchar(120) default NULL,
  `photo_width` int(4) unsigned default NULL,
  `photo_height` int(4) unsigned default NULL,
  `sig_img_filename` varchar(120) default NULL,
  `sig_img_width` int(4) unsigned default NULL,
  `sig_img_height` int(4) unsigned default NULL,
  `ignore_list` text,
  `private_messages` int(4) unsigned NOT NULL default '0',
  `accept_messages` char(1) NOT NULL default 'y',
  `last_view_bulletins` int(10) NOT NULL default '0',
  `last_bulletin_date` int(10) NOT NULL default '0',
  `ip_address` varchar(45) NOT NULL default '0',
  `join_date` int(10) unsigned NOT NULL default '0',
  `last_visit` int(10) unsigned NOT NULL default '0',
  `last_activity` int(10) unsigned NOT NULL default '0',
  `total_entries` mediumint(8) unsigned NOT NULL default '0',
  `total_comments` mediumint(8) unsigned NOT NULL default '0',
  `total_forum_topics` mediumint(8) NOT NULL default '0',
  `total_forum_posts` mediumint(8) NOT NULL default '0',
  `last_entry_date` int(10) unsigned NOT NULL default '0',
  `last_comment_date` int(10) unsigned NOT NULL default '0',
  `last_forum_post_date` int(10) unsigned NOT NULL default '0',
  `last_email_date` int(10) unsigned NOT NULL default '0',
  `in_authorlist` char(1) NOT NULL default 'n',
  `accept_admin_email` char(1) NOT NULL default 'y',
  `accept_user_email` char(1) NOT NULL default 'y',
  `notify_by_default` char(1) NOT NULL default 'y',
  `notify_of_pm` char(1) NOT NULL default 'y',
  `display_avatars` char(1) NOT NULL default 'y',
  `display_signatures` char(1) NOT NULL default 'y',
  `parse_smileys` char(1) NOT NULL default 'y',
  `smart_notifications` char(1) NOT NULL default 'y',
  `language` varchar(50) NOT NULL,
  `timezone` varchar(8) NOT NULL,
  `daylight_savings` char(1) NOT NULL default 'n',
  `localization_is_site_default` char(1) NOT NULL default 'n',
  `time_format` char(2) NOT NULL default 'us',
  `cp_theme` varchar(32) default NULL,
  `profile_theme` varchar(32) default NULL,
  `forum_theme` varchar(32) default NULL,
  `tracker` text,
  `template_size` varchar(2) NOT NULL default '20',
  `notepad` text,
  `notepad_size` varchar(2) NOT NULL default '18',
  `quick_links` text,
  `quick_tabs` text,
  `show_sidebar` char(1) NOT NULL default 'n',
  `pmember_id` int(10) NOT NULL default '0',
  `rte_enabled` char(1) NOT NULL default 'y',
  `rte_toolset_id` int(10) NOT NULL default '0',
  PRIMARY KEY  (`member_id`),
  KEY `group_id` (`group_id`),
  KEY `unique_id` (`unique_id`),
  KEY `password` (`password`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_members` WRITE;
/*!40000 ALTER TABLE `exp_members` DISABLE KEYS */;

INSERT INTO `exp_members` (`member_id`, `group_id`, `username`, `screen_name`, `password`, `salt`, `unique_id`, `crypt_key`, `authcode`, `email`, `url`, `location`, `occupation`, `interests`, `bday_d`, `bday_m`, `bday_y`, `aol_im`, `yahoo_im`, `msn_im`, `icq`, `bio`, `signature`, `avatar_filename`, `avatar_width`, `avatar_height`, `photo_filename`, `photo_width`, `photo_height`, `sig_img_filename`, `sig_img_width`, `sig_img_height`, `ignore_list`, `private_messages`, `accept_messages`, `last_view_bulletins`, `last_bulletin_date`, `ip_address`, `join_date`, `last_visit`, `last_activity`, `total_entries`, `total_comments`, `total_forum_topics`, `total_forum_posts`, `last_entry_date`, `last_comment_date`, `last_forum_post_date`, `last_email_date`, `in_authorlist`, `accept_admin_email`, `accept_user_email`, `notify_by_default`, `notify_of_pm`, `display_avatars`, `display_signatures`, `parse_smileys`, `smart_notifications`, `language`, `timezone`, `daylight_savings`, `localization_is_site_default`, `time_format`, `cp_theme`, `profile_theme`, `forum_theme`, `tracker`, `template_size`, `notepad`, `notepad_size`, `quick_links`, `quick_tabs`, `show_sidebar`, `pmember_id`, `rte_enabled`, `rte_toolset_id`)
VALUES
	(1,1,'jondaiello','Jon Daiello','8d1a7a8975ca59623f422630f890bbc2184b8829b7ab1a3ba7a60512ba53b35cdd912b1e7af6fa893c7b53b6179b7c5613f56422d91feda7d68ee5e2b27b6761','dQky[t+yA`5Y#sm{U^YML=U5bLx7hiTXld=|AVD{00}L6zfc!Ob4^]-M#f^vY){_[.\"GDCc}q#eRX9Du#GB*5B[it7C2RZO7s#EPCZBr32TT^hP.g;gVk=fZ.4ZP[T^l','512c091afb61b2fdb250b6ea4b3e967a32317b91','9dc047fb1a0c33c09f3fbea0c1c0c3454cd501eb','','johnnyrocketcreative@gmail.com','','','','',0,0,0,'','','','','','','',0,0,'',0,0,'',0,0,'',0,'y',0,0,'107.9.12.83',1361660486,1372431990,1373225999,35,0,0,0,1369320669,0,0,0,'n','y','y','y','y','y','y','y','y','english','UTC','n','n','us','','','','','20','','18','','Backup Pro(ish)|C=addons_modules&M=show_module_cp&module=backup_proish|1\nSEO Lite|C=addons_modules&M=show_module_cp&module=seo_lite|2','n',0,'y',0),
	(2,1,'russ.kennedy','Russ Kennedy','79b82e4bacaa341058ceaf9747dc85aaf1ed3e9e6b8e9f927c532efbe8600755ed5973e331644991f04cc1b71f40190c438a5bb0bc284e479e74ec4c8bfe48ff','k?28uaHa@(W[<{e3KnI-_]\"ioA#u\\SLi+BqXga7dj&Zj-$aO,n,>N%mT_0q*q\'d??}yac39}g,OSJ2A$YTW`\\ITMfSLjabDBQ#i7y5\\MtgQ7pKX1BB>`&mU9\\9qms^v/','0f7630d81b60b9ee619b5b56b7e908843363fd6d','96b22513726db98389168d2b6a8a6d7cd2e188f1','','pastorruss@clearcreekchapel.org','http://www.clearcreekchapel.org','','','',0,0,0,'','','','','','','',0,0,'',0,0,'',0,0,'',0,'y',0,0,'107.9.12.83',1361889811,1368181325,1368387615,2,0,0,0,1365002191,0,0,0,'n','y','y','y','y','y','y','y','y','english','UTC','n','n','us','','','','','20','Have styles defined according to specs','18','Issues and Bugs|https://bitbucket.org/jondaiello/clearcreek-chapel-website/issues?status=new&status=open _blank|1\nManual|http://ellislab.com/expressionengine/user-guide/index.html _blank|2','','n',0,'y',0),
	(3,6,'john.ambro','john.ambro','b09d03b7dbb94aa36f9d36190c49ba7254254acb20780407a4f5426d3c5033e03f3cfcc37a36f71fd81d4c26fe96834d4565d83e0ff44a99500a8979b411c709','n$G=!hmbLwaeE:fyP!t#\'FjsYNe?pS`voBqQ?|zmg%$[T?^WB,K$A3ZRTU+%Fte%]d)aL\\F#7GR{_uJVi3T76E\'PJgt?F?aD:p<9n\"9A-DZ|i+AeY^BrF4s\"PE1\\}.GB','0440028383ad83221dd9ec6c0e2de4437f553a4c','9f7580fa58199b00977d9e86c32d2c3af7cfe9b2','','johnnycat1700@yahoo.com','','','','',0,0,0,'','','','','','','',0,0,'',0,0,'',0,0,'',0,'y',0,0,'107.9.12.83',1363696656,0,0,0,0,0,0,0,0,0,0,'n','y','y','y','y','y','y','y','y','english','UTC','n','n','us','','','','','20','','18','','','n',0,'y',0),
	(4,7,'Chapel.Staff','Chapel Staff','97e165ddaac218af12149251be215460aab9d3d1cd599b7fdde4c203ffad61ce6dee1de4cd913af38a81f94ea74e22637bfb999cd41e269e5532fda4b010a81f','HlTPbq$[JB)j8[+gB(Hvz.a6{if7mPJDz7\"[2KRKR<cdN5F.)6u\'Fuj-YmKp]moxizC.k|Q0{jGr%Q5_&%9[pFKIOY7Bw2q9p;#c1-.>SzpIAl:5Tmc\\a=hj3X%Z]MUc','7dbcb61dc9311276dccb80d9975798fd849bfa24','243df6a0d3f73ec7f53fb8e41d3dc3f10de39785','','admin@clearcreekchapel.org','','','','',0,0,0,'','','','','','','',0,0,'',0,0,'',0,0,'',0,'y',0,0,'70.60.44.186',1364400919,1364828851,1365005590,0,0,0,0,0,0,0,0,'n','y','y','y','y','y','y','y','y','english','UTC','n','n','us','','','','','20','','18','','','n',0,'y',0),
	(5,9,'Content.Manager','Content.Manager','122c3cfc26cb46d7e1f8a8322620c1f03437cce8a3ca192ec0d7e6e2b857fb504b60b8de39986f0ea3fb1773f24dc6a14178c9c1a4a4b04648a3caed4e7e11b4','6p!^Znjc?RV=?n75*\'VyR)H\"unKdFZNY#mx,p6)2<W??>\'7kGaIl2zwB$6B;Q#:_*Qn*?.s+F\\&\"LOUs4cHXEJ5nvy\")fr+NS:g-52)PJ.]Loh)_[$OUC=fHeaa.A.d3','5f0d9e5600ba519f4f0d519a5d54918413511096','1e3972856cd3e9ebf771934c51d51448588eca27','','bookkeeper@clearcreekchapel.org','','','','',0,0,0,'','','','','','','',0,0,'',0,0,'',0,0,'',0,'y',0,0,'108.254.190.58',1365005253,1373906442,1374160964,16,0,0,0,1369755069,0,0,0,'n','y','y','y','y','y','y','y','y','english','UTC','n','n','us','','','','','20','','18','','','n',0,'y',0),
	(6,1,'myockey','Michael Yockey','be79e89daf0e23fa4dd1d93f18b13d9599f53fc6133fad77b2f550d808a42e61d35109d817af4241082f1cedcdd71585efacc22d0d2ed8b8c3a995d915c17f36','h9e:/`@Jt?s%*,lyHj0xc>uN(Y>8x8,Ju=Kr[{Xju/\\ou]u\\Oli0iV~4.7t2OIv;=x,c%*yXDrhPz*Q?vI.PpP$LoD4yFht[W-{,:Y}3[U!iZXRhk20Zy+Q)))P]`T.!','0c60e155e8dfab70537ac2312011e19fa6be2415','1073e5b85036ad4f92a67db0e3d000bd5fbdd61a',NULL,'myockey@gmail.com','','','','',NULL,NULL,NULL,'','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'107.9.25.183',1365257147,1365266394,1365268009,0,0,0,0,0,0,0,0,'n','y','y','y','y','y','y','y','y','english','UTC','n','n','us',NULL,NULL,NULL,NULL,'20',NULL,'18',NULL,NULL,'n',0,'y',0),
	(7,8,'Mark.Schindler','Mark.Schindler','4218e746c4ee931abc0af161cb818da488fb94c9cf79864d0c362d56e40222955d8582acf98bb01c9c5b7b86e96f9ba40525d519b8af9e4c9a585e578bc344d4','|7=NNw=6|j4kkM~#r8:#nbE1N7?yc?[FrM~b4sn\'.gN0!bGK2Y][?\\3Zo*N5JC@wd[emO\"q)b}>8/Mf!F&r[0RU=S(p\\X.._X~hN|C0s}k/GgXvd.IJHW:J]MLYRg{gc','b62a710dd7dc7a3284cd78278f55ba4c2dd1cde6','e40c9e0567175f8d7e1001a6b5c0ed7d4fece5ba',NULL,'pastormark@clearcreekchapel.org','','','','',NULL,NULL,NULL,'','','','','',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,'y',0,0,'108.254.190.58',1365517618,1365517761,1365517761,0,0,0,0,0,0,0,0,'n','y','y','y','y','y','y','y','y','english','UTC','n','n','us',NULL,NULL,NULL,NULL,'20',NULL,'18',NULL,NULL,'n',0,'y',0);

/*!40000 ALTER TABLE `exp_members` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_message_attachments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_message_attachments`;

CREATE TABLE `exp_message_attachments` (
  `attachment_id` int(10) unsigned NOT NULL auto_increment,
  `sender_id` int(10) unsigned NOT NULL default '0',
  `message_id` int(10) unsigned NOT NULL default '0',
  `attachment_name` varchar(50) NOT NULL default '',
  `attachment_hash` varchar(40) NOT NULL default '',
  `attachment_extension` varchar(20) NOT NULL default '',
  `attachment_location` varchar(150) NOT NULL default '',
  `attachment_date` int(10) unsigned NOT NULL default '0',
  `attachment_size` int(10) unsigned NOT NULL default '0',
  `is_temp` char(1) NOT NULL default 'y',
  PRIMARY KEY  (`attachment_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_message_copies
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_message_copies`;

CREATE TABLE `exp_message_copies` (
  `copy_id` int(10) unsigned NOT NULL auto_increment,
  `message_id` int(10) unsigned NOT NULL default '0',
  `sender_id` int(10) unsigned NOT NULL default '0',
  `recipient_id` int(10) unsigned NOT NULL default '0',
  `message_received` char(1) NOT NULL default 'n',
  `message_read` char(1) NOT NULL default 'n',
  `message_time_read` int(10) unsigned NOT NULL default '0',
  `attachment_downloaded` char(1) NOT NULL default 'n',
  `message_folder` int(10) unsigned NOT NULL default '1',
  `message_authcode` varchar(10) NOT NULL default '',
  `message_deleted` char(1) NOT NULL default 'n',
  `message_status` varchar(10) NOT NULL default '',
  PRIMARY KEY  (`copy_id`),
  KEY `message_id` (`message_id`),
  KEY `recipient_id` (`recipient_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_message_data
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_message_data`;

CREATE TABLE `exp_message_data` (
  `message_id` int(10) unsigned NOT NULL auto_increment,
  `sender_id` int(10) unsigned NOT NULL default '0',
  `message_date` int(10) unsigned NOT NULL default '0',
  `message_subject` varchar(255) NOT NULL default '',
  `message_body` text NOT NULL,
  `message_tracking` char(1) NOT NULL default 'y',
  `message_attachments` char(1) NOT NULL default 'n',
  `message_recipients` varchar(200) NOT NULL default '',
  `message_cc` varchar(200) NOT NULL default '',
  `message_hide_cc` char(1) NOT NULL default 'n',
  `message_sent_copy` char(1) NOT NULL default 'n',
  `total_recipients` int(5) unsigned NOT NULL default '0',
  `message_status` varchar(25) NOT NULL default '',
  PRIMARY KEY  (`message_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_message_folders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_message_folders`;

CREATE TABLE `exp_message_folders` (
  `member_id` int(10) unsigned NOT NULL default '0',
  `folder1_name` varchar(50) NOT NULL default 'InBox',
  `folder2_name` varchar(50) NOT NULL default 'Sent',
  `folder3_name` varchar(50) NOT NULL default '',
  `folder4_name` varchar(50) NOT NULL default '',
  `folder5_name` varchar(50) NOT NULL default '',
  `folder6_name` varchar(50) NOT NULL default '',
  `folder7_name` varchar(50) NOT NULL default '',
  `folder8_name` varchar(50) NOT NULL default '',
  `folder9_name` varchar(50) NOT NULL default '',
  `folder10_name` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_message_folders` WRITE;
/*!40000 ALTER TABLE `exp_message_folders` DISABLE KEYS */;

INSERT INTO `exp_message_folders` (`member_id`, `folder1_name`, `folder2_name`, `folder3_name`, `folder4_name`, `folder5_name`, `folder6_name`, `folder7_name`, `folder8_name`, `folder9_name`, `folder10_name`)
VALUES
	(1,'InBox','Sent','','','','','','','',''),
	(2,'InBox','Sent','','','','','','','',''),
	(6,'InBox','Sent','','','','','','','','');

/*!40000 ALTER TABLE `exp_message_folders` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_message_listed
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_message_listed`;

CREATE TABLE `exp_message_listed` (
  `listed_id` int(10) unsigned NOT NULL auto_increment,
  `member_id` int(10) unsigned NOT NULL default '0',
  `listed_member` int(10) unsigned NOT NULL default '0',
  `listed_description` varchar(100) NOT NULL default '',
  `listed_type` varchar(10) NOT NULL default 'blocked',
  PRIMARY KEY  (`listed_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_module_member_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_module_member_groups`;

CREATE TABLE `exp_module_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `module_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY  (`group_id`,`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_module_member_groups` WRITE;
/*!40000 ALTER TABLE `exp_module_member_groups` DISABLE KEYS */;

INSERT INTO `exp_module_member_groups` (`group_id`, `module_id`)
VALUES
	(8,10),
	(9,10),
	(9,12),
	(9,14);

/*!40000 ALTER TABLE `exp_module_member_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_modules
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_modules`;

CREATE TABLE `exp_modules` (
  `module_id` int(4) unsigned NOT NULL auto_increment,
  `module_name` varchar(50) NOT NULL,
  `module_version` varchar(12) NOT NULL,
  `has_cp_backend` char(1) NOT NULL default 'n',
  `has_publish_fields` char(1) NOT NULL default 'n',
  PRIMARY KEY  (`module_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_modules` WRITE;
/*!40000 ALTER TABLE `exp_modules` DISABLE KEYS */;

INSERT INTO `exp_modules` (`module_id`, `module_name`, `module_version`, `has_cp_backend`, `has_publish_fields`)
VALUES
	(2,'Emoticon','2.0','n','n'),
	(3,'Jquery','1.0','n','n'),
	(4,'Rss','2.0','n','n'),
	(5,'Safecracker','2.1','y','n'),
	(6,'Search','2.2','n','n'),
	(7,'Channel','2.0.1','n','n'),
	(8,'Stats','2.0','n','n'),
	(9,'Rte','1.0','y','n'),
	(10,'Seo_lite','1.3.6','y','y'),
	(12,'Backup_proish','1.0.4','y','n'),
	(14,'Redactee','2.2','y','n'),
	(15,'Deeploy_helper','2.0.3','y','n');

/*!40000 ALTER TABLE `exp_modules` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_online_users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_online_users`;

CREATE TABLE `exp_online_users` (
  `online_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `member_id` int(10) NOT NULL default '0',
  `in_forum` char(1) NOT NULL default 'n',
  `name` varchar(50) NOT NULL default '0',
  `ip_address` varchar(45) NOT NULL default '0',
  `date` int(10) unsigned NOT NULL default '0',
  `anon` char(1) NOT NULL,
  PRIMARY KEY  (`online_id`),
  KEY `date` (`date`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_online_users` WRITE;
/*!40000 ALTER TABLE `exp_online_users` DISABLE KEYS */;

INSERT INTO `exp_online_users` (`online_id`, `site_id`, `member_id`, `in_forum`, `name`, `ip_address`, `date`, `anon`)
VALUES
	(37360,1,0,'n','','180.76.6.227',1374500834,''),
	(37359,1,0,'n','','86.106.34.164',1374500490,''),
	(37357,1,0,'n','','72.14.199.56',1374499950,''),
	(37358,1,0,'n','','5.63.145.69',1374500038,''),
	(37363,1,0,'n','','108.254.190.58',1374501273,''),
	(37362,1,0,'n','','66.249.72.36',1374501201,''),
	(37361,1,0,'n','','5.63.145.69',1374500944,'');

/*!40000 ALTER TABLE `exp_online_users` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_password_lockout
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_password_lockout`;

CREATE TABLE `exp_password_lockout` (
  `lockout_id` int(10) unsigned NOT NULL auto_increment,
  `login_date` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL default '0',
  `user_agent` varchar(120) NOT NULL,
  `username` varchar(50) NOT NULL,
  PRIMARY KEY  (`lockout_id`),
  KEY `login_date` (`login_date`),
  KEY `ip_address` (`ip_address`),
  KEY `user_agent` (`user_agent`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_password_lockout` WRITE;
/*!40000 ALTER TABLE `exp_password_lockout` DISABLE KEYS */;

INSERT INTO `exp_password_lockout` (`lockout_id`, `login_date`, `ip_address`, `user_agent`, `username`)
VALUES
	(16,1372427485,'108.254.190.58','Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_4) AppleWebKit/536.30.1 (KHTML, like Gecko) Version/6.0.5 Safari/536.30.1','jondaiello'),
	(15,1371562737,'70.60.44.186','Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20100101 Firefox/21.0','Content.Manager'),
	(14,1371562731,'70.60.44.186','Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20100101 Firefox/21.0','Content.Manager'),
	(13,1371487207,'70.60.44.186','Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20100101 Firefox/21.0','Content.Manager');

/*!40000 ALTER TABLE `exp_password_lockout` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_ping_servers
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_ping_servers`;

CREATE TABLE `exp_ping_servers` (
  `id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `member_id` int(10) NOT NULL default '0',
  `server_name` varchar(32) NOT NULL,
  `server_url` varchar(150) NOT NULL,
  `port` varchar(4) NOT NULL default '80',
  `ping_protocol` varchar(12) NOT NULL default 'xmlrpc',
  `is_default` char(1) NOT NULL default 'y',
  `server_order` int(3) unsigned NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_redactee_toolbar_options
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_redactee_toolbar_options`;

CREATE TABLE `exp_redactee_toolbar_options` (
  `toolbar_id` int(10) unsigned NOT NULL,
  `option` varchar(60) NOT NULL,
  `value` text NOT NULL,
  PRIMARY KEY  (`toolbar_id`,`option`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_redactee_toolbar_options` WRITE;
/*!40000 ALTER TABLE `exp_redactee_toolbar_options` DISABLE KEYS */;

INSERT INTO `exp_redactee_toolbar_options` (`toolbar_id`, `option`, `value`)
VALUES
	(1,'convertDivs','true'),
	(1,'overlay','true'),
	(1,'observeImages','true'),
	(1,'protocol','\"http://\"'),
	(1,'mobile','true'),
	(1,'colors','[\"#ffffff\",\"#000000\",\"#eeece1\",\"#1f497d\",\"#4f81bd\",\"#c0504d\",\"#9bbb59\",\"#8064a2\",\"#4bacc6\",\"#f79646\",\"#ffff00\",\"#f2f2f2\",\"#7f7f7f\",\"#ddd9c3\",\"#c6d9f0\",\"#dbe5f1\",\"#f2dcdb\",\"#ebf1dd\",\"#e5e0ec\",\"#dbeef3\",\"#fdeada\",\"#fff2ca\",\"#d8d8d8\",\"#595959\",\"#c4bd97\",\"#8db3e2\",\"#b8cce4\",\"#e5b9b7\",\"#d7e3bc\",\"#ccc1d9\",\"#b7dde8\",\"#fbd5b5\",\"#ffe694\",\"#bfbfbf\",\"#3f3f3f\",\"#938953\",\"#548dd4\",\"#95b3d7\",\"#d99694\",\"#c3d69b\",\"#b2a2c7\",\"#b7dde8\",\"#fac08f\",\"#f2c314\",\"#a5a5a5\",\"#262626\",\"#494429\",\"#17365d\",\"#366092\",\"#953734\",\"#76923c\",\"#5f497a\",\"#92cddc\",\"#e36c09\",\"#c09100\",\"#7f7f7f\",\"#0c0c0c\",\"#1d1b10\",\"#0f243e\",\"#244061\",\"#632423\",\"#4f6128\",\"#3f3151\",\"#31859b\",\"#974806\",\"#7f6000\"]'),
	(1,'allowedTags','[\"code\",\"span\",\"div\",\"label\",\"a\",\"br\",\"p\",\"b\",\"i\",\"del\",\"strike\",\"img\",\"video\",\"audio\",\"iframe\",\"object\",\"embed\",\"param\",\"blockquote\",\"mark\",\"cite\",\"small\",\"ul\",\"ol\",\"li\",\"hr\",\"dl\",\"dt\",\"dd\",\"sup\",\"sub\",\"big\",\"pre\",\"code\",\"figure\",\"figcaption\",\"strong\",\"em\",\"table\",\"tr\",\"td\",\"th\",\"tbody\",\"thead\",\"tfoot\",\"h1\",\"h2\",\"h3\",\"h4\",\"h5\",\"h6\"]'),
	(1,'convertLinks','true'),
	(1,'lang','\"en\"'),
	(1,'direction','\"ltr\"'),
	(1,'shortcuts','true'),
	(1,'cleanup','true'),
	(1,'formattingTags','[\"p\",\"blockquote\",\"pre\",\"h1\",\"h2\",\"h3\",\"h4\"]'),
	(2,'wym','false'),
	(2,'colors','[\"#000000\",\"\"]'),
	(2,'mobile','true'),
	(2,'protocol','\"http://\"'),
	(2,'observeImages','true'),
	(2,'convertDivs','true'),
	(2,'overlay','true'),
	(2,'convertLinks','true'),
	(2,'direction','\"ltr\"'),
	(2,'lang','\"en\"'),
	(2,'shortcuts','true'),
	(2,'cleanup','true'),
	(2,'buttons','[\"formatting\",\"|\",\"bold\",\"italic\",\"deleted\",\"|\",\"link\",\"|\",\"alignleft\",\"aligncenter\",\"alignright\",\"|\",\"unorderedlist\",\"orderedlist\",\"|\",\"image\",\"video\",\"file\",\"|\",\"html\"]'),
	(2,'source','\"all\"'),
	(2,'formattingTags','[\"p\",\"blockquote\",\"pre\",\"h1\",\"h2\",\"h3\",\"h4\",\"h5\",\"h6\"]'),
	(0,'site_page_entry_status','open'),
	(0,'aws_bucket',''),
	(0,'aws_key','${filename}'),
	(0,'aws_AWSAccessKeyId',''),
	(0,'custom_stylesheet',''),
	(1,'buttons','[\"html\",\"|\",\"formatting\",\"|\",\"bold\",\"italic\",\"deleted\",\"|\",\"unorderedlist\",\"orderedlist\",\"outdent\",\"indent\",\"|\",\"image\",\"video\",\"file\",\"table\",\"link\",\"|\",\"fontcolor\",\"backcolor\",\"|\",\"alignleft\",\"aligncenter\",\"alignright\",\"justify\",\"|\",\"horizontalrule\"]'),
	(1,'source','\"all\"'),
	(0,'aws_Content-Type',''),
	(0,'aws_policy',''),
	(0,'aws_acl','public-read'),
	(0,'aws_signature',''),
	(1,'wym','false'),
	(1,'toolbarMode','\"toolbar\"'),
	(2,'allowedTags','[\"code\",\"span\",\"div\",\"label\",\"a\",\"br\",\"p\",\"b\",\"i\",\"del\",\"strike\",\"img\",\"video\",\"audio\",\"iframe\",\"object\",\"embed\",\"param\",\"blockquote\",\"mark\",\"cite\",\"small\",\"ul\",\"ol\",\"li\",\"hr\",\"dl\",\"dt\",\"dd\",\"sup\",\"sub\",\"big\",\"pre\",\"code\",\"figure\",\"figcaption\",\"strong\",\"em\",\"table\",\"tr\",\"td\",\"th\",\"tbody\",\"thead\",\"tfoot\",\"h1\",\"h2\",\"h3\",\"h4\",\"h5\",\"h6\"]'),
	(2,'toolbarMode','\"toolbar\"'),
	(0,'aws_enable','N');

/*!40000 ALTER TABLE `exp_redactee_toolbar_options` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_redactee_toolbars
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_redactee_toolbars`;

CREATE TABLE `exp_redactee_toolbars` (
  `toolbar_id` int(10) unsigned NOT NULL auto_increment,
  `title` varchar(60) NOT NULL,
  PRIMARY KEY  (`toolbar_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_redactee_toolbars` WRITE;
/*!40000 ALTER TABLE `exp_redactee_toolbars` DISABLE KEYS */;

INSERT INTO `exp_redactee_toolbars` (`toolbar_id`, `title`)
VALUES
	(1,'Advanced Toolbar'),
	(2,'Simple Toolbar');

/*!40000 ALTER TABLE `exp_redactee_toolbars` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_relationships
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_relationships`;

CREATE TABLE `exp_relationships` (
  `rel_id` int(6) unsigned NOT NULL auto_increment,
  `rel_parent_id` int(10) NOT NULL default '0',
  `rel_child_id` int(10) NOT NULL default '0',
  `rel_type` varchar(12) NOT NULL,
  `rel_data` mediumtext NOT NULL,
  `reverse_rel_data` mediumtext NOT NULL,
  PRIMARY KEY  (`rel_id`),
  KEY `rel_parent_id` (`rel_parent_id`),
  KEY `rel_child_id` (`rel_child_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_remember_me
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_remember_me`;

CREATE TABLE `exp_remember_me` (
  `remember_me_id` varchar(40) NOT NULL default '0',
  `member_id` int(10) default '0',
  `ip_address` varchar(45) default '0',
  `user_agent` varchar(120) default '',
  `admin_sess` tinyint(1) default '0',
  `site_id` int(4) default '1',
  `expiration` int(10) default '0',
  `last_refresh` int(10) default '0',
  PRIMARY KEY  (`remember_me_id`),
  KEY `member_id` (`member_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_reset_password
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_reset_password`;

CREATE TABLE `exp_reset_password` (
  `reset_id` int(10) unsigned NOT NULL auto_increment,
  `member_id` int(10) unsigned NOT NULL,
  `resetcode` varchar(12) NOT NULL,
  `date` int(10) NOT NULL,
  PRIMARY KEY  (`reset_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_revision_tracker
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_revision_tracker`;

CREATE TABLE `exp_revision_tracker` (
  `tracker_id` int(10) unsigned NOT NULL auto_increment,
  `item_id` int(10) unsigned NOT NULL,
  `item_table` varchar(20) NOT NULL,
  `item_field` varchar(20) NOT NULL,
  `item_date` int(10) NOT NULL,
  `item_author_id` int(10) unsigned NOT NULL,
  `item_data` mediumtext NOT NULL,
  PRIMARY KEY  (`tracker_id`),
  KEY `item_id` (`item_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_rte_tools
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_rte_tools`;

CREATE TABLE `exp_rte_tools` (
  `tool_id` int(10) unsigned NOT NULL auto_increment,
  `name` varchar(75) default NULL,
  `class` varchar(75) default NULL,
  `enabled` char(1) default 'y',
  PRIMARY KEY  (`tool_id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_rte_tools` WRITE;
/*!40000 ALTER TABLE `exp_rte_tools` DISABLE KEYS */;

INSERT INTO `exp_rte_tools` (`tool_id`, `name`, `class`, `enabled`)
VALUES
	(1,'Blockquote','Blockquote_rte','y'),
	(2,'Bold','Bold_rte','y'),
	(3,'Headings','Headings_rte','y'),
	(4,'Image','Image_rte','y'),
	(5,'Italic','Italic_rte','y'),
	(6,'Link','Link_rte','y'),
	(7,'Ordered List','Ordered_list_rte','y'),
	(8,'Underline','Underline_rte','y'),
	(9,'Unordered List','Unordered_list_rte','y'),
	(10,'View Source','View_source_rte','y');

/*!40000 ALTER TABLE `exp_rte_tools` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_rte_toolsets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_rte_toolsets`;

CREATE TABLE `exp_rte_toolsets` (
  `toolset_id` int(10) unsigned NOT NULL auto_increment,
  `member_id` int(10) default '0',
  `name` varchar(100) default NULL,
  `tools` text,
  `enabled` char(1) default 'y',
  PRIMARY KEY  (`toolset_id`),
  KEY `member_id` (`member_id`),
  KEY `enabled` (`enabled`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_rte_toolsets` WRITE;
/*!40000 ALTER TABLE `exp_rte_toolsets` DISABLE KEYS */;

INSERT INTO `exp_rte_toolsets` (`toolset_id`, `member_id`, `name`, `tools`, `enabled`)
VALUES
	(1,0,'Default','3|2|5|1|9|7|6|4|10','y');

/*!40000 ALTER TABLE `exp_rte_toolsets` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_search
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_search`;

CREATE TABLE `exp_search` (
  `search_id` varchar(32) NOT NULL,
  `site_id` int(4) NOT NULL default '1',
  `search_date` int(10) NOT NULL,
  `keywords` varchar(60) NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `total_results` int(6) NOT NULL,
  `per_page` tinyint(3) unsigned NOT NULL,
  `query` mediumtext,
  `custom_fields` mediumtext,
  `result_page` varchar(70) NOT NULL,
  PRIMARY KEY  (`search_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_search_log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_search_log`;

CREATE TABLE `exp_search_log` (
  `id` int(10) NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `member_id` int(10) unsigned NOT NULL,
  `screen_name` varchar(50) NOT NULL,
  `ip_address` varchar(45) NOT NULL default '0',
  `search_date` int(10) NOT NULL,
  `search_type` varchar(32) NOT NULL,
  `search_terms` varchar(200) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_security_hashes
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_security_hashes`;

CREATE TABLE `exp_security_hashes` (
  `hash_id` int(10) unsigned NOT NULL auto_increment,
  `date` int(10) unsigned NOT NULL,
  `session_id` varchar(40) NOT NULL default '0',
  `hash` varchar(40) NOT NULL,
  PRIMARY KEY  (`hash_id`),
  KEY `hash` (`hash`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_security_hashes` WRITE;
/*!40000 ALTER TABLE `exp_security_hashes` DISABLE KEYS */;

INSERT INTO `exp_security_hashes` (`hash_id`, `date`, `session_id`, `hash`)
VALUES
	(8011,1374499041,'0','3ac169bcf5c0a1e32d2890d01ca84e579bed488e');

/*!40000 ALTER TABLE `exp_security_hashes` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_seolite_config
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_seolite_config`;

CREATE TABLE `exp_seolite_config` (
  `seolite_config_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(10) unsigned default NULL,
  `template` text,
  `default_keywords` varchar(1024) NOT NULL,
  `default_description` varchar(1024) NOT NULL,
  `default_title_postfix` varchar(60) NOT NULL,
  PRIMARY KEY  (`seolite_config_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_seolite_config` WRITE;
/*!40000 ALTER TABLE `exp_seolite_config` DISABLE KEYS */;

INSERT INTO `exp_seolite_config` (`seolite_config_id`, `site_id`, `template`, `default_keywords`, `default_description`, `default_title_postfix`)
VALUES
	(1,1,'<title>{if \"{segment_1}\" != \"\"}{title}{/if}{site_name}</title>\n<meta name=\'keywords\' content=\'{meta_keywords}\' />\n<meta name=\'description\' content=\'{meta_description}\' />\n<link rel=\'canonical\' href=\'{canonical_url}\' />','Clearcreek Chapel, Clearcreek Chapel Springboro, Clearcreek Chapel Springboro Ohio, Clearcreek Chapel Ohio, church springboro ohio','Clearcreek Chapel is a reformed, evangelical church committed to expositional preaching, Biblical counseling, contemporary blended worship and the historical faith, based in Springboro, Ohio.',' |&nbsp;');

/*!40000 ALTER TABLE `exp_seolite_config` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_seolite_content
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_seolite_content`;

CREATE TABLE `exp_seolite_content` (
  `seolite_content_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(10) NOT NULL,
  `entry_id` int(10) NOT NULL,
  `title` varchar(1024) default NULL,
  `keywords` varchar(1024) NOT NULL,
  `description` text,
  PRIMARY KEY  (`seolite_content_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_seolite_content` WRITE;
/*!40000 ALTER TABLE `exp_seolite_content` DISABLE KEYS */;

INSERT INTO `exp_seolite_content` (`seolite_content_id`, `site_id`, `entry_id`, `title`, `keywords`, `description`)
VALUES
	(1,1,2,'','',''),
	(2,1,1,'','',''),
	(3,1,3,'','',''),
	(4,1,4,'','',''),
	(5,1,5,'','',''),
	(6,1,6,'','',''),
	(8,1,8,'Sermons','Clearcreek Chapel Sermons, Clearcreek Chapel Pulpit, Clearcreek Chapel Messages, Reformed Theology, Reformed Evangelical, Clearcreek Chapel Truth','Learn more about Clearcreek Chapel sermons, our philosophy, key series\' and listen to recent sermons.'),
	(9,1,9,'','',''),
	(10,1,10,'','',''),
	(11,1,11,'','',''),
	(12,1,12,'','',''),
	(13,1,13,'','',''),
	(14,1,14,'','',''),
	(15,1,15,'','',''),
	(16,1,16,'','',''),
	(17,1,17,'','',''),
	(18,1,18,'','',''),
	(19,1,19,'','',''),
	(20,1,20,'','',''),
	(21,1,21,'Sermon Repository','Clearcreek Chapel Sermons, Clearcreek Chapel Messages, Clearcreek Chapel Truth, Sermons Springboro Ohio, Reformed Theology Sermons, Reformed Evangelical Sermons','Find and listen to the most recent Clearcreek Chapel sermons.'),
	(22,1,22,'','',''),
	(23,1,23,'Clearcreek Chapel\'s Sermons on Next Sunday','Clearcreek Chapel, Springboro Ohio, Church, Reformed, Evangelical, Baptist, Preaching, Sermons','Titles, texts and speakers on next Sunday at the Chapel.'),
	(24,1,24,'Clearcreek Chapel\'s Last Sunday\'s Sermons','Clearcreek Chapel, Springboro Ohio, Church, Reformed, Evangelical, Baptist, Exposition, Preaching, Sermons','Titles, texts and speaker on last Sunday at the Chapel.'),
	(25,1,25,'News & Events','News about Clearcreek Chapel, Events at Clearcreek Chapel, Clearcreek Chapel News, Clearcreek Chapel Events, ','Read news and details about upcoming events at Clearcreek Chapel.'),
	(26,1,26,'','Clearcreek Chapel, Clearcreek Chapel Springboro, Biblical Counseling, Nouthetic Counseling, NANC, Christian Counseling, Biblical Counseling Center, Biblical Counseling Training',''),
	(27,1,27,'','',''),
	(28,1,28,'','',''),
	(29,1,29,'','',''),
	(30,1,30,'Worship','Clearcreek Chapel Worship, Clearcreek Chapel Music','We strive to exalt the name of the Lord through praise and worship.'),
	(41,1,41,'','',''),
	(32,1,32,'','',''),
	(33,1,33,'','',''),
	(34,1,34,'','',''),
	(35,1,35,'','',''),
	(36,1,36,'','',''),
	(37,1,37,'','',''),
	(38,1,38,'','',''),
	(39,1,39,'','',''),
	(42,1,42,'','',''),
	(43,1,43,'','',''),
	(44,1,44,'','',''),
	(45,1,45,'','',''),
	(46,1,46,'','',''),
	(47,1,47,'','',''),
	(48,1,48,'','',''),
	(49,1,49,'','',''),
	(50,1,50,'','',''),
	(51,1,51,'','',''),
	(52,1,52,'','',''),
	(53,1,53,'','',''),
	(54,1,54,'','',''),
	(55,1,55,'','',''),
	(56,1,56,'','','');

/*!40000 ALTER TABLE `exp_seolite_content` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_sessions
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_sessions`;

CREATE TABLE `exp_sessions` (
  `session_id` varchar(40) NOT NULL default '0',
  `member_id` int(10) NOT NULL default '0',
  `admin_sess` tinyint(1) NOT NULL default '0',
  `ip_address` varchar(45) NOT NULL default '0',
  `user_agent` varchar(120) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`session_id`),
  KEY `member_id` (`member_id`),
  KEY `last_activity_idx` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_sites
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_sites`;

CREATE TABLE `exp_sites` (
  `site_id` int(5) unsigned NOT NULL auto_increment,
  `site_label` varchar(100) NOT NULL default '',
  `site_name` varchar(50) NOT NULL default '',
  `site_description` text,
  `site_system_preferences` mediumtext NOT NULL,
  `site_mailinglist_preferences` text NOT NULL,
  `site_member_preferences` text NOT NULL,
  `site_template_preferences` text NOT NULL,
  `site_channel_preferences` text NOT NULL,
  `site_bootstrap_checksums` text NOT NULL,
  PRIMARY KEY  (`site_id`),
  KEY `site_name` (`site_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_sites` WRITE;
/*!40000 ALTER TABLE `exp_sites` DISABLE KEYS */;

INSERT INTO `exp_sites` (`site_id`, `site_label`, `site_name`, `site_description`, `site_system_preferences`, `site_mailinglist_preferences`, `site_member_preferences`, `site_template_preferences`, `site_channel_preferences`, `site_bootstrap_checksums`)
VALUES
	(1,'Clearcreek Chapel | Springboro, Ohio','default_site','','YTo5Mzp7czoxMDoic2l0ZV9pbmRleCI7czowOiIiO3M6ODoic2l0ZV91cmwiO3M6MzI6Imh0dHA6Ly93d3cuY2xlYXJjcmVla2NoYXBlbC5vcmcvIjtzOjE2OiJ0aGVtZV9mb2xkZXJfdXJsIjtzOjM5OiJodHRwOi8vd3d3LmNsZWFyY3JlZWtjaGFwZWwub3JnL3RoZW1lcy8iO3M6MTU6IndlYm1hc3Rlcl9lbWFpbCI7czozMDoiam9obm55cm9ja2V0Y3JlYXRpdmVAZ21haWwuY29tIjtzOjE0OiJ3ZWJtYXN0ZXJfbmFtZSI7czowOiIiO3M6MjA6ImNoYW5uZWxfbm9tZW5jbGF0dXJlIjtzOjc6ImNoYW5uZWwiO3M6MTA6Im1heF9jYWNoZXMiO3M6MzoiMTUwIjtzOjExOiJjYXB0Y2hhX3VybCI7czo0ODoiaHR0cDovL3d3dy5jbGVhcmNyZWVrY2hhcGVsLm9yZy9pbWFnZXMvY2FwdGNoYXMvIjtzOjEyOiJjYXB0Y2hhX3BhdGgiO3M6Nzk6Ii9kYXRhLzE0LzIvMTM5LzE5LzIxMzk4MzQvdXNlci8yMzQ4NTUyL2h0ZG9jcy9leHByZXNzaW9uZW5naW5lL2ltYWdlcy9jYXB0Y2hhcy8iO3M6MTI6ImNhcHRjaGFfZm9udCI7czoxOiJ5IjtzOjEyOiJjYXB0Y2hhX3JhbmQiO3M6MToieSI7czoyMzoiY2FwdGNoYV9yZXF1aXJlX21lbWJlcnMiO3M6MToibiI7czoxNzoiZW5hYmxlX2RiX2NhY2hpbmciO3M6MToibiI7czoxODoiZW5hYmxlX3NxbF9jYWNoaW5nIjtzOjE6Im4iO3M6MTg6ImZvcmNlX3F1ZXJ5X3N0cmluZyI7czoxOiJuIjtzOjEzOiJzaG93X3Byb2ZpbGVyIjtzOjE6Im4iO3M6MTg6InRlbXBsYXRlX2RlYnVnZ2luZyI7czoxOiJuIjtzOjE1OiJpbmNsdWRlX3NlY29uZHMiO3M6MToibiI7czoxMzoiY29va2llX2RvbWFpbiI7czowOiIiO3M6MTE6ImNvb2tpZV9wYXRoIjtzOjA6IiI7czoxNzoidXNlcl9zZXNzaW9uX3R5cGUiO3M6MToiYyI7czoxODoiYWRtaW5fc2Vzc2lvbl90eXBlIjtzOjI6ImNzIjtzOjIxOiJhbGxvd191c2VybmFtZV9jaGFuZ2UiO3M6MToieSI7czoxODoiYWxsb3dfbXVsdGlfbG9naW5zIjtzOjE6InkiO3M6MTY6InBhc3N3b3JkX2xvY2tvdXQiO3M6MToieSI7czoyNToicGFzc3dvcmRfbG9ja291dF9pbnRlcnZhbCI7czoxOiIxIjtzOjIwOiJyZXF1aXJlX2lwX2Zvcl9sb2dpbiI7czoxOiJ5IjtzOjIyOiJyZXF1aXJlX2lwX2Zvcl9wb3N0aW5nIjtzOjE6InkiO3M6MjQ6InJlcXVpcmVfc2VjdXJlX3Bhc3N3b3JkcyI7czoxOiJuIjtzOjE5OiJhbGxvd19kaWN0aW9uYXJ5X3B3IjtzOjE6InkiO3M6MjM6Im5hbWVfb2ZfZGljdGlvbmFyeV9maWxlIjtzOjA6IiI7czoxNzoieHNzX2NsZWFuX3VwbG9hZHMiO3M6MToieSI7czoxNToicmVkaXJlY3RfbWV0aG9kIjtzOjg6InJlZGlyZWN0IjtzOjk6ImRlZnRfbGFuZyI7czo3OiJlbmdsaXNoIjtzOjg6InhtbF9sYW5nIjtzOjI6ImVuIjtzOjEyOiJzZW5kX2hlYWRlcnMiO3M6MToieSI7czoxMToiZ3ppcF9vdXRwdXQiO3M6MToibiI7czoxMzoibG9nX3JlZmVycmVycyI7czoxOiJuIjtzOjEzOiJtYXhfcmVmZXJyZXJzIjtzOjM6IjUwMCI7czoxMToidGltZV9mb3JtYXQiO3M6MjoidXMiO3M6MTU6InNlcnZlcl90aW1lem9uZSI7czozOiJVVEMiO3M6MTM6InNlcnZlcl9vZmZzZXQiO3M6MDoiIjtzOjE2OiJkYXlsaWdodF9zYXZpbmdzIjtzOjE6Im4iO3M6MjE6ImRlZmF1bHRfc2l0ZV90aW1lem9uZSI7czowOiIiO3M6MTY6ImRlZmF1bHRfc2l0ZV9kc3QiO3M6MDoiIjtzOjE1OiJob25vcl9lbnRyeV9kc3QiO3M6MToieSI7czoxMzoibWFpbF9wcm90b2NvbCI7czo0OiJtYWlsIjtzOjExOiJzbXRwX3NlcnZlciI7czowOiIiO3M6MTM6InNtdHBfdXNlcm5hbWUiO3M6MDoiIjtzOjEzOiJzbXRwX3Bhc3N3b3JkIjtzOjA6IiI7czoxMToiZW1haWxfZGVidWciO3M6MToibiI7czoxMzoiZW1haWxfY2hhcnNldCI7czo1OiJ1dGYtOCI7czoxNToiZW1haWxfYmF0Y2htb2RlIjtzOjE6Im4iO3M6MTY6ImVtYWlsX2JhdGNoX3NpemUiO3M6MDoiIjtzOjExOiJtYWlsX2Zvcm1hdCI7czo1OiJwbGFpbiI7czo5OiJ3b3JkX3dyYXAiO3M6MToieSI7czoyMjoiZW1haWxfY29uc29sZV90aW1lbG9jayI7czoxOiI1IjtzOjIyOiJsb2dfZW1haWxfY29uc29sZV9tc2dzIjtzOjE6InkiO3M6ODoiY3BfdGhlbWUiO3M6NzoiZGVmYXVsdCI7czoyMToiZW1haWxfbW9kdWxlX2NhcHRjaGFzIjtzOjE6Im4iO3M6MTY6ImxvZ19zZWFyY2hfdGVybXMiO3M6MToieSI7czoxMjoic2VjdXJlX2Zvcm1zIjtzOjE6InkiO3M6MTk6ImRlbnlfZHVwbGljYXRlX2RhdGEiO3M6MToieSI7czoyNDoicmVkaXJlY3Rfc3VibWl0dGVkX2xpbmtzIjtzOjE6Im4iO3M6MTY6ImVuYWJsZV9jZW5zb3JpbmciO3M6MToibiI7czoxNDoiY2Vuc29yZWRfd29yZHMiO3M6MDoiIjtzOjE4OiJjZW5zb3JfcmVwbGFjZW1lbnQiO3M6MDoiIjtzOjEwOiJiYW5uZWRfaXBzIjtzOjA6IiI7czoxMzoiYmFubmVkX2VtYWlscyI7czowOiIiO3M6MTY6ImJhbm5lZF91c2VybmFtZXMiO3M6MDoiIjtzOjE5OiJiYW5uZWRfc2NyZWVuX25hbWVzIjtzOjA6IiI7czoxMDoiYmFuX2FjdGlvbiI7czo4OiJyZXN0cmljdCI7czoxMToiYmFuX21lc3NhZ2UiO3M6MzQ6IlRoaXMgc2l0ZSBpcyBjdXJyZW50bHkgdW5hdmFpbGFibGUiO3M6MTU6ImJhbl9kZXN0aW5hdGlvbiI7czoyMToiaHR0cDovL3d3dy55YWhvby5jb20vIjtzOjE2OiJlbmFibGVfZW1vdGljb25zIjtzOjE6InkiO3M6MTI6ImVtb3RpY29uX3VybCI7czo2NjoiaHR0cDovL2NsaWVudHMuam9obm55cm9ja2V0Y3JlYXRpdmUuY29tL2NsZWFyY3JlZWsvaW1hZ2VzL3NtaWxleXMvIjtzOjE5OiJyZWNvdW50X2JhdGNoX3RvdGFsIjtzOjQ6IjEwMDAiO3M6MTc6Im5ld192ZXJzaW9uX2NoZWNrIjtzOjE6InkiO3M6MTc6ImVuYWJsZV90aHJvdHRsaW5nIjtzOjE6Im4iO3M6MTc6ImJhbmlzaF9tYXNrZWRfaXBzIjtzOjE6InkiO3M6MTQ6Im1heF9wYWdlX2xvYWRzIjtzOjI6IjEwIjtzOjEzOiJ0aW1lX2ludGVydmFsIjtzOjE6IjgiO3M6MTI6ImxvY2tvdXRfdGltZSI7czoyOiIzMCI7czoxNToiYmFuaXNobWVudF90eXBlIjtzOjc6Im1lc3NhZ2UiO3M6MTQ6ImJhbmlzaG1lbnRfdXJsIjtzOjA6IiI7czoxODoiYmFuaXNobWVudF9tZXNzYWdlIjtzOjUwOiJZb3UgaGF2ZSBleGNlZWRlZCB0aGUgYWxsb3dlZCBwYWdlIGxvYWQgZnJlcXVlbmN5LiI7czoxNzoiZW5hYmxlX3NlYXJjaF9sb2ciO3M6MToieSI7czoxOToibWF4X2xvZ2dlZF9zZWFyY2hlcyI7czozOiI1MDAiO3M6MTc6InRoZW1lX2ZvbGRlcl9wYXRoIjtzOjcwOiIvZGF0YS8xNC8yLzEzOS8xOS8yMTM5ODM0L3VzZXIvMjM0ODU1Mi9odGRvY3MvZXhwcmVzc2lvbmVuZ2luZS90aGVtZXMvIjtzOjEwOiJpc19zaXRlX29uIjtzOjE6InkiO3M6MTE6InJ0ZV9lbmFibGVkIjtzOjE6InkiO3M6MjI6InJ0ZV9kZWZhdWx0X3Rvb2xzZXRfaWQiO3M6MToiMSI7czo2OiJjcF91cmwiO3M6NDE6Imh0dHA6Ly93d3cuY2xlYXJjcmVla2NoYXBlbC5vcmcvYWRtaW4ucGhwIjt9','YTozOntzOjE5OiJtYWlsaW5nbGlzdF9lbmFibGVkIjtzOjE6InkiO3M6MTg6Im1haWxpbmdsaXN0X25vdGlmeSI7czoxOiJuIjtzOjI1OiJtYWlsaW5nbGlzdF9ub3RpZnlfZW1haWxzIjtzOjA6IiI7fQ==','YTo0NDp7czoxMDoidW5fbWluX2xlbiI7czoxOiI0IjtzOjEwOiJwd19taW5fbGVuIjtzOjE6IjUiO3M6MjU6ImFsbG93X21lbWJlcl9yZWdpc3RyYXRpb24iO3M6MToibiI7czoyNToiYWxsb3dfbWVtYmVyX2xvY2FsaXphdGlvbiI7czoxOiJ5IjtzOjE4OiJyZXFfbWJyX2FjdGl2YXRpb24iO3M6NToiZW1haWwiO3M6MjM6Im5ld19tZW1iZXJfbm90aWZpY2F0aW9uIjtzOjE6Im4iO3M6MjM6Im1icl9ub3RpZmljYXRpb25fZW1haWxzIjtzOjA6IiI7czoyNDoicmVxdWlyZV90ZXJtc19vZl9zZXJ2aWNlIjtzOjE6InkiO3M6MjI6InVzZV9tZW1iZXJzaGlwX2NhcHRjaGEiO3M6MToibiI7czoyMDoiZGVmYXVsdF9tZW1iZXJfZ3JvdXAiO3M6MToiNSI7czoxNToicHJvZmlsZV90cmlnZ2VyIjtzOjY6Im1lbWJlciI7czoxMjoibWVtYmVyX3RoZW1lIjtzOjc6ImRlZmF1bHQiO3M6MTQ6ImVuYWJsZV9hdmF0YXJzIjtzOjE6InkiO3M6MjA6ImFsbG93X2F2YXRhcl91cGxvYWRzIjtzOjE6Im4iO3M6MTA6ImF2YXRhcl91cmwiO3M6NDc6Imh0dHA6Ly93d3cuY2xlYXJjcmVla2NoYXBlbC5vcmcvaW1hZ2VzL2F2YXRhcnMvIjtzOjExOiJhdmF0YXJfcGF0aCI7czo3ODoiL2RhdGEvMTQvMi8xMzkvMTkvMjEzOTgzNC91c2VyLzIzNDg1NTIvaHRkb2NzL2V4cHJlc3Npb25lbmdpbmUvaW1hZ2VzL2F2YXRhcnMvIjtzOjE2OiJhdmF0YXJfbWF4X3dpZHRoIjtzOjM6IjEwMCI7czoxNzoiYXZhdGFyX21heF9oZWlnaHQiO3M6MzoiMTAwIjtzOjEzOiJhdmF0YXJfbWF4X2tiIjtzOjI6IjUwIjtzOjEzOiJlbmFibGVfcGhvdG9zIjtzOjE6Im4iO3M6OToicGhvdG9fdXJsIjtzOjUzOiJodHRwOi8vd3d3LmNsZWFyY3JlZWtjaGFwZWwub3JnL2ltYWdlcy9tZW1iZXJfcGhvdG9zLyI7czoxMDoicGhvdG9fcGF0aCI7czo4NDoiL2RhdGEvMTQvMi8xMzkvMTkvMjEzOTgzNC91c2VyLzIzNDg1NTIvaHRkb2NzL2V4cHJlc3Npb25lbmdpbmUvaW1hZ2VzL21lbWJlcl9waG90b3MvIjtzOjE1OiJwaG90b19tYXhfd2lkdGgiO3M6MzoiMTAwIjtzOjE2OiJwaG90b19tYXhfaGVpZ2h0IjtzOjM6IjEwMCI7czoxMjoicGhvdG9fbWF4X2tiIjtzOjI6IjUwIjtzOjE2OiJhbGxvd19zaWduYXR1cmVzIjtzOjE6InkiO3M6MTM6InNpZ19tYXhsZW5ndGgiO3M6MzoiNTAwIjtzOjIxOiJzaWdfYWxsb3dfaW1nX2hvdGxpbmsiO3M6MToibiI7czoyMDoic2lnX2FsbG93X2ltZ191cGxvYWQiO3M6MToibiI7czoxMToic2lnX2ltZ191cmwiO3M6NjE6Imh0dHA6Ly93d3cuY2xlYXJjcmVla2NoYXBlbC5vcmcvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTI6InNpZ19pbWdfcGF0aCI7czo5MjoiL2RhdGEvMTQvMi8xMzkvMTkvMjEzOTgzNC91c2VyLzIzNDg1NTIvaHRkb2NzL2V4cHJlc3Npb25lbmdpbmUvaW1hZ2VzL3NpZ25hdHVyZV9hdHRhY2htZW50cy8iO3M6MTc6InNpZ19pbWdfbWF4X3dpZHRoIjtzOjM6IjQ4MCI7czoxODoic2lnX2ltZ19tYXhfaGVpZ2h0IjtzOjI6IjgwIjtzOjE0OiJzaWdfaW1nX21heF9rYiI7czoyOiIzMCI7czoxOToicHJ2X21zZ191cGxvYWRfcGF0aCI7czo4NToiL2RhdGEvMTQvMi8xMzkvMTkvMjEzOTgzNC91c2VyLzIzNDg1NTIvaHRkb2NzL2V4cHJlc3Npb25lbmdpbmUvaW1hZ2VzL3BtX2F0dGFjaG1lbnRzLyI7czoyMzoicHJ2X21zZ19tYXhfYXR0YWNobWVudHMiO3M6MToiMyI7czoyMjoicHJ2X21zZ19hdHRhY2hfbWF4c2l6ZSI7czozOiIyNTAiO3M6MjA6InBydl9tc2dfYXR0YWNoX3RvdGFsIjtzOjM6IjEwMCI7czoxOToicHJ2X21zZ19odG1sX2Zvcm1hdCI7czo0OiJzYWZlIjtzOjE4OiJwcnZfbXNnX2F1dG9fbGlua3MiO3M6MToieSI7czoxNzoicHJ2X21zZ19tYXhfY2hhcnMiO3M6NDoiNjAwMCI7czoxOToibWVtYmVybGlzdF9vcmRlcl9ieSI7czoxMToidG90YWxfcG9zdHMiO3M6MjE6Im1lbWJlcmxpc3Rfc29ydF9vcmRlciI7czo0OiJkZXNjIjtzOjIwOiJtZW1iZXJsaXN0X3Jvd19saW1pdCI7czoyOiIyMCI7fQ==','YTo2OntzOjExOiJzdHJpY3RfdXJscyI7czoxOiJuIjtzOjg6InNpdGVfNDA0IjtzOjA6IiI7czoxOToic2F2ZV90bXBsX3JldmlzaW9ucyI7czoxOiJuIjtzOjE4OiJtYXhfdG1wbF9yZXZpc2lvbnMiO3M6MToiNSI7czoxNToic2F2ZV90bXBsX2ZpbGVzIjtzOjE6Im4iO3M6MTg6InRtcGxfZmlsZV9iYXNlcGF0aCI7czo5MDoiL2RhdGEvMTQvMi8xMzkvMTkvMjEzOTgzNC91c2VyLzIzNDg1NTIvaHRkb2NzL2V4cHJlc3Npb25lbmdpbmUvZXhwcmVzc2lvbmVuZ2luZS90ZW1wbGF0ZXMvIjt9','YTo5OntzOjIxOiJpbWFnZV9yZXNpemVfcHJvdG9jb2wiO3M6MzoiZ2QyIjtzOjE4OiJpbWFnZV9saWJyYXJ5X3BhdGgiO3M6MDoiIjtzOjE2OiJ0aHVtYm5haWxfcHJlZml4IjtzOjU6InRodW1iIjtzOjE0OiJ3b3JkX3NlcGFyYXRvciI7czo0OiJkYXNoIjtzOjE3OiJ1c2VfY2F0ZWdvcnlfbmFtZSI7czoxOiJuIjtzOjIyOiJyZXNlcnZlZF9jYXRlZ29yeV93b3JkIjtzOjg6ImNhdGVnb3J5IjtzOjIzOiJhdXRvX2NvbnZlcnRfaGlnaF9hc2NpaSI7czoxOiJuIjtzOjIyOiJuZXdfcG9zdHNfY2xlYXJfY2FjaGVzIjtzOjE6InkiO3M6MjM6ImF1dG9fYXNzaWduX2NhdF9wYXJlbnRzIjtzOjE6InkiO30=','YTozOntzOjc6ImVtYWlsZWQiO2E6MDp7fXM6NzI6Ii9kYXRhLzE0LzIvMTM5LzE5LzIxMzk4MzQvdXNlci8yMzQ4NTUyL2h0ZG9jcy9leHByZXNzaW9uZW5naW5lL2luZGV4LnBocCI7czozMjoiNDYzMDk3ODYyZWU3M2VhMTI4N2ZjZGNlODJkM2ZjODQiO3M6MTg6Ii92YXIvd3d3L2luZGV4LnBocCI7czozMjoiNDYzMDk3ODYyZWU3M2VhMTI4N2ZjZGNlODJkM2ZjODQiO30=');

/*!40000 ALTER TABLE `exp_sites` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_snippets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_snippets`;

CREATE TABLE `exp_snippets` (
  `snippet_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) NOT NULL,
  `snippet_name` varchar(75) NOT NULL,
  `snippet_contents` text,
  PRIMARY KEY  (`snippet_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_specialty_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_specialty_templates`;

CREATE TABLE `exp_specialty_templates` (
  `template_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `enable_template` char(1) NOT NULL default 'y',
  `template_name` varchar(50) NOT NULL,
  `data_title` varchar(80) NOT NULL,
  `template_data` text NOT NULL,
  PRIMARY KEY  (`template_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_specialty_templates` WRITE;
/*!40000 ALTER TABLE `exp_specialty_templates` DISABLE KEYS */;

INSERT INTO `exp_specialty_templates` (`template_id`, `site_id`, `enable_template`, `template_name`, `data_title`, `template_data`)
VALUES
	(1,1,'y','offline_template','','<html>\n<head>\n\n<title>System Offline</title>\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#999999 1px solid;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>System Offline</h1>\n\n<p>This site is currently offline</p>\n\n</div>\n\n</body>\n\n</html>'),
	(2,1,'y','message_template','','<html>\n<head>\n\n<title>{title}</title>\n\n<meta http-equiv=\'content-type\' content=\'text/html; charset={charset}\' />\n\n{meta_refresh}\n\n<style type=\"text/css\">\n\nbody { \nbackground-color:	#ffffff; \nmargin:				50px; \nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size:			11px;\ncolor:				#000;\nbackground-color:	#fff;\n}\n\na {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nletter-spacing:		.09em;\ntext-decoration:	none;\ncolor:			  #330099;\nbackground-color:	transparent;\n}\n  \na:visited {\ncolor:				#330099;\nbackground-color:	transparent;\n}\n\na:active {\ncolor:				#ccc;\nbackground-color:	transparent;\n}\n\na:hover {\ncolor:				#000;\ntext-decoration:	underline;\nbackground-color:	transparent;\n}\n\n#content  {\nborder:				#000 1px solid;\nbackground-color: 	#DEDFE3;\npadding:			22px 25px 14px 25px;\n}\n\nh1 {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-weight:		bold;\nfont-size:			14px;\ncolor:				#000;\nmargin-top: 		0;\nmargin-bottom:		14px;\n}\n\np {\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		12px;\nmargin-bottom: 		14px;\ncolor: 				#000;\n}\n\nul {\nmargin-bottom: 		16px;\n}\n\nli {\nlist-style:			square;\nfont-family:		Verdana, Arial, Tahoma, Trebuchet MS, Sans-serif;\nfont-size: 			12px;\nfont-weight: 		normal;\nmargin-top: 		8px;\nmargin-bottom: 		8px;\ncolor: 				#000;\n}\n\n</style>\n\n</head>\n\n<body>\n\n<div id=\"content\">\n\n<h1>{heading}</h1>\n\n{content}\n\n<p>{link}</p>\n\n</div>\n\n</body>\n\n</html>'),
	(3,1,'y','admin_notify_reg','Notification of new member registration','New member registration site: {site_name}\n\nScreen name: {name}\nUser name: {username}\nEmail: {email}\n\nYour control panel URL: {control_panel_url}'),
	(4,1,'y','admin_notify_entry','A new channel entry has been posted','A new entry has been posted in the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nPosted by: {name}\nEmail: {email}\n\nTo read the entry please visit: \n{entry_url}\n'),
	(5,1,'y','admin_notify_mailinglist','Someone has subscribed to your mailing list','A new mailing list subscription has been accepted.\n\nEmail Address: {email}\nMailing List: {mailing_list}'),
	(6,1,'y','admin_notify_comment','You have just received a comment','You have just received a comment for the following channel:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nLocated at: \n{comment_url}\n\nPosted by: {name}\nEmail: {email}\nURL: {url}\nLocation: {location}\n\n{comment}'),
	(7,1,'y','mbr_activation_instructions','Enclosed is your activation code','Thank you for your new member registration.\n\nTo activate your new account, please visit the following URL:\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}\n\n{site_url}'),
	(8,1,'y','forgot_password_instructions','Login information','{name},\n\nTo reset your password, please go to the following page:\n\n{reset_url}\n\nYour password will be automatically reset, and a new password will be emailed to you.\n\nIf you do not wish to reset your password, ignore this message. It will expire in 24 hours.\n\n{site_name}\n{site_url}'),
	(9,1,'y','reset_password_notification','New Login Information','{name},\n\nHere is your new login information:\n\nUsername: {username}\nPassword: {password}\n\n{site_name}\n{site_url}'),
	(10,1,'y','validated_member_notify','Your membership account has been activated','{name},\n\nYour membership account has been activated and is ready for use.\n\nThank You!\n\n{site_name}\n{site_url}'),
	(11,1,'y','decline_member_validation','Your membership account has been declined','{name},\n\nWe\'re sorry but our staff has decided not to validate your membership.\n\n{site_name}\n{site_url}'),
	(12,1,'y','mailinglist_activation_instructions','Email Confirmation','Thank you for joining the \"{mailing_list}\" mailing list!\n\nPlease click the link below to confirm your email.\n\nIf you do not want to be added to our list, ignore this email.\n\n{unwrap}{activation_url}{/unwrap}\n\nThank You!\n\n{site_name}'),
	(13,1,'y','comment_notification','Someone just responded to your comment','{name_of_commenter} just responded to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comment at the following URL:\n{comment_url}\n\n{comment}\n\nTo stop receiving notifications for this comment, click here:\n{notification_removal_url}'),
	(14,1,'y','comments_opened_notification','New comments have been added','Responses have been added to the entry you subscribed to at:\n{channel_name}\n\nThe title of the entry is:\n{entry_title}\n\nYou can see the comments at the following URL:\n{comment_url}\n\n{comments}\n{comment} \n{/comments}\n\nTo stop receiving notifications for this entry, click here:\n{notification_removal_url}'),
	(15,1,'y','private_message_notification','Someone has sent you a Private Message','\n{recipient_name},\n\n{sender_name} has just sent you a Private Message titled {message_subject}.\n\nYou can see the Private Message by logging in and viewing your inbox at:\n{site_url}\n\nContent:\n\n{message_content}\n\nTo stop receiving notifications of Private Messages, turn the option off in your Email Settings.\n\n{site_name}\n{site_url}'),
	(16,1,'y','pm_inbox_full','Your private message mailbox is full','{recipient_name},\n\n{sender_name} has just attempted to send you a Private Message,\nbut your inbox is full, exceeding the maximum of {pm_storage_limit}.\n\nPlease log in and remove unwanted messages from your inbox at:\n{site_url}');

/*!40000 ALTER TABLE `exp_specialty_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_stats
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_stats`;

CREATE TABLE `exp_stats` (
  `stat_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `total_members` mediumint(7) NOT NULL default '0',
  `recent_member_id` int(10) NOT NULL default '0',
  `recent_member` varchar(50) NOT NULL,
  `total_entries` mediumint(8) NOT NULL default '0',
  `total_forum_topics` mediumint(8) NOT NULL default '0',
  `total_forum_posts` mediumint(8) NOT NULL default '0',
  `total_comments` mediumint(8) NOT NULL default '0',
  `last_entry_date` int(10) unsigned NOT NULL default '0',
  `last_forum_post_date` int(10) unsigned NOT NULL default '0',
  `last_comment_date` int(10) unsigned NOT NULL default '0',
  `last_visitor_date` int(10) unsigned NOT NULL default '0',
  `most_visitors` mediumint(7) NOT NULL default '0',
  `most_visitor_date` int(10) unsigned NOT NULL default '0',
  `last_cache_clear` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`stat_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_stats` WRITE;
/*!40000 ALTER TABLE `exp_stats` DISABLE KEYS */;

INSERT INTO `exp_stats` (`stat_id`, `site_id`, `total_members`, `recent_member_id`, `recent_member`, `total_entries`, `total_forum_topics`, `total_forum_posts`, `total_comments`, `last_entry_date`, `last_forum_post_date`, `last_comment_date`, `last_visitor_date`, `most_visitors`, `most_visitor_date`, `last_cache_clear`)
VALUES
	(1,1,7,7,'Mark.Schindler',51,0,0,0,1369754847,0,0,1374501273,39,1372772109,1374854270);

/*!40000 ALTER TABLE `exp_stats` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_status_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_status_groups`;

CREATE TABLE `exp_status_groups` (
  `group_id` int(4) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_name` varchar(50) NOT NULL,
  PRIMARY KEY  (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_status_groups` WRITE;
/*!40000 ALTER TABLE `exp_status_groups` DISABLE KEYS */;

INSERT INTO `exp_status_groups` (`group_id`, `site_id`, `group_name`)
VALUES
	(1,1,'Statuses');

/*!40000 ALTER TABLE `exp_status_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_status_no_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_status_no_access`;

CREATE TABLE `exp_status_no_access` (
  `status_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY  (`status_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_statuses
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_statuses`;

CREATE TABLE `exp_statuses` (
  `status_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_id` int(4) unsigned NOT NULL,
  `status` varchar(50) NOT NULL,
  `status_order` int(3) unsigned NOT NULL,
  `highlight` varchar(30) NOT NULL,
  PRIMARY KEY  (`status_id`),
  KEY `group_id` (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_statuses` WRITE;
/*!40000 ALTER TABLE `exp_statuses` DISABLE KEYS */;

INSERT INTO `exp_statuses` (`status_id`, `site_id`, `group_id`, `status`, `status_order`, `highlight`)
VALUES
	(1,1,1,'open',1,'009933'),
	(2,1,1,'closed',2,'990000');

/*!40000 ALTER TABLE `exp_statuses` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_template_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_template_groups`;

CREATE TABLE `exp_template_groups` (
  `group_id` int(6) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_name` varchar(50) NOT NULL,
  `group_order` int(3) unsigned NOT NULL,
  `is_site_default` char(1) NOT NULL default 'n',
  PRIMARY KEY  (`group_id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_template_groups` WRITE;
/*!40000 ALTER TABLE `exp_template_groups` DISABLE KEYS */;

INSERT INTO `exp_template_groups` (`group_id`, `site_id`, `group_name`, `group_order`, `is_site_default`)
VALUES
	(1,1,'_includes',1,'n'),
	(2,1,'home',2,'y'),
	(3,1,'blog',3,'n'),
	(4,1,'about',4,'n'),
	(5,1,'sermons',5,'n'),
	(6,1,'flocks',6,'n'),
	(7,1,'ministries',7,'n'),
	(8,1,'news-and-events',8,'n'),
	(9,1,'styleguide',9,'n');

/*!40000 ALTER TABLE `exp_template_groups` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_template_member_groups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_template_member_groups`;

CREATE TABLE `exp_template_member_groups` (
  `group_id` smallint(4) unsigned NOT NULL,
  `template_group_id` mediumint(5) unsigned NOT NULL,
  PRIMARY KEY  (`group_id`,`template_group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_template_no_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_template_no_access`;

CREATE TABLE `exp_template_no_access` (
  `template_id` int(6) unsigned NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY  (`template_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_templates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_templates`;

CREATE TABLE `exp_templates` (
  `template_id` int(10) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `group_id` int(6) unsigned NOT NULL,
  `template_name` varchar(50) NOT NULL,
  `save_template_file` char(1) NOT NULL default 'n',
  `template_type` varchar(16) NOT NULL default 'webpage',
  `template_data` mediumtext,
  `template_notes` text,
  `edit_date` int(10) NOT NULL default '0',
  `last_author_id` int(10) unsigned NOT NULL default '0',
  `cache` char(1) NOT NULL default 'n',
  `refresh` int(6) unsigned NOT NULL default '0',
  `no_auth_bounce` varchar(50) NOT NULL default '',
  `enable_http_auth` char(1) NOT NULL default 'n',
  `allow_php` char(1) NOT NULL default 'n',
  `php_parse_location` char(1) NOT NULL default 'o',
  `hits` int(10) unsigned NOT NULL default '0',
  PRIMARY KEY  (`template_id`),
  KEY `group_id` (`group_id`),
  KEY `template_name` (`template_name`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_templates` WRITE;
/*!40000 ALTER TABLE `exp_templates` DISABLE KEYS */;

INSERT INTO `exp_templates` (`template_id`, `site_id`, `group_id`, `template_name`, `save_template_file`, `template_type`, `template_data`, `template_notes`, `edit_date`, `last_author_id`, `cache`, `refresh`, `no_auth_bounce`, `enable_http_auth`, `allow_php`, `php_parse_location`, `hits`)
VALUES
	(1,1,1,'index','n','webpage','','',1361929808,0,'y',0,'','n','n','o',0),
	(2,1,1,'.doctype','n','webpage','<!doctype html>\n\n<!-- \nClearcreek Chapel\nURL: http://www.clearcreekchapel.org\n-->\n\n<!-- HTML5 Boilerplate -->\n<!--[if lt IE 7]><html class=\"no-js lt-ie9 lt-ie8 lt-ie7\" lang=\"en\"> <![endif]-->\n<!--[if (IE 7)&!(IEMobile)]><html class=\"no-js lt-ie9 lt-ie8\" lang=\"en\"><![endif]-->\n<!--[if (IE 8)&!(IEMobile)]><html class=\"no-js lt-ie9\" lang=\"en\"><![endif]-->\n<!--[if gt IE 8]><!--> <html class=\"no-js\" lang=\"en\"><!--<![endif]-->\n\n<head>\n  <meta charset=\"utf-8\">\n  \n  {exp:seo_lite entry_id=\"{embed:entry_id}\"}\n  \n  <meta name=\"HandheldFriendly\" content=\"True\">\n  <meta name=\"MobileOptimized\" content=\"320\">\n  <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n  \n  <link rel=\"stylesheet\" href=\"/assets/css/base.css\">\n  <!--[if IE]>\n    <link rel=\"stylesheet\" href=\"/assets/css/ie.css\">\n  <![endif]-->\n  \n  <!-- Helvetica Neue -->\n  <link type=\"text/css\" rel=\"stylesheet\" href=\"http://fast.fonts.com/cssapi/aa95e60d-b963-4631-bb1f-a8c100285dd5.css\"/>\n  \n  <!--[if (lt IE 9) & (!IEMobile)]>\n  <script src=\"/assets/js/selectivizr-min.js\"></script>\n  <![endif]-->\n  \n  <!-- JavaScript -->\n  <script src=\"/assets/js/modernizr-2.5.3-min.js\"></script>\n  \n  <link rel=\"shortcut icon\" href=\"/favicon.ico\">\n  <meta http-equiv=\"cleartype\" content=\"on\">\n</head>\n\n<body class=\"clearfix\">\n<div class=\"wrapper\">\n','',1365270479,1,'y',0,'','n','n','o',0),
	(3,1,1,'.header','n','webpage','  <header class=\"main\" role=\"main\">\n    <div class=\"content-width\">\n      <h1><a href=\"{path=\'\'}\"><img src=\"/assets/img/logo.png\" alt=\"Clearcreek Chapel Logo\">Clearcreek Chapel</a></h1>\n      \n      <nav>\n        <a href=\"#\" class=\"menu-link\">Menu</a>\n        \n        <ul class=\"main-nav\">\n          <li class=\"home\"><a href=\"{path=\'\'}\">Home</a></li>\n          <li><a href=\"{path=\'about\'}\">About Us</a></li>\n          <li><a href=\"{path=\'sermons\'}\">Sermons</a></li>\n          <li><a href=\"{path=\'flocks\'}\">Flocks</a></li>\n          <li><a href=\"{path=\'ministries\'}\">Ministries</a></li>\n          <li><a href=\"{path=\'news-and-events\'}\">News & Events</a></li>\n        </ul>\n      </nav>\n    </div>\n  </header>\n','',1365270468,1,'y',0,'','n','n','o',0),
	(4,1,1,'.footer','n','webpage','</div>\n\n  <footer class=\"main\" role=\"main\">\n    <div class=\"content-width\">\n      <nav>\n        <ul>\n          <li class=\"top-level\"><h2><a href=\"{path=\'about\'}\">About Us</a></h2></li>\n          {exp:channel:entries channel=\"about\" disable=\"categories|category_fields|member_data|pagination\" dynamic=\"off\"}\n          <li><a href=\"{title_permalink=\'about/\'}\">{title}</a></li>\n          {/exp:channel:entries}\n        </ul>\n        <ul>\n\n          <li class=\"top-level\"><h2><a href=\"{path=\'sermons\'}\">Sermons</a></h2></li>\n          {exp:channel:entries channel=\"sermons\" disable=\"categories|category_fields|member_data|pagination\" dynamic=\"off\"}\n          <li><a href=\"{title_permalink=\'sermons/\'}\">{title}</a></li>\n          {/exp:channel:entries}\n        </ul>\n        <ul>\n\n          <li class=\"top-level\"><h2><a href=\"{path=\'flocks\'}\">Flocks</a></li></a></h2></li>\n          {exp:channel:entries channel=\"flocks\" disable=\"categories|category_fields|member_data|pagination\" dynamic=\"off\"}\n          <li><a href=\"{title_permalink=\'flocks/\'}\">{title}</a></li>\n          {/exp:channel:entries}\n        </ul>\n        <ul>\n          <li class=\"top-level\"><h2><a href=\"{path=\'ministries\'}\">Ministries</a></h2></li>\n          {exp:channel:entries channel=\"ministries\" disable=\"categories|category_fields|member_data|pagination\" dynamic=\"off\"}\n          <li><a href=\"{title_permalink=\'ministries/\'}\">{title}</a></li>\n          {/exp:channel:entries}\n        </ul>\n        <ul>\n          <li class=\"top-level\"><h2><a href=\"{path=\'news-and-events\'}\">News and Events</a></h2></li>\n          {exp:channel:entries channel=\"news\" disable=\"categories|category_fields|member_data|pagination\" dynamic=\"off\"}\n          <li><a href=\"{path=\'news-and-events\'}\">{title}</a></li>\n          {/exp:channel:entries}\n        </ul>\n      </nav>\n      \n      <address>\n        <a href=\"http://maps.google.com/maps?q=2738+Pennyroyal+Road,+Miamisburg,+OH&oe=utf-8&client=firefox-beta&channel=fflb&hnear=2738+Pennyroyal+Rd,+Miamisburg,+Warren,+Ohio+45342&gl=us&t=m&z=16\" class=\"url\" target=\"_blank\">2738 Pennyroyal Road, Miamisburg, OH</a><br>\n        P.O. Box 327<br>\n        Springboro, Ohio 45066\n        <span class=\"phone\">(937) 885-2143</span><br>\n        <a href=\"mailto:contact@clearcreekchapel.org\">contact@clearcreekchapel.org</a>\n      </address>\n\n      \n      <small>© Copyright 2013 <a href=\"{path=\'\'}\" class=\"url\">Clearcreekchapel.org</a></small> \n      \n    </div>\n  </footer>\n  \n\n  <script type=\"text/javascript\" src=\"https://maps.googleapis.com/maps/api/js?key=AIzaSyB0NH4LJUHxjG-qVZZPI0nrsYzHQiNgYio&sensor=false\"></script>\n  <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js\"></script>\n  <script>window.jQuery || document.write(\'<script src=\"/assets/js/jquery-1.7.2.min.js\"><\\/script>\')</script>\n  \n  <script src=\"/assets/js/plugins.js\"></script>\n  <script src=\"/assets/js/script.js\"></script>\n  <script src=\"/assets/js/helper.js\"></script>\n  <script>\n    (function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\n    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n    })(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\n  \n    ga(\'create\', \'UA-39869646-1\', \'clearcreekchapel.org\');\n    ga(\'send\', \'pageview\');\n  \n  </script>\n</body>\n</html>','',1365283136,1,'y',0,'','n','n','o',0),
	(5,1,2,'index','n','webpage','{exp:channel:entries channel=\"home\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\"}\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n    {/exp:channel:entries}\n\n{embed=\"_includes/.header\"}\n\n  <section class=\"hero content-width\">\n    {exp:channel:entries channel=\"homepage_hero\" limit=\"1\" orderby=\"random\" disable=\"categories|category_fields|member_data|pagination\"}\n    <img src=\"{home_banner_image}\" alt=\"{home_banner_text}\">\n    {/exp:channel:entries}\n\n{exp:channel:entries channel=\"home\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\"}\n    <section class=\"overview\">\n      <div class=\"inner\">\n        <h1>{home_overview_headline}</h1>\n        {home_overview_paragraph}\n      </div>\n    </section>\n  </section>\n  \n  <section class=\"featured-blocks content-width clearfix\">\n    <a class=\"block first\" href=\"{path=\'{block_one_link}\'}\">\n      <h1>{block_one_headline}</h1>\n      <p>{block_one_paragraph}</p>\n      <p class=\"more\">{block_one_link_text}</p>\n    </a>\n    \n    <a class=\"block second\" href=\"{path=\'{block_two_link}\'}\">\n      <h1>{block_two_headline}</h1>\n      <p>{block_two_paragraph}</p>\n      <p class=\"more\">{block_two_link_text}</p>\n    </a>\n    \n    <a class=\"block third\" href=\"{path=\'{block_three_link}\'}\">\n      <h1>{block_three_headline}</h1>\n      <p>{block_three_paragraph}</p>\n      <p class=\"more\">{block_three_link_text}</p>\n    </a>\n  </section>\n\n{/exp:channel:entries}\n{embed=\"_includes/.footer\"}','',1364757018,1,'y',0,'','n','n','o',51704),
	(6,1,3,'index','n','webpage','{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n{embed=\"_includes/.header\"}\n\n<section class=\"bread\">\n    <div class=\"content-width\">\n      <a href=\"index.html\" class=\"home crumb\">Home</a> \\ <a href=\"about.html\" class=\"crumb\">Sermons</a> \\ <span class=\"crumb\">Sermon Library</span>\n    </div>\n  </section>\n  \n  <section class=\"content content-width clearfix\">\n    <h1 class=\"page-title\">Latest Sermons</h1>\n    \n    <section class=\"page-content\">\n      {exp:ajw_feedparser \n        url=\"http://www.sermonaudio.com/rss_source.rss?sourceid=thechapel&filter=mp3\"\n        cache_refresh=\"30\"\n        limit=\"10\"\n      }\n      <summary>\n        <time>{pubDate}</time>\n        <h1>{title}</h1>\n        <cite>{itunes:author}</cite>\n        {if \"{itunes:subtitle}\" != \"\"}<p><em>Passage: {itunes:subtitle}</em></p>{/if}\n        <p class=\"more\"><a href=\"{enclosure@url}\" class=\"listen-now\">Listen Now</a> or <a href=\"{link}\" target=\"_blank\">View More Details</a></p>\n      </summary>\n      {/exp:ajw_feedparser}\n    </section>\n  </section>\n\n\n{embed=\"_includes/.footer\"}','',1362591041,1,'y',0,'','n','n','o',34),
	(7,1,4,'index','n','webpage','{preload_replace:current_channel=\"about\"}\n\n{if \"{segment_2}\" == \"\"}\n\n{exp:channel:entries channel=\"landing_pages\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\" entry_id=\"2\"}\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n\n{embed=\"_includes/.header\"}\n\n  <section class=\"hero content-width\">\n    <img src=\"{section_banner_image}\" alt=\"{section_banner_text}\">\n    \n    <section class=\"overview\">\n      <div class=\"inner\">\n        <h1>{section_overview_title}</h1>\n        {section_overview_paragraph}\n      </div>\n    </section>    \n  </section>\n{/exp:channel:entries}\n\n  \n  <section class=\"subnav\">\n    <nav class=\"content-width\">\n      <ul>\n        {exp:channel:entries channel=\"{current_channel}\" limit=\"6\" orderby=\"date\" sort=\"desc\" disable=\"categories|category_fields|member_data|pagination\"}\n        <li>\n          <a href=\"{title_permalink=\'{current_channel}\'}\" class=\"block\">\n            <img src=\"{preview_image}\" alt=\"{title}\">\n            <h1>{title}</h1>\n          </a>\n        </li>\n        {/exp:channel:entries}\n      </ul>\n    </nav>\n  </section>\n\n{if:else}\n\n{exp:channel:entries channel=\"{current_channel}\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\"}\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n\n{embed=\"_includes/.header\"}\n\n  <section class=\"bread\">\n    <div class=\"content-width\">\n      <a href=\"{path=\'\'}\" class=\"home crumb\">Home</a> \\ <a href=\"{path=\'{current_channel}\'}\" class=\"crumb\">About</a> \\ <span class=\"crumb\">{title}</span>\n    </div>\n  </section>\n  \n  <section class=\"content content-width clearfix\">\n    <h1 class=\"page-title\">{title}</h1>\n    \n    <section class=\"page-content\">\n      {if \"{segment_2}\" == \"location\"}<div id=\"cc_map\"></div>{/if}\n      \n      {main_body_area}\n    </section>\n    \n    <aside>\n      {sidebar_area}\n    </aside>\n  </section>\n{/exp:channel:entries}\n{/if}\n\n{embed=\"_includes/.footer\"}','',1365516363,1,'y',0,'','n','n','o',5085),
	(8,1,5,'index','n','webpage','{preload_replace:current_channel=\"sermons\"}\n\n{if \"{segment_2}\" == \"\"}\n\n{exp:channel:entries channel=\"landing_pages\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\" entry_id=\"8\"}\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n\n{embed=\"_includes/.header\"}\n\n  <section class=\"hero content-width\">\n    <img src=\"{section_banner_image}\" alt=\"{section_banner_text}\">\n    \n    <section class=\"overview\">\n      <div class=\"inner\">\n        <h1>{section_overview_title}</h1>\n        {section_overview_paragraph}\n      </div>\n    </section>\n  </section>\n{/exp:channel:entries}\n\n  \n  <section class=\"subnav\">\n    <nav class=\"content-width\">\n      <ul>\n        {exp:channel:entries channel=\"{current_channel}\" limit=\"6\" orderby=\"date\" sort=\"desc\" disable=\"categories|category_fields|member_data|pagination\"}\n        <li>\n          <a href=\"{title_permalink=\'{current_channel}\'}\" class=\"block\">\n            <img src=\"{preview_image}\" alt=\"{title}\">\n            <h1>{title}</h1>\n          </a>\n        </li>\n        {/exp:channel:entries}\n        <li><a href=\"http://www.sermonaudio.com/source_detail.asp?sourceid=thechapel\" target=\"_blank\" class=\"block\">\n          <img src=\"./assets/img/subnav-sermons-sermon-audio-2x.png\" alt=\"Download sermons from Clearcreek Chapel in Springboro Ohio on SermonAudio.com\">\n          <h1>SermonAudio.com</h1>\n        </a></li>\n      </ul>\n    </nav>\n  </section>\n\n{if:else}\n\n{exp:channel:entries channel=\"{current_channel}\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\"}\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n\n{embed=\"_includes/.header\"}\n\n  <section class=\"bread\">\n    <div class=\"content-width\">\n      <a href=\"{path=\'\'}\" class=\"home crumb\">Home</a> \\ <a href=\"{path=\'{current_channel}\'}\" class=\"crumb\">Sermons</a> \\ <span class=\"crumb\">{title}</span>\n    </div>\n  </section>\n  \n  <section class=\"content content-width clearfix\">\n    <h1 class=\"page-title\">{title}</h1>\n    \n    <section class=\"page-content\">\n      {main_body_area}\n    </section>\n    \n    <aside>\n      {sidebar_area}\n    </aside>\n  </section>\n{/exp:channel:entries}\n{/if}\n\n{embed=\"_includes/.footer\"}','',1365516289,1,'y',0,'','n','n','o',4749),
	(9,1,6,'index','n','webpage','{preload_replace:current_channel=\"flocks\"}\n\n{if \"{segment_2}\" == \"\"}\n\n{exp:channel:entries channel=\"landing_pages\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\" entry_id=\"9\"}\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n\n{embed=\"_includes/.header\"}\n\n  <section class=\"hero content-width\">\n    <img src=\"{section_banner_image}\" alt=\"{section_banner_text}\">\n    \n    <section class=\"overview\">\n      <div class=\"inner\">\n        <h1>{section_overview_title}</h1>\n        {section_overview_paragraph}\n      </div>\n    </section>\n  </section>\n{/exp:channel:entries}\n\n  \n  <section class=\"subnav\">\n    <nav class=\"content-width\">\n      <ul>\n        {exp:channel:entries channel=\"{current_channel}\" limit=\"6\" orderby=\"date\" sort=\"desc\" disable=\"categories|category_fields|member_data|pagination\"}\n        <li>\n          <a href=\"{title_permalink=\'{current_channel}\'}\" class=\"block\">\n            <img src=\"{preview_image}\" alt=\"{title}\">\n            <h1>{title}</h1>\n          </a>\n        </li>\n        {/exp:channel:entries}\n        <li class=\"two-cols\"><img src=\"./assets/img/flocks-filler-fall-festival-2x.png\" alt=\"Clearcreek Chapel in Springboro, Ohio fall festival\"></li>\n      </ul>\n    </nav>\n  </section>\n\n{if:else}\n\n{exp:channel:entries channel=\"{current_channel}\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\"}\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n\n{embed=\"_includes/.header\"}\n\n  <section class=\"bread\">\n    <div class=\"content-width\">\n      <a href=\"{path=\'\'}\" class=\"home crumb\">Home</a> \\ <a href=\"{path=\'{current_channel}\'}\" class=\"crumb\">Flocks</a> \\ <span class=\"crumb\">{title}</span>\n    </div>\n  </section>\n  \n  <section class=\"content content-width clearfix\">\n    <h1 class=\"page-title\">{title}</h1>\n    \n    <section class=\"page-content\">\n      {main_body_area}\n    </section>\n    \n    <aside>\n      {sidebar_area}\n    </aside>\n  </section>\n{/exp:channel:entries}\n{/if}\n\n{embed=\"_includes/.footer\"}','',1365516404,1,'y',0,'','n','n','o',2835),
	(10,1,7,'index','n','webpage','{preload_replace:current_channel=\"ministries\"}\n\n{if \"{segment_2}\" == \"\"}\n\n{exp:channel:entries channel=\"landing_pages\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\" entry_id=\"10\"}\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n\n{embed=\"_includes/.header\"}\n\n  <section class=\"hero content-width\">\n    <img src=\"{section_banner_image}\" alt=\"{section_banner_text}\">\n    \n    <section class=\"overview\">\n      <div class=\"inner\">\n        <h1>{section_overview_title}</h1>\n        {section_overview_paragraph}\n      </div>\n    </section>\n  </section>\n{/exp:channel:entries}\n\n  \n  <section class=\"subnav\">\n    <nav class=\"content-width\">\n      <ul>\n        {exp:channel:entries channel=\"{current_channel}\" limit=\"6\" orderby=\"date\" sort=\"desc\" disable=\"categories|category_fields|member_data|pagination\"}\n        <li>\n          <a href=\"{title_permalink=\'{current_channel}\'}\" class=\"block\">\n            <img src=\"{preview_image}\" alt=\"{title}\">\n            <h1>{title}</h1>\n          </a>\n        </li>\n        {/exp:channel:entries}\n        <!--<li><img src=\"./assets/img/subnav-ministries-children-2x.jpg\" alt=\"Clearcreek Chapel\'s children\'s ministries\"></li>-->\n      </ul>\n    </nav>\n  </section>\n\n{if:else}\n\n{exp:channel:entries channel=\"{current_channel}\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\"}\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n\n{embed=\"_includes/.header\"}\n\n  <section class=\"bread\">\n    <div class=\"content-width\">\n      <a href=\"{path=\'\'}\" class=\"home crumb\">Home</a> \\ <a href=\"{path=\'{current_channel}\'}\" class=\"crumb\">Ministries</a> \\ <span class=\"crumb\">{title}</span>\n    </div>\n  </section>\n  \n  <section class=\"content content-width clearfix\">\n    <h1 class=\"page-title\">{title}</h1>\n    \n    <section class=\"page-content\">\n      {main_body_area}\n    </section>\n    \n    <aside>\n      {sidebar_area}\n    </aside>\n  </section>\n{/exp:channel:entries}\n{/if}\n\n{embed=\"_includes/.footer\"}','',1367856738,1,'y',0,'','n','n','o',7266),
	(12,1,8,'index','n','webpage','{preload_replace:current_channel=\"news\"}\n\n\n{exp:channel:entries channel=\"landing_pages\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\" entry_id=\"25\"}\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n\n{embed=\"_includes/.header\"}\n\n   <section class=\"hero content-width\">\n    <img src=\"{section_banner_image}\" alt=\"{section_banner_text}\">\n    \n    <section class=\"overview\">\n      <div class=\"inner\">\n        <h1>{section_overview_title}</h1>\n        {section_overview_paragraph}\n      </div>\n    </section>\n  </section>\n{/exp:channel:entries}\n\n{exp:channel:entries channel=\"{current_channel}\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\" entry_id=\"51\"}\n  <section class=\"content content-width clearfix\">\n    <h1 class=\"page-title\">{title}</h1>\n    \n    <section class=\"page-content\">\n      {main_body_area}\n      \n      <div class=\"events\">\n      <!--Begin SermonAudio Link Button-->\n        <SCRIPT LANGUAGE=\"JavaScript\" type=\"text/javascript\" src=\"http://www.sermonaudio.com/code_calendar.asp?sourceid=TheChapel\"></SCRIPT>\n      <!--End SermonAudio Link Button-->\n      </div>\n\n      \n    </section>\n{/exp:channel:entries}          \n    <aside>\n      <h1>More Chapel Events</h1>\n      <nav>\n        <ul>\n      {exp:channel:entries channel=\"news_listing\" disable=\"categories|category_fields|member_data|pagination\"}\n          <li><a href=\"{title_permalink=\'news-and-events/details\'}\">{title}</a></li>\n      {/exp:channel:entries}\n        </ul>\n      </nav>\n    </aside>\n  </section>\n\n\n{embed=\"_includes/.footer\"}\n\n','',1368238150,1,'y',0,'','n','n','o',1117),
	(13,1,5,'sermon-repository','n','webpage','{preload_replace:current_channel=\"sermons\"}\n{exp:channel:entries channel=\"{current_channel}\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\" entry_id=\"21\"}\n\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n{embed=\"_includes/.header\"}\n\n<section class=\"bread\">\n    <div class=\"content-width\">\n      <a href=\"{path=\'\'}\" class=\"home crumb\">Home</a> \\ <a href=\"{path=\'sermons\'}\" class=\"crumb\">Sermons</a> \\ <span class=\"crumb\">Sermon Repository</span>\n    </div>\n  </section>\n  \n  <section class=\"content content-width clearfix\">\n    <h1 class=\"page-title\">Latest Sermons</h1>\n    \n    <section class=\"page-content\">\n      {main_body_area}\n{/exp:channel:entries}\n      <div class=\"feed sermons\">\n        {exp:magpie url=\"http://www.sermonaudio.com/rss_source.rss?SourceID=thechapel&filter=mp3&sortby=date\" limit=\"4\" refresh=\"720\"}\n        {items}\n          <summary>\n          <time>{pubdate}</time>\n          <h1>{title}</h1>\n          {if \"{itunes:author}\" != \"\"}<cite>Speaker: {itunes:author}</cite>{/if}\n          {if \"{itunes:subtitle}\" != \"\"}<em>Passage: {itunes:subtitle}</em>{/if}\n          <!--<div class=\"audio-player\"><audio id=\"player2\" src=\"{enclosure@url}\" type=\"audio/mp3\" controls=\"controls\"></div>-->\n          <p class=\"more\"><a href=\"{link}\" target=\"_blank\">View Details</a> and listen on SermonAudio.com</p>\n          </summary>\n        {/items}\n        {/exp:magpie}\n        \n        <!--{exp:ajw_feedparser \n          url=\"http://www.sermonaudio.com/rss_source.rss?SourceID=thechapel&filter=mp3&sortby=date\"\n          cache_refresh=\"30\"\n          limit=\"10\"\n\n        }\n        <summary>\n          <time>{pubDate}</time>\n          <h1>{title}</h1>\n          <cite>Speaker: {itunes:author}</cite>\n          {if \"{itunes:subtitle}\" != \"\"}<em>Passage: {itunes:subtitle}</em>{/if}\n          <div class=\"audio-player\"><audio id=\"player2\" src=\"{enclosure@url}\" type=\"audio/mp3\" controls=\"controls\"></div>\n          <p class=\"more\"><a href=\"{link}\" target=\"_blank\">View Details</a> on SermonAudio.com</p>\n        </summary>\n        {/exp:ajw_feedparser}-->\n      </div>\n      \n      <a href=\"http://www.sermonaudio.com/source_detail.asp?sourceid=thechapel\" target=\"_blank\" class=\"more\">More sermons available from SermonAudio.com</a>\n      \n    </section>\n{exp:channel:entries channel=\"{current_channel}\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\" entry_id=\"21\"}    \n    <aside>\n      {sidebar_area}\n    </aside>\n  </section>\n{/exp:channel:entries}\n\n{embed=\"_includes/.footer\"}\n','',1365217661,1,'y',0,'','n','n','o',855),
	(14,1,9,'index','n','webpage','{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n\n{embed=\"_includes/.header\"}\n\n  <section class=\"bread\">\n    <div class=\"content-width\">\n      <a href=\"{path=\'\'}\" class=\"home crumb\">Home</a> \\ <span class=\"crumb\">Styleguide</span>\n    </div>\n  </section>\n  \n  <section class=\"content content-width clearfix\">\n    <h1 class=\"page-title\">Entry Title</h1>\n    \n    <section class=\"page-content\">\n      <h1>H1 Heading</h1>\n      <p>Paragraph text aenean eu leo quam. <a href=\"#\">This text is linked</a> lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <p><b>Bold text</b> aenean eu leo quam. <i>Italic text</i> sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <h2>H2 Heading</h2>\n      <p>Paragraph text aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <h3>H3 Heading</h3>\n      <p>Paragraph text aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <h4>H4 Heading</h4>\n      <p>Paragraph text aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <h5>H5 Heading</h5>\n      <p>Paragraph text aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <h6>H6 Heading</h6>\n      <p>Paragraph text aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <blockquote>Blockquote text curabitur blandit tempus porttitor. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</blockquote>\n      \n      <ul>\n        <li>Unordered list item</li>\n        <li>Unordered list item</li>\n        <li>Unordered list item</li>\n        <li>Unordered list item</li>\n        <li>Unordered list item</li>\n      </ul>\n      \n      <ul>\n        <li>Ordered list item</li>\n        <li>Ordered list item</li>\n        <li>Ordered list item</li>\n        <li>Ordered list item</li>\n        <li>Ordered list item</li>\n      </ul>\n    </section>\n    \n    <aside>\n      <h1>Sidebar H1 Heading</h1>\n      <p>Paragraph text aenean eu leo quam. <a href=\"#\">This text is linked</a> lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <p><b>Bold text</b> aenean eu leo quam. <i>Italic text</i> sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <h2>Sidebar H2 Heading</h2>\n      <p>Paragraph text aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <h3>Sidebar H3 Heading</h3>\n      <p>Paragraph text aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <h4>Sidebar H4 Heading</h4>\n      <p>Paragraph text aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <h5>Sidebar H5 Heading</h5>\n      <p>Paragraph text aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <h6>Sidebar H6 Heading</h6>\n      <p>Paragraph text aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec ullamcorper nulla non metus auctor fringilla.</p>\n      \n      <blockquote>Blockquote text curabitur blandit tempus porttitor. Morbi leo risus, porta ac consectetur ac, vestibulum at eros.</blockquote>\n      \n      <ul>\n        <li>Unordered list item</li>\n        <li>Unordered list item</li>\n        <li>Unordered list item</li>\n        <li>Unordered list item</li>\n        <li>Unordered list item</li>\n      </ul>\n      \n      <ul>\n        <li>Ordered list item</li>\n        <li>Ordered list item</li>\n        <li>Ordered list item</li>\n        <li>Ordered list item</li>\n        <li>Ordered list item</li>\n      </ul>\n\n    </aside>\n  </section>\n\n{embed=\"_includes/.footer\"}','',1364914030,1,'y',0,'','n','n','o',12),
	(15,1,4,'leaders','n','webpage','{preload_replace:current_channel=\"about\"}\n\n{exp:channel:entries channel=\"{current_channel}\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\" entry_id=\"12\"}\n\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n{embed=\"_includes/.header\"}\n\n<section class=\"bread\">\n    <div class=\"content-width\">\n      <a href=\"{path=\'\'}\" class=\"home crumb\">Home</a> \\ <a href=\"{path=\'about\'}\" class=\"crumb\">About</a> \\ <span class=\"crumb\">{title}</span>\n    </div>\n  </section>\n  \n  <section class=\"content content-width clearfix\">\n    <h1 class=\"page-title\">{title}</h1>\n    \n    <section class=\"page-content\">\n    {main_body_area}\n    \n    {/exp:channel:entries}\n      \n      <div class=\"listing elders\">\n        <h1>Elders</h1>\n        {exp:channel:entries channel=\"leaders\" category=\"1\" orderby=\"last_name\" sort=\"asc\" disable=\"category_fields|member_data|pagination\"}\n        <summary>\n          {if \"{leader_image}\" != \"\"}<img src=\"{leader_image}\" alt=\"{title}\">{/if}\n          <h1>{title} {last_name}</h1>\n          {if \"{position_title}\" != \"\"}<em class=\"position\">{position_title}</em>{/if}\n          {if \"{oversite_area}\" != \"\"}<em class=\"oversite\">{oversite_area}</em>{/if}\n          {if \"{elders_flock}\" != \"\"}<em class=\"flock\">Shepherd over the {elders_flock} Flock</em>{/if}\n          <p class=\"summary\">{bio_preview_overview}</p>\n        </summary>\n        {/exp:channel:entries}\n        \n        <h1>Office Staff</h1>\n        {exp:channel:entries channel=\"leaders\" category=\"2\" orderby=\"last_name\" sort=\"asc\" disable=\"category_fields|member_data|pagination\"}\n        <summary>\n          {if \"{leader_image}\" != \"\"}<img src=\"{leader_image}\" alt=\"{title}\">{/if}\n          <h1>{title} {last_name}</h1>\n          {if \"{position_title}\" != \"\"}<em class=\"position\">{position_title}</em>{/if}\n          {if \"{oversite_area}\" != \"\"}<em>{oversite_area}</em>{/if}\n          <p class=\"summary\">{bio_preview_overview}</p>\n        </summary>\n        {/exp:channel:entries}\n        \n        <h1>Custodial Staff</h1>\n        {exp:channel:entries channel=\"leaders\" category=\"3\" orderby=\"last_name\" sort=\"asc\" disable=\"category_fields|member_data|pagination\"}\n        <summary>\n          {if \"{leader_image}\" != \"\"}<img src=\"{leader_image}\" alt=\"{title}\">{/if}\n          <h1>{title} {last_name}</h1>\n          {if \"{position_title}\" != \"\"}<em class=\"position\">{position_title}</em>{/if}\n          {if \"{oversite_area}\" != \"\"}<em>{oversite_area}</em>{/if}\n          <p class=\"summary\">{bio_preview_overview}</p>\n        </summary>\n        {/exp:channel:entries}\n      </div>\n      \n    </section>\n    \n    {exp:channel:entries channel=\"{current_channel}\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\" entry_id=\"12\"}\n    <aside>\n      {sidebar_area}\n    </aside>\n    {/exp:channel:entries}\n    \n  </section>\n\n\n{embed=\"_includes/.footer\"}\n','',1365205775,1,'y',0,'','n','n','o',889),
	(16,1,2,'sitemap','n','xml','<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n\n<urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">\n\n   {exp:channel:entries channel=\"home\" disable=\"categories|category_fields|member_data|pagination\"}\n   <url>\n      <loc>{path=\'\'}</loc>\n      <lastmod>{edit_date format=\"%Y-%m-%d\"}</lastmod>\n      <changefreq>weekly</changefreq>\n      <priority>0.8</priority>\n   </url>\n   {/exp:channel:entries}\n   \n   {exp:channel:entries channel=\"about\" disable=\"categories|category_fields|member_data|pagination\"}\n   <url>\n      <loc>{title_permalink=\'about\'}</loc>\n      <lastmod>{edit_date format=\"%Y-%m-%d\"}</lastmod>\n      <changefreq>weekly</changefreq>\n      <priority>0.8</priority>\n   </url>\n   {/exp:channel:entries}\n   \n   \n   {exp:channel:entries channel=\"sermons\" disable=\"categories|category_fields|member_data|pagination\"}\n   <url>\n      <loc>{title_permalink=\'sermons\'}</loc>\n      <lastmod>{edit_date format=\"%Y-%m-%d\"}</lastmod>\n      <changefreq>weekly</changefreq>\n      <priority>0.8</priority>\n   </url>\n   {/exp:channel:entries}\n   \n   \n   {exp:channel:entries channel=\"flocks\" disable=\"categories|category_fields|member_data|pagination\"}\n   <url>\n      <loc>{title_permalink=\'flocks\'}</loc>\n      <lastmod>{edit_date format=\"%Y-%m-%d\"}</lastmod>\n      <changefreq>weekly</changefreq>\n      <priority>0.8</priority>\n   </url>\n   {/exp:channel:entries}\n   \n   {exp:channel:entries channel=\"ministries\" disable=\"categories|category_fields|member_data|pagination\"}\n   <url>\n      <loc>{title_permalink=\'ministries\'}</loc>\n      <lastmod>{edit_date format=\"%Y-%m-%d\"}</lastmod>\n      <changefreq>weekly</changefreq>\n      <priority>0.8</priority>\n   </url>\n   {/exp:channel:entries}\n   \n   {exp:channel:entries channel=\"news\" disable=\"categories|category_fields|member_data|pagination\"}\n   <url>\n      <loc>{path=\'news-and-events\'}</loc>\n      <lastmod>{edit_date format=\"%Y-%m-%d\"}</lastmod>\n      <changefreq>weekly</changefreq>\n      <priority>0.8</priority>\n   </url>\n   {/exp:channel:entries}\n\n</urlset> ','',1365293845,1,'y',0,'','n','n','o',62),
	(17,1,8,'details','n','webpage','{preload_replace:current_channel=\"news_listing\"}\n\n{exp:channel:entries channel=\"{current_channel}\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\"}\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n\n{embed=\"_includes/.header\"}\n\n  <section class=\"bread\">\n    <div class=\"content-width\">\n      <a href=\"{path=\'\'}\" class=\"home crumb\">Home</a> \\ <a href=\"{path=\'news-and-events\'}\" class=\"crumb\">News & Events</a> \\ <span class=\"crumb\">{title}</span>\n    </div>\n  </section>\n  \n  <section class=\"content content-width clearfix\">\n    <h1 class=\"page-title\">{title}</h1>\n    \n    <section class=\"page-content\">\n    \n      {if \"{event_date}\" != \"\"}<h1>Event Date: {event_date}</h1>{/if}\n      {news_body}\n    </section>\n    \n    <aside>\n      <p>If you\'d like to see more, you can go <a href=\"{path=\'news-and-events\'}\">back to News & Events</a>.</p>\n    </aside>\n  </section>\n{/exp:channel:entries}\n\n{embed=\"_includes/.footer\"}\n\n','',1368238200,1,'y',0,'','n','n','o',341),
	(18,1,5,'sermon-repository-upgrade','n','webpage','{preload_replace:current_channel=\"sermons\"}\n{exp:channel:entries channel=\"{current_channel}\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\" entry_id=\"21\"}\n\n{embed=\"_includes/.doctype\" entry_id=\"{entry_id}\"}\n{embed=\"_includes/.header\"}\n\n<section class=\"bread\">\n    <div class=\"content-width\">\n      <a href=\"{path=\'\'}\" class=\"home crumb\">Home</a> \\ <a href=\"{path=\'sermons\'}\" class=\"crumb\">Sermons</a> \\ <span class=\"crumb\">Sermon Repository</span>\n    </div>\n  </section>\n  \n  <section class=\"content content-width clearfix\">\n    <h1 class=\"page-title\">Latest Sermons</h1>\n    \n    <section class=\"page-content\">\n      {main_body_area}\n{/exp:channel:entries}\n      <div class=\"feed sermons\">\n        {exp:magpie url=\"http://www.sermonaudio.com/rss_source.rss?SourceID=thechapel&filter=mp3&sortby=date\" limit=\"4\" refresh=\"720\"}\n        {items}\n          <summary>\n          <time>{pubdate}</time>\n          <h1>{title}</h1>\n          {if \"{itunes:author}\" != \"\"}<cite>Speaker: {itunes:author}</cite>{/if}\n          {if \"{itunes:subtitle}\" != \"\"}<em>Passage: {itunes:subtitle}</em>{/if}\n          <!--<div class=\"audio-player\"><audio id=\"player2\" src=\"{enclosure@url}\" type=\"audio/mp3\" controls=\"controls\"></div>-->\n          <p class=\"more\"><a href=\"{link}\" target=\"_blank\">View Details</a> and listen on SermonAudio.com</p>\n          </summary>\n        {/items}\n        {/exp:magpie}\n        \n        {!--{exp:ajw_feedparser \n          url=\"http://www.sermonaudio.com/rss_source.rss?SourceID=thechapel&filter=mp3&sortby=date\"\n          cache_refresh=\"30\"\n          limit=\"10\"\n          debug=\"yes\"\n        }\n        <summary>\n          <time>{pubDate}</time>\n          <h1>{title}</h1>\n          <cite>Speaker: {itunes:author}</cite>\n          {if \"{itunes:subtitle}\" != \"\"}<em>Passage: {itunes:subtitle}</em>{/if}\n          <!--<div class=\"audio-player\"><audio id=\"player2\" src=\"{enclosure@url}\" type=\"audio/mp3\" controls=\"controls\"></div>-->\n          <p class=\"more\"><a href=\"{link}\" target=\"_blank\">View Details</a> on SermonAudio.com</p>\n        </summary>\n        {/exp:ajw_feedparser}--}\n      </div>\n      \n      <a href=\"http://www.sermonaudio.com/source_detail.asp?sourceid=thechapel\" target=\"_blank\" class=\"more\">More sermons available from SermonAudio.com</a>\n      \n    </section>\n{exp:channel:entries channel=\"{current_channel}\" limit=\"1\" disable=\"categories|category_fields|member_data|pagination\" entry_id=\"21\"}    \n    <aside>\n      {sidebar_area}\n    </aside>\n  </section>\n{/exp:channel:entries}\n\n{embed=\"_includes/.footer\"}','',1368643273,1,'n',0,'','n','n','o',5);

/*!40000 ALTER TABLE `exp_templates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_throttle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_throttle`;

CREATE TABLE `exp_throttle` (
  `throttle_id` int(10) unsigned NOT NULL auto_increment,
  `ip_address` varchar(45) NOT NULL default '0',
  `last_activity` int(10) unsigned NOT NULL default '0',
  `hits` int(10) unsigned NOT NULL,
  `locked_out` char(1) NOT NULL default 'n',
  PRIMARY KEY  (`throttle_id`),
  KEY `ip_address` (`ip_address`),
  KEY `last_activity` (`last_activity`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;



# Dump of table exp_upload_no_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_upload_no_access`;

CREATE TABLE `exp_upload_no_access` (
  `upload_id` int(6) unsigned NOT NULL,
  `upload_loc` varchar(3) NOT NULL,
  `member_group` smallint(4) unsigned NOT NULL,
  PRIMARY KEY  (`upload_id`,`member_group`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_upload_no_access` WRITE;
/*!40000 ALTER TABLE `exp_upload_no_access` DISABLE KEYS */;

INSERT INTO `exp_upload_no_access` (`upload_id`, `upload_loc`, `member_group`)
VALUES
	(1,'cp',8),
	(1,'cp',5),
	(1,'cp',7);

/*!40000 ALTER TABLE `exp_upload_no_access` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table exp_upload_prefs
# ------------------------------------------------------------

DROP TABLE IF EXISTS `exp_upload_prefs`;

CREATE TABLE `exp_upload_prefs` (
  `id` int(4) unsigned NOT NULL auto_increment,
  `site_id` int(4) unsigned NOT NULL default '1',
  `name` varchar(50) NOT NULL,
  `server_path` varchar(255) NOT NULL default '',
  `url` varchar(100) NOT NULL,
  `allowed_types` varchar(3) NOT NULL default 'img',
  `max_size` varchar(16) default NULL,
  `max_height` varchar(6) default NULL,
  `max_width` varchar(6) default NULL,
  `properties` varchar(120) default NULL,
  `pre_format` varchar(120) default NULL,
  `post_format` varchar(120) default NULL,
  `file_properties` varchar(120) default NULL,
  `file_pre_format` varchar(120) default NULL,
  `file_post_format` varchar(120) default NULL,
  `cat_group` varchar(255) default NULL,
  `batch_location` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `site_id` (`site_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

LOCK TABLES `exp_upload_prefs` WRITE;
/*!40000 ALTER TABLE `exp_upload_prefs` DISABLE KEYS */;

INSERT INTO `exp_upload_prefs` (`id`, `site_id`, `name`, `server_path`, `url`, `allowed_types`, `max_size`, `max_height`, `max_width`, `properties`, `pre_format`, `post_format`, `file_properties`, `file_pre_format`, `file_post_format`, `cat_group`, `batch_location`)
VALUES
	(1,1,'Uploads','/data/14/2/139/19/2139834/user/2348552/htdocs/expressionengine/uploads/','http://www.clearcreekchapel.org/uploads/','all','','','','','','','','','','','');

/*!40000 ALTER TABLE `exp_upload_prefs` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
