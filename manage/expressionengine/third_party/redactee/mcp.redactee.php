<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once PATH_THIRD.'redactee/config.php';
require_once PATH_THIRD.'redactee/sql.redactee.php';

class Redactee_mcp {
	
	function __construct() {
		$this->sql = new Redactee_sql();
		$this->EE = &get_instance();
	}
	
	function index() {
		$this->EE->functions->redirect(BASE.'&C=addons_fieldtypes&M=global_settings&ft=redactee');
	}
}
