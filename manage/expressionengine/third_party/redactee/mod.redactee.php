<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Redactee
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Produced and sold in cooperation with Lobster War Machine (http://lobsterwarmachine.com/)
 * @author Jeremy Messenger (http://jlmessenger.com/)
 * @copyright 2012 Jeremy Messenger
 **/

require_once PATH_THIRD.'redactee/config.php';
require_once PATH_THIRD.'redactee/sql.redactee.php';

class Redactee {
	
	function __construct() {
		$this->sql = new Redactee_sql();
		$this->EE = &get_instance();
	}
	
	function load() {
		$selector = $this->EE->TMPL->fetch_param('selector', FALSE);
		$toolbar_id = $this->EE->TMPL->fetch_param('toolbar', FALSE);
		if ( ! ctype_digit($toolbar_id) && $toolbar_id) {
			$titles = $this->sql->toolbar_titles();
			$toolbar_id = array_search($toolbar_id, $titles);
		}
		if ( ! $toolbar_id) {
			return '';
		}
		$height = $this->EE->TMPL->fetch_param('minheight', '');
		
		$field_settings = $this->sql->global_default_settings;
		$field_settings['toolbar_id'] = $toolbar_id;
		if (ctype_digit($height)) {
			$field_settings['minHeight'] = $height;
		}
		
		$this->head = array();
		$this->sql->_add_redactor_head($toolbar_id, '', $field_settings, array($this, '_add_to_head'));
		
		if ($selector) {
			$this->_add_to_head("<script type=\"text/javascript\">$(function(){ redactee_toolbars.setup($('{$selector}'), 'c{$toolbar_id}'); });</script>");
		}
		return implode("\n", $this->head);
	}
	
	function _add_to_head($str) {
		$this->head[] = $str;
	}
	
	function _show_error($message = FALSE) {
		header($_SERVER['SERVER_PROTOCOL'].' 500 Internal Server Error');
		if ($message) {
			echo 'Error: '.$message;
		}
		exit(0);
	}
	
	function addJsonResponseHeaders() {
		header('Vary: Accept');
		if (isset($_SERVER['HTTP_ACCEPT']) &&
			(strpos($_SERVER['HTTP_ACCEPT'], 'application/json') !== false)) {
			header('Content-type: application/json');
		} else {
			header('Content-type: text/plain');
		}
	}
	
	function pagesGetJson() {
		if ( ! $this->EE->input->get('ACT')) {
			return ''; // action only
		}
		
		$field_id = $this->EE->input->get('field');
		if ( ! $field_id) {
			$this->_show_error('Missing required parameter'); // required param missing
		}
		
		$member_id = $this->EE->session->userdata('member_id');
		if ($member_id === '' || $member_id < 1) {
			$this->_show_error('Login required'); // not logged in
		}
		
		$has_structure = $this->sql->use_structure();
		$has_pages = $this->sql->use_pages();
		
		if ( ! $has_pages && ! $has_structure) {
			$this->_show_error('No site pages'); // no pages/structure module
		}
		
		$settings = $this->sql->get_field_settings($field_id);
		if ( ! is_array($settings) || ! isset($settings['toolbar_id']) ||
				! isset($settings['site_pages']) || $settings['site_pages'] === 'none') {
			$this->_show_error('Unable to Read Field Settings');
		}
		
		$global = $this->sql->get_global();
		$status = strtolower($global['site_page_entry_status']);
		$status_exclude = FALSE;
		if (strncmp($status, 'not ', 4) === 0) {
			$status_exclude = TRUE;
			$status = substr($status, 4);
		}
		$statuses = explode('|', $status);
		
		$this->addJsonResponseHeaders();
		
		echo '[';
		if ($has_structure) {
			$branch_entry_id = 0;
			if (ctype_digit($settings['site_pages'])) {
				$branch_entry_id = $settings['site_pages'];
			}
			$pages = $this->sql->_structure_pages($branch_entry_id, $global['site_page_entry_status']);
			$channels = array();
			if ($branch_entry_id !== 0) {
				$data = array_shift($pages);
				if ($data['listing_cid'] > 0) {
					$channels[$data['listing_cid']] = array($data['uri'], substr($data['uri'], -1) === '/');
				}
			}
			$first = TRUE;
			if (is_array($pages) && count($pages)) {
				foreach ($pages as $data) {
					$out = array(
							'title' => $data['title'],
							'pagelink' => $data['uri']
						);
					if ($data['depth'] > 1) {
						$out['depth'] = $data['depth']-1;
					}
					if ($first) {
						$first = FALSE;
					} else {
						echo ',';
					}
					echo $this->sql->json_encode($out);
					if ($data['listing_cid'] > 0) {
						$channels[$data['listing_cid']] = array($data['uri'], substr($data['uri'], -1) === '/');
					}
				}
			}
			if (isset($settings['site_listings']) && $settings['site_listings'] === 'Y' && count($channels)) {
				$this->EE->db->select('channel_titles.channel_id, entry_id, title, channel_title, url_title');
				$this->EE->db->where_in('channel_titles.channel_id', array_keys($channels));
				if (count($statuses) > 0) {
					if ($status_exclude) {
						$this->EE->db->where_not_in('channel_titles.status', $statuses);
					} else {
						$this->EE->db->where_in('channel_titles.status', $statuses);
					}
				}
				$this->EE->db->order_by('title');
				$this->EE->db->join('channels', 'channel_titles.channel_id = channels.channel_id');
				$q = $this->EE->db->get('channel_titles');
				foreach ($q->result() as $row) {
					list($uri, $has_slash) = $channels[$row->channel_id];
					$page_link = $this->sql->make_path($uri, $row->url_title);
					if ($has_slash) { // add trailing slash
						$page_link .= '/';
					}
					if ($first) {
						$first = FALSE;
					} else {
						echo ',';
					}
					echo $this->sql->json_encode(array(
							'title' => $row->title,
							'group' => $row->channel_title,
							'pagelink' => $page_link
						));
				}
				$q->free_result();
			}
		} elseif ($has_pages) {
			$pages = $this->sql->_site_pages();
			if (is_array($pages) && count($pages)) {
				$this->EE->db->select('entry_id, title, channel_title');
				$this->EE->db->where_in('entry_id', array_keys($pages));
				if (count($statuses) > 0) {
					if ($status_exclude) {
						$this->EE->db->where_not_in('channel_titles.status', $statuses);
					} else {
						$this->EE->db->where_in('channel_titles.status', $statuses);
					}
				}
				$this->EE->db->order_by('title');
				$this->EE->db->join('channels', 'channel_titles.channel_id = channels.channel_id');
				$q = $this->EE->db->get('channel_titles');
				$first = TRUE;
				foreach ($q->result() as $i => $row) {
					if ($first) {
						$first = FALSE;
					} else {
						echo ',';
					}
					echo $this->sql->json_encode(array(
							'title' => $row->title,
							'group' => $row->channel_title,
							'pagelink' => $pages[$row->entry_id]
						));
				}
				$q->free_result();
			}
		}
		echo ']';
		exit(0);
	}
	
	function imageGetJson() {
		if ( ! $this->EE->input->get('ACT')) {
			return ''; // action only
		}
		
		$field_id = $this->EE->input->get('field');
		if ( ! $field_id) {
			$this->_show_error('Missing required parameter'); // required param missing
		}
		
		$member_id = $this->EE->session->userdata('member_id');
		if ($member_id === '' || $member_id < 1) {
			$this->_show_error('Login required'); // not logged in
		}
		
		$settings = $this->sql->get_field_settings($field_id);
		if ( ! is_array($settings) || ! isset($settings['toolbar_id']) ||
				! isset($settings['image_browse']) || $settings['image_browse'] === 'none') {
			$this->_show_error('Unable to read field settings');
		}
		
		$this->addJsonResponseHeaders();
		
		$group_id = $this->EE->session->userdata('group_id');
		
		$no_access = $this->sql->upload_no_access($group_id);
		
		$prefs = $this->sql->upload_prefs();
		$subdir = FALSE;
		if ($settings['image_browse'] !== 'all') {
			if (ctype_digit($settings['image_browse'])) {
				$loc_id = $settings['image_browse'];
			} else {
				list($loc_id, $subdir) = explode('/', $settings['image_browse'], 2);
			}
			
			if (isset($prefs[$loc_id])) {
				// limit to a single folder
				$prefs = array($loc_id => $prefs[$loc_id]);
			} else {
				$prefs = array();
			}
		}
		
		$has_assets = $this->sql->use_assets();
		$recurse = $has_assets && isset($settings['image_browse_subdirs']) && $settings['image_browse_subdirs'] === 'Y';
		
		$first = TRUE;
		echo '[';
		foreach ($prefs as $loc_id => $pref) {
			if (in_array($loc_id, $no_access)) {
				continue; // access denied for this location
			}
			if ($subdir === FALSE || ! $has_assets) {
				$this->_dir_json($pref->name, $pref->server_path, $pref->url, $first, $recurse);
			} else {
				$path = $this->sql->make_path($pref->server_path, $subdir);
				$url = $this->sql->make_path($pref->url, $subdir);
				$name = $pref->name.'/'.$pref->name;
				$this->_dir_json($name, $path, $url, $first, $recurse);
			}
		}
		echo ']';
		exit(0);
	}
	
	function _dir_json($folder, $path, $url, &$first, $recurse=FALSE) {
		if (substr($path, -1) !== '/') {
			$path .= '/';
		}
		if (substr($url, -1) !== '/') {
			$url .= '/';
		}
		$subdirs = array();
		
		$thumb_path = $path.'_thumbs/';
		if ( ! is_dir($thumb_path)) {
			$thumb_path = FALSE;
		}
		
		if (is_dir($path)) {
			$d = dir($path);
			while (false !== ($entry = $d->read())) {
				if ($entry[0] === '.') {
					continue;
				}
				$p = $path.$entry;
				if (is_file($p)) {
					if (preg_match('/\\.(tiff?|gif|png|jpe?g|ico|bmp|psd)$/i', $entry)) {
						if ($first) {
							$first = FALSE;
						} else {
							echo ',';
						}
						$img_src = $url.$entry;
						$thumb_src = $img_src;
						if ($thumb_path !== FALSE) {
							$ts = $thumb_path.$entry;
							if (is_file($ts)) {
								$thumb_src = $url.'_thumbs/'.$entry;
							}
						}
						
						echo $this->sql->json_encode(array(
								'thumb' => $thumb_src,
								'image' => $img_src,
								'title' => $entry,
								'folder' => $folder
							));
					}
				} else {
					$subdirs[] = array($p, $entry);
				}
			}
			$d->close();
		}
		if ($recurse && count($subdirs)) {
			asort($subdirs);
			foreach ($subdirs as $sd) {
				list($p, $entry) = $sd;
				
				if ($entry === '_thumbs') {
					continue;
				}
				if (is_dir($p)) {
					$this->_dir_json($folder.'/'.$entry, $p, $url.$entry, $first, $recurse);
				}
			}
		}
	}
	
	function imageUpload() {
		if ( ! $this->EE->input->get('ACT')) {
			return ''; // action only
		}
		
		$file_data = $this->_upload_file('image_upload_id', TRUE);
		if ( ! is_array($file_data)) {
			$this->_show_error($file_data);
		}
		
		$this->addJsonResponseHeaders();
		
		echo $this->sql->json_encode(array('filelink' => $file_data['file_url']));
		exit(0);
	}
	
	function fileUpload() {
		if ( ! $this->EE->input->get('ACT')) {
			return ''; // action only
		}
		
		$file_data = $this->_upload_file('file_upload_id', FALSE);
		if ( ! is_array($file_data)) {
			$this->_show_error($file_data);
		}
		
		$this->addJsonResponseHeaders();
		
		echo $this->sql->json_encode(array(
				'filelink' => $file_data['file_url'],
				'filename' => $file_data['file_name']
			));
		exit(0);
	}
	
	function _upload_file($setting_upload_id_key, $image_only=FALSE) {
		
		$s3_key = $this->EE->input->get('key');
		if ($s3_key) {
			$global = $this->sql->get_global();
			if (isset($global['aws_enable']) && $global['aws_enable'] === 'Y' &&
					isset($global['aws_bucket']) && $global['aws_bucket'] !== '') {
				return array(
						'file_name' => basename($s3_key),
						'file_url' => "https://{$global['aws_bucket']}.s3.amazonaws.com/{$s3_key}"
					);
			}
			return 'Amazon S3 disabled';
		}
		
		
		$field_id = $this->EE->input->get('field');
		if ( ! $field_id) {
			return 'Missing required parameter'; // required param missing
		}
		
		$member_id = $this->EE->session->userdata('member_id');
		if ($member_id === '' || $member_id < 1) {
			return 'Login required'; // not logged in
		}
		
		if ( ! isset($_FILES['file']) || $_FILES['file']['size'] == 0) {
			return 'No file uploaded';
		}
		
		$settings = $this->sql->get_field_settings($field_id);
		if ( ! is_array($settings) || ! isset($settings['toolbar_id']) ||
				! isset($settings[$setting_upload_id_key]) || $settings[$setting_upload_id_key] === 'none') {
			return 'No upload location set';
		}
		
		$has_assets = $this->sql->use_assets();
		$subdir = FALSE;
		if (ctype_digit($settings[$setting_upload_id_key])) {
			$loc_id = $settings[$setting_upload_id_key];
		} else {
			list($loc_id, $subdir) = explode('/', $settings[$setting_upload_id_key], 2);
		}
		
		$group_id = $this->EE->session->userdata('group_id');
		
		$no_access = $this->sql->upload_no_access($group_id, $loc_id);
		if (in_array($loc_id, $no_access)) {
			return 'Access denied';
		}
		
		$prefs = $this->sql->upload_prefs();
		if ( ! isset($prefs[$loc_id])) {
			return 'Upload location invalid';
		}
		$pref = $prefs[$loc_id];
		
		$file_data = $this->sql->upload_file($loc_id, 'file', $image_only);
		if (array_key_exists('error', $file_data)) {
			return strip_tags($file_data['error']);
		}
		
		if ($has_assets && $subdir) {
			// move to subfolder
			$cur_path = $file_data['rel_path'];
			$subdir_path = $this->sql->make_path($pref->server_path, $subdir);
			if (substr($subdir_path, -1) !== '/') {
				$subdir_path .= '/';
			}
			
			if (is_dir($subdir_path)) {
				$entry = $file_data['file_data_orig_name'];
				$dot = strrpos($entry, '.');
				if ($dot) {
					$ext = substr($entry, $dot);
					$name = substr($entry, 0, $dot);
				} else {
					$ext = '';
					$name = $entry;
				}
				
				$conflict = TRUE;
				$p = $subdir_path.$entry;
				$new_name = $entry;
				$ext = strrchr($entry, '.');
				for ($i = 0; $i<1000; $i++) {
					if (file_exists($p)) {
						$conflict = TRUE;
						$new_name = $name.$i.$ext;
						$p = $subdir_path.$new_name;
					} else {
						$conflict = FALSE;
						break;
					}
				}
				if ( ! $conflict) {
					$new_path = $p;
					$moved_file = rename($cur_path, $new_path);
					if ($moved_file) {
						$cur_thumb_path = $this->sql->make_path(dirname($cur_path), '_thumbs', $file_data['file_name']);
						$new_thumb_path = $this->sql->make_path($subdir_path, '_thumbs', $new_name);
						rename($cur_thumb_path, $new_thumb_path);
						
						$new_data = array(
								'file_url' => $this->sql->make_path($pref->url, $subdir, $new_name),
								'file_name' => $new_name
							);
						
						if (isset($file_data['file_id'])) {
							$this->EE->db->where('file_id', $file_data['file_id']);
							$this->EE->db->delete('files');
						}
						
						return $new_data;
					}
					
				} // else - leave alone (even though in wrong folder)
			}
			
			
			print_r($file_data);
		}
		
		$file_data['file_url'] = $this->sql->make_path($pref->url, $file_data['file_name']);
		return $file_data;
	}
}
