<?php
/**
 * Redactee
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Produced and sold in cooperation with Lobster War Machine (http://lobsterwarmachine.com/)
 * @author Jeremy Messenger (http://jlmessenger.com/)
 * @copyright 2012 Jeremy Messenger
 **/

if ( ! defined('REDACTEE_NAME')) {
	define('REDACTEE_NAME', 'Redactee');
	define('REDACTEE_VER', '2.2');
	define('REDACTEE_DESC', 'Redactor.js WYSIWYG for EE');
	define('REDACTEE_CLASS', 'Redactee');
}


// NSM Addon Updater
$config['name'] = REDACTEE_NAME;
$config['version'] = REDACTEE_VER;
$config['nsm_addon_updater']['versions_xml'] = 'http://jlmessenger.com/redactee.xml';

