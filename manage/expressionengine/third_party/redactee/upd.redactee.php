<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Redactee
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Produced and sold in cooperation with Lobster War Machine (http://lobsterwarmachine.com/)
 * @author Jeremy Messenger (http://jlmessenger.com/)
 * @copyright 2012 Jeremy Messenger
 **/

require_once PATH_THIRD.'redactee/config.php';
require_once PATH_THIRD.'redactee/sql.redactee.php';

class Redactee_upd {
	
	var $version = REDACTEE_VER;
	
	function __construct() {
		$this->sql = new Redactee_sql();
		$this->EE = &get_instance();
	}
	
	function install() {
		$default_toolbar_id = $this->sql->setup_db();
		
		$this->EE->db->insert('modules', array(
				'module_name' => REDACTEE_CLASS,
				'module_version' => REDACTEE_VER,
				'has_cp_backend' => 'y',
				'has_publish_fields' => 'n'
			));
		
		$this->EE->db->insert_batch('actions', array(
			array(
				'class' => REDACTEE_CLASS,
				'method' => 'imageGetJson'
			),
			array(
				'class' => REDACTEE_CLASS,
				'method' => 'imageUpload'
			),
			array(
				'class' => REDACTEE_CLASS,
				'method' => 'fileUpload'
			),
			array(
				'class' => REDACTEE_CLASS,
				'method' => 'pagesGetJson'
			)
		));
		
		return TRUE;
	}
	
	function update($current=FALSE) {
		$updated = FALSE;
		if (version_compare($current, '1.1', '<')) {
			$updated = TRUE;
			$this->EE->db->insert('actions', array(
				'class' => REDACTEE_CLASS,
				'method' => 'pagesGetJson'
			));
		}
		if (version_compare($current, '1.2', '<')) {
			$updated = TRUE;
			$this->EE->db->where('option', 'source');
			$this->EE->db->where('value', 'true');
			$this->EE->db->update('redactee_toolbar_options', array('value' => '"all"'));
			$this->EE->db->where('option', 'source');
			$this->EE->db->where('value', 'false');
			$this->EE->db->update('redactee_toolbar_options', array('value' => '"none"'));
		}
		if (version_compare($current, '2.1', '<')) {
			$updated = TRUE;
			$this->EE->db->select('toolbar_id, value');
			$this->EE->db->where('option', 'autoresize');
			$q = $this->EE->db->get('redactee_toolbar_options');
			$autoresize = array();
			foreach ($q->result() as $row){
				$autoresize[$row->toolbar_id] = $row->value !== 'false' ? 'Y' : '';
			}
			$q->free_result();
			//fix fields
			$this->_update_table_setting('channel_fields', 'field', 'autoresize', $autoresize, 'Y');
			//fix matrix
			$this->_update_table_setting('matrix_cols', 'col', 'autoresize', $autoresize, 'Y');
			//fix low
			$this->_update_table_setting('low_variables', 'variable', 'autoresize', $autoresize, 'Y');
			$this->EE->db->where('option', 'autoresize');
			$this->EE->db->delete('redactee_toolbar_options');
		}
		if (version_compare($current, '2.2', '<')) {
			$settings = array();
			$this->EE->db->select('option, value');
			$this->EE->db->where('toolbar_id', 0);
			$q = $this->EE->db->get('redactee_toolbar_options');
			foreach ($q->result() as $row){
				if (strncmp($row->option, 'aws_', 4) === 0 || $row->option === 'custom_stylesheet') {
					$settings[$row->option] = $row->value;
				} else {
					$settings['aws_'.$row->option] = $row->value;
				}
			}
			$q->free_result();
			$this->sql->save_global($settings);
		}
		
		return $updated;
	}
	
	function _update_table_setting($table, $field_prefix, $setting_name, $toolbar_lookup, $default_val) {
		if ($this->EE->db->table_exists($table)) {
			$id_field = $field_prefix.'_id';
			$type_field = $field_prefix.'_type';
			$settings_field = $field_prefix.'_settings';
			
			$this->EE->db->select("$id_field AS id, $settings_field AS settings");
			$this->EE->db->where($type_field, 'redactee');
			$q = $this->EE->db->get($table);
			foreach ($q->result() as $row){
				$b64 = $row->settings;
				$ser = base64_decode($b64);
				if ($ser === FALSE) {
					continue;
				}
				$settings = @unserialize($ser);
				if ( ! is_array($settings)) {
					continue;
				}
				if ( ! isset($settings['toolbar_id'])) {
					continue;
				}
				$toolbar_id = $settings['toolbar_id'];
				$val = isset($toolbar_lookup[$toolbar_id]) ? $toolbar_lookup[$toolbar_id] : $default_val;
				if (isset($settings[$setting_name]) && $settings[$setting_name] === $val) {
					continue; // already set to correct value
				}
				$settings[$setting_name] = $val;
				$ser = serialize($settings);
				$b64 = base64_encode($ser);
				$this->EE->db->where($id_field, $row->id);
				$this->EE->db->update($table, array($settings_field => $b64));
			}
			$q->free_result();
		}
	}
	
	function uninstall() {
		$this->EE->db->select('module_id');
		$this->EE->db->where('module_name', REDACTEE_CLASS);
		$q = $this->EE->db->get('modules');
		$mod_ids = array();
		foreach ($q->result() as $row) {
			$mod_ids[] = $row->module_id;
		}
		$q->free_result();
		
		if (count($mod_ids)) {
			$this->EE->db->where_in('module_id', $mod_ids);
			$this->EE->db->delete('module_member_groups');
			
			$this->EE->db->where_in('module_id', $mod_ids);
			$this->EE->db->delete('modules');
		}
		
		$this->EE->db->where('class', REDACTEE_CLASS);
		$this->EE->db->delete('actions');
		
		$this->EE->load->dbforge();
		$this->EE->dbforge->drop_table('redactee_toolbar_options');
		$this->EE->dbforge->drop_table('redactee_toolbars');
		
		return TRUE;
	}
}
