<?php if (! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Redactee
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Produced and sold in cooperation with Lobster War Machine (http://lobsterwarmachine.com/)
 * @author Jeremy Messenger (http://jlmessenger.com/)
 * @copyright 2012 Jeremy Messenger
 **/

class Redactee_sql {

	var $global_default_settings = array(
			'toolbar_id' => '',
			'image_upload_id' => 'none',
			'file_upload_id' => 'none',
			'image_browse' => 'none',
			'image_browse_subdirs' => '',
			'minHeight' => '',
			'autoresize' => 'Y',
			'site_pages' => 'none',
			'site_listings' => ''
		);
	var $site_id;
	var $lang_path;
	var $theme_url;
	var $base_url;
	var $cp_path;
	var $cache;

	function __construct() {
		$this->EE = &get_instance();
		
		$this->site_id = ctype_digit($this->EE->config->item('site_id')) ? $this->EE->config->item('site_id') : 1;
		$this->base_url = $this->EE->config->item('base_url');
		
		$this->cp_path = $this->EE->config->item('assets_cp_path');
		if ( ! $this->cp_path) {
			$this->cp_path = SYSDIR;
		}
		
		$this->lang_path = PATH_THIRD_THEMES.'redactee/redactor/lang/';
		$third_url_base = $this->EE->config->item('url_third_themes');
		if ($third_url_base == '') {
			$third_url_base = $this->make_path($this->EE->config->item('theme_folder_url'), 'third_party');
		}
		$this->theme_url = $this->make_path($third_url_base, 'redactee/');
		
		if ( ! isset($this->EE->session->cache['redactee'])) {
			$this->EE->session->cache['redactee'] = array();
		}
		$this->cache =& $this->EE->session->cache['redactee'];
	}
	
	//-- system configuration
	function use_structure() {
		if ( ! isset($this->cache['use_structure'])) {
			$this->cache['use_structure'] = FALSE;
			$this->_loaded_modules();
			if (in_array('Structure', $this->cache['loaded_modules'])) {
				if (is_file(PATH_THIRD.'structure/sql.structure.php')) {
					$this->cache['use_structure'] = TRUE;
				}
			}
		}
		return $this->cache['use_structure'];
	}
	
	function use_pages() {
		$this->_loaded_modules();
		return in_array('Pages', $this->cache['loaded_modules']);
	}
	
	function use_assets() {
		$this->_loaded_modules();
		return in_array('Assets', $this->cache['loaded_modules']);
	}
	
	function _loaded_modules() {
		if ( ! isset($this->cache['loaded_modules'])) {
			$this->cache['loaded_modules'] = array();
			$this->EE->db->select('module_name');
			$this->EE->db->where_in('module_name', array('Structure', 'Pages', 'Assets'));
			$q = $this->EE->db->get('modules');
			foreach ($q->result() as $row) {
				$this->cache['loaded_modules'][] = $row->module_name;
			}
			$q->free_result();
		}
	}
	
	//-- redactor options
	// get default values for each config option
	function redactor_defaults_php() {
		$values = array();
		foreach ($this->redactor_option_configs() as $option => $config) {
			$values[$option] = $config['default'];
		}
		return $values;
	}
	
	// convert php values to JSON - uses defaults if not set
	function opts_php_to_json(&$values) {
		foreach ($this->redactor_option_configs() as $option => $config) {
			$value = isset($values[$option]) ? $values[$option] : $config['default'];
			switch ($config['type']) {
				case 'bool':
					$value = $value ? 'true' : 'false';
					break;
				//case 'list':
				//case 'buttons':
				//case 'text':
				//case 'select':
				default:
					$value = $this->json_encode($value);
					break;
			}
			$values[$option] = $value;
		}
	}
	
	// convert JSON values to php - uses defaults if not set
	function opts_json_to_php(&$values) {
		foreach ($this->redactor_option_configs() as $option => $config) {
			if (isset($values[$option])) {
				switch ($config['type']) {
					case 'bool':
						$values[$option] = ($values[$option] === 'true');
						break;
					//case 'list':
					//case 'buttons':
					//case 'text':
					//case 'select':
					default:
						$values[$option] = json_decode($values[$option]);
						break;
				}
			} else {
				$values[$option] = $config['default'];
			}
		}
	}
	
	// full list of config options
	function redactor_option_configs() {
		return array(
			//'toolbar' => array('type'=>'bool', 'default'=>TRUE), // 'object'?
			'toolbarMode' => array('type'=>'select', 'default'=>'toolbar', 'options'=>array('toolbar'=>'Regular Toolbar', 'air'=>'Air Mode Toolbar', 'fixed'=>'Toolbar Fixed to Top', 'box'=>'Toolbar Fixed to Editor', 'off'=>'Disable Toolbar')),
			//'air' => array('type'=>'bool', 'default'=>FALSE),
			//'airButtons' => just copy from buttons if air = TRUE
			'buttons' => array('type'=>'buttons', 'default'=>array(
					'html', '|', 'formatting', '|', 'bold', 'italic', 'deleted', '|',
					'unorderedlist', 'orderedlist', 'outdent', 'indent', '|',
					'image', 'video', 'file', 'table', 'link', '|',
					'fontcolor', 'backcolor', '|', 
					'alignleft', 'aligncenter', 'alignright', 'justify', '|',
					'horizontalrule'
				)),
			'source' => array('type'=>'select', 'default'=>'all', 'options'=>array('all'=>'Everyone', 'admin'=>'Super Admin Only', 'none'=>'Nobody (never show)')),
			'formattingTags' => array('type'=>'list', 'rows'=>7, 'default'=>array(
					'p', 'blockquote', 'pre', 'h1', 'h2', 'h3', 'h4'
				)),
			//'focus' => array('type'=>'bool', 'default'=>FALSE),
			'wym' => array('type'=>'bool', 'default'=>FALSE), // visual structure
			'cleanup' => array('type'=>'bool', 'default'=>TRUE),
			'shortcuts' => array('type'=>'bool', 'default'=>TRUE),
			'lang' => array('type'=>'text', 'default'=>'en'),
			'direction' => array('type'=>'select', 'default'=>'ltr', 'options'=>array('ltr'=>'Left to Right', 'rtl'=>'Right to Left')),
			//'autoresize' => array('type'=>'bool', 'default'=>TRUE),
			//'fixed' => array('type'=>'bool', 'default'=>FALSE),
			'convertLinks' => array('type'=>'bool', 'default'=>TRUE),
			'convertDivs' => array('type'=>'bool', 'default'=>TRUE),
			//'paragraphy' => array('type'=>'bool', 'default'=>TRUE),
			//'autosave' => array('type'=>'bool', 'default'=>FALSE),
			//'interval' => 60,
			//'imageGetJson' => array('type'=>'bool', 'default'=>FALSE), // set to string if enabled image picker '/path/to/php',
			//'imageUpload' => array('type'=>'bool', 'default'=>FALSE), // set to string if enabled image upload '/path/to/php',
			//'fileUpload' => array('type'=>'bool', 'default'=>FALSE), // set to string if enabled image upload '/path/to/php',
			//'uploadFields' => FALSE, // hidden upload files
			'overlay' => array('type'=>'bool', 'default'=>TRUE), // overlay for modal windows
			'observeImages' => array('type'=>'bool', 'default'=>TRUE),
			'protocol' => array('type'=>'select', 'default'=>'http://', 'options'=>array('http://'=>'http', 'https://'=>'https')),
			'mobile' => array('type'=>'bool', 'default'=>TRUE),
			'colors' => array('type'=>'list', 'default'=>array(
					'#ffffff', '#000000', '#eeece1', '#1f497d', '#4f81bd', '#c0504d', '#9bbb59', '#8064a2', '#4bacc6', '#f79646', '#ffff00',
					'#f2f2f2', '#7f7f7f', '#ddd9c3', '#c6d9f0', '#dbe5f1', '#f2dcdb', '#ebf1dd', '#e5e0ec', '#dbeef3', '#fdeada', '#fff2ca',
					'#d8d8d8', '#595959', '#c4bd97', '#8db3e2', '#b8cce4', '#e5b9b7', '#d7e3bc', '#ccc1d9', '#b7dde8', '#fbd5b5', '#ffe694',
					'#bfbfbf', '#3f3f3f', '#938953', '#548dd4', '#95b3d7', '#d99694', '#c3d69b', '#b2a2c7', '#b7dde8', '#fac08f', '#f2c314',
					'#a5a5a5', '#262626', '#494429', '#17365d', '#366092', '#953734', '#76923c', '#5f497a', '#92cddc', '#e36c09', '#c09100',
					'#7f7f7f', '#0c0c0c', '#1d1b10', '#0f243e', '#244061', '#632423', '#4f6128', '#3f3151', '#31859b', '#974806', '#7f6000'
				)),
			'allowedTags' => array('type'=>'list', 'default'=>array(
					"code", "span", "div", "label", "a", "br", "p", "b", "i", "del", "strike",
					"img", "video", "audio", "iframe", "object", "embed", "param", "blockquote",
					"mark", "cite", "small", "ul", "ol", "li", "hr", "dl", "dt", "dd", "sup", "sub",
					"big", "pre", "code", "figure", "figcaption", "strong", "em", "table", "tr", "td",
					"th", "tbody", "thead", "tfoot", "h1", "h2", "h3", "h4", "h5", "h6"
				)),
		);
	}
	
	//-- add toolbar to a page
	function _add_redactor_head($toolbar_id, $field_ref, $field_settings, $head_callback=FALSE) {
		if ($head_callback === FALSE) {
			$head_callback = array($this->EE->cp, 'add_to_head');
		}
		if ( ! isset($this->cache['redactor_head'])) {
			call_user_func($head_callback, "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$this->theme_url}redactor/redactor.css\" />");
			call_user_func($head_callback, "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$this->theme_url}redactee.css\" />");
			$global = $this->get_global();
			if ($global['custom_stylesheet'] !== '') {
				call_user_func($head_callback, "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$global['custom_stylesheet']}\" />");
			}
			call_user_func($head_callback, "<script src=\"{$this->theme_url}redactor/redactor.js\"></script>");
			call_user_func($head_callback, "<script src=\"{$this->theme_url}redactee.js\"></script>");
			
			$this->cache['redactor_head'] = array();
			$this->cache['redactor_lang'] = array();
		}
		if ( ! isset($this->cache['redactor_head'][$toolbar_id]) ) {
			$settings = $this->get_toolbar($toolbar_id);
			
			if (isset($settings['options']['lang']) && $settings['options']['lang'] !== '"en"') {
				$lang = json_decode($settings['options']['lang']);
				if ( ! isset($this->cache['redactor_lang'][$lang])) {
					$lang_file = $this->get_lang_file($lang);
					if ($lang_file) {
						call_user_func($head_callback, "<script src=\"{$this->theme_url}redactor/lang/{$lang_file}\"></script>");
					}
					$this->cache['redactor_lang'][$lang] = TRUE;
				}
			}
			
			$js = $this->_add_redactor_config($settings, $field_ref, $field_settings);
			call_user_func($head_callback, "<script type=\"text/javascript\">\nredactee_toolbars.c{$toolbar_id} = {\n{$js}\n};\n</script>");
			$this->cache['redactor_head'][$toolbar_id] = TRUE;
		}
	}
	
	function _add_redactor_config($settings, $field_ref, $field_settings) {
		$configs = $this->redactor_option_configs();
		
		$global = $this->get_global();
		
		$data = array();
		foreach ($configs as $option => $config) {
			if (isset($settings['options'][$option])) {
				$v = $settings['options'][$option];
				if ($option === 'toolbarMode') {
					switch ($v) {
						case '"off"':
							$data[] = 'toolbar: false';
							break;
						case '"air"':
							$data[] = 'air: true';
							$data[] = 'airButtons: '.$settings['options']['buttons'];
							break;
						case '"fixed"':
							$data[] = 'fixed: true';
							break;
						case '"box"':
							$data[] = 'fixed: true';
							$data[] = 'fixedBox: true';
						// case 'toolbar' - nothing to do
					}
					continue;
				} elseif ($option === 'source') {
					switch ($v) {
						case 'true':
						case '"all"':
							$data[] = 'source: true';
							break;
						case '"admin"':
							$group_id = $this->EE->session->userdata('group_id');
							if ($group_id == 1) {
								$data[] = 'source: true';
							} else {
								$data[] = 'source: false';
							}
							break;
						default:
						//case 'false':
						//case '"none"':
							$data[] = 'source: false';
							break;
						
					}
					continue;
				}
				$data[] = $option.': '.$v;
			}
		}
		
		$field_settings = array_merge($this->global_default_settings, $field_settings);
		if ($field_settings['minHeight'] !== '' && ctype_digit($field_settings['minHeight'])) {
			$data[] = 'minHeight: '.$field_settings['minHeight'];
		}
		if ($field_settings['autoresize'] !== 'Y') {
			$data[] = 'autoresize: false';
		}
		
		$add_aws = FALSE;
		if ($field_settings['image_upload_id'] !== 'none') {
			if ($field_settings['image_upload_id'] === 'aws') {
				if (isset($global['aws_enable']) && $global['aws_enable'] === 'Y' &&
						isset($global['aws_bucket']) && $global['aws_bucket'] !== '') {
					$data[] = 'imageUpload: '.$this->json_encode("https://{$global['aws_bucket']}.s3.amazonaws.com/");
					$add_aws = TRUE;
				}
			} else {
				$v = $this->get_action_url_json('imageUpload', $field_ref);
				$data[] = 'imageUpload: '.$v;
			}
		}
		if ($field_settings['file_upload_id'] !== 'none') {
			if ($field_settings['file_upload_id'] === 'aws') {
				if (isset($global['aws_enable']) && $global['aws_enable'] === 'Y' &&
						isset($global['aws_bucket']) && $global['aws_bucket'] !== '') {
					$data[] = 'fileUpload: '.$this->json_encode("https://{$global['aws_bucket']}.s3.amazonaws.com/");
					$add_aws = TRUE;
				}
			} else {
				$v = $this->get_action_url_json('fileUpload', $field_ref);
				$data[] = 'fileUpload: '.$v;
			}
		}
		if ($field_settings['image_browse'] !== 'none') {
			$v = $this->get_action_url_json('imageGetJson', $field_ref);
			$data[] = 'imageGetJson: '.$v;
		}
		
		if ($field_settings['site_pages'] !== 'none') {
			$v = $this->get_action_url_json('pagesGetJson', $field_ref);
			$data[] = 'pagesGetJson: '.$v;
		}
		
		if ($add_aws) {
			$aws_opts = array_intersect_key(
					$global,
					array_fill_keys(array('aws_key', 'aws_AWSAccessKeyId', 'aws_acl', 'aws_policy', 'aws_signature', 'aws_Content-Type'), '')
				);
			
			$aws_data = array();
			foreach ($aws_opts as $k => $v) {
				$k = substr($k, 4); // trim 'aws_' prefix
				$aws_data[] = "\"$k\": ".$this->json_encode($v);
			}
			$aws_data[] = '"success_action_redirect": '.$this->get_action_url_json('imageUpload', $field_ref);
			$data[] = 'uploadCrossDomain: true';
			$data[] = 'uploadFields: {'.implode(", ", $aws_data).'}';
		}
		
		return implode(",\n", $data);
	}
	
	//-- misc helper methods
	// used to order by string length
	function _cmp_long_to_short($a, $b) {
		$la = strlen($a);
		$lb = strlen($b);
		return $la === $lb ? 0 : ($la > $lb ? -1 : 1);
	}
	
	function _trimslash(&$p, $i, $l) {
		if ($i === 0) {
			$p = rtrim($p, '/');
		} elseif ($i === ($l-1)) {
			$p = ltrim($p, '/');
		} else {
			$p = trim($p, '/');
		}
	}
	
	function make_path() {
		$path_list = func_get_args();
		array_walk($path_list, array($this, '_trimslash'), count($path_list));
		return implode('/', $path_list);
	}
	
	function is_rel_path($path) {
		return ! preg_match('/^(\/|\\\|[a-zA-Z]+:)/', $path);
	}
	
	function json_encode($str) {
		return preg_replace('/(?<!\\\\)\\\\\\//', '/', json_encode($str));
	}
	
	//-- tag data conversion
	function save_assets(&$body) {
		if ( ! $this->use_assets()) {
			return;
		}
		preg_match_all('/{filedir_(\d+)}([^\'"\\/]+\\/[^\'"]+\\.(?:tiff?|gif|png|jpe?g|ico|bmp|psd))/i', $body, $matches);
		if (count($matches[0])) {
			
			$assets = array();
			
			$prefs = $this->upload_prefs();
			
			foreach ($matches[0] as $i => $orig_tag) {
				$loc_id = $matches[1][$i];
				$asset = $matches[2][$i];
				
				if ( ! isset($prefs[$loc_id])) {
					continue; // invalid filedir
				}
				if (strpos($asset, '..') !== FALSE) {
					continue; // can't have relative paths
				}
				if (in_array($orig_tag, $assets, TRUE)) {
					continue; // already matched - save time
				}
				
				$path = $this->make_path($prefs[$loc_id]->server_path, $asset);
				if ( ! is_file($path)) {
					continue; // invalid file
				}
				
				$asset_id = FALSE;
				$this->EE->db->select('asset_id');
				$this->EE->db->where('file_path', $orig_tag);
				$q = $this->EE->db->get('assets');
				if ($q->num_rows() > 0) {
					$asset_id = $q->row()->asset_id;
				}
				$q->free_result();
				if ( ! $asset_id) {
					$ok = $this->EE->db->insert('assets', array('file_path' => $orig_tag));
					if ($ok) {
						$asset_id = $this->EE->db->insert_id();
					}
				}
				if ($asset_id) {
					$assets[$asset_id] = $orig_tag;
				}
			}
			
			if (count($assets)) {
				uasort($assets, array($this, '_cmp_long_to_short'));
				
				$from = array();
				$to = array();
				foreach($assets as $asset_id => $orig) {
					$from[] = $orig;
					$to[] = LD.'asset_'.$asset_id.' '.base64_encode($orig).RD;
				}
				$body = str_replace($from, $to, $body);
			}
		}
	}
	
	function get_assets(&$body) {
		$has_assets = $this->use_assets();
		
		preg_match_all('/{asset_(\d+) ([a-zA-Z0-9+\\/]+={0,2})}/', $body, $matches);
		if (count($matches[0])) {
			
			$asset_paths = array();
			if ($has_assets) {
				$asset_ids = array_unique($matches[1]);
				$this->EE->db->select('asset_id, file_path');
				$this->EE->db->where_in('asset_id', $asset_ids);
				$q = $this->EE->db->get('assets');
				foreach ($q->result() as $row) {
					$asset_paths["{$row->asset_id}"] = $row->file_path;
				}
			}
			
			$orig_tags = array();
			foreach ($matches[0] as $i => $orig_tag) {
				$asset_id = $matches[1][$i];
				$raw = $matches[2][$i];
				
				if ( ! isset($asset_paths[$asset_id])) {
					$asset_paths[$asset_id] = base64_decode($raw);
				}
				
				$orig_tags[$asset_id] = $orig_tag;
			}
			
			if (count($orig_tags)) {
				uasort($orig_tags, array($this, '_cmp_long_to_short'));
				
				$from = array();
				$to = array();
				foreach ($orig_tags as $asset_id => $orig_tag) {
					$from[] = $orig_tag;
					$to[] = isset($asset_paths[$asset_id]) ? $asset_paths[$asset_id] : '';
				}
				$body = str_replace($from, $to, $body);
			}
		}
	}
	
	function _tag_replacements($fix_trailing = FALSE) {
		$site_pages = $this->_site_pages();
		$upload_urls = $this->_upload_locations();
		
		uasort($site_pages, array($this, '_cmp_long_to_short'));
		uasort($upload_urls, array($this, '_cmp_long_to_short'));
		
		$tags = array();
		$urls = array();
		
		if ($fix_trailing) { // ex: try to replace "/about/" before trying "/about"
			foreach ($site_pages as $entry_id => $url) {
				if ($url !== '/') {
					if ($url[strlen($url)-1] === '/') {
						$url = substr($url, 0, -1);
					}
					$tags[] = LD.'page_'.$entry_id.RD;
					$urls[] = $url.'/';
					$tags[] = LD.'page_'.$entry_id.RD;
					$urls[] = $url;
				}
			}
		} else {
			foreach ($site_pages as $entry_id => $url) {
				if ($url !== '/') {
					$tags[] = LD.'page_'.$entry_id.RD;
					$urls[] = $url;
				}
			}
		}
		foreach ($upload_urls as $loc_id => $url) {
			$tags[] = LD.'filedir_'.$loc_id.RD;
			$urls[] = $url;
		}
		
		return array($tags, $urls);
	}
	
	function tag_to_url(&$data) {
		$r = $this->_tag_replacements();
		$data = str_replace($r[0], $r[1], $data);
	}
	
	function url_to_tag(&$data) {
		$r = $this->_tag_replacements(TRUE);
		$data = str_replace($r[1], $r[0], $data);
	}
	
	function _structure_pages($branch_entry_id=0, $status=FALSE) {
		if ( ! class_exists('Sql_structure', FALSE)) {
			require_once(PATH_THIRD.'structure/sql.structure.php');
		}
		$fix_tmpl = FALSE;
		if (property_exists($this->EE, 'TMPL')) {
			$fix_tmpl = $this->EE->TMPL;
		}
		$this->EE->TMPL = new Redactee_Fake_Tmpl();
		$s_sql = new Sql_structure();
		if ($status === FALSE) {
			$global = $this->get_global();
			$status = $global['site_page_entry_status'];
		}
		$current_id = FALSE; $mode = ''; $show_depth = -1; $max_depth = -1; $include = array(); $exclude = array(); $show_overview = 'no'; $rename_overview = 'title'; $show_expired = 'no'; $show_future = 'no';
		if ($branch_entry_id > 0) {
			$show_overview = 'yes';
		}
		$data = $s_sql->get_selective_data($this->site_id, $current_id, $branch_entry_id, $mode, $show_depth, $max_depth, $status, $include, $exclude, $show_overview, $rename_overview, $show_expired, $show_future);
		if ($fix_tmpl !== FALSE) {
			$this->EE->TMPL = $fix_tmpl;
		}
		return $data;
	}
	
	// list of upload locations
	function _upload_locations() {
		$prefs = $this->upload_prefs();
		$urls = array();
		foreach ($prefs as $loc_id => $pref) {
			$urls[$loc_id] = $pref->url;
		}
		return $urls;
	}
	
	// list of site pages
	function _site_pages() {
		if ( ! isset($this->cache['site_pages'])) {
			$page_data = $this->EE->config->item('site_pages');
			if (isset($page_data[$this->site_id]) && isset($page_data[$this->site_id]['uris'])) {
				$base = $page_data[$this->site_id]['url'];
				$prefs = $page_data[$this->site_id]['uris'];
				foreach ($prefs as $entry_id => $url) {
					if ($url === '/') {
						continue;
					}
					$prefs[$entry_id] = $this->make_path($base, $url);
				}
			} else {
				$prefs = array();
			}
			$this->cache['site_pages'] = $prefs;
		}
		return $this->cache['site_pages'];
	}
	
	//-- file management
	// get all upload preferences (fixed for CP vs non CP paths)
	function upload_prefs() {
		$cachekey = 'upload_prefs';
		if ( ! isset($this->cache[$cachekey])) {
			$in_cp = REQ === 'CP';
			$prefs = array();
			$this->EE->db->select('id, name, url, server_path');
			$this->EE->db->where('site_id', $this->site_id);
			$this->EE->db->order_by('name');
			$q = $this->EE->db->get('upload_prefs');
			foreach ($q->result() as $row) {
				if (strpos($row->url, ':') === FALSE) {
					$row->url = $this->make_path($this->base_url, $row->url);
				}
				if ( ! $in_cp && $this->is_rel_path($row->server_path)) {
					$row->server_path = $this->make_path($this->cp_path, $row->server_path);
				}
				$prefs[$row->id] = $row;
			}
			$q->free_result();
			$this->cache[$cachekey] = $prefs;
		}
		return $this->cache[$cachekey];
	}
	
	function upload_titles() {
		$prefs = $this->upload_prefs();
		$titles = array();
		foreach ($prefs as $loc_id => $pref) {
			$titles[$loc_id] = $pref->name;
		}
		return $titles;
	}
	
	function subdir_titles($loc_id) {
		$cachekey = 'subdirs_'.$loc_id;
		if ( ! isset($this->cache[$cachekey])) {
			$prefs = $this->upload_prefs();
			if ( ! isset($prefs[$loc_id])) {
				return array(); // invalid location id
			}
			$pref = $prefs[$loc_id];
			if ( ! is_dir($pref->server_path)) {
				return array(); // upload server path invalid
			}
			$this->cache[$cachekey] = $this->_dirs($pref->server_path, $pref->name, $loc_id);
		}
		return $this->cache[$cachekey];
	}
	
	function _dirs($path, $title, $loc_id) {
		if (substr($path, -1) !== '/') {
			$path .= '/';
		}
		
		$subdirs = array();
		$d = dir($path);
		while (false !== ($entry = $d->read())) {
			if ($entry[0] === '.' || $entry === '_thumbs') {
				continue;
			}
			$p = $path.$entry;
			if (is_dir($p)) {
				$t = $title.'/'.$entry;
				$l = $loc_id.'/'.$entry;
				$subdirs[$l] = $t;
				$subdirs += $this->_dirs($p.'/', $t, $l);
			}
		}
		$d->close();
		return $subdirs;
	}
	
	function upload_no_access($group_id, $loc_id = FALSE) {
		$this->EE->db->select('upload_id');
		$this->EE->db->where('member_group', $group_id);
		if ($loc_id) {
			$this->EE->db->where('upload_id', $loc_id);
		}
		$q = $this->EE->db->get('upload_no_access');
		$no_access = array();
		foreach ($q->result() as $row) {
			$no_access[] = $row->upload_id;
		}
		$q->free_result();
		return $no_access;
	}
	
	function upload_file($loc_id, $field, $image_only=FALSE) {
		if ( ! isset($this->cache['loaded_filemanager'])) {
			$this->EE->load->library('Filemanager');
			$this->cache['loaded_filemanager'] = TRUE;
		}
		$wd = getcwd();
		chdir($this->cp_path);
		$info = $this->EE->filemanager->upload_file($loc_id, $field, $image_only);
		chdir($wd);
		return $info;
	}
	
	//-- settings and toolbars
	// get fieldsettings based on field_id
	// special field_id formats include:
	//		low:[variable_id]		- for low variables
	//		[field_id]:[matrix_col_id] - for matrix variables
	//		field_id				- for channel fields
	function get_field_settings($field_id) {
		if (strncmp($field_id, 'low:', 4) === 0) {
			// look up low variable field
			$field_id = substr($field_id, 4);
			$this->EE->db->select('variable_settings AS settings');
			$this->EE->db->where('variable_id', $field_id);
			$this->EE->db->where('variable_type', 'redactee');
			$q = $this->EE->db->get('low_variables');
		} elseif (strpos($field_id, ':') !== FALSE) {
			// look up matrix data
			list($field_id, $col_id) = explode(':', $field_id, 2);
			$this->EE->db->select('col_settings AS settings');
			$this->EE->db->where('field_id', $field_id);
			$this->EE->db->where('col_id', $col_id);
			$this->EE->db->where('col_type', 'redactee');
			$q = $this->EE->db->get('matrix_cols');
		} else {
			// look up channel data
			$this->EE->db->select('field_settings AS settings');
			$this->EE->db->where('field_id', $field_id);
			$this->EE->db->where('field_type', 'redactee');
			$q = $this->EE->db->get('channel_fields');
		}
		if ($q->num_rows() === 0) {
			return NULL; // no settings
		}
		$b64 = $q->row()->settings;
		$q->free_result();
		$ser = base64_decode($b64);
		if ($ser === FALSE) {
			return array(); // no setting
		}
		$settings = @unserialize($ser);
		if ( ! is_array($settings)) {
			return array();
		}
		return $settings;
	}
	
	function get_lang_options() {
		$cache_key = 'lang_options';
		if ( ! isset($this->cache[$cache_key])) {
			$langs = array();
			
			if (is_dir($this->lang_path)) {
				$d = dir($this->lang_path);
				while (false !== ($entry = $d->read())) {
					if ($entry[0] === '.') {
						continue;
					}
					$c = preg_match('/^lang\.([\w_]+)\.([^\.]+)\.js$/', $entry, $m);
					if ($c) {
						$langs[$m[1]] = ucwords(strtr($m[2], '_', ' '));
					}
				}
				$d->close();
			}
			asort($langs);
			$langs = array('en' => 'English') + $langs;
			$this->cache[$cache_key] = $langs;
		}
		return $this->cache[$cache_key];
	}
	
	function get_lang_file($lang) {
		if (is_dir($this->lang_path)) {
			$lang_file = glob($this->lang_path.'lang.'.$lang.'.*.js');
			if (is_array($lang_file) && count($lang_file)) {
				return substr($lang_file[0], strlen($this->lang_path));
			}
		}
		return FALSE;
	}
	
	function get_global() {
		$cache_key = 'global_options';
		if ( ! isset($this->cache[$cache_key])) {
			$settings = array(
					'site_page_entry_status' => 'open',
					'custom_stylesheet' => '',
					'aws_enable' => 'N',
					'aws_bucket' => '',
					'aws_key' => '${filename}',
					'aws_AWSAccessKeyId' => '',
					'aws_acl' => 'public-read',
					'aws_policy' => '',
					'aws_signature' => '',
					'aws_Content-Type' => ''
				);
			
			$this->EE->db->select('option, value');
			$this->EE->db->where('toolbar_id', 0);
			$q = $this->EE->db->get('redactee_toolbar_options');
			foreach ($q->result() as $row) {
				$newvar = 'aws_'.$row->option;
				if (array_key_exists($newvar, $settings)) {
					// append new 'aws_' prefix to oldvars
					$settings[$newvar] = $row->value;
				} else {
					$settings[$row->option] = $row->value;
				}
			}
			$q->free_result();
			
			$this->cache[$cache_key] = $settings;
		}
		return $this->cache[$cache_key];
	}
	
	function save_global($settings) {
		$this->EE->db->where('toolbar_id', 0);
		$this->EE->db->delete('redactee_toolbar_options');
		
		$data = array();
		foreach ($settings as $option => $value) {
			$data[] = array(
				'toolbar_id' => 0,
				'option' => $option,
				'value' => $value
			);
		}
		$this->EE->db->insert_batch('redactee_toolbar_options', $data);
	}
	
	// save settings to DB (config options should be PHP)
	function save_toolbar($settings, $toolbar_id = FALSE) {
		if ( ! $toolbar_id) {
			$this->EE->db->insert('redactee_toolbars', array(
				'title' => $settings['title']
			));
			$toolbar_id = $this->EE->db->insert_id();
		} else {
			$this->EE->db->where('toolbar_id', $toolbar_id);
			$this->EE->db->update('redactee_toolbars', array(
				'title' => $settings['title']
			));
			$this->EE->db->where('toolbar_id', $toolbar_id);
			$this->EE->db->delete('redactee_toolbar_options');
		}
		$options = $settings['options'];
		$this->opts_php_to_json($options);
		
		$data = array();
		foreach ($options as $option => $value) {
			$data[] = array(
				'toolbar_id' => $toolbar_id,
				'option' => $option,
				'value' => $value
			);
		}
		$this->EE->db->insert_batch('redactee_toolbar_options', $data);
		return $toolbar_id;
	}
	
	function delete_toolbar($toolbar_id) {
		$this->EE->db->where('toolbar_id', $toolbar_id);
		$this->EE->db->delete('redactee_toolbar_options');
		
		$this->EE->db->where('toolbar_id', $toolbar_id);
		$this->EE->db->delete('redactee_toolbars');
	}
	
	// get list of all toolbar titles
	function toolbar_titles() {
		$cachekey = 'toolbar_titles';
		if ( ! isset($this->cache[$cachekey])) {
			$this->EE->db->order_by('title');
			$q = $this->EE->db->get('redactee_toolbars');
			$titles = array();
			foreach ($q->result() as $row){
				$titles[$row->toolbar_id] = $row->title;
			}
			$q->free_result();
			$this->cache[$cachekey] = $titles;
		}
		return $this->cache[$cachekey];
	}
	
	// get toolbar (with pageload cache)
	function get_toolbar($toolbar_id) {
		$cache_key = 'toolbar_options_'.$toolbar_id;
		if ( ! isset($this->cache[$cache_key])) {
			return $this->_load_toolbar($toolbar_id);
		}
		return $this->cache[$cache_key];
	}
	
	// get toolbar (always from db)
	function _load_toolbar($toolbar_id) {
		$this->EE->db->select('title');
		$this->EE->db->where('toolbar_id', $toolbar_id);
		$q = $this->EE->db->get('redactee_toolbars');
		if ($q->num_rows() === 0) {
			return FALSE;
		}
		$settings = array(
			'title' => $q->row()->title,
			'options' => array()
		);
		$q->free_result();
		
		$this->EE->db->select('option, value');
		$this->EE->db->where('toolbar_id', $toolbar_id);
		$q = $this->EE->db->get('redactee_toolbar_options');
		foreach ($q->result() as $row){
			$settings['options'][$row->option] = $row->value;
		}
		$q->free_result();
		
		$this->cache['toolbar_options_'.$toolbar_id] = $settings;
		return $settings;
	}
	
	// get full url to action page
	function get_action_url_json($method, $field) {
		$this->EE->db->select('action_id');
		$this->EE->db->where('class', REDACTEE_CLASS);
		$this->EE->db->where('method', $method);
		$q = $this->EE->db->get('actions');
		if ($q->num_rows() === 0) {
			return 'false';
		}
		$act = $q->row()->action_id;
		$q->free_result();
		
		$url = $this->EE->functions->fetch_site_index();
		if (strpos($url, '?') === FALSE) {
			$v = $url.'?ACT='.$act.'&field='.urlencode($field);
		} else {
			$v = $url.'&ACT='.$act.'&field='.urlencode($field);
		}
		return $this->json_encode($v);
	}
	
	function setup_db() {
		$this->EE->load->dbforge();
		$this->EE->dbforge->add_field(array(
				'toolbar_id'	=> array('type' => 'int', 'constraint' => '10', 'unsigned' => TRUE, 'null' => FALSE, 'auto_increment' => TRUE),
				'title'			=> array('type' => 'varchar', 'constraint' => '60', 'null' => FALSE),
		));
		$this->EE->dbforge->add_key('toolbar_id', TRUE);
		$this->EE->dbforge->create_table('redactee_toolbars', TRUE);
		
		$this->EE->dbforge->add_field(array(
				'toolbar_id'	=> array('type' => 'int', 'constraint' => '10', 'unsigned' => TRUE, 'null' => FALSE),
				'option'		=> array('type' => 'varchar', 'constraint' => '60', 'null' => FALSE),
				'value'			=> array('type' => 'text', 'null' => FALSE)
		));
		$this->EE->dbforge->add_key('toolbar_id', TRUE);
		$this->EE->dbforge->add_key('option', TRUE);
		$this->EE->dbforge->create_table('redactee_toolbar_options', TRUE);
		
		$this->EE->db->select_min('toolbar_id', 'toolbar_id');
		$q = $this->EE->db->get('redactee_toolbars');
		$default_toolbar_id = $q->row()->toolbar_id;
		$q->free_result();
		if ( ! $default_toolbar_id) {
			$default_toolbar_id = $this->save_toolbar(array(
				'title' => 'Advanced Toolbar',
				'options' => array()
			));
			$this->save_toolbar(array(
				'title' => 'Simple Toolbar',
				'options' => array('source' => FALSE, 'buttons' => array('bold', 'italic', 'deleted', '|', 'unorderedlist', 'orderedlist', '|', 'fontcolor', 'link'))
			));
		}
		
		return $default_toolbar_id;
	}
}
class Redactee_Fake_Tmpl {
	var $cache_timestamp = '';
	function fetch_param($key, $default) {
		return $default;
	}
}
