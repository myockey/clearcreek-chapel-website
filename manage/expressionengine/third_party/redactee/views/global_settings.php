<style type="text/css">
	.redactee-settings-tab { padding: 5px 9px; background: #D0D9E1; color: #5F6C74; border: none; border-radius: 3px; font-size: 12px; line-height: 12px; cursor: pointer; }
	.redactee-settings-tab:hover { background: #C0C9D1; }
	.redactee-active, .redactee-settings-tab.redactee-active:hover { background: #E11842; color: #fff; }
	.redactee-settings-new { margin: 2px 10px; }
	table.redactee-settings { border-collapse: collapse; width: 100%; }
	table.redactee-settings td, table.redactee-settings th { border: 1px solid #D0D7DF; }
	table.redactee-settings th { padding: 4px 10px; text-align: right; }
	table.redactee-settings td { padding: 0; }
	table.redactee-settings td input[type=text], table.redactee-settings td textarea { border-radius: 0; margin: 0; }
	table.redactee-settings td input[type=checkbox], table.redactee-settings td select { margin-left: 3px; }
	table.redactee-settings .row-color-1 { }
	table.redactee-settings .row-color-0 { background: #F4F6F6; }
	table.redactee-settings .row-spacer td, table.redactee-settings .row-spacer th { border: none; background: transparent; }

	body .redactor_toolbar li a.redactee_btn_separator { background-position: 25px; width: 10px; text-align: center; }
	.redactee_btn_separator hr { height: 23px; width: 1px; }
	.redactor_toolbar li.redactee_toolbar_label {
		color: #5F6C74;
		width: 110px;
		height: 25px;
		line-height: 30px;
		font-size: 12px;
		font-weight: bold;
		text-align: right;
		padding-right: 6px;
	}
	body .redactor_toolbar.ui-sortable-helper { overflow: visible !important; border: none; background: transparent; }
	.redactor_toolbar.ui-sortable-helper li a { border-color: #98a6ba; background-color: #dde4ef; }
</style>
<script type="text/javascript">
$(function(){
	var new_count = 1;
	var $toolbar_new = $('#toolbar-new').clone();
	$('#toolbar-new').empty();

	$('#toolbars-menu').delegate('.redactor_toolbar a', 'click', function(){ return false; });

	var button_copy = function(e, $item){
		var $helper = $('<ul class="redactor_toolbar"></ul>').append($item.clone());
		return $helper; 
	};

	var button_sortable = 'li:not(.redactee_toolbar_label,.redactor_separator)';
	var setup_sortable = function($available) {
		var $blank_sep = $available.children('li[data-button="|"]').first().clone();
		var htmlid = $available.attr('id');
		var toolbar_id = htmlid.substr(18);
		var $toolbar = $('#toolbar-current-'+toolbar_id);
		var $input = $('#buttons-'+toolbar_id);
		var options = {
			connectWith: '.toolbar-connect-'+toolbar_id,
			items: button_sortable,
			appendTo: 'body',
			helper: button_copy,
			update: function(e, ui) {
				var items = [];
				$toolbar.children(button_sortable).each(function(){
					items.push($(this).attr('data-button'));
				});
				$input.val(items.join(' '));

				$available.children('li[data-button="|"]').remove();
				$available.append($blank_sep.clone());
			}
		};
		$available.sortable(options).disableSelection();
		$toolbar.sortable(options).disableSelection();
	};

	$('[id^="toolbar-available-"]').each(function(){
		setup_sortable($(this));
	});

	$('#toolbars-menu').delegate('.redactee-settings-tab', 'click', function(){
		$('.redactee-settings').hide();
		$('.redactee-active').removeClass('redactee-active');
		$(this).addClass('redactee-active');
		$($(this).attr('href')).show();
		return false;
	});
	$('.redactee-settings-new').click(function(){
		$('.redactee-settings').hide();
		var $config = $toolbar_new.clone();
		var htmlid = 'toolbar-new-'+new_count;
		$config.attr('id', htmlid);

		$('input,select,textarea', $config).each(function(){
			var name = $(this).attr('name');
			$(this).attr('name', 'tb[new]['+new_count+']'+name.substr(7));
			var id = $(this).attr('id');
			var new_id = id+'-'+new_count;
			$(this).attr('id', new_id);
			$('[for="'+id+'"]', $config).attr('for', new_id);
		});
		var $available_buttons = $('#toolbar-available-new', $config);
		$('#toolbar-available-new,#toolbar-current-new', $config).each(function(){
			var id = $(this).attr('id');
			$(this).attr('id', id+'-'+new_count).removeClass('toolbar-connect-new').addClass('toolbar-connect-new-'+new_count);
		});

		var $title = $('#title-new-'+new_count, $config);
		var new_title = $title.val()+' '+new_count;
		$title.val(new_title);
		var $button = $('<button class="redactee-settings-tab" href="#'+htmlid+'"></button>').text(new_title);
		$(this).before($button);
		$('#delete-new-'+new_count, $config).click(function() {
			$button.remove();
			$config.remove();
			$('#toolbars-menu').children('button').last().click();
		});

		$('#toolbar-new').after($config);
		$button.click();
		setup_sortable($available_buttons);
		new_count++;
		return false;
	});
});
</script>
<div id="toolbars-menu">
<button class="redactee-settings-tab" href="#global-options">Global Settings</button>
<?php
$shown = FALSE;
foreach ($toolbar_titles as $toolbar_id => $title):
if ($shown) {
	$style = '';
} else {
	$shown = TRUE;
	$style = ' redactee-active';
}
?>
<button class="redactee-settings-tab<?php echo $style; ?>" href="#toolbar-<?php echo $toolbar_id; ?>"><?php echo $title; ?></button>
<?php endforeach; ?>
<a class="redactee-settings-new" href="#add-toolbar">New Toolbar Configuration</a><br><br>
</div>

<table class="redactee-settings" style="display: none;" id="global-options">
	<tr class="row-color-1">
		<th width="200"><label><?php echo lang('custom_stylesheet'); ?></label>
			<br><em><?php echo lang('help_custom_stylesheet'); ?></em></th>
		<td><?php echo form_input('global[custom_stylesheet]', $global['custom_stylesheet']); ?></td>
	</tr>
	<tr class="row-spacer">
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr class="row-color-1">
		<th width="200"><label><?php echo lang('site_page_entry_status'); ?></label>
			<br><em><?php echo lang('help_site_page_entry_status'); ?></em></th>
		<td><?php echo form_input('global[site_page_entry_status]', $global['site_page_entry_status']); ?></td>
	</tr>
	<tr class="row-spacer">
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr class="row-color-1">
		<th><label><?php echo lang('aws_enable'); ?></label></th>
		<td><?php echo form_checkbox('global[aws_enable]', 'Y', $global['aws_enable'] === 'Y'); ?></td>
	</tr>
	<tr class="row-color-0">
		<th><label><?php echo lang('aws_bucket'); ?></label></th>
		<td><?php echo form_input('global[aws_bucket]', $global['aws_bucket']); ?></td>
	</tr>
	<tr class="row-color-1">
		<th><label><?php echo lang('aws_key'); ?></label></th>
		<td><?php echo form_input('global[aws_key]', $global['aws_key']); ?></td>
	</tr>
	<tr class="row-color-0">
		<th><label><?php echo lang('aws_access_key'); ?></label></th>
		<td><?php echo form_input('global[aws_AWSAccessKeyId]', $global['aws_AWSAccessKeyId']); ?></td>
	</tr>
	<tr class="row-color-1">
		<th><label><?php echo lang('aws_acl'); ?></label></th>
		<td><?php echo form_input('global[aws_acl]', $global['aws_acl']); ?></td>
	</tr>
	<tr class="row-color-0">
		<th><label><?php echo lang('aws_policy'); ?></label></th>
		<td><?php echo form_input('global[aws_policy]', $global['aws_policy']); ?></td>
	</tr>
	<tr class="row-color-1">
		<th><label><?php echo lang('aws_signature'); ?></label></th>
		<td><?php echo form_input('global[aws_signature]', $global['aws_signature']); ?></td>
	</tr>
	<tr class="row-color-0">
		<th><label><?php echo lang('aws_content_type'); ?></label></th>
		<td><?php echo form_input('global[aws_Content-Type]', $global['aws_Content-Type']); ?></td>
	</tr>
</table>

<?php
$shown = FALSE;
foreach ($toolbar_settings as $toolbar_id => $settings):
$count = 0;
if ($shown) {
	$style = ' style="display: none;"';
} else {
	$shown = TRUE;
	$style = '';
}
?>
<table class="redactee-settings"<?php echo $style; ?> id="toolbar-<?php echo $toolbar_id; ?>">
	<tr class="row-color-1">
		<th width="200"><label><?php echo lang('toolbar_delete'); ?></label></th>
		<td><?php echo form_checkbox("tb[{$toolbar_id}][delete]", 'Y', FALSE, "id=\"delete-{$toolbar_id}\""); ?></td>
	</tr>
	<tr class="row-color-0">
		<th><label for="title-<?php echo $toolbar_id; ?>"><?php echo lang('toolbar_title'); ?></label></th>
		<td><?php echo form_input("tb[{$toolbar_id}][title]", $settings['title'], "id=\"title-{$toolbar_id}\""); ?></td>
	</tr>
<?php foreach ($option_configs as $option => $config):
	$count++;
	$value = $settings['options'][$option];
	$htmlid = $option.'-'.$toolbar_id;
	$params = array(
			'id' => $htmlid,
			'name' => 'tb['.$toolbar_id.'][options]['.$option.']',
		);
	$label = lang('opt_'.$option);
	$hk = 'help_'.$option;
	$help = lang($hk);
	if ($help === $hk) {
		$help = FALSE;
	}
?>
	<tr class="row-color-<?php echo ($count % 2); ?>">
		<th><label for="<?php echo $htmlid; ?>"><?php echo $label; ?></label><?php if ($help):?>
			<br><em><?php echo $help; ?></em>
		<?php endif; ?></th>
		<td><?php
	switch($config['type']):
	case 'buttons':
		echo '<ul class="redactor_toolbar toolbar-connect-'.$toolbar_id.'" id="toolbar-available-'.$toolbar_id.'">';
		echo '<li class="redactee_toolbar_label">'.lang('opt_buttons_available').'</li><li class="redactor_separator"></li>';
		foreach ($all_buttons as $button) {
			if ($button === '|') {
				echo '<li data-button="|"><a class="redactee_btn_separator" href="#"><hr></a></li>';
			} elseif ( ! in_array($button, $value)) {
				echo '<li data-button="'.$button.'"><a class="redactor_btn_'.$button.'" href="#">'.$button.'</a></li>';
			}
		}
		echo '</ul>';
		echo '<ul class="redactor_toolbar toolbar-connect-'.$toolbar_id.'" id="toolbar-current-'.$toolbar_id.'">';
		echo '<li class="redactee_toolbar_label">'.lang('opt_buttons_current').'</li><li class="redactor_separator"></li>';
		foreach ($value as $button) {
			if ($button === '|') {
				echo '<li data-button="|"><a class="redactee_btn_separator" href="#"><hr></a></li>';
			} else {
				echo '<li data-button="'.$button.'"><a class="redactor_btn_'.$button.'" href="#">'.$button.'</a></li>';
			}
		}
		echo '</ul>';
		$params['hidden'] = 'hidden';
		$params['value'] = implode(" ", $value);
		echo form_input($params);
		
		//echo form_textarea($name, implode(" ", $value), $params);
		break;
	case 'bool':
		$params['checked'] = $value;
		$params['value'] = 'Y';
		echo form_checkbox($params);
		break;
	case 'select':
		echo form_dropdown($params['name'], $config['options'], $value, 'id="'.$params['id'].'"');
		break;
	case 'list':
		$params['value'] = implode("\n", $value);
		if (isset($config['rows'])) {
			$params['rows'] = $config['rows'];
		}
		echo form_textarea($params);
		break;
	default:
		$params['value'] = $value;
		echo form_input($params);
		break;
	endswitch; ?></td>
	</tr>
<?php endforeach; ?>
</table>
<?php
endforeach; ?>
