<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Redactee
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT
 * LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO
 * EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR
 * THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * 
 * Produced and sold in cooperation with Lobster War Machine (http://lobsterwarmachine.com/)
 * @author Jeremy Messenger (http://jlmessenger.com/)
 * @copyright 2012 Jeremy Messenger
 **/

require_once PATH_THIRD.'redactee/config.php';
require_once PATH_THIRD.'redactee/sql.redactee.php';

class Redactee_ft extends EE_Fieldtype {

	var $info = array(
		'name' => REDACTEE_NAME,
		'version' => REDACTEE_VER
	);

	function __construct() {
		parent::__construct();
		
		$this->sql = new Redactee_sql();
		$this->EE = &get_instance();
	}
	
	function install() {
		$default_toolbar_id = $this->sql->setup_db();
		return array_merge($this->sql->global_default_settings, array('toolbar_id' => $default_toolbar_id));
	}
	
	function update($ver) {
		return TRUE;
	}
	
	function display_global_settings() {
		$this->EE->lang->loadfile('redactee');
		$this->EE->load->helper('form');
		
		$this->EE->cp->add_to_head("<link rel=\"stylesheet\" type=\"text/css\" href=\"{$this->sql->theme_url}redactee.css\" />");
		$this->EE->cp->add_to_head("<link rel=\"stylesheet\" type=\"text/css\" href=\"{$this->sql->theme_url}redactor/redactor.css\" />");
		$this->EE->cp->add_to_head("<script type=\"text/javascript\" src=\"{$this->sql->theme_url}jquery-ui-sortable.min.js\"></script>");
		
		$vars = array(
			'global_settings' => $this->settings,
			'global' => $this->sql->get_global(),
			'toolbar_titles' => $this->sql->toolbar_titles(),
			'toolbar_settings' => array(),
			'option_configs' => $this->sql->redactor_option_configs()
		);
		
		foreach ($vars['toolbar_titles'] as $toolbar_id => $title) {
			$vars['toolbar_settings'][$toolbar_id] = $this->sql->_load_toolbar($toolbar_id);
			$this->sql->opts_json_to_php($vars['toolbar_settings'][$toolbar_id]['options']);
		}
		$vars['toolbar_settings']['new'] = array(
			'title' => 'New Toolbar',
			'options' => $this->sql->redactor_defaults_php()
		);
		$vars['all_buttons'] = array_values(array_filter($vars['toolbar_settings']['new']['options']['buttons'], array($this, '_remove_separators')));
		$vars['all_buttons'][] = '|';
		
		$vars['option_configs']['lang']['type'] = 'select';
		$vars['option_configs']['lang']['options'] = $this->sql->get_lang_options();
		
		return $this->EE->load->view('global_settings', $vars, TRUE);
	}
	
	function _remove_separators($item) {
		return $item !== '|';
	}
	
	function _validate_post_to_php($values) {
		foreach ($this->sql->redactor_option_configs() as $option => $config) {
			$value = isset($values[$option]) ? $values[$option] : '';
			switch ($config['type']) {
				case 'bool':
					$value = ($value === 'Y');
					break;
				case 'select':
					if ($value === '' || ! isset($config['options'][$value])) {
						$value = $config['default'];
					}
					break;
				case 'list':
					if ($value) {
						$value = preg_split("/\s+/", $value);
					} else {
						$value = array();
					}
					break;
				case 'buttons':
					if ($value) {
						$value = preg_split("/\s+/", $value);
					} else {
						$value = array();
					}
					break;
				// 'text' - nothing to do really
			}
			$values[$option] = $value;
		}
		return $values;
	}
	
	function save_global_settings() {
		$this->EE->lang->loadfile('redactee');
		
		$toolbar_titles = $this->sql->toolbar_titles();
		
		$toolbars = $_POST['tb'];
		$global = $_POST['global'];
		
		$global['aws_enable'] = isset($global['aws_enable']) && $global['aws_enable'] === 'Y' ? 'Y' : 'N';
		$this->sql->save_global($global);
		
		$to_delete = array();
		foreach ($toolbar_titles as $toolbar_id => $t) {
			if (isset($toolbars[$toolbar_id]) && is_array($toolbars[$toolbar_id])) {
				if (isset($toolbars[$toolbar_id]['delete']) && $toolbars[$toolbar_id]['delete'] === 'Y') {
					$to_delete[] = $toolbar_id;
					continue;
				}
				if (isset($toolbars[$toolbar_id]['options']) && is_array($toolbars[$toolbar_id]['options'])) {
					// save toolbar
					$settings = array(
						'title' => isset($toolbars[$toolbar_id]['title']) && $toolbars[$toolbar_id]['title'] !== '' ? $toolbars[$toolbar_id]['title'] : $t,
						'options' => $this->_validate_post_to_php($toolbars[$toolbar_id]['options'])
					);
					$this->sql->save_toolbar($settings, $toolbar_id);
				}
			}
		}
		
		if (isset($toolbars['new']) && is_array($toolbars['new'])) {
			foreach ($toolbars['new'] as $settings) {
				// create toolbar
				$settings = $this->_validate_post_to_php($settings);
				if (isset($settings['title']) && $settings['title'] !== '' && isset($settings['options']) && is_array($settings['options'])) {
					// TODO XXX title required message
					$settings = array(
						'title' => $settings['title'],
						'options' => $this->_validate_post_to_php($settings['options'])
					);
					$toolbar_id = $this->sql->save_toolbar($settings);
					$toolbar_titles[$toolbar_id] = $settings['title'];
				} else {
					$this->EE->session->set_flashdata('message_failure', lang('global_config_cant_save'));
				}
			}
		}
		
		foreach ($to_delete as $toolbar_id) {
			if (count($toolbar_titles) <= 1) {
				$this->EE->session->set_flashdata('message_failure', lang('global_config_cant_delete'));
				break; // don't delete last one
			}
			$this->sql->delete_toolbar($toolbar_id);
			unset($toolbar_titles[$toolbar_id]);
		}
		
		$toolbar_ids = array_keys($toolbar_titles);
		if ( ! in_array($this->settings['toolbar_id'], $toolbar_ids)) {
			$this->settings['toolbar_id'] = $toolbar_ids[0];
		}
		
		$this->EE->session->set_flashdata('message_success', lang('global_config_saved'));
		
		return array_merge($this->sql->global_default_settings, array('toolbar_id' => $this->settings['toolbar_id']));
	}
	
	function _display_settings($settings) {
		$this->EE->lang->loadfile('redactee');
		$this->EE->load->helper('form');
		
		$settings = array_merge(
				$this->sql->global_default_settings,
				$settings
			);
		
		$global = $this->sql->get_global();
		
		$toolbar_options = $this->sql->toolbar_titles();
		$upload_options = $this->sql->upload_titles();
		
		$rows = array();
		$rows[] = array(
				lang('fieldtype_toolbar_config'),
				form_dropdown('redactee[toolbar_id]', $toolbar_options, $settings['toolbar_id'])
			);
		$rows[] = array(
				lang('fieldtype_min_height'),
				form_input('redactee[minHeight]', $settings['minHeight'], 'placeholder="blank for auto"')
			);
		$rows[] = array(
				lang('fieldtype_autoresize'),
				form_checkbox('redactee[autoresize]', 'Y', $settings['autoresize'] === 'Y')
			);
		
		$has_assets = $this->sql->use_assets();
		if ($has_assets) {
			foreach ($upload_options as $loc_id => $title) {
				$upload_options += $this->sql->subdir_titles($loc_id);
			}
			asort($upload_options);
		}
		
		$browse_options = $upload_options;
		$browse_options = array('none' => lang('fieldtype_disable_upload_option'), 'all'=>lang('fieldtype_image_browse_opt_all')) + $browse_options;
		
		if (isset($global['aws_enable']) && $global['aws_enable'] === 'Y') {
			$upload_options = array('aws' => lang('fieldtype_aws_upload_option')) + $upload_options;
		}
		$upload_options = array('none' => lang('fieldtype_disable_upload_option')) + $upload_options;
		
		$rows[] = array(
				lang('fieldtype_image_upload_dir'),
				form_dropdown('redactee[image_upload_id]', $upload_options, $settings['image_upload_id'])
			);
		$rows[] = array(
				lang('fieldtype_image_browse'),
				form_dropdown('redactee[image_browse]', $browse_options, $settings['image_browse'])
			);
		if ($has_assets) {
			$rows[] = array(
				lang('fieldtype_image_browse_subdirs'),
				form_checkbox('redactee[image_browse_subdirs]', 'Y', $settings['image_browse_subdirs'] === 'Y')
			);
		}
		$rows[] = array(
				lang('fieldtype_file_upload_dir'),
				form_dropdown('redactee[file_upload_id]', $upload_options, $settings['file_upload_id'])
			);
		
		$has_structure = $this->sql->use_structure();
		$has_pages = $this->sql->use_pages();
		
		$site_page_options = array();
		
		if ($has_structure || $has_pages) {
			$site_page_options = array(
				'none' => lang('fieldtype_site_pages_option'),
				'all'  => lang('fieldtype_site_pages_option_all')
			);
			if ($has_structure) {
				$pages = $this->sql->_structure_pages();
				$page_options = array();
				foreach ($pages as $entry_id => $data) {
					if ($data['depth'] > 1) {
						$page_options[$entry_id] = str_repeat('&nbsp;', ($data['depth']-1)*2).'-'.$data['title'];
					} else {
						$page_options[$entry_id] = $data['title'];
					}
				}
				if (count($page_options)) {
					$site_page_options[lang('fieldtype_site_pages_limit')] = $page_options;
				}
			}
		}
		
		if (count($site_page_options)) {
			$rows[] = array(
				lang('fieldtype_site_pages'),
				form_dropdown('redactee[site_pages]', $site_page_options, $settings['site_pages'])
			);
		}
		if ($has_structure) {
			$rows[] = array(
				lang('fieldtype_site_listings'),
				form_checkbox('redactee[site_listings]', 'Y', $settings['site_listings'] === 'Y')
			);
		}
		
		return $rows;
	}
	
	function display_settings($settings) {
		$rows = $this->_display_settings($settings);
		foreach ($rows as $r) {
			$this->EE->table->add_row($r[0], $r[1]);
		}
	}
	
	function display_cell_settings($settings) {
		return $this->_display_settings($settings);
	}
	
	function display_var_settings($settings) {
		return $this->_display_settings($settings);
	}
	
	function save_settings($settings) {
		$settings = array_merge(
			$this->sql->global_default_settings,
			array_fill_keys(array('image_browse_subdirs', 'autoresize', 'site_listings'), ''), // force checkboxes default to blank
			$this->EE->input->post('redactee'),
			array(
				'field_fmt' => 'none',
				'field_show_fmt' => 'n',
				'field_type' => 'redactee'
			)
		);
		return $settings;
	}
	
	function save_cell_settings($settings) {
		$settings = array_merge(
			$this->sql->global_default_settings,
			$settings['redactee']
		);
		return $settings;
	}
	
	function save_var_settings() {
		$settings = array_merge(
			$this->sql->global_default_settings,
			$this->EE->input->post('redactee')
		);
		return $settings;
	}
	
	function _display_field($data, $field_name, $field_ref) {
		$toolbar_id = $this->settings['toolbar_id'];
		
		$this->sql->_add_redactor_head($toolbar_id, $field_ref, $this->settings);
		
		$this->sql->get_assets($data);
		$this->sql->tag_to_url($data);
		return "<textarea class=\"redactee-editor\" data-config=\"c$toolbar_id\" name=\"$field_name\">$data</textarea>";
	}
	
	function display_field($data) {
		if (REQ == 'PAGE') {
			$this->settings = array_merge($this->settings, $this->sql->get_field_settings($this->field_id));
		}
		return $this->_display_field($data, $this->field_name, $this->field_id);
	}
	
	function display_cell($data) {
		$field_key = $this->field_id.':'.$this->col_id;
		if (REQ == 'PAGE') {
			$this->settings = array_merge($this->settings, $this->sql->get_field_settings($field_key));
		}
		return $this->_display_field($data, $this->cell_name, $field_key);
	}
	
	function display_var_field($data) {
		return $this->_display_field($data, $this->field_name, 'low:'.$this->var_id);
	}
	
	function validate($data) {
		if ( ! $data && $this->settings['field_required'] === 'y') {
			return lang('required');
		}
		
		return TRUE;
	}
	
	function validate_cell($data) {
		if ( ! $data && $this->settings['col_required'] === 'y') {
			return lang('col_required');
		}
		
		return TRUE;
	}
	
	function save($data) {
		$toolbar = $this->sql->get_toolbar($this->settings['toolbar_id']);
		$allowed_tags = FALSE;
		if (isset($toolbar['options']) && isset($toolbar['options']['allowedTags'])) {
			$tags = json_decode($toolbar['options']['allowedTags']);
			if (is_array($tags) && count($tags)) {
				$allowed_tags = $tags;
			}
		}
		if ($allowed_tags === FALSE) {
			// use default allowed tags
			$options = $this->sql->redactor_option_configs();
			$allowed_tags = $options['allowedTags']['defaults'];
		}
		$data = strip_tags($data, '<'.implode('><', $allowed_tags).'>');
		
		$this->sql->url_to_tag($data);
		$this->sql->save_assets($data);
		return $data;
	}
	
	function save_cell($data) {
		return $this->save($data);
	}
	
	function save_var_field($data) {
		return $this->save($data);
	}
	
	function pre_process($data) {
		$this->sql->get_assets($data);
		$this->sql->tag_to_url($data);
		return $data;
	}
	
	function replace_tag($data, $params = array(), $tagdata = FALSE) {
		return $data;
	}
	
	function display_var_tag($data) {
		$data = $this->pre_process($data);
		return $this->replace_tag($data);
	}
}
